#!/usr/bin/env python3
#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File mods2mdf.py
#
# Created on: May 13, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import sys
from astropy import log

"""
This scripts is used to create mask description file starting from mms MODS file
"""

#Checks inputs
if (3<= len(sys.argv) <= 4) is False:
  log.error(f"Usage {sys.argv[0]} mms_file mdf_file [--overwrite]")
  sys.exit()

#Reads input file
mms_name = sys.argv[1]

#Reads output file
mdf_name = sys.argv[2]

#Checks if overwrite argument is provided
if len(sys.argv) == 4:
  if sys.argv[4].lower() != '--overwrite':
    log.error(f"Invalid {sys.argv[4]} parameter")
    sys.exit()
  overwrite = True
else:
  overwrite = False

#Checks if file already exists
if overwrite is False and os.path.isfile(mdf_name):
  log.error(f"File {mdf_name} already exists!")
  sys.exit()


#Reads mms file content
with open(mms_name, 'r') as mms_file:
  mms_lines = mms_file.readlines()

#Parses mms file
skip = True
slits = {}

for line in mms_lines:
  line = line.strip()
  #Skips comment
  if line.startswith('#'):
    continue

  #No slit line
  try:
    key, value = line.split()
  except:
    continue

  group, obj, prop = key.split('.')
  #No slit line
  if group != 'INS':
    continue

  #New slit
  if prop == 'NAME':
    #Defines new slit
    target = obj
    skip = False
    slits[obj]={}

  if skip is True:
    continue

  #No slit line
  if obj != target:
    continue

  #Skips reference slit
  if value == 'refslit':
    skip = True
    continue

  slits[obj][prop]=value


#Converts into mdf files

#Opens file and defines header
mdf_file = open(mdf_name, 'w')
mdf_file.write("#ID\tDIMX\tDIMY\tX\tY\tROT\tWID\tLEN\tREF\n")
ref_num = 0
for obj in slits:
  slit = slits[obj]
  if len(slit) == 0:
    continue

  _id = slit["NAME"]
  widmm = slit["WIDMM"]
  lenmm = slit["LENMM"]
  xmm = slit["XMM"]
  ymm = slit["YMM"]
  rot = slit["ROT"]
  ref=0
  wid = slit["WID"]
  _len = slit["LEN"]

  #Marks square slits as reference slits
  if wid == _len:
    ref=1
    ref_num+=1
    _id = f"Ref_{ref_num}"

  #Dumps slit lines
  mdf_file.write(f"{_id}\t{widmm}\t{lenmm}\t{xmm}\t{ymm}\t{rot}\t{wid}\t{_len}\t{ref}\n")

mdf_file.close()

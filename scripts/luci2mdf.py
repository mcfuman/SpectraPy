#!/usr/bin/env python3
#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File luci2mdf.py
#
# Created on: May 13, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import sys
from astropy import log
from astropy.io import fits

"""
This scripts is used to create mask description file
starting from luci file which contains mask description
"""

if len(set(['-h', '--help']).intersection(sys.argv[1:])) > 0:
  print(f"Usage {sys.argv[0]} fits_file mdf_file [--overwrite]")
  sys.exit(0)


# Checks inputs
if (3 <= len(sys.argv) <= 4) is False:
  log.error(f"Usage {sys.argv[0]} fits_file mdf_file [--overwrite]")
  sys.exit()

# Reads input file
luci_file = sys.argv[1]

# Reads output file
mdf_name = sys.argv[2]

# Checks if overwrite argument is provided
if len(sys.argv) == 4:
  if sys.argv[3].lower() != '--overwrite':
    log.error(f"Invalid {sys.argv[3]} parameter")
    sys.exit()
  overwrite = True
else:
  overwrite = False

# Checks if file already exists
if overwrite is False and os.path.isfile(mdf_name):
  log.error(f"File {mdf_name} already exists!")
  sys.exit()


# Reads mms file content
header = fits.open(luci_file, mode='readonly')[0].header

# Parses luci file
skip = True
slits = {}

nslit = 0
for key in header:
  if key.startswith('TGT') and key.endswith('NAM'):
    nslit = max(nslit, int(key[3:5]))

for k in range(1, nslit + 1):
  slit = "%02d" % k
  obj = header[f"TGT{slit}NAM"]
  if obj == "refslit":
    continue

  if obj in slits:
    log.warning(f"Slit {obj} already exists! Rename it")

    duplicated = 1
    new_obj = obj + "_%02d" % duplicated
    while new_obj in slits:
      duplicated += 1
      new_obj = obj + "_%02d" % duplicated
    obj = new_obj

  slits[obj] = {}
  slits[obj]["NAME"] = obj
  slits[obj]["WIDMM"] = header[f"MOS{slit}WMM"]
  slits[obj]["LENMM"] = header[f"MOS{slit}LMM"]
  slits[obj]["XMM"] = header[f"MOS{slit}XPO"]
  slits[obj]["YMM"] = header[f"MOS{slit}YPO"]
  slits[obj]["ROT"] = header[f"MOS{slit}PA"]
  slits[obj]["WID"] = header[f"MOS{slit}WAS"]
  slits[obj]["LEN"] = header[f"MOS{slit}LAS"]


# Converts into mdf files

# Opens file and defines header
mdf_file = open(mdf_name, 'w')
mdf_file.write("#ID\tDIMX\tDIMY\tX\tY\tROT\tWID\tLEN\tREF\n")
ref_num = 0

for obj in slits:
  slit = slits[obj]

  _id = slit["NAME"]
  widmm = slit["WIDMM"]
  lenmm = slit["LENMM"]
  xmm = slit["XMM"]
  ymm = slit["YMM"]
  rot = slit["ROT"]
  ref = 0
  wid = slit["WID"]
  _len = slit["LEN"]

  # Marks square slits as reference slits
  if wid == _len:
    ref = 1
    ref_num += 1
    _id = f"Ref_{ref_num}"

  # Dumps slit lines
  mdf_file.write(f"{_id}\t{widmm}\t{lenmm}\t{xmm}\t{ymm}\t{rot}\t{wid}\t{_len}\t{ref}\n")

mdf_file.close()

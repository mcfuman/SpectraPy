#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File model1diter_test.py
#
# Created on: Mar 19, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import pickle
import numpy as np
from astropy.table import Table

import pytest
from pytest_mock import mocker

from spectrapy.modelscalib.model1diter import Model1dIterator


class TestModel1dIterator(object):
    """Test for Model1dIterator class"""

    def test_init1(self):
        """
        """
        model_iter = Model1dIterator([11, 22, 33])

        assert len(model_iter) == 0
        assert model_iter._slit_ids == [11, 22, 33]
        assert model_iter._catalog is None
        assert model_iter._1d_models == {}
        assert model_iter._k == 0

    def test_load_models1(self, mocker):
        mocker.patch('os.path.isfile', return_value=False)
        model_iter = Model1dIterator([11, 22, 33])

        with pytest.raises(Exception) as no_file:
            model_iter.load_models('mock')

        assert no_file.type is IOError
        assert no_file.value.args[0] == "Invalid file mock!"

    def test_load_models2(self, mocker):
        mocker.patch('os.path.isfile', return_value=True)
        m = mocker.mock_open()
        mocker.patch('spectrapy.modelscalib.model1diter.open', m)
        mocker.patch('pickle.load', return_value={11: 'mock1', 22: 'mock2'})

        model_iter = Model1dIterator([11, 22, 33])

        model_iter.load_models('mock')

        assert model_iter.has_model(11) is True
        assert model_iter.has_model(22) is True
        assert model_iter.has_model(33) is False

    def test_load_models3(self, mocker):
        mocker.patch('os.path.isfile', return_value=True)
        mock_open = mocker.mock_open()
        mocker.patch('spectrapy.modelscalib.model1diter.open', mock_open)
        mocker.patch('pickle.load', return_value={11: 'mock1', 44: 'mock4'})

        model_iter = Model1dIterator([11, 22, 33])

        with pytest.raises(Exception) as invalid_model:
            model_iter.load_models('mock')

        assert invalid_model.type is IndexError
        assert invalid_model.value.args[0] == "Model ID 44 not in slit IDs!"

    def test_dump_models1(self, mocker):
        mocker.patch('os.path.isfile', return_value=True)

        model_iter = Model1dIterator([11, 22, 33])

        with pytest.raises(Exception) as already_exists:
            model_iter.dump_models('mock')

        assert already_exists.type is IOError
        assert already_exists.value.args[0] == "File mock already exists!"

    def test_dump_models2(self, mocker):
        mocker.patch('os.path.isfile', return_value=False)
        mock_open = mocker.mock_open()
        mocker.patch('spectrapy.modelscalib.model1diter.open', mock_open)
        mocker.patch('pickle.dump')

        model_iter = Model1dIterator([11, 22, 33])

        model_iter.dump_models('mock')

        args, kwargs = pickle.dump.call_args
        assert args[0] is model_iter._1d_models
        #assert args[1] is mock_open

    def test_current_list(self):
        model_iter = Model1dIterator([11, 22, 33])

        assert model_iter.current_slit == 11

        model_iter.next()
        assert model_iter.current_slit == 22

        model_iter.next()
        assert model_iter.current_slit == 33

    def test_len1(self):
        model_iter = Model1dIterator([11, 22, 33])

        assert len(model_iter) == 0

    def test_len2(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock')

        assert len(model_iter) == 1

    def test_len3(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock1')
        model_iter.next()
        model_iter.set_model('mock2')

        assert len(model_iter) == 2

    def test_len4(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock1')
        model_iter.next()
        model_iter.set_model('mock2')
        model_iter.next()
        model_iter.set_model('mock3')

        assert len(model_iter) == 3

    def test_len5(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock1')
        model_iter.next()
        model_iter.set_model('mock2')
        model_iter.next()
        model_iter.set_model('mock3')
        model_iter.prev()
        model_iter.set_model('mock4')

        assert len(model_iter) == 3

    def test_getitem1(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock1')
        assert model_iter.get_model() == 'mock1'

    def test_getitem2(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock1')
        model_iter.next()
        model_iter.set_model('mock2')

        assert model_iter.get_model() == 'mock2'
        assert model_iter.get_model(11) == 'mock1'

    def test_hasmodel1(self):
        model_iter = Model1dIterator([11, 22, 33])

        assert model_iter.has_model() is False
        assert model_iter.has_model(11) is False
        assert model_iter.has_model(22) is False
        assert model_iter.has_model(33) is False

    def test_hasmodel2(self):
        model_iter = Model1dIterator([11, 22, 33])
        model_iter.set_model('mock', slit_id=22)
        assert model_iter.has_model() is False
        assert model_iter.has_model(11) is False
        assert model_iter.has_model(22) is True
        assert model_iter.has_model(33) is False

    def test_hasmodel3(self):
        model_iter = Model1dIterator([11, 22, 33])

        with pytest.raises(Exception) as invalid_id:
            model_iter.has_model(44)

        assert invalid_id.type is IndexError
        assert invalid_id.value.args[0] == "Invalid slit ID 44!"

    def test_setmodel1(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock11')

        assert model_iter.has_model(11) is True
        assert model_iter.has_model(22) is False
        assert model_iter.has_model(33) is False

    def test_setmodel2(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock22', slit_id=22)

        assert model_iter.has_model(11) is False
        assert model_iter.has_model(22) is True
        assert model_iter.has_model(33) is False

    def test_setmodel3(self):
        model_iter = Model1dIterator([11, 22, 33])

        with pytest.raises(Exception) as invalid_id:
            model_iter.set_model('mock44', slit_id=44)

        assert invalid_id.type is IndexError
        assert invalid_id.value.args[0] == "Invalid slit ID 44!"

        assert model_iter.has_model(11) is False
        assert model_iter.has_model(22) is False
        assert model_iter.has_model(33) is False

    def test_catalog1(self):
        model_iter = Model1dIterator([11, 22, 33])

        with pytest.raises(Exception) as no_catalog:
            model_iter.catalog

        assert no_catalog.type is AttributeError
        assert no_catalog.value.args[0] == "No catalog!"

    def test_catalog2(self):
        catalog = Table(data=[[1, 2], ['Ar', 'Ne']], names=['pos', 'label'])
        model_iter = Model1dIterator([11, 22, 33], catalog=catalog)

        assert np.allclose(model_iter.catalog['pos'], [1, 2])
        assert np.alltrue(model_iter.catalog['label'] == ['Ar', 'Ne'])

    def test_has_next(self):
        model_iter = Model1dIterator([11, 22, 33])
        assert model_iter.has_next is True

        model_iter.next()
        assert model_iter.has_next is True

        model_iter.next()
        assert model_iter.has_next is False

        model_iter.prev()
        assert model_iter.has_next is True

    def test_has_prev(self):

        model_iter = Model1dIterator([11, 22, 33])
        assert model_iter.has_prev is False

        model_iter.next()
        assert model_iter.has_prev is True

        model_iter.next()
        assert model_iter.has_prev is True

        model_iter.next()
        assert model_iter.has_prev is True

        model_iter.prev()
        assert model_iter.has_prev is True

        model_iter.prev()
        assert model_iter.has_prev is False

    def test_getitem(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock11')

        assert model_iter[11] == 'mock11'

    def test_iter(self):
        model_iter = Model1dIterator([11, 22, 33])

        model_iter.set_model('mock11')
        model_iter.set_model('mock33', 33)

        for model in model_iter:
            assert model in (11, 33)

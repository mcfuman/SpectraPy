#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File idscalib_test.py
#
# Created on: Mar 10, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock
from astropy.table import Table

import spectrapy
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d
from spectrapy.models.crvmodel import CrvModel2d
from spectrapy.models.idsmodel import IdsModel2d, IdsModel1d

from spectrapy.models.model import Model2d

from spectrapy.modelscalib.optcalib import OptModelCalibration
from spectrapy.modelscalib.idscalib import IdsModelCalibration
from spectrapy.mask.mask import Mask


class TestIdsModelCalibration(object):
    """Test for the calibration of the Optical Model"""

    # @pytest.fixture(autouse=True)
    # def mock_ds9(self, mocker):
    #    mocker.patch('spectrapy.modelscalib.idscalib.IdsModelCalibration._ds9')
        # mocker.patch('spectrapy.modelscalib.idscalib.IdsModelCalibration._ds9.reset_background')

    def get_calib(self, mocker, instrument=None, mask=None, opt=None, crv=None, ids=None):
        if instrument is None:
            instrument = Instrument()
        if mask is None:
            mask = Mask()
        if opt is None:
            opt = OptModel2d(1, 1)
        if crv is None:
            crv = CrvModel2d(2, 1, 1)
        if ids is None:
            ids = IdsModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, mask=mask, opt=opt, crv=crv, ids=ids)
        calib._ds9 = MagicMock()
        return calib

    def test_init1(self, mocker):
        opt = OptModel2d(1, 1)
        crv = CrvModel2d(2, 1, 1)
        ids = IdsModel2d(2, 1, 1)
        mask = Mask()
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        mocker.patch('spectrapy.modelscalib.idscalib.CrvModelCalibration.__init__')
        calib = IdsModelCalibration(instrument, mask=mask, opt=opt, crv=crv, ids=ids)
        args, kwargs = spectrapy.modelscalib.idscalib.CrvModelCalibration.__init__.call_args

        assert args[0] is calib
        assert args[1] is instrument
        assert args[2] is mask
        assert args[3] is opt
        assert args[4] is crv

    def test_ids1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument)

        assert calib._ids is None
        with pytest.raises(Exception):
            calib.ids

    def test_ids2(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(2, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument)
        calib.ids = ids

        assert calib.ids is ids

    def test_ids3(sel, mocker):
        instrument = Instrument()
        ids = IdsModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument)

        mocker.patch('spectrapy.modelscalib.idscalib.IdsModel2d.load', return_value=ids)
        calib.ids = "mock"

        assert calib.ids is ids

    def test_ids4(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        with pytest.raises(Exception) as invalid_ids:
            calib = IdsModelCalibration(instrument, ids=1.2)

        assert invalid_ids.type is TypeError
        assert invalid_ids.value.args[0] == "Invalid input Ids Model"

    def test_new_ids_model(self, mocker):
        instrument = Instrument()
        instrument.linear_dispersion = 1.0
        ids = IdsModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument)
        calib.new_ids_model(1, 2, 4)

        assert calib.ids.deg == 1
        assert calib.ids.xdeg == 2
        assert calib.ids.ydeg == 4

    def test_refit_single_slit1(self, mocker):
        instrument = Instrument()
        instrument.linear_dispersion = 1.0

        ids = IdsModel2d(2, 1, 1)
        model = np.random.rand(3, 2, 2)
        ids.model = model

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)
        with pytest.raises(Exception) as no_regions:
            calib._refit_single_slit("mock", {})

        assert no_regions.type is ValueError
        assert no_regions.value.args[0] == "No regions!"

    def test_refit_single_slit2(self, mocker):
        instrument = Instrument()
        instrument.linear_dispersion = 1.0

        ids = IdsModel2d(2, 1, 1)
        model = np.random.rand(3, 2, 2)
        ids.model = model

        model_1d = IdsModel1d([1., 2., 3.], 1000.0)
        mocker.patch.object(ids, 'get_delta_dispersion')
        mocker.patch.object(ids, 'refit_slit', return_value=model_1d)

        mask = Mask()
        slit = MagicMock()
        mocker.patch.object(slit, 'get_pix_position', return_value=(None, None))

        regions = {0: np.random.rand(2, 2)}
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)
        calib._refit_single_slit(slit, regions)

        assert np.allclose(calib.ids.model[0], [[1, 0], [0, 0]])
        assert np.allclose(calib.ids.model[1], [[2, 0], [0, 0]])
        assert np.allclose(calib.ids.model[2], [[3, 0], [0, 0]])

    def test_set_line_regions1(self, mocker):
        instrument = Instrument()
        instrument.linear_dispersion = 1.0

        ids = IdsModel2d(2, 1, 1)
        mock_ds9 = MagicMock()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)
        calib._ds9 = mock_ds9

        pieces = {0.2: None}

        mocker.patch.object(OptModelCalibration, 'data_to_region',
                            return_value=[1.2, 2.3, 3.4, 4.5])
        mocker.patch.object(IdsModelCalibration, 'IDS_REGION_TYPE', new="M")

        mocker.patch.object(mock_ds9, 'set')
        calib._set_line_regions(pieces, {'pos': 3.14, 'flag': 1}, "ABC")

        args, kwargs = mock_ds9.set.call_args

        assert args[0] == "regions"
        assert args[1] == "image; M 1.2 2.3 3.4 4.5 # line=1 0 tag={3.14} tag={ABC:0.2} text={3.14} color=green"

    def test_set_line_regions2(self, mocker):
        instrument = Instrument()
        instrument.linear_dispersion = 1.0

        ids = IdsModel2d(2, 1, 1)
        mock_ds9 = MagicMock()

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)
        calib._ds9 = mock_ds9

        pieces = {0.2: None}

        mocker.patch.object(OptModelCalibration, 'data_to_region',
                            return_value=[1.2, 2.3, 3.4, 4.5])
        mocker.patch.object(IdsModelCalibration, 'IDS_REGION_TYPE', new="M")

        mocker.patch.object(mock_ds9, 'set')
        calib._set_line_regions(pieces, {'pos': 3.14, 'flag': 0}, "ABC")

        args, kwargs = mock_ds9.set.call_args

        assert args[0] == "regions"
        assert args[1] == "image; M 1.2 2.3 3.4 4.5 # line=1 0 tag={3.14} tag={ABC:0.2} text={3.14} color=pink"

    def test_set_line_regions3(self, mocker):
        instrument = Instrument()
        instrument.linear_dispersion = 1.0

        ids = IdsModel2d(2, 1, 1)
        mock_ds9 = MagicMock()

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)
        calib._ds9 = mock_ds9

        pieces = {0.2: None}

        mocker.patch.object(OptModelCalibration, 'data_to_region',
                            return_value=[1.2, 2.3, 3.4, 4.5])
        mocker.patch.object(IdsModelCalibration, 'IDS_REGION_TYPE', new="M")

        mocker.patch.object(mock_ds9, 'set')
        calib._set_line_regions(pieces, {'pos': 3.14, 'flag': 2}, "ABC")

        args, kwargs = mock_ds9.set.call_args

        assert args[0] == "regions"
        assert args[1] == "image; M 1.2 2.3 3.4 4.5 # line=1 0 tag={3.14} tag={ABC:0.2} text={3.14} color=red"

    def test__load_catalog1(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(3, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)

        catalog = Table(data=([1000., 2000., 3000.], ['Ar', 'Kr', 'Ne']))

        mocker.patch('spectrapy.modelscalib.idscalib.Catalog.load', return_value=catalog)

        with pytest.raises(Exception) as invalid_catalog:
            calib._load_catalog("mock")

        assert invalid_catalog.type is ArithmeticError
        assert invalid_catalog.value.args[0] == "Number of lines must be > deg of IdsModel"

    def test__load_catalog2(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, ids=ids)

        catalog = Table(data=([1000., 2000., 3000.], ['Ar', 'Kr', 'Ne'], [1, 0, 1]),
                        names=['pos', 'label', 'flag'])

        mocker.patch('spectrapy.modelscalib.idscalib.Catalog.load', return_value=catalog)

        lines = calib._load_catalog("mock")

        assert np.allclose(lines['pos'], catalog['pos'])
        assert np.alltrue(lines['label'] == catalog['label'])
        assert np.alltrue(lines['flag'] == catalog['flag'])

    def test_plot_ids_model1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument)

        mocker.patch('spectrapy.modelscalib.idscalib.Catalog.load')
        mocker.patch.object(calib._ds9, 'reset_background')
        calib._ds9._image = True

        with pytest.raises(Exception) as no_ids:
            calib.plot_ids_model("mock")

        assert no_ids.type is AttributeError
        assert no_ids.value.args[0] == "IdsModel not set yet!"

    def test_plot_ids_model3(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()
        slit = MagicMock()
        mocker.patch.object(mask, '_slits', new={"A": slit})
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, mask=mask, ids=ids)

        catalog = Table(data=([1000., 2000., 3000.], ['Ar', 'Kr', 'Ne']))
        mocker.patch.object(calib, '_load_catalog', return_value=catalog)
        mocker.patch.object(calib._ds9, 'reset_background')
        calib._ds9._image = True

        with pytest.raises(Exception) as invalid_id:
            calib.plot_ids_model("mock", slit_id="B")

        assert invalid_id.type is IndexError
        assert invalid_id.value.args[0] == "Invalid slit B"

    def test_plot_ids_model4(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()
        slit = MagicMock()
        mocker.patch.object(slit, 'get_pix_vertices')
        mocker.patch.object(mask, '_slits', new={"A": slit})
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        #mocker.patch('spectrapy.dataio.display.DS9.__init__', new=MagicMock())
        # mocker.patch('spectrapy.dataio.display.Display.clear')
        calib = IdsModelCalibration(instrument, mask=mask, ids=ids)
        calib._ds9 = MagicMock()

        catalog = Table(data=([1000., 2000., 3000.], ['Ar', 'Kr', 'Ne']))

        pixels = [{"0.00": np.random.rand(4)}, {"0.00": np.random.rand(4)},
                  {"0.00": np.random.rand(4)}]
        mocker.patch.object(calib, '_get_line_pixels', side_effect=pixels)
        mocker.patch.object(calib._ds9, 'reset_background')
        mocker.patch.object(calib, '_set_slit_regions')
        mocker.patch.object(calib, 'display_single_slit')
        mocker.patch.object(calib, '_load_catalog', return_value=catalog)
        calib._ds9._image = True
        calib._ds9.xstart = 0
        calib._ds9.ystart = 0

        mocker.patch.object(calib, '_set_line_regions')
        calib.plot_ids_model("mock", slit_id="A")

        assert calib._set_line_regions.call_count == 3
        expected = [mocker.call(pixels[0], catalog[0], "A"), mocker.call(pixels[1], catalog[1], "A"),
                    mocker.call(pixels[2], catalog[2], "A")]
        assert calib._set_line_regions.call_args_list == expected

    def test_plot_ids_model5(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()
        slitA = MagicMock()
        slitB = MagicMock()
        mocker.patch.object(slitA, 'get_pix_vertices')
        mocker.patch.object(slitB, 'get_pix_vertices')

        mocker.patch.object(mask, '_slits', new={"A": slitA, "B": slitB})
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, mask=mask, ids=ids)

        mocker.patch.object(calib._ds9, 'reset_background')
        mocker.patch.object(calib, '_set_slit_regions')

        calib._ds9._image = True

        catalog = Table(data=([1000., 2000., 3000.], ['Ar', 'Kr', 'Ne']))
        mocker.patch.object(calib, '_load_catalog', return_value=catalog)
        mocker.patch.object(calib, '_plot_catalog_regions')

        calib.plot_ids_model(catalog, nsplit=3)

        assert calib._plot_catalog_regions.call_count == 2
        expected = [mocker.call(catalog, "A", 3), mocker.call(catalog, "B", 3)]

        assert calib._plot_catalog_regions.call_args_list == expected

    def test_plot_ids_model6(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()
        slitA = MagicMock()
        slitB = MagicMock()
        mocker.patch.object(slitA, 'reference', new=False)
        mocker.patch.object(slitB, 'reference', new=True)
        mocker.patch.object(slitA, 'get_pix_vertices')
        mocker.patch.object(slitB, 'get_pix_vertices')

        mocker.patch.object(mask, '_slits', new={"A": slitA, "B": slitB})
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, mask=mask, ids=ids)

        mocker.patch.object(calib._ds9, 'reset_background')
        mocker.patch.object(calib, '_set_slit_regions')
        calib._ds9._image = True

        catalog = Table(data=([1000., 2000., 3000.], ['Ar', 'Kr', 'Ne']))
        mocker.patch.object(calib, '_load_catalog', return_value=catalog)
        mocker.patch.object(calib, '_plot_catalog_regions')

        calib.plot_ids_model(catalog, nsplit=3)

        assert calib._plot_catalog_regions.call_count == 1
        expected = [mocker.call(catalog, "A", 3), ]

        assert calib._plot_catalog_regions.call_args_list == expected

    def test_plot_ids_model7(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()
        slitA = MagicMock()
        slitB = MagicMock()

        mocker.patch.object(mask, '_slits', new={"A": slitA, "B": slitB})

        mocker.patch('spectrapy.dataio.display.DS9.__init__', new=MagicMock())
        mocker.patch('spectrapy.dataio.display.Display.clear')
        calib = IdsModelCalibration(instrument, mask=mask, ids=ids)

        with pytest.raises(Exception) as no_image:
            calib.plot_ids_model("mock")

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_fit_ids_model1(self, mocker):

        calib = self.get_calib(mocker)
        mocker.patch.object(calib, '_parse_ids_regions', return_value={})

        with pytest.raises(Exception) as no_regions:
            calib.fit_ids_model()

        assert no_regions.type is IOError
        assert no_regions.value.args[0] == "No regions"

    def test_fit_ids_model2(self, mocker):

        calib = self.get_calib(mocker)
        slitA = MagicMock()
        calib._mask._slits = {'A': slitA}
        mocker.patch.object(calib, '_parse_ids_regions', return_value={'A': {'0.00': []}})

        mocker.patch.object(calib, '_refit_single_slit')

        calib.fit_ids_model(single_slit=True)
        args, kwargs = calib._refit_single_slit.call_args

        assert args[0] is slitA
        assert args[1] == {'0.00': []}
        assert len(args) == 2

    def test_fit_ids_model3(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()
        slitA = MagicMock()
        slitB = MagicMock()
        # calib = self.get_calib()
        slitA = MagicMock()
        slitB = MagicMock()
        slitC = MagicMock()
        mocker.patch.object(mask, '_slits', new={"A": slitA, "B": slitB, "C": slitC})
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument, mask=mask, ids=ids)

        poss = np.random.rand(6, 2)
        mocker.patch.object(slitA, 'get_pix_position', side_effect=[(poss[0], None), (poss[1], None)])
        mocker.patch.object(slitB, 'get_pix_position', return_value=(poss[2], None))
        mocker.patch.object(slitC, 'get_pix_position', side_effect=[(poss[3], None), (poss[4], None), (poss[5], None)])

        calib._mask._slits = {'A': slitA, 'B': slitB, 'C': slitC}

        mocker.patch.object(calib.ids, 'get_delta_dispersion', return_value=0.0)

        dispersions = {'A': {'0.00': np.zeros((2, 2)), '0.50': np.zeros((2, 2))},
                       'B': {'0.00': np.zeros((2, 2))},
                       'C': {'0.00': np.zeros((2, 2)), '0.50': np.zeros((2, 2)), '1.0': np.zeros((2, 2))}}
        mocker.patch.object(calib, '_parse_ids_regions', return_value=dispersions)

        coeffs = 10 * np.random.rand(6, 4)
        variances = np.random.rand(6, 4)
        models = []
        for i in range(6):
            model = MagicMock()
            model.coeffs = coeffs[i]
            model.variance = variances[i]
            mocker.patch.object(model, 'log_coeffs')
            models.append(model)

        mocker.patch.object(calib.ids, 'refit_slit', side_effect=models)
        mocker.patch.object(calib.ids, 'refit')

        calib.fit_ids_model()

        args, kwargs = calib.ids.refit.call_args

        assert np.allclose(args[0], poss[:, 0])
        assert np.allclose(args[1], poss[:, 1])
        assert np.allclose(args[2], coeffs)
        assert np.allclose(kwargs['var'], variances)

    def test_fit_ids_model4(self, mocker):
        instrument = Instrument()
        ids = IdsModel2d(1, 1, 1)
        mask = Mask()

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = IdsModelCalibration(instrument)
        mocker.patch.object(calib._ds9, 'replace_regions')
        mocker.patch.object(calib, '_parse_ids_regions', return_value=[])

        with pytest.raises(Exception):
            calib.fit_ids_model(filename="mockfile")

        args, kwargs = calib._ds9.replace_regions.call_args

        assert args[0] == "mockfile"

    def test_parse_ids_regions1(self, mocker):
        calib = self.get_calib(mocker)

        xstart = 10 * np.random.rand()
        ystart = 10 * np.random.rand()

        calib._ds9.xstart = xstart
        calib._ds9.ystart = ystart

        region = MagicMock()
        region.name = 'marco'
        regions = [region, ]

        pixels = 100 * np.random.rand(3, 2)
        region = MagicMock()
        region.name = IdsModelCalibration.IDS_REGION_TYPE
        attr = {1: {'tag': ['A:0.00', 3.14]}}
        region.attr = attr
        region.coord_list = pixels[0]
        regions.append(region)

        region = MagicMock()
        region.name = IdsModelCalibration.IDS_REGION_TYPE
        attr = {1: {'tag': ['A:0.50', 3.14]}}
        region.attr = attr
        region.coord_list = pixels[1]
        regions.append(region)

        region = MagicMock()
        region.name = IdsModelCalibration.IDS_REGION_TYPE
        attr = {1: {'tag': ['A:0.50', 7.14]}}
        region.attr = attr
        region.coord_list = pixels[2]
        regions.append(region)

        mocker.patch.object(calib._ds9, 'regions', new=regions)

        line_pixs = calib._parse_ids_regions()

        assert len(line_pixs) == 1
        assert 'A' in line_pixs
        assert len(line_pixs['A']) == 2
        assert '0.00' in line_pixs['A']
        assert '0.50' in line_pixs['A']

        assert np.allclose(line_pixs['A']['0.00'][0][0], 3.14)
        assert np.allclose(line_pixs['A']['0.00'][0][1], pixels[0][0] + xstart - 1)
        assert np.allclose(line_pixs['A']['0.00'][0][2], pixels[0][1] + ystart - 1)

        assert np.allclose(line_pixs['A']['0.50'][0][0], 3.14)
        assert np.allclose(line_pixs['A']['0.50'][0][1], pixels[1][0] + xstart - 1)
        assert np.allclose(line_pixs['A']['0.50'][0][2], pixels[1][1] + ystart - 1)

        assert np.allclose(line_pixs['A']['0.50'][1][0], 7.14)
        assert np.allclose(line_pixs['A']['0.50'][1][1], pixels[2][0] + xstart - 1)
        assert np.allclose(line_pixs['A']['0.50'][1][2], pixels[2][1] + ystart - 1)

    def test_parse_ids_regions2(self, mocker):
        calib = self.get_calib(mocker)

        xstart = 10 * np.random.rand()
        ystart = 10 * np.random.rand()

        calib._ds9.xstart = xstart
        calib._ds9.ystart = ystart

        regions = []

        pixels = 100 * np.random.rand(3, 2)
        region = MagicMock()
        region.name = IdsModelCalibration.IDS_REGION_TYPE
        attr = {1: {'tag': ['3.14', 'A:0.00']}}
        region.attr = attr
        region.coord_list = pixels[0]
        regions.append(region)

        region = MagicMock()
        region.name = IdsModelCalibration.IDS_REGION_TYPE
        attr = {1: {'tag': ['3.14', 'A:0.50']}}
        region.attr = attr
        region.coord_list = pixels[1]
        regions.append(region)

        region = MagicMock()
        region.name = IdsModelCalibration.IDS_REGION_TYPE
        attr = {1: {'tag': ['7.14', 'A:0.50']}}
        region.attr = attr
        region.coord_list = pixels[2]
        regions.append(region)

        mocker.patch.object(calib._ds9, 'regions', new=regions)

        line_pixs = calib._parse_ids_regions()

        assert len(line_pixs) == 1
        assert 'A' in line_pixs
        assert len(line_pixs['A']) == 2
        assert '0.00' in line_pixs['A']
        assert '0.50' in line_pixs['A']

        assert np.allclose(line_pixs['A']['0.00'][0][0], 3.14)
        assert np.allclose(line_pixs['A']['0.00'][0][1], pixels[0][0] + xstart - 1)
        assert np.allclose(line_pixs['A']['0.00'][0][2], pixels[0][1] + ystart - 1)

        assert np.allclose(line_pixs['A']['0.50'][0][0], 3.14)
        assert np.allclose(line_pixs['A']['0.50'][0][1], pixels[1][0] + xstart - 1)
        assert np.allclose(line_pixs['A']['0.50'][0][2], pixels[1][1] + ystart - 1)

        assert np.allclose(line_pixs['A']['0.50'][1][0], 7.14)
        assert np.allclose(line_pixs['A']['0.50'][1][1], pixels[2][0] + xstart - 1)
        assert np.allclose(line_pixs['A']['0.50'][1][2], pixels[2][1] + ystart - 1)

    def test_get_line_pixels(self, mocker):
        calib = self.get_calib(mocker)

        slit = MagicMock()
        poss = np.random.rand(2, 2)
        mocker.patch.object(slit, 'get_pix_position', side_effect=[(poss[0], None), (poss[1], None)])
        mocker.patch.object(calib.ids, 'compute', side_effect=[(None, None), (None, None)])
        delta_curves = np.random.rand(2, 2)
        mocker.patch.object(calib.crv, 'compute', side_effect=[(delta_curves[0], None), (delta_curves[1], None)])

        line_positions = calib._get_line_pixels(slit, {'pos': 3.14}, nsplit=2)

        assert len(line_positions) == 2
        assert '0.00' in line_positions
        assert '1.00' in line_positions

        expected = [poss[0][0] + delta_curves[0][0], poss[0][1] + delta_curves[0][1],
                    poss[0][0] + delta_curves[0][0], poss[0][1] + 10 + delta_curves[0][1]],
        assert np.allclose(line_positions['0.00'], expected)

        expected = [poss[1][0] + delta_curves[1][0], poss[1][1] + delta_curves[1][1],
                    poss[1][0] + delta_curves[1][0], poss[1][1] + 10 + delta_curves[1][1]],
        assert np.allclose(line_positions['1.00'], expected)

    def test_auto_set_trace_limits1(self, mocker):
        calib = self.get_calib(mocker)
        pixs = (1000 * np.random.rand(2)).astype(int)

        crv = MagicMock()
        mocker.patch.object(crv, 'dispersion_direction', new='LR')
        calib._crv = crv

        ds9 = MagicMock()
        mocker.patch.object(ds9, 'xlen', new=pixs[0])
        mocker.patch.object(ds9, 'ylen', new=pixs[1])
        calib._ds9 = ds9

        mocker.patch.object(calib, 'set_trace_limits')
        calib._auto_set_trace_limits()

        assert calib.set_trace_limits.call_count == 1
        expected = [mocker.call(pixs[0] // 2, pixs[0] // 2), ]

        assert calib.set_trace_limits.call_args_list == expected

    def test_auto_set_trace_limits2(self, mocker):
        calib = self.get_calib(mocker)
        pixs = (1000 * np.random.rand(2)).astype(int)

        crv = MagicMock()
        mocker.patch.object(crv, 'dispersion_direction', new='UB')
        calib._crv = crv

        ds9 = MagicMock()
        mocker.patch.object(ds9, 'xlen', new=pixs[0])
        mocker.patch.object(ds9, 'ylen', new=pixs[1])
        calib._ds9 = ds9

        mocker.patch.object(calib, 'set_trace_limits')
        calib._auto_set_trace_limits()

        assert calib.set_trace_limits.call_count == 1
        expected = [mocker.call(pixs[1] // 2, pixs[1] // 2), ]

        assert calib.set_trace_limits.call_args_list == expected

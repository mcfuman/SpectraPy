#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File optcalib_test.py
#
# Created on: Feb 28, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock
from unittest.mock import patch

import spectrapy
from spectrapy.dataio.image import Image
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d

from spectrapy.modelscalib.optcalib import OptModelCalibration
from spectrapy.mask.mask import Mask


class TestOptModelCalibration(object):
    """Test for the calibration of the Optical Model"""

    def get_calib(self, instrument, mocker, mask=None, opt=None):
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = OptModelCalibration(instrument, mask=mask, opt=opt)
        calib._ds9 = MagicMock()
        return calib

    def test_init1(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        mocker.patch('spectrapy.modelscalib.optcalib.Instrument.load', return_value=instrument)
        calib = self.get_calib('mock', mocker)

        assert isinstance(calib.instrument, Instrument)
        assert calib.instrument.dispersion_direction == 'LR'

    def test_init2(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker)

        assert isinstance(calib.instrument, Instrument)
        assert calib.instrument.dispersion_direction == 'RL'

    def test_init3(self, mocker):
        with pytest.raises(Exception) as invalid_instrument:
            calib = self.get_calib(1.2, mocker)

        assert invalid_instrument.type is TypeError
        assert invalid_instrument.value.args[0] == "Invalid input Instrument type"

    def test_init4(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        mask = Mask()

        mocker.patch('spectrapy.modelscalib.optcalib.Mask.load', return_value=mask)
        calib = self.get_calib(instrument, mocker, mask='mock')

        assert isinstance(calib.mask, Mask)

    def test_init5(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        mask = Mask()

        calib = self.get_calib(instrument, mocker, mask=mask)

        assert isinstance(calib.mask, Mask)

    def test_init6(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        with pytest.raises(Exception) as invalid_mask:
            calib = self.get_calib(instrument, mocker, mask=1.2)

        assert invalid_mask.type is TypeError
        assert invalid_mask.value.args[0] == "Invalid input Mask type"

    def test_init7(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        mask = Mask()

        calib = self.get_calib(instrument, mocker, mask=mask)

        assert calib._mask.opt is None

    def test_init8(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        mask = Mask()
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        assert isinstance(calib.opt, OptModel2d)
        assert calib._mask is None

    def test_init9(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        mask = Mask()
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        mocker.patch('spectrapy.modelscalib.optcalib.OptModel2d.load', return_value=opt)

        calib = self.get_calib(instrument, mocker, mask=mask, opt='mock')

        assert isinstance(calib.opt, OptModel2d)
        assert calib._mask.opt is not None

    def test_init10(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        mask = Mask()
        opt = OptModel2d(1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.OptModel2d.load', return_value=opt)

        with pytest.raises(Exception) as invalid_opt:
            calib = self.get_calib(instrument, mocker, mask=mask, opt=2.3)

        assert invalid_opt.type is TypeError
        assert invalid_opt.value.args[0] == "Invalid input OptModel type"

    def test_init11(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)

        with pytest.raises(Exception) as match_error:
            calib = self.get_calib(instrument, mocker, opt=opt)

        assert match_error.type is AttributeError
        assert match_error.value.args[0] == "OptModel and Instrument dispersion directions don't match"

    def test_optmodel(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        calib = self.get_calib(instrument, mocker)

        with pytest.raises(Exception) as no_opt:
            calib.opt

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "OptModel not set yet!"

    def test_mask(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        calib = self.get_calib(instrument, mocker)

        with pytest.raises(Exception) as no_mask:
            calib.mask

        assert no_mask.type is AttributeError
        assert no_mask.value.args[0] == "Mask not set yet!"

    def test_from_region_to_data(self, mocker):
        mock_regions = MagicMock()

        coords = 10 * np.random.rand(10, 2) + 3.
        mocker.patch.object(mock_regions, 'coord_list', new=coords)
        data = OptModelCalibration.region_to_data(mock_regions)

        assert np.allclose(coords - 1, data)

    def test_from_data_to_region(self, mocker):
        data = 2 + np.random.rand(3, 4) * 10
        region = OptModelCalibration.data_to_region(data)

        assert np.allclose(data + 1, region)

    def test_new_opt_model1(self, mocker):
        instrument = Instrument()
        instrument.pixel_scale = 1.0

        calib = self.get_calib(instrument, mocker)

        with pytest.raises(Exception):
            calib.opt

        calib.new_opt_model(2, 2)
        assert calib.opt.xdeg == 2
        assert calib.opt.ydeg == 2

    def test_new_opt_model2(self, mocker):
        instrument = Instrument()
        instrument.pixel_scale = 1.0

        mask = Mask()
        mocker.patch.object(mask, 'compute_scales', return_value=(1.2, 3.4))
        calib = self.get_calib(instrument, mocker, mask)

        calib.new_opt_model(2, 2)
        assert calib.opt.xdeg == 2
        assert calib.opt.ydeg == 2
        assert calib.opt is mask.opt
        assert np.allclose(calib.opt.xscale, 1.2)
        assert np.allclose(calib.opt.yscale, 3.4)

    def test_left_border_to_px_center1(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] - 0.5)

    def test_left_border_to_px_center2(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] - 0.5)

    def test_left_border_to_px_center3(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] + 0.5)

    def test_left_border_to_px_center4(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] + 0.5)

    def test_left_border_to_px_center5(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0] + 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_left_border_to_px_center6(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] + 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_left_border_to_px_center7(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0] - 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_left_border_to_px_center8(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._left_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] - 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_right_border_to_px_center1(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] + 0.5)

    def test_right_border_to_px_center2(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] + 0.5)

    def test_right_border_to_px_center3(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] - 0.5)

    def test_right_border_to_px_center4(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] - 0.5)

    def test_right_border_to_px_center5(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0] - 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_right_border_to_px_center6(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] - 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_right_border_to_px_center7(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[0], orig[0] + 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_right_border_to_px_center8(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._right_border_to_px_center(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] + 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_px_center_to_left_border1(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] + 0.5)

    def test_px_center_to_left_border2(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] + 0.5)

    def test_px_center_to_left_border3(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] - 0.5)

    def test_px_center_to_left_border4(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] - 0.5)

    def test_px_center_to_left_border5(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[0], orig[0] - 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_px_center_to_left_border6(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] - 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_px_center_to_left_border7(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[0], orig[0] + 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_px_center_to_left_border8(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_left_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] + 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_px_center_to_right_border1(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] - 0.5)

    def test_px_center_to_right_border2(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] - 0.5)

    def test_px_center_to_right_border3(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[0], orig[0])
        assert np.allclose(pixels[1], orig[1] + 0.5)

    def test_px_center_to_right_border4(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'RL'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0])
        assert np.allclose(pixels[:, 1], orig[:, 1] + 0.5)

    def test_px_center_to_right_border5(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[0], orig[0] + 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_px_center_to_right_border6(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'BU'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] + 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_px_center_to_right_border7(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[0], orig[0] - 0.5)
        assert np.allclose(pixels[1], orig[1])

    def test_px_center_to_right_border8(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'UB'

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = 10 * np.random.rand(5, 2) + 2
        orig = np.copy(pixels)

        calib._px_center_to_right_border(pixels)

        assert np.allclose(pixels[:, 0], orig[:, 0] - 0.5)
        assert np.allclose(pixels[:, 1], orig[:, 1])

    def test_fit_opt_model1(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)
        mocker.patch.object(calib, '_parse_opt_regions', return_value={})

        with pytest.raises(Exception) as error:
            calib.fit_opt_model()

        assert error.type is ValueError

    def test_fit_opt_model2(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        mask = Mask()
        slit_A = MagicMock()
        slit_B = MagicMock()

        mock_vertices = {'A': np.random.rand(4, 2), 'B': np.random.rand(4, 2)}
        mocker.patch.object(slit_A, 'vertices', new=mock_vertices['A'])
        mocker.patch.object(slit_B, 'vertices', new=mock_vertices['B'])
        mock_slits = {'A': slit_A, 'B': slit_B}
        mocker.patch.object(mask, '_slits', new=mock_slits)

        calib = self.get_calib(instrument, mocker, mask=mask, opt=opt)
        mock_regions = {'A': np.random.rand(4, 2), 'B': np.random.rand(4, 2)}
        mocker.patch.object(calib, '_parse_opt_regions', return_value=mock_regions)

        mocker.patch.object(opt, 'refit')
        calib.fit_opt_model()

        args, kwargs = opt.refit.call_args

        expected = []
        for slit in mask:
            expected.extend(mock_vertices[slit])
        assert np.allclose(args[0], expected)

        expected = []
        for slit in mask:
            expected.extend(mock_regions[slit])
        assert np.allclose(args[1], expected)

    def test_fit_opt_model3(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        mask = Mask()
        slit_A = MagicMock()
        slit_B = MagicMock()

        mock_vertices = {'A': np.random.rand(8), 'B': np.random.rand(8)}
        mocker.patch.object(slit_A, 'vertices', new=mock_vertices['A'])
        mocker.patch.object(slit_B, 'vertices', new=mock_vertices['B'])
        mock_slits = {'A': slit_A, 'B': slit_B}
        mocker.patch.object(mask, '_slits', new=mock_slits)

        calib = self.get_calib(instrument, mocker, mask=mask, opt=opt)
        mock_regions = {'A': np.random.rand(8), 'B': np.random.rand(8)}
        mocker.patch.object(calib, '_parse_opt_regions', return_value=mock_regions)

        mocker.patch.object(opt, 'refit')
        calib.fit_opt_model()

        args, kwargs = opt.refit.call_args

        expected = []
        for slit in mask:
            expected.extend(mock_vertices[slit])
        expected = np.array(expected)
        expected.shape = (8, 2)
        assert np.allclose(args[0], expected)

        expected = []
        for slit in mask:
            expected.extend(mock_regions[slit])
        expected = np.array(expected)
        expected.shape = (8, 2)
        assert np.allclose(args[1], expected)

    def test_reg_plot_slit(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        slit = MagicMock()
        vertices = np.random.rand(4, 2)
        mocker.patch.object(slit, 'get_pix_vertices', return_value=vertices)
        slit.slit_id = "ABC"

        mocker.patch.object(calib, '_set_slit_regions')
        calib._reg_plot_slit(slit, color="MOCK_RED")

        args, kwargs = calib._set_slit_regions.call_args

        assert np.allclose(args[0], vertices)
        assert args[1] == "ABC"
        assert kwargs["color"] == "MOCK_RED"

    def test_set_slit_regions(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        pixels = np.array([1, 2, 3, 4])
        mocker.patch.object(OptModelCalibration, 'data_to_region', return_value=pixels)
        mocker.patch.object(calib, '_px_center_to_left_border')
        mocker.patch.object(OptModelCalibration, 'OPT_REGION_TYPE', new="MOCK")

        mocker.patch.object(calib._ds9, 'set')
        calib._set_slit_regions(pixels, "ABC", color="mock")

        args, kwargs = calib._ds9.set.call_args

        assert args[0] == "regions"
        assert args[1] == "image; MOCK(1 2 3 4)#text={ABC} color=mock edit=0"

    def test_plot_opt_model1(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)

        calib = self.get_calib(instrument, mocker, opt=opt)

        mocker.patch.object(calib._ds9, 'reset_background')
        mocker.patch.object(OptModelCalibration, 'data_to_region', return_value=[100, 200])

        mocker.patch.object(calib._ds9, 'set')
        calib.plot_opt_model(color="mock_red")

        args, kwargs = calib._ds9.set.call_args

        assert args[0] == "regions"
        assert args[1] == "image; point 100 200 #point=cross 40 tag={OptModelCenter} color=mock_red"

    def test_plot_opt_model2(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1)
        mask = Mask()
        slit = MagicMock()

        mocker.patch.object(mask, '_slits', new={"ABC": slit})

        calib = self.get_calib(instrument, mocker, mask=mask, opt=opt)

        mocker.patch.object(calib._ds9, 'reset_background')
        mocker.patch.object(OptModelCalibration, 'data_to_region', return_value=[100, 200])
        mocker.patch.object(calib, '_reg_plot_slit')

        mocker.patch.object(calib._ds9, 'set')
        calib.plot_opt_model(color="mock_red")

        args, kwargs = calib._ds9.set.call_args

        assert args[0] == "regions"
        assert args[1] == "image; point 100 200 #point=cross 40 tag={OptModelCenter} color=mock_red"

        args, kwargs = calib._reg_plot_slit.call_args

        assert args[0] is slit

    def test_parse_opt_regions1(self, mocker):
        instrument = Instrument()
        calib = self.get_calib(instrument, mocker)

        calib._ds9 = MagicMock()

        region = MagicMock()
        region.name = 'marco'

        calib._ds9.regions = [region, ]

        pixels = calib._parse_opt_regions()
        assert len(pixels) == 0

    def test_parse_opt_regions2(self, mocker):
        instrument = Instrument()
        calib = self.get_calib(instrument, mocker)

        region1 = MagicMock()
        region1.name = OptModelCalibration.OPT_REGION_TYPE
        reg_value1 = {'text': 'A', 'color': 'green'}
        region1.attr = {}
        region1.attr[1] = reg_value1

        region2 = MagicMock()
        region2.name = OptModelCalibration.OPT_REGION_TYPE
        reg_value2 = {'text': 'B', 'color': 'red'}
        region2.attr = {}
        region2.attr[1] = reg_value2

        region3 = MagicMock()
        region3.name = OptModelCalibration.OPT_REGION_TYPE
        reg_value3 = {'text': 'C', 'color': 'green'}
        region3.attr = {}
        region3.attr[1] = reg_value3

        calib._ds9 = MagicMock()
        calib._ds9.regions = [region1, region2, region3]
        points = [np.random.rand(2), np.random.rand(2), np.random.rand(2)]
        mocker.patch('spectrapy.modelscalib.optcalib.OptModelCalibration.region_to_data',
                     side_effect=points)
        mocker.patch.object(calib, '_left_border_to_px_center')

        pixels = calib._parse_opt_regions()
        assert len(pixels) == 3
        assert np.allclose(pixels['A'], points[0])
        assert np.allclose(pixels['B'], points[1])
        assert np.allclose(pixels['C'], points[2])

    def test_load_image(self, mocker):
        instrument = Instrument()
        calib = self.get_calib(instrument, mocker)

        instrument.data_hdu = 'mock_data'
        instrument.var_hdu = 'mock_var'
        instrument.flag_hdu = 'mock_flag'

        calib._ds9 = MagicMock()
        mocker.patch.object(calib._ds9, 'load_image')

        calib.load_image('mock_file')

        args, kwargs = calib._ds9.load_image.call_args

        assert args[0] == 'mock_file'
        assert kwargs['data_hdu'] == 'mock_data'
        assert len(args) == 1
        assert len(kwargs) == 1

    def test_sort_slit_by_distances1(self, mocker):
        instrument = Instrument()
        mask = Mask()
        opt = OptModel2d(1, 1)
        calib = self.get_calib(instrument, mocker, mask=mask, opt=opt)
        calib._ds9 = MagicMock()

        opt.xshift = 100
        opt.yshift = 50

        slitA = MagicMock()
        #mocker.patch.object(slitA, 'reference', return_value=False)
        mocker.patch.object(slitA, 'get_pix_center', return_value=(np.array([10, 2]), None))

        slitB = MagicMock()
        #mocker.patch.object(slitB, 'reference', return_value=False)
        mocker.patch.object(slitB, 'get_pix_center', return_value=(np.array([101, 54]), None))

        slitC = MagicMock()
        #mocker.patch.object(slitC, 'reference', return_value=False)
        mocker.patch.object(slitC, 'get_pix_center', return_value=(np.array([110, 50]), None))

        mask._slits = {'A': slitA, 'B': slitB, 'C': slitC}

        ids = calib._sort_slit_by_distances(True)

        assert ids[0] == 'B'
        assert ids[1] == 'C'
        assert ids[2] == 'A'

    def test_sort_slit_by_distances2(self, mocker):
        instrument = Instrument()
        mask = Mask()
        opt = OptModel2d(1, 1)
        calib = self.get_calib(instrument, mocker, mask=mask, opt=opt)
        calib._ds9 = MagicMock()

        opt.xshift = 100
        opt.yshift = 50

        slitA = MagicMock()
        slitA.reference = False
        mocker.patch.object(slitA, 'get_pix_center', return_value=(np.array([10, 2]), None))

        slitB = MagicMock()
        slitB.reference = False
        mocker.patch.object(slitB, 'get_pix_center', return_value=(np.array([101, 54]), None))

        slitC = MagicMock()
        slitC.reference = True
        mocker.patch.object(slitC, 'get_pix_center', return_value=(np.array([110, 50]), None))

        mask._slits = {'A': slitA, 'B': slitB, 'C': slitC}

        ids = calib._sort_slit_by_distances(False)

        assert ids[0] == 'B'
        assert ids[1] == 'A'

    def test_restart_display(self, mocker):
        instrument = Instrument()
        mask = Mask()
        opt = OptModel2d(1, 1)
        calib = self.get_calib(instrument, mocker, mask=mask, opt=opt)
        id1 = id(calib._ds9)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib.restart_display()
        id2 = id(calib._ds9)
        assert id1 != id2


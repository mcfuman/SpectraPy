#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File crvcalib_test.py
#
# Created on: Mar 08, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import spectrapy
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d
from spectrapy.models.crvmodel import CrvModel2d
from spectrapy.models.model import Model2d


from spectrapy.modelscalib.crvcalib import CrvModelCalibration
from spectrapy.mask.mask import Mask


class TestCrvModelCalibration(object):
    """Test for the calibration of the Optical Model"""

    # @pytest.fixture(autouse=True)
    # def mock_ds9(self, mocker):
    #    mocker.patch('spectrapy.dataio.display.Display')
    #    #mocker.patch('pyds9.DS9')
    #    pass

    def test_init1(self, mocker):
        opt = OptModel2d(1, 1)
        crv = CrvModel2d(2, 1, 1)
        mask = Mask()
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        mocker.patch('spectrapy.modelscalib.crvcalib.OptModelCalibration.__init__')
        calib = CrvModelCalibration(instrument, mask=mask, opt=opt, crv=crv)
        args, kwargs = spectrapy.modelscalib.crvcalib.OptModelCalibration.__init__.call_args

        assert args[0] is calib
        assert args[1] is instrument
        assert args[2] is mask
        assert args[3] is opt

        assert calib.crv is crv

        assert len(calib._traces) == 0
        assert len(calib._variances) == 0

        assert calib._red is None
        assert calib._blue is None

    def test_init2(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        mocker.patch('spectrapy.modelscalib.crvcalib.OptModelCalibration.__init__')
        calib = CrvModelCalibration(instrument)
        args, kwargs = spectrapy.modelscalib.crvcalib.OptModelCalibration.__init__.call_args

        assert args[0] is calib
        assert args[1] is instrument
        assert args[2] is None
        assert args[3] is None

        assert calib._crv is None

        assert len(calib._traces) == 0
        assert len(calib._variances) == 0

        assert calib._red is None
        assert calib._blue is None

    def test_crv1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        assert calib._crv is None
        with pytest.raises(Exception) as no_crv:
            calib.crv

        assert no_crv.type is AttributeError
        assert no_crv.value.args[0] == "CrvModel not set yet!"

    def test_crv2(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(2, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib.crv = crv

        assert calib.crv is crv

    def test_crv3(sel, mocker):
        instrument = Instrument()
        crv = CrvModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        mocker.patch('spectrapy.modelscalib.crvcalib.CrvModel2d.load', return_value=crv)
        calib.crv = "mock"

        assert calib.crv is crv

    def test_crv4(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        with pytest.raises(Exception) as invalid_model:
            calib = CrvModelCalibration(instrument, crv=1.2)

        assert invalid_model.type is IOError
        assert invalid_model.value.args[0] == "Invalid input CrvModel"

    def test_new_crv_model(self, mocker):
        instrument = Instrument()

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib.new_crv_model(1, 2, 4)

        assert calib.crv.deg == 1
        assert calib.crv.xdeg == 2
        assert calib.crv.ydeg == 4

    def test_get_spectrum_area(self, mocker):
        instrument = Instrument()
        mask = Mask()
        slit = MagicMock()

        crv = CrvModel2d(2, 1, 1)

        mocker.patch.object(Mask, '__getitem__', return_value=slit)
        mocker.patch.object(slit, 'get_slit_pix', return_value=(np.zeros(2), None))
        mocker.patch('spectrapy.dataio.display.Display.image_dims')

        verteces = [((2, 3), (11, 1)), ((3, 5), (10, 6))]
        mocker.patch.object(crv, 'get_trace_edges', side_effect=verteces)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, mask, crv=crv)
        area = calib._get_spectrum_area('mock', left_margin=2, right_margin=4)

        assert np.allclose(area[0], 2)
        assert np.allclose(area[1], 1)
        assert np.allclose(area[2], 9)
        assert np.allclose(area[3], 5)
        args, kwargs = slit.get_slit_pix.call_args

        assert np.allclose(args[0], 2)
        assert np.allclose(args[1], 4)
        assert len(args) == 2
        assert len(kwargs) == 0

    def test_reload_traces_from_regions(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        calib._traces = {'mock': [1, 2, 3]}
        calib._variances = {'mockv': [10, 20, 30]}

        mocker.patch.object(calib, '_reg_parse_crv_regions', return_value={'A': [4, 5]})

        calib.reload_traces_from_regions()

        assert len(calib._traces) == 1
        assert calib._traces['A'] == [4, 5]
        assert len(calib._variances) == 1

    def test_plot_crv_model(self, mocker):
        instrument = Instrument()

        mask = Mask()
        slitA = MagicMock()
        slitB = MagicMock()
        slitC = MagicMock()

        mask._slits = {'A': slitA, 'B': slitB, 'C': slitC}

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, mask)
        calib._ds9 = MagicMock()
        calib._ds9.xlen = 1000
        calib._ds9.ylen = 1000

        mocker.patch.object(calib, '_reg_plot_spectrum_trace')
        mocker.patch.object(calib, '_reg_plot_slit')
        mocker.patch.object(calib._ds9, 'reset_background')
        mocker.patch.object(calib, '_crv')

        calib.plot_crv_model(npixels=7)

        calib._ds9.reset_background.assert_called_once()

        calls = [mocker.call(slitA, 7, pos=(0,)), mocker.call(slitB, 7, pos=(0,)),
                 mocker.call(slitC, 7, pos=(0,))]
        calib._reg_plot_spectrum_trace.assert_has_calls(calls, any_order=True)

        calls = [mocker.call(slitA, color=CrvModelCalibration.BK_REGION_COLOR),
                 mocker.call(slitB, color=CrvModelCalibration.BK_REGION_COLOR),
                 mocker.call(slitC, color=CrvModelCalibration.BK_REGION_COLOR)]
        calib._reg_plot_slit.assert_has_calls(calls, any_order=True)

    def test_reg_plot_spectrum_trace(self, mocker):
        instrument = Instrument()

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        points = np.random.rand(10)
        mocker.patch.object(calib, '_get_pix_trace', return_value=points)
        mocker.patch.object(calib, '_reg_set_pix_trace')

        slit = MagicMock()
        mocker.patch.object(slit, 'slit_id', new='mock')
        calib._reg_plot_spectrum_trace(slit, 7, color='cobalto')

        args, kwargs = calib._get_pix_trace.call_args
        assert len(args) == 3
        assert args[0] is slit
        assert args[1] == 0
        assert args[2] == 7

        assert len(kwargs) == 0

        args, kwargs = calib._reg_set_pix_trace.call_args
        assert len(args) == 2
        assert np.allclose(args[0], points)
        assert args[1] == 'mock'

        assert len(kwargs) == 2
        assert kwargs['color'] == 'cobalto'
        assert kwargs['t'] == 0

    def test_reg_set_pix_trace1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib._ds9 = MagicMock()

        points = 100 * np.random.rand(3, 2)
        points = points.astype(int)

        mocker.patch.object(calib._ds9, 'set')
        mocker.patch('spectrapy.modelscalib.optcalib.OptModelCalibration.data_to_region',
                     side_effect=[points[0], points[1], points[2]])
        mocker.patch.object(calib, '_px_center_to_left_border')

        calib._reg_set_pix_trace(points - 20, 'mock', color='magenta')

        regions = f"image; {CrvModelCalibration.CRV_REGION_TYPE} {points[0][0]} {points[0][1]} "
        regions += "#point=cross 20 tag={mock} tag={t=0.00} text={mock} color=magenta\n"
        regions += f"image; {CrvModelCalibration.CRV_REGION_TYPE} {points[1][0]} {points[1][1]} "
        regions += "#point=cross 20 tag={mock} tag={t=0.00} text={mock} color=magenta\n"
        regions += f"image; {CrvModelCalibration.CRV_REGION_TYPE} {points[2][0]} {points[2][1]} "
        regions += "#point=cross 20 tag={mock} tag={t=0.00} text={mock} color=magenta\n"

        args, kwargs = calib._ds9.set.call_args

        assert len(args) == 2
        assert args[0] == "regions"
        assert args[1] == regions
        assert len(kwargs) == 0

    def test_reg_set_pix_trace2(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib._ds9 = MagicMock()

        points = 100 * np.random.rand(3, 2)
        points = points.astype(int)

        mocker.patch.object(calib._ds9, 'set')

        mocker.patch('spectrapy.modelscalib.optcalib.OptModelCalibration.data_to_region',
                     side_effect=[points[0], points[1], points[2]])
        mocker.patch.object(calib, '_px_center_to_right_border')

        calib._reg_set_pix_trace(points - 20, 'mock', t=1, color='magenta')

        regions = f"image; {CrvModelCalibration.CRV_REGION_TYPE} {points[0][0]} {points[0][1]} "
        regions += "#point=cross 20 tag={mock} tag={t=1.00} text={mock} color=magenta\n"
        regions += f"image; {CrvModelCalibration.CRV_REGION_TYPE} {points[1][0]} {points[1][1]} "
        regions += "#point=cross 20 tag={mock} tag={t=1.00} text={mock} color=magenta\n"
        regions += f"image; {CrvModelCalibration.CRV_REGION_TYPE} {points[2][0]} {points[2][1]} "
        regions += "#point=cross 20 tag={mock} tag={t=1.00} text={mock} color=magenta\n"

        args, kwargs = calib._ds9.set.call_args

        assert len(args) == 2
        assert args[0] == "regions"
        assert args[1] == regions
        assert len(kwargs) == 0

    def test_reg_plot_crv_regions(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        regions = {'0.00': {'A': np.random.rand(6), 'B': np.random.rand(6)}}
        mocker.patch.object(calib, '_reg_set_pix_trace')

        calib._reg_plot_crv_regions(regions, color='green')
        calls = [mocker.call(regions['0.00']['A'], 'A', t=0.0, color='green'),
                 mocker.call(regions['0.00']['B'], 'B', t=0.0, color='green')]
        calib._reg_set_pix_trace.assert_has_calls(calls, any_order=True)

    def test_get_pix_trace1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        slit = MagicMock()
        mocker.patch.object(slit, 'get_pix_left_edge', return_value=(None, None))
        mocker.patch.object(slit, 'get_pix_position', return_value=(None, None))

        with pytest.raises(Exception) as invalid_npixels:
            calib._get_pix_trace(slit, 0, np.array([1, 2]))

        assert invalid_npixels.type is TypeError
        assert invalid_npixels.value.args[0] == "Invalid npixels parameter value"

    def test_get_pix_trace2(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(2, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)

        slit = MagicMock()

        mocker.patch.object(slit, 'get_pix_position', return_value=(None, None))

        with pytest.raises(Exception) as invalid_npixels:
            calib._get_pix_trace(slit, 0, 1.2)

        assert invalid_npixels.type is ArithmeticError
        assert invalid_npixels.value.args[0] == "Parameter value npixels too low"

    def test_get_pix_trace3(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(2, 1, 1)
        crv.dispersion_direction = 'LR'
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)
        calib.set_trace_limits(200, 100)

        slit = MagicMock()
        margins = np.random.rand(2)
        mocker.patch.object(slit, 'get_pix_position', return_value=(margins, None))
        mocker.patch.object(crv, 'compute', return_value=(np.zeros(2), np.zeros(2)))

        calib._get_pix_trace(slit, 0, 5)

        args, kwargs = crv.compute.call_args

        assert len(args) == 2
        assert np.allclose(args[0], margins)
        assert np.allclose(args[1], np.linspace(-200., 100., 5))
        assert len(kwargs) == 0

    def test_set_trace_limits1(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)

        with pytest.raises(Exception) as invalid_limit:
            calib.set_trace_limits(-10, 10)

        assert invalid_limit.type is ValueError
        assert invalid_limit.value.args[0] == "The blue amount of pixels must be positive"

    def test_set_trace_limits2(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)

        with pytest.raises(Exception) as invalid_limit:
            calib.set_trace_limits(10, -10)

        assert invalid_limit.type is ValueError
        assert invalid_limit.value.args[0] == "The red amount of pixels must be positive"

    def test_set_trace_limits3(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)
        crv.dispersion_direction = 'RL'
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)
        calib.set_trace_limits(20, 10)

        assert calib._red == 10
        assert calib._blue == -20

    def test_set_trace_limits4(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)
        crv.dispersion_direction = 'LR'
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)

        calib.set_trace_limits(20, 10)

        assert calib._red == 10
        assert calib._blue == -20

    def test_display_single_slit(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, mask, crv=crv)
        calib._ds9 = MagicMock()

        slit = MagicMock()
        mask._slits = {'mock': slit}
        v = np.random.rand(4, 2)
        v2 = np.array(v)

        mocker.patch.object(slit, 'get_pix_vertices', return_value=v)
        mocker.patch.object(calib, '_get_spectrum_area', return_value=np.zeros(4))
        mocker.patch.object(calib, '_set_slit_regions')

        mocker.patch('spectrapy.dataio.display.Display.trim_image')
        xstart = np.random.rand()
        ystart = np.random.rand()

        mocker.patch.object(calib._ds9, 'xstart', new=xstart)
        mocker.patch.object(calib._ds9, 'ystart', new=ystart)

        calib.display_single_slit('mock')
        args, kwargs = calib._set_slit_regions.call_args

        v2[:, 0] -= xstart
        v2[:, 1] -= ystart

        assert len(args) == 2
        assert np.allclose(args[0], v2)
        assert args[1] == 'mock'

        assert len(kwargs) == 1
        assert kwargs['color'] == CrvModelCalibration.BK_REGION_COLOR

    def test_fit_crv_model1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        mocker.patch.object(calib, 'reload_traces_from_regions')

        with pytest.raises(Exception) as no_traces:
            calib.fit_crv_model()

        assert no_traces.type is ValueError
        assert no_traces.value.args[0] == "No traces"

    def test_fit_crv_model2(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)

        mocker.patch.object(crv, 'refit')
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)
        mocker.patch.object(calib, 'reload_traces_from_regions')

        calib._traces = {'0.00': {'A': []}}
        calib.fit_crv_model()

    def test_fit_crv_model3(self, mocker):
        instrument = Instrument()
        crv = CrvModel2d(1, 1, 1)

        mocker.patch.object(crv, 'refit')
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, crv=crv)
        mocker.patch.object(calib, 'reload_traces_from_regions')
        mocker.patch.object(calib._ds9, 'replace_regions')

        calib._traces = {'0.00': {'A': []}}
        calib.fit_crv_model(filename="mockfile")

        args, kwargs = calib._ds9.replace_regions.call_args

        assert args[0] == "mockfile"

    def test_fit_crv_model4(self, mocker):
        instrument = Instrument()
        mask = Mask()
        crv = CrvModel2d(2, 1, 0)

        slitA = MagicMock()
        slitB = MagicMock()
        slitC = MagicMock()
        mask._slits = {'A': slitA, 'B': slitB, 'C': slitC}
        slit_positions = 100 * np.random.rand(3, 2)
        mocker.patch.object(slitA, 'get_pix_position', return_value=(slit_positions[0], None))
        mocker.patch.object(slitB, 'get_pix_position', return_value=(slit_positions[1], None))
        mocker.patch.object(slitC, 'get_pix_position', return_value=(slit_positions[2], None))

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, mask=mask, crv=crv)
        mocker.patch.object(calib, 'reload_traces_from_regions')
        slit_traces = 100 * np.random.rand(3, 20, 2)
        calib._traces = {'0.00': {'A': slit_traces[0], 'B': slit_traces[1], 'C': slit_traces[2]}}

        coeffs = 10 * np.random.rand(3, 4)
        variances = np.random.rand(3, 4)
        modelA = MagicMock()
        modelA.coeffs = coeffs[0]
        modelA.variance = variances[0]
        mocker.patch.object(modelA, 'log_coeffs')

        modelB = MagicMock()
        modelB.coeffs = coeffs[1]
        modelB.variance = variances[1]
        mocker.patch.object(modelB, 'log_coeffs')

        modelC = MagicMock()
        modelC.coeffs = coeffs[2]
        modelC.variance = variances[2]
        mocker.patch.object(modelC, 'log_coeffs')

        mocker.patch.object(crv, 'refit')
        mocker.patch.object(crv, 'refit_trace', side_effect=[modelA, modelB, modelC])

        calib.fit_crv_model(True)

        for i in range(3):
            assert np.allclose(crv.refit_trace.call_args_list[i][0][0], slit_positions[i])
            assert np.allclose(crv.refit_trace.call_args_list[i][0][1], slit_traces[i].T[0])
            assert np.allclose(crv.refit_trace.call_args_list[i][0][2], slit_traces[i].T[1])
            assert crv.refit_trace.call_args_list[i][1]['wdata'] is None

        args, kwargs = crv.refit.call_args

        assert len(args) == 3
        assert np.allclose(args[0], slit_positions[:, 0])
        assert np.allclose(args[1], slit_positions[:, 1])
        assert np.allclose(args[2], coeffs)

        assert len(kwargs) == 1
        assert np.allclose(kwargs['var'], variances)

    def test_fit_crv_model5(self, mocker):
        instrument = Instrument()
        mask = Mask()
        crv = CrvModel2d(2, 1, 0)

        slitA = MagicMock()
        slitB = MagicMock()
        slitC = MagicMock()
        mask._slits = {'A': slitA, 'B': slitB, 'C': slitC}
        slit_positions = 100 * np.random.rand(3, 2)
        mocker.patch.object(slitA, 'get_pix_position', return_value=(slit_positions[0], None))
        mocker.patch.object(slitB, 'get_pix_position', return_value=(slit_positions[1], None))
        mocker.patch.object(slitC, 'get_pix_position', return_value=(slit_positions[2], None))

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument, mask=mask, crv=crv)
        mocker.patch.object(calib, 'reload_traces_from_regions')

        slit_traces = 100 * np.random.rand(3, 20, 2)
        slit_traces = slit_traces.astype(int)
        calib._traces = {'0.00': {'A': slit_traces[0], 'B': slit_traces[1], 'C': slit_traces[2]}}

        slit_variances = 100 * np.random.rand(3, 20, 2)
        calib._variances = {'0.00': {'A': slit_variances[0], 'B': slit_variances[1], 'C': slit_variances[2]}}

        coeffs = 10 * np.random.rand(3, 4)
        variances = np.random.rand(3, 4)
        modelA = MagicMock()
        modelA.coeffs = coeffs[0]
        modelA.variance = variances[0]
        mocker.patch.object(modelA, 'log_coeffs')

        modelB = MagicMock()
        modelB.coeffs = coeffs[1]
        modelB.variance = variances[1]
        mocker.patch.object(modelB, 'log_coeffs')

        modelC = MagicMock()
        modelC.coeffs = coeffs[2]
        modelC.variance = variances[2]
        mocker.patch.object(modelC, 'log_coeffs')

        mocker.patch.object(crv, 'refit')
        mocker.patch.object(crv, 'refit_trace', side_effect=[modelA, modelB, modelC])

        calib.fit_crv_model(weight=True)

        for i in range(3):
            assert np.allclose(crv.refit_trace.call_args_list[i][0][0], slit_positions[i])
            assert np.allclose(crv.refit_trace.call_args_list[i][0][1], slit_traces[i].T[0])
            assert np.allclose(crv.refit_trace.call_args_list[i][0][2], slit_traces[i].T[1])
            assert np.allclose(crv.refit_trace.call_args_list[i][1]['wdata'], 1 / np.sqrt(slit_variances[i]))

        args, kwargs = crv.refit.call_args
        assert len(args) == 3
        assert np.allclose(args[0], slit_positions[:, 0])
        assert np.allclose(args[1], slit_positions[:, 1])
        assert np.allclose(args[2], coeffs)

        assert len(kwargs) == 1
        assert np.allclose(kwargs['var'], variances)

    def test_reg_parse_crv_regions1(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib._ds9 = MagicMock()
        region = MagicMock()
        region.name = 'marco'

        calib._ds9.regions = [region, ]

        pixels = calib._reg_parse_crv_regions()
        assert len(pixels) == 0

    def test_reg_parse_crv_regions2(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib._ds9 = MagicMock()
        region = MagicMock()
        region.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value = {'text': 'A', 'color': 'green', 'tag': []}
        region.attr = {}
        region.attr[1] = reg_value

        calib._ds9.regions = [region, ]

        with pytest.raises(Exception) as no_edge:
            calib._reg_parse_crv_regions()

        assert no_edge.type is ValueError
        assert no_edge.value.args[0] == "Error in region file. No valid edge tag"

    def test_reg_parse_crv_regions3(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)
        calib._ds9 = MagicMock()
        region = MagicMock()
        region.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value = {'text': 'A', 'color': 'green', 'tag': ['t=1.10', ]}
        region.attr = {}
        region.attr[1] = reg_value

        calib._ds9.regions = [region, ]

        with pytest.raises(Exception) as no_edge:
            calib._reg_parse_crv_regions()

        assert no_edge.type is ValueError
        assert no_edge.value.args[0] == "Error in region file. No valid edge tag"

    def test_reg_parse_crv_regions4(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        region1 = MagicMock()
        region1.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value1 = {'text': 'A', 'color': 'green', 'tag': ['t=0.00', ]}
        region1.attr = {}
        region1.attr[1] = reg_value1

        region2 = MagicMock()
        region2.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value2 = {'text': 'A', 'color': 'red', 'tag': ['t=0.00', ]}
        region2.attr = {}
        region2.attr[1] = reg_value2

        region3 = MagicMock()
        region3.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value3 = {'text': 'A', 'color': 'green', 'tag': ['t=0.00', ]}
        region3.attr = {}
        region3.attr[1] = reg_value3

        calib._ds9 = MagicMock()
        calib._ds9.regions = [region1, region2, region3]
        # mocker.patch.object(calib._ds9, 'regions', side_effect=[region1, region2, region3])#'spectrapy.dataio.display.Display.regions', return_value=[region1, region2, region3])

        points = [np.random.rand(2), np.random.rand(2), np.random.rand(2)]
        mocker.patch('spectrapy.modelscalib.optcalib.OptModelCalibration.region_to_data',
                     side_effect=points)
        mocker.patch.object(calib, '_left_border_to_px_center')

        pixels = calib._reg_parse_crv_regions()
        assert len(pixels) == 1
        assert len(pixels['0.00']) == 1
        assert len(pixels['0.00']['A']) == 2
        assert np.allclose(pixels['0.00']['A'], [points[0], points[2]])

    def test_reg_parse_crv_regions5(self, mocker):
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = CrvModelCalibration(instrument)

        region1 = MagicMock()
        region1.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value1 = {'text': 'A', 'color': 'green', 'tag': ['t=1.00', ]}
        region1.attr = {}
        region1.attr[1] = reg_value1

        region2 = MagicMock()
        region2.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value2 = {'text': 'A', 'color': 'red', 'tag': ['t=1.00', ]}
        region2.attr = {}
        region2.attr[1] = reg_value2

        region3 = MagicMock()
        region3.name = CrvModelCalibration.CRV_REGION_TYPE
        reg_value3 = {'text': 'A', 'color': 'green', 'tag': ['t=1.00', ]}
        region3.attr = {}
        region3.attr[1] = reg_value3

        calib._ds9 = MagicMock()
        calib._ds9.regions = [region1, region2, region3]

        points = [np.random.rand(2), np.random.rand(2), np.random.rand(2)]
        mocker.patch('spectrapy.modelscalib.optcalib.OptModelCalibration.region_to_data',
                     side_effect=points)
        mocker.patch.object(calib, '_right_border_to_px_center')

        pixels = calib._reg_parse_crv_regions()
        assert len(pixels) == 1
        assert len(pixels['1.00']) == 1
        assert len(pixels['1.00']['A']) == 2
        assert np.allclose(pixels['1.00']['A'], [points[0], points[2]])

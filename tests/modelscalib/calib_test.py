#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File calib_test.py
#
# Created on: Mar 08, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import spectrapy
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d
from spectrapy.models.crvmodel import CrvModel2d
from spectrapy.models.idsmodel import IdsModel2d

from spectrapy.models.model import Model2d


from spectrapy.modelscalib.calib import ModelsCalibration
from spectrapy.modelscalib.model1diter import Model1dIterator
from spectrapy.mask.mask import Mask


class TestModelCalibration(object):
    """Test for the calibration of the Optical Model"""

    def get_calib(self, mocker, instrument=None, mask=None, opt=None, crv=None, ids=None):
        if instrument is None:
            instrument = Instrument()
        if mask is None:
            mask = Mask()
        if opt is None:
            opt = OptModel2d(1, 1)
        if crv is None:
            crv = CrvModel2d(2, 1, 1)
        if ids is None:
            ids = IdsModel2d(2, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = ModelsCalibration(instrument, mask=mask, opt=opt, crv=crv, ids=ids)
        calib._ds9 = MagicMock()
        return calib

    def test_init1(self, mocker):
        opt = OptModel2d(1, 1)
        crv = CrvModel2d(2, 1, 1)
        ids = IdsModel2d(2, 1, 1)

        mask = Mask()
        instrument = Instrument()
        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        mocker.patch('spectrapy.modelscalib.idscalib.IdsModelCalibration.__init__')
        calib = ModelsCalibration(instrument, mask=mask, opt=opt, crv=crv, ids=ids)
        args, kwargs = spectrapy.modelscalib.idscalib.IdsModelCalibration.__init__.call_args

        assert args[0] is calib
        assert args[1] is instrument
        assert args[2] is mask
        assert args[3] is opt
        assert args[4] is crv
        assert args[5] is ids

    def test_ids_iter1(self, mocker):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib._ds9, 'has_image', return_value=False)

        with pytest.raises(AttributeError) as no_image:
            calib.ids_iter(None)

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_ids_iter2(self, mocker):
        calib = self.get_calib(mocker)

        catalog = np.array([1.0, 2.0, 3.0])
        ids = ['A', 'B']

        mocker.patch.object(calib, '_load_catalog', return_value=catalog)
        mocker.patch.object(calib, '_sort_slit_by_distances', return_value=ids)
        mocker.patch.object(calib, '_calibrate_single_slit')
        mocker.patch('spectrapy.modelscalib.calib.Model1dIterator')
        calib.ids_iter(None, None)

        assert calib._ids_iter is not None
        args, kwargs = spectrapy.modelscalib.calib.Model1dIterator.call_args

        assert args[0] == ['A', 'B']
        assert np.allclose(args[1], catalog)

    def test_fit_slit1(self, mocker):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib, '_parse_ids_regions', return_value={})

        with pytest.raises(Exception) as no_regions:
            calib._fit_slit()

        assert no_regions.type is AttributeError
        assert no_regions.value.args[0] == "No regions!"

    def test_fit_slit2(self, mocker):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib, '_parse_ids_regions', return_value={'A': [1, 2, 3]})
        calib._ids_iter = MagicMock()
        mocker.patch.object(calib._ids_iter, 'current_slit', new='B')
        with pytest.raises(Exception) as no_regions:
            calib._fit_slit()

        assert no_regions.type is IndexError
        assert no_regions.value.args[0] == "No slit B in regions!"

    def test_fit_slit3(self, mocker):
        calib = self.get_calib(mocker)
        calib._ids.dispersion_direction = 'LR'
        mocker.patch.object(calib, '_parse_ids_regions', return_value={'A': [1, 2, 3]})
        ids_iter = Model1dIterator(['A', ], [1000.0, 2000.])
        ids_iter._1d_models = {'A': None}
        mocker.patch.object(ids_iter, 'set_model')
        mocker.patch.object(calib, '_refit_single_slit')

        calib._ids_iter = ids_iter
        calib._mask._slits = {'A': None}
        mocker.patch.object(calib._ds9, 'set')

        #mocker.patch.object(ids_iter, 'current_slit', return_value='A')
        calib._fit_slit()

        assert calib._ds9.set.call_args_list[0][0][0] == "frame new"
        assert calib._ds9.set.call_args_list[1][0][0] == "tile yes"
        assert calib._ds9.set.call_args_list[2][0][0] == "tile row"

    def test_fit_slit4(self, mocker):
        calib = self.get_calib(mocker)
        calib._ids.dispersion_direction = 'RL'
        mocker.patch.object(calib, '_parse_ids_regions', return_value={'A': [1, 2, 3]})
        ids_iter = Model1dIterator(['A', ], [1000.0, 2000.])
        ids_iter._1d_models = {'A': None}
        mocker.patch.object(ids_iter, 'set_model')
        mocker.patch.object(calib, '_refit_single_slit')

        calib._ids_iter = ids_iter
        calib._mask._slits = {'A': None}
        mocker.patch.object(calib._ds9, 'set')

        #mocker.patch.object(ids_iter, 'current_slit', return_value='A')
        calib._fit_slit()

        assert calib._ds9.set.call_args_list[0][0][0] == "frame new"
        assert calib._ds9.set.call_args_list[1][0][0] == "tile yes"
        assert calib._ds9.set.call_args_list[2][0][0] == "tile row"

    def test_fit_slit5(self, mocker):
        calib = self.get_calib(mocker)
        calib._ids.dispersion_direction = 'BU'
        mocker.patch.object(calib, '_parse_ids_regions', return_value={'A': [1, 2, 3]})
        ids_iter = Model1dIterator(['A', ], [1000.0, 2000.])
        ids_iter._1d_models = {'A': None}
        mocker.patch.object(ids_iter, 'set_model')
        mocker.patch.object(calib, '_refit_single_slit')

        calib._ids_iter = ids_iter
        calib._mask._slits = {'A': None}
        mocker.patch.object(calib._ds9, 'set')

        #mocker.patch.object(ids_iter, 'current_slit', return_value='A')
        calib._fit_slit()

        assert calib._ds9.set.call_args_list[0][0][0] == "frame new"
        assert calib._ds9.set.call_args_list[1][0][0] == "tile yes"
        assert calib._ds9.set.call_args_list[2][0][0] == "tile column"

    def test_fit_slit6(self, mocker):
        calib = self.get_calib(mocker)
        calib._ids.dispersion_direction = 'UB'
        mocker.patch.object(calib, '_parse_ids_regions', return_value={'A': [1, 2, 3]})
        ids_iter = Model1dIterator(['A', ], [1000.0, 2000.])
        ids_iter._1d_models = {'A': None}
        mocker.patch.object(ids_iter, 'set_model')
        mocker.patch.object(calib, '_refit_single_slit')

        calib._ids_iter = ids_iter
        calib._mask._slits = {'A': None}
        mocker.patch.object(calib._ds9, 'set')

        #mocker.patch.object(ids_iter, 'current_slit', return_value='A')
        calib._fit_slit()

        assert calib._ds9.set.call_args_list[0][0][0] == "frame new"
        assert calib._ds9.set.call_args_list[1][0][0] == "tile yes"
        assert calib._ds9.set.call_args_list[2][0][0] == "tile column"

    def test_next1(self, mocker, caplog):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib, '_fit_slit')
        mocker.patch.object(calib, '_calibrate_single_slit')

        calib.next()
        assert caplog.record_tuples[-1][2] == "IDS interactive calibation is OFF"

        assert not calib._fit_slit.called
        assert not calib._calibrate_single_slit.called

    def test_next2(self, mocker, caplog):
        calib = self.get_calib(mocker)
        calib._ids_iter = MagicMock()
        mocker.patch.object(calib, '_fit_slit')
        mocker.patch.object(calib._ids_iter, 'next')
        mocker.patch.object(calib, '_calibrate_single_slit')

        calib._ids_iter.has_next = False
        calib.next(fit=True)
        assert caplog.record_tuples[-1][2] == "No more slits"

        assert calib._fit_slit.called
        assert not calib._ids_iter.next.called
        assert not calib._calibrate_single_slit.called

    def test_next3(self, mocker):
        calib = self.get_calib(mocker)
        calib._ids_iter = MagicMock()
        mocker.patch.object(calib, '_fit_slit')
        mocker.patch.object(calib._ids_iter, 'next')
        mocker.patch.object(calib, '_calibrate_single_slit')

        calib._ids_iter.has_next = True
        calib.next(fit=False)

        assert not calib._fit_slit.called
        assert calib._ids_iter.next.called
        assert calib._calibrate_single_slit.called

    def test_prev1(self, mocker, caplog):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib, '_fit_slit')
        mocker.patch.object(calib, '_calibrate_single_slit')

        calib.prev()
        assert caplog.record_tuples[-1][2] == "IDS interactive calibation is OFF"

        assert not calib._fit_slit.called
        assert not calib._calibrate_single_slit.called

    def test_prev2(self, mocker, caplog):
        calib = self.get_calib(mocker)
        calib._ids_iter = MagicMock()
        mocker.patch.object(calib, '_fit_slit')
        mocker.patch.object(calib._ids_iter, 'prev')
        mocker.patch.object(calib, '_calibrate_single_slit')

        calib._ids_iter.has_prev = False
        calib.prev(fit=True)
        assert caplog.record_tuples[-1][2] == "No more slits"

        assert calib._fit_slit.called
        assert not calib._ids_iter.prev.called
        assert not calib._calibrate_single_slit.called

    def test_prev3(self, mocker):
        calib = self.get_calib(mocker)
        calib._ids_iter = MagicMock()
        mocker.patch.object(calib, '_fit_slit')
        mocker.patch.object(calib._ids_iter, 'prev')
        mocker.patch.object(calib, '_calibrate_single_slit')

        calib._ids_iter.has_prev = True
        calib.prev(fit=False)

        assert not calib._fit_slit.called
        assert calib._ids_iter.prev.called
        assert calib._calibrate_single_slit.called

    def test_calibrate_single_slit1(self, mocker):
        calib = self.get_calib(mocker)
        ids_iter = MagicMock()
        calib._ids_iter = ids_iter
        ids_iter.current_slit = 'A'
        mocker.patch.object(ids_iter, 'has_model', return_value=False)
        mocker.patch.object(ids_iter, 'catalog', new=np.array([1.0, 2.0, 3.0]))
        mocker.patch('spectrapy.modelscalib.calib.IdsModelCalibration.display_single_slit')
        mocker.patch.object(calib, '_plot_catalog_regions')

        ids = np.random.rand(*calib._ids.model.shape)
        calib._ids.model = ids

        calib._calibrate_single_slit()

        assert np.allclose(calib._ids.model, ids)

        args, kwargs = spectrapy.modelscalib.calib.IdsModelCalibration.display_single_slit.call_args
        assert args[0] is calib
        assert args[1] == 'A'

        args, kwargs = calib._plot_catalog_regions.call_args
        assert np.allclose(args[0], np.array([1.0, 2.0, 3.0]))
        assert args[1] == 'A'

    def test_calibrate_single_slit2(self, mocker):
        calib = self.get_calib(mocker)
        ids_iter = MagicMock()
        calib._ids_iter = ids_iter
        ids_iter.current_slit = 'A'
        mocker.patch.object(ids_iter, 'has_model', return_value=True)
        mocker.patch.object(ids_iter, 'catalog', new=np.array([1.0, 2.0, 3.0]))
        mocker.patch('spectrapy.modelscalib.calib.IdsModelCalibration.display_single_slit')
        mocker.patch.object(calib, '_plot_catalog_regions')

        model = MagicMock()
        coeffs = np.random.rand(calib._ids.model.shape[0])
        model.coeffs = coeffs
        mocker.patch.object(ids_iter, 'get_model', return_value={'0.00': model})

        ids = np.random.rand(*calib._ids.model.shape)
        calib._ids.model = ids

        calib._calibrate_single_slit()

        assert not np.allclose(calib._ids.model, ids)
        new_ids = np.zeros(calib._ids.model.shape)
        for i in range(len(coeffs)):
            new_ids[i][0][0] = coeffs[i]
        assert np.allclose(calib._ids.model, new_ids)

        args, kwargs = spectrapy.modelscalib.calib.IdsModelCalibration.display_single_slit.call_args
        assert args[0] is calib
        assert args[1] == 'A'

        args, kwargs = calib._plot_catalog_regions.call_args
        assert np.allclose(args[0], np.array([1.0, 2.0, 3.0]))
        assert args[1] == 'A'

    def test_stop_iter1(self, mocker, caplog):

        calib = self.get_calib(mocker)
        calib.stop_iter()

        assert caplog.record_tuples[-1][2] == "IDS interactive calibation is OFF"

    def test_stop_iter2(self, mocker, caplog):

        calib = self.get_calib(mocker)
        calib._ids_iter = MagicMock()
        mocker.patch.object(calib._ds9, 'set')
        mocker.patch.object(calib, 'plot_ids_model')

        calib.stop_iter(fit=False)

        assert caplog.record_tuples[-1][2] == "Fit is False. Ignored calibration adjustment"

        assert calib._ds9.set.call_args_list[0][0][0] == "frame 2"
        assert calib._ds9.set.call_args_list[1][0][0] == "frame delete"

        assert calib._ids_iter is None

    def test_stop_iter3(self, mocker, caplog):

        calib = self.get_calib(mocker)
        ids_iter = Model1dIterator(['A', 'B'], [1000.0, 2000.])
        ids_iter._1d_models = {'A': None, 'B': None}

        mocker.patch.object(calib._ds9, 'set')
        mocker.patch.object(calib, 'plot_ids_model')

        coeffs = np.random.rand(3, 4)
        variance = np.random.rand(3, 4)

        model1 = MagicMock()
        model1.coeffs = coeffs[0]
        model1.variance = variance[0]
        model2 = MagicMock()
        model2.coeffs = coeffs[1]
        model2.variance = variance[1]
        model3 = MagicMock()
        model3.coeffs = coeffs[2]
        model3.variance = variance[2]

        pos = np.random.rand(3, 2)
        slitA = MagicMock()
        mocker.patch.object(slitA, 'get_pix_position', side_effect=((pos[0], None), ))
        slitB = MagicMock()
        mocker.patch.object(slitB, 'get_pix_position', side_effect=((pos[1], None), (pos[2], None)))

        calib._mask._slits = {'A': slitA, 'B': slitB}
        mocker.patch.object(ids_iter, 'get_model', side_effect=({'0.00': model1},
                                                                {'0.00': model2, '1.00': model3}))
        calib._ids_iter = ids_iter
        mocker.patch.object(calib.ids, 'refit')

        calib.stop_iter(fit=True)

        args, kwargs = calib.ids.refit.call_args

        assert np.allclose(args[0], pos[:, 0])
        assert np.allclose(args[1], pos[:, 1])
        assert np.allclose(args[2], coeffs)
        assert np.allclose(kwargs['var'], variance)

        assert calib._ds9.set.call_args_list[0][0][0] == "frame 2"
        assert calib._ds9.set.call_args_list[1][0][0] == "frame delete"

        assert calib._ids_iter is None

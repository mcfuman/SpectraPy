#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File resamplingbox_test.py
#
# Created on: May 11, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#


import numpy as np
import pytest

from spectrapy.extraction.resamplingbox import ResamplingBox


class TestResamplingBox(object):
    """Test for ResamplingBox"""

    def test_init1(self):
        """
        """

        box = ResamplingBox()

        assert np.allclose(box.radius, 0.)
        assert box._pos is None
        assert box._xrange is None
        assert box._yrange is None

    def test_set_position1(self):
        box = ResamplingBox(2)

        box.set_position(np.array([2.4, 8.4]))

        assert np.allclose(box._pos, [2.4, 8.4])
        assert box.xstart == 1
        assert box.ystart == 7
        assert box.xend == 4
        assert box.yend == 10

        assert box.xsize == 4
        assert box.ysize == 4

        assert np.allclose(box.xrange, np.arange(1, 5))
        assert np.allclose(box.yrange, np.arange(7, 11))

    def test_set_position2(self):
        box = ResamplingBox(2)

        box.set_position(np.array([2.4, 8.6]))

        assert np.allclose(box._pos, [2.4, 8.6])
        assert box.xstart == 1
        assert box.ystart == 7
        assert box.xend == 4
        assert box.yend == 10

        assert box.xsize == 4
        assert box.ysize == 4

        assert np.allclose(box.xrange, np.arange(1, 5))
        assert np.allclose(box.yrange, np.arange(7, 11))

    def test_set_position3(self):
        box = ResamplingBox(2)

        box.set_position(np.array([2.6, 8.6]))

        assert np.allclose(box._pos, [2.6, 8.6])
        assert box.xstart == 1
        assert box.ystart == 7
        assert box.xend == 4
        assert box.yend == 10

        assert box.xsize == 4
        assert box.ysize == 4

        assert np.allclose(box.xrange, np.arange(1, 5))
        assert np.allclose(box.yrange, np.arange(7, 11))

    def test_set_position4(self):
        box = ResamplingBox(2)

        box.set_position(np.array([2.5, 8.5]))

        assert np.allclose(box._pos, [2.5, 8.5])
        assert box.xstart == 1
        assert box.ystart == 7
        assert box.xend == 4
        assert box.yend == 10

        assert box.xsize == 4
        assert box.ysize == 4

        assert np.allclose(box.xrange, np.arange(1, 5))
        assert np.allclose(box.yrange, np.arange(7, 11))

    def test_has_position1(self):
        box = ResamplingBox(2)

        assert box.has_position() is False

    def test_has_position2(self):
        box = ResamplingBox(2)
        box.set_position(np.array([2.6, 8.6]))

        assert box.has_position() is True

    def test_radius(self):
        box = ResamplingBox(2)
        box.set_position(np.array([2.6, 8.6]))

        assert box._radius is not None
        assert box._pos is not None
        assert box._xrange is not None
        assert box._yrange is not None

        box.radius = 3.5
        assert np.allclose(box._radius, 3.5)
        assert box._pos is None
        assert box._xrange is None
        assert box._yrange is None

    def test_out_of_circle1(self):
        box = ResamplingBox(2)

        with pytest.raises(Exception) as no_position:
            box.is_out_of_circle(None)

        assert no_position.type is AttributeError
        assert no_position.value.args[0] == "Position not set yet!"

    def test_out_of_circle2(self):
        box = ResamplingBox(2)
        box.set_position(np.array([2.4, 8.4]))

        assert box.is_out_of_circle(np.array([2.4, 8.4])) == False

        assert box.is_out_of_circle(np.array([1.4, 8.4])) == False
        assert box.is_out_of_circle(np.array([0.41, 8.4])) == False
        assert box.is_out_of_circle(np.array([0.39, 8.4])) == True

        assert box.is_out_of_circle(np.array([3.4, 8.4])) == False
        assert box.is_out_of_circle(np.array([4.39, 8.4])) == False
        assert box.is_out_of_circle(np.array([4.41, 8.4])) == True

        assert box.is_out_of_circle(np.array([2.4, 7.4])) == False
        assert box.is_out_of_circle(np.array([2.4, 6.41])) == False
        assert box.is_out_of_circle(np.array([2.4, 6.39])) == True

        assert box.is_out_of_circle(np.array([2.4, 9.4])) == False
        assert box.is_out_of_circle(np.array([2.4, 10.39])) == False
        assert box.is_out_of_circle(np.array([2.4, 10.41])) == True

    def test_out_of_box1(self):
        box = ResamplingBox(2)

        with pytest.raises(Exception) as no_position:
            box.is_out_of_box(None)

        assert no_position.type is AttributeError
        assert no_position.value.args[0] == "Position not set yet!"

    def test_out_of_box2(self):
        box = ResamplingBox(2)
        box.set_position(np.array([2.4, 8.4]))

        assert box.is_out_of_box(np.array([0.9, 8.4])) == True
        assert box.is_out_of_box(np.array([1.1, 8.4])) == False
        assert box.is_out_of_box(np.array([2.0, 8.4])) == False
        assert box.is_out_of_box(np.array([3.0, 8.4])) == False
        assert box.is_out_of_box(np.array([3.9, 8.4])) == False
        assert box.is_out_of_box(np.array([4.1, 8.4])) == True

        assert box.is_out_of_box(np.array([2.4, 6.9])) == True
        assert box.is_out_of_box(np.array([2.4, 7.1])) == False
        assert box.is_out_of_box(np.array([2.4, 8.0])) == False
        assert box.is_out_of_box(np.array([2.4, 9.0])) == False
        assert box.is_out_of_box(np.array([2.4, 9.9])) == False
        assert box.is_out_of_box(np.array([2.4, 10.1])) == True

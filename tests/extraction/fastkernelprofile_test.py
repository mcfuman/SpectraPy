#!/usr/bin/python3

import struct
import array

import numpy as np
import pytest

from spectrapy.extraction.fastresamplingkernel import FastResamplingKernel
from spectrapy.extraction.resamplingkernel import ResamplingKernel


class TestFastResamplingKernel(object):
    """Test for ResamplingKernel"""

    def test_ProfileShape(self):
        """
        """

        k = ResamplingKernel(2., 1000)
        fk = FastResamplingKernel()

        assert np.allclose(k.profile, fk.profile)

    def test_compute_weights(self):
        k = ResamplingKernel(2., 1000)
        fk = FastResamplingKernel()

        for xpos in np.arange(-1, 1, 0.1):
            for ypos in np.arange(-1, 1, 0.1):
                pos = np.array([xpos, ypos])
                k.position = pos
                fk.compute_weights(pos)

                assert np.allclose(k._weights, fk._weights)

    def test_out_of_circle1(self):
        fk = FastResamplingKernel()
        pos = [2, 3]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)
        value = fk.out_of_circle()

        assert np.allclose(value, 25 - 3 * 4)
        w = fk._weights

        assert np.allclose(w[0][0], 0.0)
        assert np.allclose(w[0][1], 0.0)
        assert np.allclose(w[1][0], 0.0)

        assert np.allclose(w[-1][0], 0.0)
        assert np.allclose(w[-1][1], 0.0)
        assert np.allclose(w[-2][0], 0.0)

        assert np.allclose(w[0][-1], 0.0)
        assert np.allclose(w[0][-2], 0.0)
        assert np.allclose(w[1][-1], 0.0)

        assert np.allclose(w[-1][-1], 0.0)
        assert np.allclose(w[-1][-2], 0.0)
        assert np.allclose(w[-2][-1], 0.0)

    def test_out_of_circle2(self):
        fk = FastResamplingKernel()
        pos = [2.5, 3]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 4)
        fk._weights = np.ones((5, 4), dtype=np.float64)
        value = fk.out_of_circle()

        assert np.allclose(value, 20 - 8)
        w = fk._weights

        assert np.allclose(w[0], 0.0)
        assert np.allclose(w[-1], 0.0)

    def test_out_of_circle3(self):
        fk = FastResamplingKernel()
        pos = [2, 3.5]
        fk.compute_weights(pos)

        assert fk._weights.shape == (4, 5)
        fk._weights = np.ones((4, 5), dtype=np.float64)
        value = fk.out_of_circle()

        assert np.allclose(value, 20 - 8)
        w = fk._weights

        assert np.allclose(w[:, 0], 0.0)
        assert np.allclose(w[:, -1], 0.0)

    def test_out_of_circle4(self):
        fk = FastResamplingKernel()
        pos = [2.5, 3.5]
        fk.compute_weights(pos)

        assert fk._weights.shape == (4, 4)
        fk._weights = np.ones((4, 4), dtype=np.float64)
        value = fk.out_of_circle()

        assert np.allclose(value, 16 - 4)
        w = fk._weights

        assert np.allclose(w[0][0], 0.0)
        assert np.allclose(w[-1][0], 0.0)
        assert np.allclose(w[0][-1], 0.0)
        assert np.allclose(w[-1][-1], 0.0)

    def test_out_of_image1(self):
        fk = FastResamplingKernel()
        pos = [2, 3]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        data = np.zeros((10, 10))

        fk.out_of_image(data)

        # Al contained
        assert np.allclose(fk._weights, np.ones((5, 5)))

    def test_out_of_image2(self):
        fk = FastResamplingKernel()
        pos = [1, 3]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        data = np.zeros((10, 10))

        fk.out_of_image(data)

        # Out on the left
        exp = np.ones((5, 5))
        exp[:, 0] = 0
        assert np.allclose(fk._weights, exp)

    def test_out_of_image3(self):
        fk = FastResamplingKernel()
        pos = [8, 3]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        data = np.zeros((10, 10))

        fk.out_of_image(data)

        # Out on the right
        exp = np.ones((5, 5))
        exp[:, -1] = 0
        assert np.allclose(fk._weights, exp)

    def test_out_of_image4(self):
        fk = FastResamplingKernel()
        pos = [3, 1]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        data = np.zeros((10, 10))

        fk.out_of_image(data)

        # Out on the bottom
        exp = np.ones((5, 5))
        exp[0] = 0
        assert np.allclose(fk._weights, exp)

    def test_out_of_image5(self):
        fk = FastResamplingKernel()
        pos = [3, 8]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        data = np.zeros((10, 10))

        fk.out_of_image(data)

        # Out on the top
        exp = np.ones((5, 5))
        exp[-1] = 0
        assert np.allclose(fk._weights, exp)

    def test_out_of_image6(self):
        fk = FastResamplingKernel()
        pos = [1, 1]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        data = np.zeros((10, 10))

        fk.out_of_image(data)

        # Out on corner
        exp = np.ones((5, 5))
        exp[0] = 0
        exp[:, 0] = 0
        assert np.allclose(fk._weights, exp)

    def test_invalid_mask(self):
        fk = FastResamplingKernel()
        pos = [3, 3]
        fk.compute_weights(pos)

        assert fk._weights.shape == (5, 5)
        fk._weights = np.ones((5, 5), dtype=np.float64)

        mask = np.random.randint(2, size=(7, 7)).astype(bool)
        data = np.ma.array(np.zeros((7, 7)), mask=mask)

        fk.invalid_mask(data)

        assert np.alltrue(~mask[1:-1, 1:-1] == fk._weights.astype(bool))

    def test_compute_value1(self):
        fk = FastResamplingKernel()
        pos = [3.5, 3.5]
        fk.compute_weights(pos)
        allw = fk.out_of_circle()
        data = np.ones((10, 10))

        value, var, rate = fk.compute_value(allw, data)
        assert np.allclose(value, 1.0)
        assert np.isnan(var)
        assert np.allclose(rate, 1.0)

    def test_compute_value2(self):
        fk = FastResamplingKernel()
        f = ResamplingKernel(2., 1000)

        data = np.random.rand(10, 10)
        data = np.ma.array(data, mask=np.zeros((10, 10)).astype(bool))

        for y in np.arange(3.5, 4.5, 0.1):
            for x in np.arange(3.5, 4.5, 0.1):
                pos = np.array([x, y])
                fk.compute_weights(pos)
                allw = fk.out_of_circle()
                value, var, rate = fk.compute_value(allw, data)

                rvalue, rvar, rrate = f.interpolate(pos, data)

                assert np.allclose(value, rvalue)
                assert np.isnan(var)
                assert np.allclose(rate, rrate)

    def test_compute_value3(self):
        fk = FastResamplingKernel()
        f = ResamplingKernel(2., 1000)

        data = np.random.rand(10, 10)
        data = np.ma.array(data, mask=np.zeros((10, 10)).astype(bool))
        variances = np.random.rand(10, 10)
        sigma = np.sqrt(variances)

        for y in np.arange(3.5, 4.5, 0.1):
            for x in np.arange(3.5, 4.5, 0.1):
                pos = np.array([x, y])
                fk.compute_weights(pos)
                allw = fk.out_of_circle()
                value, var, rate = fk.compute_value(allw, data, sigma=sigma)

                rvalue, rvar, rrate = f.interpolate(pos, data, sigma=sigma)

                assert np.allclose(value, rvalue)
                assert np.allclose(var, rvar)
                assert np.allclose(rate, rrate)

    def test_compute_value4(self):
        fk = FastResamplingKernel()
        f = ResamplingKernel(2., 1000)

        data = np.random.rand(10, 10)
        data = np.ma.array(data, mask=np.zeros((10, 10)).astype(bool))
        variances = np.random.rand(10, 10)
        sigma = np.sqrt(variances)

        for y in np.arange(3.5, 4.5, 0.1):
            for x in np.arange(3.5, 4.5, 0.1):
                pos = np.array([x, y])
                fk.compute_weights(pos)
                allw = fk.out_of_circle()
                value, var, rate = fk.compute_value(allw, data, sigma=sigma, cov=True)

                rvalue, rvar, rrate = f.interpolate(pos, data, sigma=sigma, cov=True)

                assert np.allclose(value, rvalue)
                assert np.allclose(var, rvar)
                assert np.allclose(rate, rrate)

    def test_interpolate1(self, mocker):
        fk = FastResamplingKernel()

        mocker.patch.object(fk, 'compute_weights')
        mocker.patch.object(fk, 'out_of_circle', return_value='wall')
        mocker.patch.object(fk, 'out_of_image')
        mocker.patch.object(fk, 'invalid_mask')
        mocker.patch.object(fk, 'compute_value')

        fk.interpolate('pos', 'data', sigma='sigma', cov='cov')

        assert fk.compute_weights.call_args_list[0][0][0] == 'pos'
        assert fk.out_of_image.call_args_list[0][0][0] == 'data'
        assert fk.invalid_mask.call_args_list[0][0][0] == 'data'
        assert fk.compute_value.call_args_list[0][0][0] == 'wall'
        assert fk.compute_value.call_args_list[0][0][1] == 'data'
        assert fk.compute_value.call_args_list[0][1]['sigma'] == 'sigma'
        assert fk.compute_value.call_args_list[0][1]['cov'] == 'cov'






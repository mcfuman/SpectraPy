#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File exponentialfilter_test.py
#
# Created on: May 11, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#


import numpy as np
from astropy.io import fits
from astropy.table import Table
import pytest
from unittest.mock import MagicMock

import spectrapy
from spectrapy.extraction.exponentialfilter import ExponentialFilter


class TestExponentialFilter(object):
    """Test for Exponential Filter"""

    def test_init1(self, mocker):
        """
        """

        engine = ExponentialFilter()

        assert np.allclose(engine._kernel.radius, 2.0)
        assert engine._kernel.pxsample == 1000
        assert engine._wstart is None
        assert engine._wend is None
        assert engine._wstride is None

    def test_init2(self):
        """
        """

        engine = ExponentialFilter()

        assert np.allclose(engine._kernel.radius, 2.0)
        assert engine._kernel.pxsample == 1000
        assert engine._wstart is None
        assert engine._wend is None
        assert engine._wstride is None

    def test_set_extraction_range1(self, mocker):

        engine = ExponentialFilter()

        with pytest.raises(Exception) as invalid_range:
            engine.set_extraction_range(11.0, 10.0, 1.0)

        assert invalid_range.type is ValueError
        assert invalid_range.value.args[0] == "Invalid extracion range"

    def test_set_extraction_range2(self, mocker):

        engine = ExponentialFilter()

        with pytest.raises(Exception) as invalid_range:
            engine.set_extraction_range(1.0, 10.0, -1.0)

        assert invalid_range.type is ValueError
        assert invalid_range.value.args[0] == "Invalid extracion range"

    def test_set_extraction_range3(self, mocker):
        engine = ExponentialFilter()

        engine.set_extraction_range(10.0, 20.0, 2.0)

        assert np.allclose(engine._wstart, 10.0)
        assert np.allclose(engine._wend, 20.0)
        assert np.allclose(engine._wstride, 2.0)

    def test_has_extraction_range1(self, mocker):

        engine = ExponentialFilter()
        engine._wstart = 10.0
        assert engine.has_extraction_range() == False

    def test_has_extraction_range2(self, mocker):

        engine = ExponentialFilter()
        engine._wend = 10.0
        assert engine.has_extraction_range() == False

    def test_has_extraction_range3(self, mocker):

        engine = ExponentialFilter()
        engine._wstride = 10.0
        assert engine.has_extraction_range() == False

    def test_has_extraction_range4(self, mocker):
        engine = ExponentialFilter()
        engine._wstart = 10.0
        engine._wstride = 10.0
        assert engine.has_extraction_range() == False

    def test_has_extraction_range5(self, mocker):

        engine = ExponentialFilter()
        engine._wend = 10.0
        engine._wstride = 10.0
        assert engine.has_extraction_range() == False

    def test_has_extraction_range6(self, mocker):

        engine = ExponentialFilter()
        engine._wend = 10.0
        engine._wstart = 10.0
        assert engine.has_extraction_range() == False

    def test_has_extraction_range7(self, mocker):

        engine = ExponentialFilter()
        engine._wend = 10.0
        engine._wstart = 10.0
        engine._wstride = 10.0

        assert engine.has_extraction_range() == True

    def test_load_image1(self, mocker):
        engine = ExponentialFilter()
        instrument = MagicMock()
        ext_table = MagicMock()
        ext_table.instrument = instrument
        instrument.data_hdu = None
        instrument.var_hdu = None
        instrument.err_hdu = None
        instrument.flag_hdu = None

        image = MagicMock()
        image.data = None
        image.variances = None
        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)

        engine.load_image(ext_table, "mock")

        assert engine._image is image

    def test_load_image2(self, mocker):
        engine = ExponentialFilter()
        instrument = MagicMock()
        ext_table = MagicMock()
        ext_table.instrument = instrument
        instrument.data_hdu = "data_hdu"
        instrument.var_hdu = "var_hdu"
        instrument.err_hdu = "err_hdu"
        instrument.flag_hdu = "flag_hdu"

        image = MagicMock()
        image.data = None
        image.variances = None
        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        #mocker.patch.object(image, 'load_variances')

        engine.load_image(ext_table, "mock")

        assert engine._image is image

        assert image.load_variances.call_args_list[0][0][0] == "mock"
        assert len(image.load_variances.call_args_list[0][0]) == 1
        assert image.load_variances.call_args_list[0][1]["var_hdu"] == "var_hdu"
        assert len(image.load_variances.call_args_list[0][1]) == 1

        assert image.load_variances.call_args_list[1][0][0] == "mock"
        assert len(image.load_variances.call_args_list[1][0]) == 1
        assert image.load_variances.call_args_list[1][1]["err_hdu"] == "err_hdu"
        assert len(image.load_variances.call_args_list[1][1]) == 1

        assert image.load_flags.call_args_list[0][0][0] == "mock"
        assert len(image.load_flags.call_args_list[0][0]) == 1
        assert image.load_flags.call_args_list[0][1]["flag_hdu"] == "flag_hdu"
        assert len(image.load_flags.call_args_list[0][1]) == 1

    def test_extract1(self, mocker):

        engine = ExponentialFilter()
        with pytest.raises(Exception) as no_range:
            engine.extract(None, None, None, None)

        assert no_range.type is AttributeError
        assert no_range.value.args[0] == "No extraction range"

    def test_extract2(self, mocker, tmpdir):
        engine = ExponentialFilter()
        engine.set_extraction_range(10., 20., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', new=('2', ))

        image = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)

        engine.extract(None, "imagefile", objlist=('1', ))

    def test_extract3(self, mocker):

        engine = ExponentialFilter()
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5)
        points2 = np.random.rand(5)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        interp_results[:, 1] = np.nan
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)

        image = MagicMock()
        image.data = None
        image.variances = None
        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=False)

        hdulist = engine.extract(None, "imagefile")
        assert len(hdulist) == 3

        assert np.allclose(hdulist[1].data.ravel(), interp_results.T[0])
        assert np.allclose(hdulist[2].data.ravel(), interp_results.T[2])

    def test_extract4(self, mocker):

        engine = ExponentialFilter()
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5)
        points2 = np.random.rand(5)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)

        image = MagicMock()
        image.data = None
        image.variances = 1

        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=True)

        hdulist = engine.extract(None, "imagefile")
        assert len(hdulist) == 4

        assert np.allclose(hdulist[1].data.ravel(), interp_results.T[0])
        assert np.allclose(hdulist[2].data.ravel(), interp_results.T[1])
        assert np.allclose(hdulist[3].data.ravel(), interp_results.T[2])

    def test_extract5(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5)
        points2 = np.random.rand(5)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        interp_results[:, 1] = np.nan
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)

        image = MagicMock()
        image.data = None
        image.variances = None
        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=False)

        hdulist = engine.extract(None, "imagefile")
        assert len(hdulist) == 3

        assert np.allclose(hdulist[1].data.ravel(), interp_results.T[0])
        assert np.allclose(hdulist[2].data.ravel(), interp_results.T[2])

    def test_extract6(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5)
        points2 = np.random.rand(5)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)

        image = MagicMock()
        image.data = None
        image.variances = 1

        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=True)

        hdulist = engine.extract(None, "imagefile")
        assert len(hdulist) == 4

        assert np.allclose(hdulist[1].data.ravel(), interp_results.T[0])
        assert np.allclose(hdulist[2].data.ravel(), interp_results.T[1])
        assert np.allclose(hdulist[3].data.ravel(), interp_results.T[2])

    def test_extract7(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)

        with pytest.raises(Exception) as no_image:
            engine.extract(ext_table, None)

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_extract8(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5, 2)
        points2 = np.random.rand(5, 2)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)
        shift1 = np.random.rand(2)
        mocker.patch.object(engine, 'compute_trace_adjustment', return_value=shift1)
        mocker.patch.object(engine, '_slit_extraction')

        image = MagicMock()
        image.data = None
        image.variances = 1

        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=True)

        engine.extract(None, "imagefile", xadjust=True)

        assert engine._slit_extraction.call_args_list[0][0][0] == [row1, row2]
        assert np.allclose(engine._slit_extraction.call_args_list[0][1]['shift'], shift1)

    def test_extract9(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5, 2)
        points2 = np.random.rand(5, 2)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)
        shift2 = np.random.rand(2)
        mocker.patch.object(engine, 'compute_lambda_adjustment', return_value=shift2)
        mocker.patch.object(engine, '_slit_extraction')

        image = MagicMock()
        image.data = None
        image.variances = 1

        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=True)

        engine.extract(None, "imagefile", lines=[10000.0, ])

        assert engine._slit_extraction.call_args_list[0][0][0] == [row1, row2]
        assert np.allclose(engine._slit_extraction.call_args_list[0][1]['shift'], shift2)

    def test_extract10(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)

        ext_table = MagicMock()
        mocker.patch('spectrapy.extraction.exponentialfilter.ExtTable.load', return_value=ext_table)
        mocker.patch.object(ext_table, 'keys', return_value=('2', ))
        mocker.patch.object(ext_table, '__contains__', return_value=True)
        row1 = MagicMock()
        row2 = MagicMock()
        points1 = np.random.rand(5, 2)
        points2 = np.random.rand(5, 2)

        mocker.patch.object(row1, 'compute', return_value=points1)
        mocker.patch.object(row2, 'compute', return_value=points2)
        mocker.patch.object(ext_table, '__getitem__', return_value=[row1, row2])

        interp_results = np.random.rand(5 * 2, 3)
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)
        shift1 = np.random.rand(2)
        mocker.patch.object(engine, 'compute_trace_adjustment', return_value=shift1)
        shift2 = np.random.rand(2)
        mocker.patch.object(engine, 'compute_lambda_adjustment', return_value=shift2)
        mocker.patch.object(engine, '_slit_extraction')

        image = MagicMock()
        image.data = None
        image.variances = 1

        mocker.patch('spectrapy.extraction.exponentialfilter.Image.load', return_value=image)
        mocker.patch.object(image, 'has_variances', return_value=True)

        engine.extract(None, "imagefile", xadjust=True, lines=[10000.0, ])

        assert engine._slit_extraction.call_args_list[0][0][0] == [row1, row2]
        assert np.allclose(engine._slit_extraction.call_args_list[0][1]['shift'], shift1 + shift2)


    def test_slit_extraction(self, mocker):

        engine = ExponentialFilter(cython=False)
        engine.set_extraction_range(10., 21., 2.5)
        engine._kernel = MagicMock()

        image = MagicMock()
        image.data = None
        mocker.patch.object(image, 'has_variances', return_value=False)

        exr_row = MagicMock()
        points = np.random.rand(7)
        mocker.patch.object(exr_row, 'compute', return_value=points.copy())
        slit = [exr_row, MagicMock()]
        waves = np.linspace(10, 15, 7)
        row_start = 0
        row_end = 1
        mocker.patch.object(engine, '_image', return_value=(image, ))

        interp_results = np.random.rand(7)
        mocker.patch.object(engine._kernel, 'interpolate', side_effect=interp_results)

        shift = 10*np.random.random()
        engine._slit_extraction(slit, waves, row_start, row_end, shift=shift)

        for i in range(7):
            assert np.isclose(engine._kernel.interpolate.call_args_list[i][0][0], points[i]+shift)


    def test_compute_trace_adjustment1(self, mocker):
        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')

        ext_table = MagicMock()

        with pytest.raises(AttributeError) as no_image:
            engine.compute_trace_adjustment(ext_table, datafile=True, objlist=None)

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_compute_trace_adjustment2(self, mocker):
        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')

        ext_table = MagicMock()

        with pytest.raises(AttributeError) as no_image:
            engine.compute_trace_adjustment(ext_table, datafile=None, objlist=None)

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"


    def test_compute_trace_adjustment3(self, mocker):

        ext_table = MagicMock()
        mocker.patch.object(ext_table, 'keys', return_value=('a', 'b'))

        a_row1= MagicMock()
        a_orient = np.array([1, 0])
        a_center1 = 100*np.random.rand(2)
        a_row1.ORIENTATION = 'ORIENTATION'
        mocker.patch.object(a_row1, '__getitem__', return_value=a_orient)
        a_row1.center=a_center1

        a_row2= MagicMock()
        a_center2 = 100*np.random.rand(2)
        mocker.patch.object(a_row2, '__getitem__', return_value=a_orient)
        a_row2.center=a_center2

        a_slit = [a_row1, a_row2]

        b_row1= MagicMock()
        b_orient = np.array([-1, 0])
        b_center1 = 100*np.random.rand(2)
        b_row1.ORIENTATION = 'ORIENTATION'
        mocker.patch.object(b_row1, '__getitem__', return_value=b_orient)
        b_row1.center = b_center1

        b_row2= MagicMock()
        b_center2 = 100*np.random.rand(2)
        mocker.patch.object(b_row2, '__getitem__', return_value=b_orient)
        b_row2.center = b_center2

        b_slit = [b_row1, b_row2]

        mocker.patch.object(ext_table, '__getitem__', side_effect=[a_slit, b_slit])

        engine = MagicMock()
        engine.image = MagicMock()
        edges = [(100*np.random.rand(2), None),
                 (100*np.random.rand(2), None),
                 (100*np.random.rand(2), None),
                 (100*np.random.rand(2), None)]
        mocker.patch.object(engine, 'compute_slice_edge', side_effect=edges)

        mocker.patch('spectrapy.extraction.exponentialfilter.Sobel', return_value=engine)
        mocker.patch('spectrapy.extraction.exponentialfilter.sigma_clipped_stats', return_value=(3, 0, 0))

        exp_filter = ExponentialFilter(cython=False)

        image = MagicMock()
        exp_filter._image = image

        value = exp_filter.compute_trace_adjustment(ext_table, datafile=None, objlist=['a',])

        assert len(engine.compute_slice_edge.call_args_list) == 2
        shifts = spectrapy.extraction.exponentialfilter.sigma_clipped_stats.call_args_list[0][0][0]
        exp_shifts = [a_orient.dot(edges[0][0])-a_orient.dot(a_center1),
                      a_orient.dot(edges[1][0])-a_orient.dot(a_center2)]
        assert np.allclose(shifts, exp_shifts)
        assert np.allclose(value, 3*a_orient)


    def test_compute_trace_adjustment4(self, mocker):

        ext_table = MagicMock()
        mocker.patch.object(ext_table, 'keys', return_value=('a', 'b'))

        a_row1= MagicMock()
        a_orient = np.array([1, 0])
        a_center1 = 100*np.random.rand(2)
        a_row1.ORIENTATION = 'ORIENTATION'
        mocker.patch.object(a_row1, '__getitem__', return_value=a_orient)
        a_row1.center=a_center1

        a_row2= MagicMock()
        a_center2 = 100*np.random.rand(2)
        mocker.patch.object(a_row2, '__getitem__', return_value=a_orient)
        a_row2.center=a_center2

        a_slit = [a_row1, a_row2]

        b_row1= MagicMock()
        b_center1 = 100*np.random.rand(2)
        b_row1.ORIENTATION = 'ORIENTATION'
        mocker.patch.object(b_row1, '__getitem__', return_value=a_orient)
        b_row1.center = b_center1

        b_row2= MagicMock()
        b_center2 = 100*np.random.rand(2)
        mocker.patch.object(b_row2, '__getitem__', return_value=a_orient)
        b_row2.center = b_center2

        b_slit = [b_row1, b_row2]

        mocker.patch.object(ext_table, '__getitem__', side_effect=[a_slit, b_slit])

        engine = MagicMock()
        engine.image = MagicMock()
        edges = [(100*np.random.rand(2), None),
                 (100*np.random.rand(2), None),
                 (100*np.random.rand(2), None),
                 (100*np.random.rand(2), None)]
        mocker.patch.object(engine, 'compute_slice_edge', side_effect=edges)

        mocker.patch('spectrapy.extraction.exponentialfilter.Sobel', return_value=engine)
        mocker.patch('spectrapy.extraction.exponentialfilter.sigma_clipped_stats', return_value=(3, 0, 0))

        exp_filter = ExponentialFilter(cython=False)

        image = MagicMock()
        exp_filter._image = image

        value = exp_filter.compute_trace_adjustment(ext_table, datafile=None, objlist=None)

        assert len(engine.compute_slice_edge.call_args_list) == 4
        shifts = spectrapy.extraction.exponentialfilter.sigma_clipped_stats.call_args_list[0][0][0]
        exp_shifts = [a_orient.dot(edges[0][0])-a_orient.dot(a_center1),
                      a_orient.dot(edges[1][0])-a_orient.dot(a_center2),
                      a_orient.dot(edges[2][0])-a_orient.dot(b_center1),
                      a_orient.dot(edges[3][0])-a_orient.dot(b_center2),
                      ]
        assert np.allclose(shifts, exp_shifts)
        assert np.allclose(value, 3*a_orient)

    def test_compute_lambda_adjustment1(self, mocker):

        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')

        ext_table = MagicMock()

        with pytest.raises(AttributeError) as no_image:
            engine.compute_lambda_adjustment(ext_table, lines=None, datafile=True, objlist=None)

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_compute_lambda_adjustment2(self, mocker):
        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')

        ext_table = MagicMock()

        with pytest.raises(AttributeError) as no_image:
            engine.compute_lambda_adjustment(ext_table, lines=None, datafile=None, objlist=None)

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_compute_lambda_adjustment3(self, mocker):
        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')

        ext_table = MagicMock()
        engine._image = MagicMock()

        shift = engine.compute_lambda_adjustment(ext_table, lines=Table(), datafile=None, objlist=None)

        assert np.allclose(shift, [0,0])

    def test_compute_lambda_adjustment4(self, mocker):
        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')

        ext_table = MagicMock()
        engine._image = MagicMock()

        mocker.patch('spectrapy.extraction.exponentialfilter.Catalog.load', return_value = Table())

        shift = engine.compute_lambda_adjustment(ext_table, lines=None, datafile=None, objlist=None)

        assert np.allclose(shift, [0,0])


    def test_compute_lambda_adjustment5(self, mocker):
        ext_table = MagicMock()
        mocker.patch.object(ext_table, 'keys', return_value=('a',))

        rows = []
        crvs = []
        idss = []
        centers = []
        det_poss = []
        crvs_computes = []
        idss_computes = []
        rows_computes = []
        for i in range(2):
            rows.append(MagicMock())
            crvs.append(MagicMock())
            idss.append(MagicMock())
            crvs_computes.append([100*np.random.rand(10, 2), None])
            idss_computes.append([100*np.random.rand(10, 2), None])
            rows_computes.append(100*np.random.rand(10, 2))
            centers.append(100*np.random.rand(2))
            det_poss.append(100*np.random.rand(2))
            mocker.patch.object(rows[-1], 'compute', return_value=rows_computes[-1].copy())
            mocker.patch.object(crvs[-1], 'compute', return_value=crvs_computes[-1].copy())
            mocker.patch.object(idss[-1], 'compute', return_value=idss_computes[-1].copy())

            rows[-1].ORIENTATION = 'ORIENTATION'
            rows[-1].DET_POS = 'DET_POS'
            rows[-1].center=centers[-1].copy()
            rows[-1].crv = crvs[-1]
            rows[-1].ids = idss[-1]
            rows[-1].items = {}
            rows[-1].items["DET_POS"] = det_poss[-1].copy()

        a_slit = rows[:2]
        mocker.patch.object(ext_table, '__getitem__', return_value=a_slit)#, a_slit, b_slit])

        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')
        engine._wstart = 1
        engine._wend =2
        engine._wstride = 0.2

        engine._image = MagicMock()
        fluxes = MagicMock()
        mocker.patch.object(engine._image, 'interpolate', return_value=(fluxes, None))
        mocker.patch('spectrapy.extraction.exponentialfilter.sigma_clipped_stats', return_value=[1.0]*3)
        line_pos = np.random.rand(2, 2)
        mocker.patch('spectrapy.extraction.exponentialfilter.WavelengthCalibration.compute_line_positions',
                     return_value = line_pos)
        shift = engine.compute_lambda_adjustment(ext_table, lines=Table(np.zeros((10,3))), datafile=None)

        deltas = []
        for i in range(2):
            deltas.append(rows_computes[i][:, 0] - (det_poss[i] + crvs_computes[i][0])[:, 0])

        assert np.allclose(np.array(deltas).ravel(),
                           spectrapy.extraction.exponentialfilter.sigma_clipped_stats.call_args_list[0][0][0])

        assert np.allclose(idss[0].compute.call_args_list[0][0][0], np.arange(1, 2, 0.2))
        assert np.allclose(idss[1].compute.call_args_list[0][0][0], np.arange(1, 2, 0.2))

        assert np.allclose(crvs[0].compute.call_args_list[0][0][0], idss_computes[0][0])
        assert np.allclose(crvs[0].compute.call_args_list[1][0][0], line_pos[:,1])
        assert np.allclose(crvs[1].compute.call_args_list[0][0][0], idss_computes[1][0])
        assert np.allclose(crvs[1].compute.call_args_list[1][0][0], line_pos[:,1])

        assert np.allclose(rows[0].compute.call_args_list[0][0][0], line_pos[:,0])
        assert np.allclose(rows[1].compute.call_args_list[0][0][0], line_pos[:,0])

    def test_compute_lambda_adjustment6(self, mocker):
        ext_table = MagicMock()
        mocker.patch.object(ext_table, 'keys', return_value=('a', 'b'))

        rows = []
        crvs = []
        idss = []
        centers = []
        det_poss = []
        crvs_computes = []
        idss_computes = []
        rows_computes = []
        for i in range(2):
            rows.append(MagicMock())
            crvs.append(MagicMock())
            idss.append(MagicMock())
            crvs_computes.append([100*np.random.rand(10, 2), None])
            idss_computes.append([100*np.random.rand(10, 2), None])
            rows_computes.append(100*np.random.rand(10, 2))
            centers.append(100*np.random.rand(2))
            det_poss.append(100*np.random.rand(2))
            mocker.patch.object(rows[-1], 'compute', return_value=rows_computes[-1].copy())
            mocker.patch.object(crvs[-1], 'compute', return_value=crvs_computes[-1].copy())
            mocker.patch.object(idss[-1], 'compute', return_value=idss_computes[-1].copy())

            rows[-1].ORIENTATION = 'ORIENTATION'
            rows[-1].DET_POS = 'DET_POS'
            rows[-1].center=centers[-1].copy()
            rows[-1].crv = crvs[-1]
            rows[-1].ids = idss[-1]
            rows[-1].items = {}
            rows[-1].items["DET_POS"] = det_poss[-1].copy()

        a_slit = rows[:2]
        mocker.patch.object(ext_table, '__getitem__', return_value=a_slit)#, a_slit, b_slit])

        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')
        engine._wstart = 1
        engine._wend =2
        engine._wstride = 0.2

        engine._image = MagicMock()
        fluxes = MagicMock()
        mocker.patch.object(engine._image, 'interpolate', return_value=(fluxes, None))
        mocker.patch('spectrapy.extraction.exponentialfilter.sigma_clipped_stats', return_value=[1.0]*3)
        line_pos = np.random.rand(2, 2)
        mocker.patch('spectrapy.extraction.exponentialfilter.WavelengthCalibration.compute_line_positions',
                     return_value = line_pos)
        shift = engine.compute_lambda_adjustment(ext_table, lines=Table(np.zeros((10,3))), datafile=None, objlist=['a'])

        deltas = []
        for i in range(2):
            deltas.append(rows_computes[i][:, 0] - (det_poss[i] + crvs_computes[i][0])[:, 0])

        assert np.allclose(np.array(deltas).ravel(),
                           spectrapy.extraction.exponentialfilter.sigma_clipped_stats.call_args_list[0][0][0])

        assert np.allclose(idss[0].compute.call_args_list[0][0][0], np.arange(1, 2, 0.2))
        assert np.allclose(idss[1].compute.call_args_list[0][0][0], np.arange(1, 2, 0.2))

        assert np.allclose(crvs[0].compute.call_args_list[0][0][0], idss_computes[0][0])
        assert np.allclose(crvs[0].compute.call_args_list[1][0][0], line_pos[:,1])
        assert np.allclose(crvs[1].compute.call_args_list[0][0][0], idss_computes[1][0])
        assert np.allclose(crvs[1].compute.call_args_list[1][0][0], line_pos[:,1])

        assert np.allclose(rows[0].compute.call_args_list[0][0][0], line_pos[:,0])
        assert np.allclose(rows[1].compute.call_args_list[0][0][0], line_pos[:,0])


    def test_compute_lambda_adjustment7(self, mocker):
        ext_table = MagicMock()
        mocker.patch.object(ext_table, 'keys', return_value=('a', 'b'))

        rows = []
        crvs = []
        idss = []
        centers = []
        det_poss = []
        crvs_computes = []
        idss_computes = []
        rows_computes = []
        for i in range(2):
            rows.append(MagicMock())
            crvs.append(MagicMock())
            idss.append(MagicMock())
            crvs_computes.append([100*np.random.rand(10, 2), None])
            idss_computes.append([100*np.random.rand(10, 2), None])
            rows_computes.append(100*np.empty((10,2)))
            rows_computes[-1][:] = np.nan
            centers.append(100*np.random.rand(2))
            det_poss.append(100*np.random.rand(2))
            mocker.patch.object(rows[-1], 'compute', return_value=rows_computes[-1].copy())
            mocker.patch.object(crvs[-1], 'compute', return_value=crvs_computes[-1].copy())
            mocker.patch.object(idss[-1], 'compute', return_value=idss_computes[-1].copy())

            rows[-1].ORIENTATION = 'ORIENTATION'
            rows[-1].DET_POS = 'DET_POS'
            rows[-1].center=centers[-1].copy()
            rows[-1].crv = crvs[-1]
            rows[-1].ids = idss[-1]
            rows[-1].items = {}
            rows[-1].items["DET_POS"] = det_poss[-1].copy()

        a_slit = rows[:2]
        mocker.patch.object(ext_table, '__getitem__', return_value=a_slit)#, a_slit, b_slit])

        engine = ExponentialFilter(cython=False)
        mocker.patch.object(engine, 'load_image')
        engine._wstart = 1
        engine._wend =2
        engine._wstride = 0.2

        engine._image = MagicMock()
        fluxes = MagicMock()
        mocker.patch.object(engine._image, 'interpolate', return_value=(fluxes, None))
        mocker.patch('spectrapy.extraction.exponentialfilter.sigma_clipped_stats', return_value=[1.0]*3)
        line_pos = np.random.rand(2, 2)
        mocker.patch('spectrapy.extraction.exponentialfilter.WavelengthCalibration.compute_line_positions',
                     return_value = line_pos)
        shift = engine.compute_lambda_adjustment(ext_table, lines=Table(np.zeros((10,3))), datafile=None, objlist=['a'])

        assert np.allclose(shift, [0, 0])

        assert len(spectrapy.extraction.exponentialfilter.sigma_clipped_stats.call_args_list) == 0

        assert np.allclose(idss[0].compute.call_args_list[0][0][0], np.arange(1, 2, 0.2))
        assert np.allclose(idss[1].compute.call_args_list[0][0][0], np.arange(1, 2, 0.2))

        assert np.allclose(crvs[0].compute.call_args_list[0][0][0], idss_computes[0][0])
        assert np.allclose(crvs[0].compute.call_args_list[1][0][0], line_pos[:,1])
        assert np.allclose(crvs[1].compute.call_args_list[0][0][0], idss_computes[1][0])
        assert np.allclose(crvs[1].compute.call_args_list[1][0][0], line_pos[:,1])

        assert np.allclose(rows[0].compute.call_args_list[0][0][0], line_pos[:,0])
        assert np.allclose(rows[1].compute.call_args_list[0][0][0], line_pos[:,0])

#!/usr/bin/python3
import os
import struct
import array

import numpy as np
import pytest

from spectrapy.extraction.kernelprofile import KernelProfile


class TestKernelProfile(object):
    """Test for ResamplingKernel"""

    @staticmethod
    def loadKernelData():
        """Loads kernel data from file
        """

        size = struct.calcsize('d')
        path = os.path.dirname(__file__)
        filename = os.path.join(path, 'binprofile')

        # Read len
        with open(filename, 'rb') as f:
            l = 0
            while f.read(size):
                l += 1

        # Create array
        y = array.array('d')
        with open(filename, 'rb') as f:
            y.fromfile(f, l)

        return np.array(y)

    def test_ProfileShape(self):
        """
        """

        k = KernelProfile(2., 1000)

        data = TestKernelProfile.loadKernelData()

        assert len(k._data) == len(data)
        assert np.allclose(k._data, data)

    def test_has_profile1(self):
        k = KernelProfile(0, 0)
        assert k.has_profile() is False

    def test_has_profile2(self):
        k = KernelProfile(1, 1)
        assert k.has_profile()

    def test_profile1(self):
        k = KernelProfile(0, 0)

        with pytest.raises(Exception):
            k.profile

    def test_profile2(self):
        k = KernelProfile(1, 20)
        p = k.profile

        assert np.allclose(p, k._data)

    def test_profile3(self):
        k = KernelProfile(1, 20)
        profile = [1, 2, 3, 4.]
        k.profile = profile

        assert np.allclose(k.profile, profile)

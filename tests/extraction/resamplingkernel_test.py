#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File resamplingkernel_test.py
#
# Created on: May 11, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#


import numpy as np
import pytest

from spectrapy.extraction.resamplingkernel import ResamplingKernel


class TestResamplingKernel(object):
    """Test for ResamplingKernel"""

    def test_init1(self):
        """
        """

        kernel = ResamplingKernel()

        assert np.allclose(kernel.radius, 2)
        assert kernel.pxsample == 1000
        assert kernel._weights is None

    def test_position1(self):
        kernel = ResamplingKernel()
        assert kernel.position is None

    def test_position2(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.4, 3.4])
        assert np.allclose(kernel.position, [2.4, 3.4])

    def test_out_of_circle1(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.4, 3.4])

        expected = np.zeros((4, 4), dtype=bool)
        expected[0][3] = True
        expected[3][0] = True
        expected[3][3] = True

        assert np.alltrue(kernel._out_of_circle() == expected)

    def test_out_of_circle2(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 3.4])

        expected = np.zeros((4, 4), dtype=bool)
        expected[0][0] = True
        expected[3][3] = True
        expected[3][0] = True

        assert np.alltrue(kernel._out_of_circle() == expected)

    def test_out_of_circle3(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.4, 3.6])

        expected = np.zeros((4, 4), dtype=bool)
        expected[0][0] = True
        expected[0][3] = True
        expected[3][3] = True

        assert np.alltrue(kernel._out_of_circle() == expected)

    def test_out_of_circle4(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 3.6])

        expected = np.zeros((4, 4), dtype=bool)
        expected[0][0] = True
        expected[0][3] = True
        expected[3][0] = True

        assert np.alltrue(kernel._out_of_circle() == expected)

    def test_out_of_circle5(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([3.0, 5.0])

        expected = np.zeros((5, 5), dtype=bool)
        expected[0][0] = True
        expected[0][1] = True
        expected[1][0] = True

        expected[0][4] = True
        expected[1][4] = True
        expected[0][3] = True

        expected[4][0] = True
        expected[3][0] = True
        expected[4][1] = True

        expected[4][4] = True
        expected[4][3] = True
        expected[3][4] = True

        assert np.alltrue(kernel._out_of_circle() == expected)

    def test_out_of_image1(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([1.6, 3.6])

        data = np.ones((10, 20))

        expected = np.zeros((4, 4), dtype=bool)
        assert np.alltrue(kernel._out_of_image(data) == expected)

    def test_out_of_image2(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([0.6, 3.6])

        data = np.ones((10, 20))

        expected = np.zeros((4, 4), dtype=bool)
        expected[:, 0] = True
        assert np.alltrue(kernel._out_of_image(data) == expected)

    def test_out_of_image3(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([1.6, 0.6])

        data = np.ones((10, 20))

        expected = np.zeros((4, 4), dtype=bool)
        expected[0] = True

        assert np.alltrue(kernel._out_of_image(data) == expected)

    def test_out_of_image4(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([18.6, 3.6])

        data = np.ones((10, 20))

        expected = np.zeros((4, 4), dtype=bool)
        expected[:, 3] = True
        assert np.alltrue(kernel._out_of_image(data) == expected)

    def test_out_of_image5(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([17.6, 8.6])

        data = np.ones((10, 20))

        expected = np.zeros((4, 4), dtype=bool)
        expected[3] = True

        assert np.alltrue(kernel._out_of_image(data) == expected)

    def test_invalid_mask1(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 4.6])

        mask = np.zeros((10, 20), dtype=bool)
        data = np.ma.array(np.ones((10, 20)), mask=mask)

        expected = np.zeros((4, 4), dtype=bool)

        assert np.alltrue(kernel._invalid_mask(data) == expected)

    def test_invalid_mask2(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 4.6])

        mask = np.zeros((10, 20), dtype=bool)
        mask[3][1] = True
        data = np.ma.array(np.ones((10, 20)), mask=mask)

        expected = np.zeros((4, 4), dtype=bool)
        expected[0][0] = True
        assert np.alltrue(kernel._invalid_mask(data) == expected)

    def test_invalid_mask3(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 4.6])

        mask = np.zeros((10, 20), dtype=bool)
        mask[3][4] = True
        data = np.ma.array(np.ones((10, 20)), mask=mask)

        expected = np.zeros((4, 4), dtype=bool)
        expected[0][3] = True
        assert np.alltrue(kernel._invalid_mask(data) == expected)

    def test_invalid_mask4(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 4.6])

        mask = np.zeros((10, 20), dtype=bool)
        mask[6][4] = True
        data = np.ma.array(np.ones((10, 20)), mask=mask)

        expected = np.zeros((4, 4), dtype=bool)
        expected[3][3] = True
        assert np.alltrue(kernel._invalid_mask(data) == expected)

    def test_invalid_mask5(self):
        kernel = ResamplingKernel()
        kernel.position = np.array([2.6, 4.6])

        mask = np.zeros((10, 20), dtype=bool)
        mask[6][1] = True
        data = np.ma.array(np.ones((10, 20)), mask=mask)

        expected = np.zeros((4, 4), dtype=bool)
        expected[3][0] = True
        assert np.alltrue(kernel._invalid_mask(data) == expected)

    def test_interpolate1(self, mocker):
        kernel = ResamplingKernel()

        mask = np.zeros((10, 20), dtype=bool)
        data = np.ma.array(np.random.rand(10, 20), mask=mask)

        xw = np.arange(1, 5).astype(np.float64)
        yw = np.arange(5, 9).astype(np.float64)

        mocker.patch.object(kernel, 'get_profile_value', side_effect=[xw, yw])
        value, var, rate = kernel.interpolate(np.array([2.6, 3.6]), data)

        weights = np.tensordot(yw, xw, axes=0)
        # Out of circle
        weights[0][0] = 0.0
        weights[0][3] = 0.0
        weights[3][0] = 0.0
        # weights/=weights.sum()

        assert np.allclose(kernel._weights, weights)
        assert var is None
        assert np.allclose(rate, 1.0)
        X, Y = np.meshgrid(np.arange(1, 5), np.arange(2, 6))
        assert np.allclose(value, (data[Y, X] * weights).sum() / weights.sum())

    def test_interpolate2(self, mocker):
        kernel = ResamplingKernel()

        mask = np.zeros((10, 20), dtype=bool)
        data = np.ma.array(np.random.rand(10, 20), mask=mask)

        xw = np.arange(1, 6).astype(np.float64)
        yw = np.arange(6, 11).astype(np.float64)

        mocker.patch.object(kernel, 'get_profile_value', side_effect=[xw, yw])
        value, var, rate = kernel.interpolate(np.array([0.0, 0.0]), data)

        weights = np.tensordot(yw, xw, axes=0)
        # Out of circle
        weights[4][4] = 0.0
        weights[3][4] = 0.0
        weights[4][3] = 0.0
        weights[0][4] = 0.0
        weights[1][4] = 0.0
        weights[0][3] = 0.0
        weights[4][0] = 0.0
        weights[3][0] = 0.0
        weights[4][1] = 0.0
        weights[0][0] = 0.0
        weights[1][0] = 0.0
        weights[0][1] = 0.0

        all_weights = weights.sum()

        # Out of image
        weights[:, 0] = 0.0
        weights[:, 1] = 0.0
        weights[0] = 0.0
        weights[1] = 0.0

        expected_rate = weights.sum() / all_weights

        # weights/=weights.sum()

        assert np.allclose(kernel._weights, weights)
        assert var is None
        assert np.allclose(rate, expected_rate)

        X, Y = np.meshgrid(np.arange(-2, 3), np.arange(-2, 3))
        assert np.allclose(value, (data[Y, X] * weights).sum() / weights.sum())

    def test_interpolate3(self, mocker):
        kernel = ResamplingKernel()

        mask = np.zeros((10, 20), dtype=bool)
        mask[0][0] = True
        mask[0][1] = True

        data = np.ma.array(np.random.rand(10, 20), mask=mask)

        xw = np.arange(1, 6).astype(np.float64)
        yw = np.arange(6, 11).astype(np.float64)

        mocker.patch.object(kernel, 'get_profile_value', side_effect=[xw, yw])
        value, var, rate = kernel.interpolate(np.array([0.0, 0.0]), data)

        weights = np.tensordot(yw, xw, axes=0)
        # Out of circle
        weights[4][4] = 0.0
        weights[3][4] = 0.0
        weights[4][3] = 0.0
        weights[0][4] = 0.0
        weights[1][4] = 0.0
        weights[0][3] = 0.0
        weights[4][0] = 0.0
        weights[3][0] = 0.0
        weights[4][1] = 0.0
        weights[0][0] = 0.0
        weights[1][0] = 0.0
        weights[0][1] = 0.0

        all_weights = weights.sum()

        # Out of image
        weights[:, 0] = 0.0
        weights[:, 1] = 0.0
        weights[0] = 0.0
        weights[1] = 0.0

        # mask
        weights[2][2] = 0.0
        weights[2][3] = 0.0

        expected_rate = weights.sum() / all_weights

        # weights/=weights.sum()

        assert np.allclose(kernel._weights, weights)
        assert var is None
        assert np.allclose(rate, expected_rate)

        X, Y = np.meshgrid(np.arange(-2, 3), np.arange(-2, 3))
        assert np.allclose(value, (data[Y, X] * weights).sum() / weights.sum())

    def test_interpolate4(self, mocker):
        kernel = ResamplingKernel()

        mask = np.zeros((10, 20), dtype=bool)
        mask[0][0] = True
        mask[0][1] = True

        data = np.ma.array(np.random.rand(10, 20), mask=mask)
        variance = np.ma.array(0.1 * np.random.rand(10, 20))

        xw = np.arange(1, 6).astype(np.float64)
        yw = np.arange(6, 11).astype(np.float64)

        mocker.patch.object(kernel, 'get_profile_value', side_effect=[xw, yw])
        value, var, rate = kernel.interpolate(np.array([0.0, 0.0]), data, sigma=np.sqrt(variance))

        weights = np.tensordot(yw, xw, axes=0)
        # Out of circle
        weights[4][4] = 0.0
        weights[3][4] = 0.0
        weights[4][3] = 0.0
        weights[0][4] = 0.0
        weights[1][4] = 0.0
        weights[0][3] = 0.0
        weights[4][0] = 0.0
        weights[3][0] = 0.0
        weights[4][1] = 0.0
        weights[0][0] = 0.0
        weights[1][0] = 0.0
        weights[0][1] = 0.0

        all_weights = weights.sum()

        # Out of image
        weights[:, 0] = 0.0
        weights[:, 1] = 0.0
        weights[0] = 0.0
        weights[1] = 0.0

        # mask
        weights[2][2] = 0.0
        weights[2][3] = 0.0

        expected_rate = weights.sum() / all_weights

        wsum = weights.sum()
        wsum2 = (weights**2).sum()

        assert np.allclose(kernel._weights, weights)
        assert np.allclose(rate, expected_rate)

        X, Y = np.meshgrid(np.arange(-2, 3), np.arange(-2, 3))
        assert np.allclose(value, (data[Y, X] * (weights / wsum)).sum())
        assert np.allclose(var, (variance[Y, X] * (weights * weights / wsum2)).sum())

    def test_interpolate5(self, mocker):
        kernel = ResamplingKernel()

        mask = np.zeros((10, 20), dtype=bool)
        mask[0][0] = True
        mask[0][1] = True

        data = np.ma.array(np.random.rand(10, 20), mask=mask)
        variance = np.ma.array(0.1 * np.random.rand(10, 20))

        xw = np.zeros(5, dtype=np.float64)
        yw = xw

        mocker.patch.object(kernel, 'get_profile_value', side_effect=[xw, yw])
        value, var, rate = kernel.interpolate(np.array([0.0, 0.0]), data, sigma=np.sqrt(variance))

        weights = np.tensordot(xw, yw, axes=0)

        assert np.allclose(kernel._weights, weights)
        assert np.allclose(rate, 0.0)

        assert np.allclose(value, 0.0)
        assert np.allclose(var, np.inf)

    def test_interpolate6(self, mocker):
        kernel = ResamplingKernel()

        mask = np.zeros((10, 20), dtype=bool)
        mask[0][0] = True
        mask[0][1] = True

        data = np.ma.array(np.random.rand(10, 20), mask=mask)
        variance = np.ma.array(0.1 * np.random.rand(10, 20))

        xw = np.arange(1, 6).astype(np.float64)
        yw = np.arange(6, 11).astype(np.float64)

        mocker.patch.object(kernel, 'get_profile_value', side_effect=[xw, yw])
        value, var, rate = kernel.interpolate(np.array([0.0, 0.0]), data, sigma=np.sqrt(variance),
                                              cov=True)

        weights = np.tensordot(yw, xw, axes=0)
        # Out of circle
        weights[4][4] = 0.0
        weights[3][4] = 0.0
        weights[4][3] = 0.0
        weights[0][4] = 0.0
        weights[1][4] = 0.0
        weights[0][3] = 0.0
        weights[4][0] = 0.0
        weights[3][0] = 0.0
        weights[4][1] = 0.0
        weights[0][0] = 0.0
        weights[1][0] = 0.0
        weights[0][1] = 0.0

        all_weights = weights.sum()

        # Out of image
        weights[:, 0] = 0.0
        weights[:, 1] = 0.0
        weights[0] = 0.0
        weights[1] = 0.0

        # mask
        weights[2][2] = 0.0
        weights[2][3] = 0.0

        expected_rate = weights.sum() / all_weights

        wsum = weights.sum()
        wsum2 = (weights**2).sum()

        assert np.allclose(kernel._weights, weights)
        assert np.allclose(rate, expected_rate)

        X, Y = np.meshgrid(np.arange(-2, 3), np.arange(-2, 3))
        assert np.allclose(value, (data[Y, X] * (weights / wsum)).sum())

        cov = (np.sqrt(variance[Y, X]) * weights).sum()
        cov = cov * cov / wsum2
        assert np.allclose(var, cov)

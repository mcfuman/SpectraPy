#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File image_test.py
#
# Created on: Feb 11, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import numpy as np

from spectrapy.dataio.image import Image
from spectrapy.dataio.display import Display
from astropy.io import fits


class TestDisplay(object):
    """Test for Display class"""

    @pytest.fixture(autouse=True)
    def mock_ds9(self, mocker):
        mocker.patch('spectrapy.dataio.display.DS9')
        mocker.patch('spectrapy.dataio.display.Display.set')

    def test_init_1(self, mocker):
        """
        """
        mocker.patch('spectrapy.dataio.display.Display.clear')
        display = Display()
        display.clear.assert_called_once()

        assert display._image is None
        assert display._col_start == 0
        assert display._row_start == 0

    def test_init_2(self, mocker):
        """
        """
        display = Display()

        expected = [mocker.call("frame delete all"), mocker.call("frame new")]
        assert display.set.call_args_list == expected

    def test_load_image1(self, mocker):

        data = np.random.rand(2, 3)
        image = Image(data)

        data_hdu = 'data'

        display = Display()

        mocker.patch('spectrapy.dataio.display.Image.load', return_value=image)
        display.load_image('mock', data_hdu=data_hdu)

        args, kwargs = display._image.load.call_args

        assert args[0] == 'mock'
        assert kwargs['data_hdu'] == 'data'

    def test_load_image2(self, mocker):

        data = np.random.rand(2, 3)
        image = Image(data)

        data_hdu = 'data'
        var_hdu = 'vars'

        display = Display()

        mocker.patch('spectrapy.dataio.display.Image.load', return_value=image)
        display.load_image('mock', data_hdu=data_hdu, var_hdu=var_hdu)

        args, kwargs = display._image.load.call_args

        assert args[0] == 'mock'
        assert kwargs['data_hdu'] == data_hdu
        assert kwargs['var_hdu'] == var_hdu
        assert kwargs['err_hdu'] is None

    def test_load_image3(self, mocker):

        data = np.random.rand(2, 3)
        image = Image(data)

        data_hdu = 'data'
        var_hdu = 'vars'
        err_hdu = 'errs'

        display = Display()

        mocker.patch('spectrapy.dataio.display.Image.load', return_value=image)
        display.load_image('mock', data_hdu=data_hdu, var_hdu=var_hdu, err_hdu=err_hdu)

        args, kwargs = display._image.load.call_args

        assert args[0] == 'mock'
        assert kwargs['data_hdu'] == data_hdu
        assert kwargs['var_hdu'] == var_hdu
        assert kwargs['err_hdu'] == err_hdu

    def test_load_image4(self, mocker):

        data = np.random.rand(2, 3)
        image = Image(data)

        data_hdu = 'data'
        flag_hdu = 'flags'

        display = Display()

        mocker.patch('spectrapy.dataio.display.Image.load', return_value=image)
        display.load_image('mock', data_hdu=data_hdu, flag_hdu=flag_hdu)

        args, kwargs = display._image.load.call_args

        assert args[0] == 'mock'
        assert kwargs['data_hdu'] == data_hdu
        assert kwargs['flag_hdu'] == flag_hdu

    def test_from_region_to_data(self, mocker):
        mock_regions = MagicMock()

        coords = 10 * np.random.rand(10, 2) + 3.
        mocker.patch.object(mock_regions, 'coord_list', new=coords)
        data = Display.region_to_data(mock_regions)

        assert np.allclose(coords - 1, data)

    def test_from_data_to_region(self, mocker):
        data = 2 + np.random.rand(3, 4) * 10
        region = Display.data_to_region(data)

        assert np.allclose(data + 1, region)

    def test_reset_background1(self, mocker):
        data = np.random.rand(2, 3)

        image = Image(data)

        display = Display()

        display ._image = image

        mocker.patch.object(display, 'set_np2arr')
        display.reset_background()

        args, kwargs = display.set_np2arr.call_args

        assert np.allclose(args[0], data)
        assert len(args) == 1
        assert len(kwargs) == 0

        assert display._row_start == 0
        assert display._col_start == 0

    def test_reset_background2(self, mocker):
        data = np.random.rand(2, 3)

        image = Image(data)
        image.datafile = "mock"
        image.data_hdu = 2
        display = Display()

        display._image = image

        display.reset_background()
        args, kwargs = display.set.call_args

        assert args[0] == "fits mock[2]"
        assert len(args) == 1
        assert len(kwargs) == 0

        assert display._row_start == 0
        assert display._col_start == 0

    def test_reset_background3(self, mocker):
        display = Display()

        with pytest.raises(Exception) as no_image:
            display.reset_background()

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "Image not loaded yet!"

    def test_join_frames(self, mocker):

        display = Display()
        display.join_frames()

        expected = [mocker.call("frame 2"), mocker.call("frame delete")]
        assert display.set.call_args_list[-2:] == expected

    def test_split_frames1(self, mocker):
        display = Display()
        display.split_frames()

        expected = [mocker.call("frame new"), mocker.call("tile yes"), mocker.call("tile row")]
        assert display.set.call_args_list[-3:] == expected

    def test_split_frames2(self, mocker):
        display = Display()
        display.split_frames(axis=1)

        expected = [mocker.call("frame new"), mocker.call("tile yes"), mocker.call("tile column")]
        assert display.set.call_args_list[-3:] == expected

    def test_trim_image1(self, mocker):
        display = Display()
        data = np.random.rand(20, 30)

        image = Image(data)
        display._image = image
        mocker.patch.object(display, 'set_np2arr')
        display.trim_image(1, 1, 10, 5)

        args, _ = display.set_np2arr.call_args

        assert np.allclose(args[0], data[1:7, 1:12])

    def test_trim_image2(self, mocker):
        display = Display()
        data = np.random.rand(20, 30)

        image = Image(data)
        display._image = image
        mocker.patch.object(display, 'set_np2arr')
        with pytest.raises(Exception) as out_of_range:
            display.trim_image(1, 1, -2, 2)

        assert out_of_range.type is IndexError
        assert out_of_range.value.args[0] == "Sub image out of range"

    def test_get_sub_image(self, mocker):
        display = Display()
        data = np.random.rand(20, 30)

        image = Image(data)
        display._image = image
        mocker.patch.object(image, 'get_subimage')
        display.get_subimage(1, 2, 3, 4)
        args, kwargs = image.get_subimage.call_args

        assert args[0] == 1
        assert args[1] == 2
        assert args[2] == 3
        assert args[3] == 4

    def test_image_dims(self, mocker):
        display = Display()
        data = np.zeros((20, 30))

        image = Image(data)
        display._image = image

        dims = display.image_dims

        assert dims[0] == 30
        assert dims[1] == 20

    def test_xystart(self, mocker):
        display = Display()
        data = np.random.rand(20, 30)

        image = Image(data)
        display._image = image

        assert display.xstart == 0
        assert display.ystart == 0

        mocker.patch.object(display, 'set_np2arr')
        display.trim_image(1, 2, 10, 5)

        assert display.xstart == 1
        assert display.ystart == 2

    def test_image1(self, mocker):
        display = Display()

        with pytest.raises(Exception) as no_image:
            display.image

        assert no_image.type is AttributeError
        assert no_image.value.args[0] == "No image"

    def test_image2(self, mocker):
        display = Display()

        image = Image(np.random.rand(20, 30))
        display._image = image

        assert display.image is image

    def test_replace_regions1(self, mocker):
        display = Display()

        mocker.patch('os.path.isfile', return_value=False)

        with pytest.raises(Exception) as no_file:
            display.replace_regions("mockfile")

        assert no_file.type is IOError
        assert no_file.value.args[0] == "Invalid region file mockfile"

    def test_replace_regions2(self, mocker):
        display = Display()

        mocker.patch('os.path.isfile', return_value=True)
        mocker.patch.object(display, 'set')
        display.replace_regions("mockfile")

        args, kwargs = display.set.call_args

        assert display.set.call_args_list[0][0][0] == "regions delete all"
        assert display.set.call_args_list[1][0][0] == "regions load mockfile"

    def test_has_image1(self, mocker):
        display = Display()

        assert display.has_image() is False

    def test_has_image2(self, mocker):
        display = Display()

        image = Image(np.random.rand(20, 30))
        display._image = image

        assert display.has_image() is True

    def test_xlen1(self, mocker):
        display = Display()
        image = Image(np.zeros((20, 30)))
        display._image = image

        assert display.xlen == 30

    def test_xlen2(self, mocker):
        display = Display()

        assert display.xlen == 0

    def test_ylen1(self, mocker):
        display = Display()
        image = Image(np.zeros((20, 30)))
        display._image = image

        assert display.ylen == 20

    def test_ylen2(self, mocker):
        display = Display()

        assert display.ylen == 0


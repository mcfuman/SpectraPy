#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File spectra_test.py
#
# Created on: Feb 29, 2020
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest
from unittest.mock import MagicMock

from spectrapy.dataio.spectra import Spectra

from astropy.io import fits
from astropy import units as u
import spectrapy


class TestSpectra(object):
    """Test for Spectra class"""

    def test_init_1(self):
        """
        """

        spec = Spectra()

        assert len(spec.spectra_ids) == 0
        assert len(spec._waves) == 0
        assert len(spec) == 1

    def test_init_2(self):
        """
        """

        hdulist = [fits.PrimaryHDU()]

        spec = Spectra(hdus=hdulist)

        assert len(spec.spectra_ids) == 0
        assert len(spec._waves) == 0
        assert len(spec) == 1

    def test_init_3(self):
        """
        """

        hdulist = [fits.PrimaryHDU(), fits.ImageHDU()]

        spec = Spectra(hdus=hdulist)

        assert len(spec.spectra_ids) == 0
        assert len(spec._waves) == 0
        assert len(spec) == 2

    def test_init_4(self):
        """
        """

        hdulist = [fits.PrimaryHDU(), fits.TableHDU()]

        spec = Spectra(hdus=hdulist)

        assert len(spec.spectra_ids) == 0
        assert len(spec._waves) == 0
        assert len(spec) == 2

    def test_init_5(self):
        """
        """
        image = fits.ImageHDU(data=np.zeros((3, 15)))
        image.header['ETYPE'] = Spectra.DATA + "MOCK"
        hdulist = [fits.PrimaryHDU(), image]

        spec = Spectra(hdus=hdulist)

        assert len(spec.spectra_ids) == 0
        assert len(spec._waves) == 0
        assert len(spec) == 2

    def test_init_6(self):
        """
        """
        image = fits.ImageHDU(data=np.zeros((3, 15)))
        image.header['ETYPE'] = Spectra.DATA
        image.header['SLIT_ID'] = "alpha"
        image.header['CRVAL1'] = 3.14
        image.header['CDELT1'] = 2.7

        hdulist = [fits.PrimaryHDU(), image]

        spec = Spectra(hdus=hdulist)

        assert len(spec.spectra_ids) == 1
        assert len(spec._waves) == 1
        assert spec._spectra_ids[0] == "alpha"
        assert np.allclose(spec._waves["alpha"][0], 3.14)
        assert np.allclose(spec._waves["alpha"][1], 2.7)
        assert spec._waves["alpha"][2] == 15
        assert len(spec._waves["alpha"]) == 3
        assert len(spec) == 2

    def test_init_7(self):
        """
        """
        image = fits.ImageHDU(data=np.zeros((3, 15)))
        image.header['ETYPE'] = Spectra.DATA
        image.header['SLIT_ID'] = "alpha"
        image.header['CRVAL1'] = 3.14
        image.header['CDELT1'] = 2.7

        hdulist = [image, ]

        spec = Spectra(hdus=hdulist)

        assert len(spec.spectra_ids) == 1
        assert len(spec._waves) == 1
        assert spec._spectra_ids[0] == "alpha"
        assert np.allclose(spec._waves["alpha"][0], 3.14)
        assert np.allclose(spec._waves["alpha"][1], 2.7)
        assert spec._waves["alpha"][2] == 15
        assert len(spec._waves["alpha"]) == 3
        assert len(spec) == 2

    def test_open(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU(), ])
        import spectrapy
        mocker.patch('spectrapy.dataio.spectra.fits.open', side_effect=[hdulist, ])
        spec = Spectra.open("mock", mode='test1', memmap='test2', save_backup='test3',
                            cache='test4', lazy_load_hdus='test5', param='test6')
        args, kwargs = spectrapy.dataio.spectra.fits.open.call_args

        assert len(args) == 1
        assert args[0] == "mock"

        assert len(kwargs) == 6
        assert kwargs["mode"] == "test1"
        assert kwargs["memmap"] == "test2"
        assert kwargs["save_backup"] == "test3"
        assert kwargs["cache"] == "test4"
        assert kwargs["lazy_load_hdus"] == "test5"
        assert kwargs["param"] == "test6"

        assert isinstance(spec, Spectra)

    def test_add_dataset1(self, mocker):
        spec = Spectra()
        mocker.patch.object(Spectra, '_set_header')
        dataset = np.empty((3, 5, 12))

        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.full((5, 12), np.nan)
        dataset[2] = np.full((5, 12), np.nan)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "test")

        assert len(Spectra._set_header.call_args_list) == 1
        args = Spectra._set_header.call_args_list[0][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[0])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.DATA

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        assert len(spec) == 2

    def test_add_dataset2(self, mocker):
        spec = Spectra()
        mocker.patch.object(Spectra, '_set_header')
        dataset = np.empty((3, 5, 12))

        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.random.rand(5, 12)
        dataset[2] = np.full((5, 12), np.nan)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "test")

        assert len(Spectra._set_header.call_args_list) == 2
        args = Spectra._set_header.call_args_list[0][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[0])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.DATA

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        args = Spectra._set_header.call_args_list[1][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[1])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.VARIANCE

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        assert len(spec) == 3

    def test_add_dataset3(self, mocker):
        spec = Spectra()
        mocker.patch.object(Spectra, '_set_header')
        dataset = np.empty((3, 5, 12))

        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.random.rand(5, 12)
        dataset[2] = np.random.rand(5, 12)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "test")

        assert len(Spectra._set_header.call_args_list) == 3
        args = Spectra._set_header.call_args_list[0][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[0])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.DATA

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        args = Spectra._set_header.call_args_list[1][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[1])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.VARIANCE

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        args = Spectra._set_header.call_args_list[2][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[2])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.QUALITY

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        assert len(spec) == 4

    def test_add_dataset4(self, mocker):
        spec = Spectra()
        mocker.patch.object(Spectra, '_set_header')
        dataset = np.empty((3, 5, 12))

        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.full((5, 12), np.nan)
        dataset[2] = np.full((5, 12), np.nan)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "test")

        assert len(Spectra._set_header.call_args_list) == 1
        args = Spectra._set_header.call_args_list[0][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[0])
        assert np.allclose(args[1], 12.3)
        assert np.allclose(args[2], 0.7)
        assert args[3] == "123"
        assert args[4] == "test"
        assert args[5] == Spectra.DATA

        assert len(spec._waves) == 1
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert len(spec.spectra_ids) == 1
        assert spec.spectra_ids[0] == "123"

        dataset = np.empty((3, 4, 15))

        dataset[0] = np.random.rand(4, 15)
        dataset[1] = np.full((4, 15), np.nan)
        dataset[2] = np.full((4, 15), np.nan)

        spec.add_dataset(dataset, 45.6, 0.8, "456", "test2")

        assert len(Spectra._set_header.call_args_list) == 2
        args = Spectra._set_header.call_args_list[1][0]

        assert isinstance(args[0], fits.ImageHDU)
        assert np.allclose(args[0].data, dataset[0])
        assert np.allclose(args[1], 45.6)
        assert np.allclose(args[2], 0.8)
        assert args[3] == "456"
        assert args[4] == "test2"
        assert args[5] == Spectra.DATA

        assert len(spec._waves) == 2
        assert np.allclose(spec._waves["123"], (12.3, 0.7, 12))
        assert np.allclose(spec._waves["456"], (45.6, 0.8, 15))

        assert len(spec.spectra_ids) == 2
        assert spec.spectra_ids[0] == "123"
        assert spec.spectra_ids[1] == "456"

        assert len(spec) == 3

    def test_add_dataset5(self, mocker):
        spec = Spectra()
        mocker.patch.object(Spectra, '_set_header')
        dataset = np.empty((3, 5, 12))

        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.full((5, 12), np.nan)
        dataset[2] = np.full((5, 12), np.nan)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "test")

        with pytest.raises(Exception) as already_exists:
            spec.add_dataset(dataset, 12.3, 0.8, "123", "test")

        assert already_exists.type is IndexError
        assert already_exists.value.args[0] == "Dataset 123 already exists!"

        assert len(spec) == 2

    def test_get_wavelengths1(self, mocker):
        spec = Spectra()

        with pytest.raises(Exception) as no_spec:
            spec.get_wavelengths("123")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No spectrum 123!"

    def test_get_wavelengths2(self, mocker):
        spec = Spectra()

        dataset = np.empty((3, 4, 6))

        dataset[0] = np.random.rand(4, 6)
        dataset[1] = np.full((4, 6), np.nan)
        dataset[2] = np.full((4, 6), np.nan)

        spec.add_dataset(dataset, 10.2, 0.7, "456", "test2")

        waves = spec.get_wavelengths("456")

        assert len(waves) == 6
        assert np.allclose(waves, (10.2, 10.9, 11.6, 12.3, 13.0, 13.7))

    def test_set_header(self, mocker):
        image = fits.ImageHDU()

        Spectra._set_header(image, 10.2, 0.7, "123", "SPEC", "DATA")

        assert image.header['SLIT_ID'] == "123"
        assert image.header['ECONTENT'] == "SPEC"
        assert image.header['ETYPE'] == "DATA"
        assert image.header['EXTNAME'] == "123.SPEC.DATA"

        assert image.header['CTYPE1'] == "WAVE"
        assert image.header['CTYPE2'] == "PIXEL"

        assert image.header['CRPIX1'] == 1
        assert image.header['CRPIX2'] == 1

        assert np.allclose(image.header['CDELT1'], 0.7)
        assert np.allclose(image.header['CDELT2'], 1)

        assert np.allclose(image.header['CRVAL1'], 10.2)
        assert np.allclose(image.header['CRVAL2'], 1)

        assert image.header['CUNIT1'] == str(u.Angstrom)
        assert image.header['CUNIT2'] == str(u.pixel)

    def test_get_dataset1(self, mocker):
        spec = Spectra()

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset2(self, mocker):
        spec = Spectra()

        dataset = np.empty((3, 5, 12))
        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.full((5, 12), np.nan)
        dataset[2] = np.full((5, 12), np.nan)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "2DCOUNTS")

        assert len(spec) == 2

        dataset1 = spec.get_dataset("123", "2DCOUNTS")

        assert np.allclose(dataset[0], dataset1[0])
        assert np.alltrue(np.isnan(dataset1[1]))
        assert np.alltrue(np.isnan(dataset1[2]))

    def test_get_dataset3(self, mocker):
        spec = Spectra()

        dataset = np.empty((3, 5, 12))
        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.random.rand(5, 12)
        dataset[2] = np.full((5, 12), np.nan)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "2DCOUNTS")

        assert len(spec) == 3

        dataset1 = spec.get_dataset("123", "2DCOUNTS")

        assert np.allclose(dataset[0], dataset1[0])
        assert np.allclose(dataset[1], dataset1[1])
        assert np.alltrue(np.isnan(dataset1[2]))

    def test_get_dataset4(self, mocker):
        spec = Spectra()

        dataset = np.empty((3, 5, 12))
        dataset[0] = np.random.rand(5, 12)
        dataset[1] = np.random.rand(5, 12)
        dataset[2] = np.random.rand(5, 12)

        spec.add_dataset(dataset, 12.3, 0.7, "123", "2DCOUNTS")

        assert len(spec) == 4

        dataset1 = spec.get_dataset("123", "2DCOUNTS")

        assert np.allclose(dataset[0], dataset1[0])
        assert np.allclose(dataset[1], dataset1[1])
        assert np.allclose(dataset[2], dataset1[2])

    def test_get_dataset5(self, mocker):
        spec = Spectra([fits.TableHDU(), ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset6(self, mocker):
        image = fits.ImageHDU()
        spec = Spectra([image, ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset7(self, mocker):
        image = fits.ImageHDU()
        image.header['SLIT_ID'] = "1234"
        spec = Spectra([image, ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset8(self, mocker):
        image = fits.ImageHDU()
        image.header['SLIT_ID'] = "123"
        spec = Spectra([image, ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset9(self, mocker):
        image = fits.ImageHDU()
        image.header['SLIT_ID'] = "123"
        image.header['ECONTENT'] = "3DCOUNTS"
        spec = Spectra([image, ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset10(self, mocker):
        image = fits.ImageHDU()
        image.header['SLIT_ID'] = "123"
        image.header['ECONTENT'] = "2DCOUNTS"
        spec = Spectra([image, ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_get_dataset11(self, mocker):
        image = fits.ImageHDU()
        image.header['SLIT_ID'] = "123"
        image.header['ECONTENT'] = "2DCOUNTS"
        image.header['ETYPE'] = "DATADATA"

        spec = Spectra([image, ])

        with pytest.raises(Exception) as no_spec:
            spec.get_dataset("123", "2DCOUNTS")

        assert no_spec.type is IndexError
        assert no_spec.value.args[0] == "No 2DCOUNTS for object 123"

    def test_dump_join(self, mocker, tmpdir):
        image1 = fits.ImageHDU()
        image1.header['SLIT_ID'] = "123"
        image1.header['ECONTENT'] = "2DCOUNTS"
        image1.header['ETYPE'] = "DATA"
        image1.header['CRVAL1'] = 1000.
        image1.header['CDELT1'] = 10.

        data1 = np.random.rand(3, 10)
        image1.data = data1.copy()

        image2 = fits.ImageHDU()
        image2.header['SLIT_ID'] = "abc"
        image2.header['ECONTENT'] = "2DCOUNTS"
        image2.header['CRVAL1'] = 1000.
        image2.header['CDELT1'] = 10.

        data2 = np.random.rand(3, 10)
        image2.data = data2.copy()

        image3 = fits.ImageHDU()
        image3.header['SLIT_ID'] = "456"
        image3.header['ECONTENT'] = "2DCOUNTS"
        image3.header['ETYPE'] = "DATA"
        image3.header['CRVAL1'] = 1000.
        image3.header['CDELT1'] = 10.

        data3 = np.random.rand(3, 10)
        image3.data = data3.copy()

        image4 = fits.ImageHDU()
        image4.header['SLIT_ID'] = "def"
        image4.header['ECONTENT'] = "2DCOUNTS"
        image4.header['ETYPE'] = "DDATA"
        image4.header['CRVAL1'] = 1000.
        image4.header['CDELT1'] = 10.

        data4 = np.random.rand(3, 10)
        image4.data = data4.copy()

        spec = Spectra([image1, image2, image3, image4])

        tmpfile = tmpdir.join('tmpname.dat')
        spec.dump_join(tmpfile.basename, overwrite=True)

        joined = fits.open(tmpfile.basename)[1]
        exp_data = np.concatenate([data1, np.full((1, 10), np.nan), data3])
        print(exp_data)

        assert np.allclose(joined.data[0:3], data1)
        assert np.alltrue(np.isnan(joined.data[3]))
        assert np.allclose(joined.data[4:], data3)

        assert joined.header['EXTNAME'] == 'JOINED'


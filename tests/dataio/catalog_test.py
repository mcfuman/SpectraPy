#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File catalog_test.py
#
# Created on: Dec 8, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
from astropy.table import Table
import pytest
from unittest.mock import MagicMock

import spectrapy
from spectrapy.dataio.catalog import Catalog


class TestCatalog(object):
    """Test for Image class"""

    def test_load1(self, mocker):

        catalog = Table(data=([4.0, 1.0, 3.0, 2.0, 1.0, 5.0], ['Ar', 'Ne', 'Xr', 'Hg', 'Hg', 'Xr']),
                        names=['pos', 'label'])

        lines = Catalog.load(catalog)

        assert np.allclose(lines['pos'], [1.0, 2.0, 3.0, 4.0, 5.0])
        assert np.alltrue(lines['label'] == ['Ne', 'Hg', 'Xr', 'Ar', 'Xr'])

    def test_load_catalog2(self, mocker):

        mocker.patch('spectrapy.dataio.catalog.Table.read',
                     side_effect=[Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')]),
                                  Table(data=[(2.0, 1.0, 5.0), ('Hg', 'Hg', 'Xr')])])

        catalog = ["A", "B"]

        lines = Catalog.load(catalog)

        assert np.allclose(lines['pos'], [1.0, 2.0, 3.0, 4.0, 5.0])
        assert np.alltrue(lines['label'] == ['Ne', 'Hg', 'Xr', 'Ar', 'Xr'])

    def test_load_catalog3(self, mocker):

        mocker.patch('spectrapy.dataio.catalog.Table.read',
                     side_effect=[Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')]),
                                  Table(data=[(2.0, 1.0, 5.0), ('Hg', 'Hg', 'Xr')])])

        catalog = ["A", "B"]

        lines = Catalog.load(catalog, wstart=1.8)

        assert np.allclose(lines['pos'], [2.0, 3.0, 4.0, 5.0])
        assert np.alltrue(lines['label'] == ['Hg', 'Xr', 'Ar', 'Xr'])

    def test_load_catalog4(self, mocker):

        mocker.patch('spectrapy.dataio.catalog.Table.read',
                     side_effect=[Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')]),
                                  Table(data=[(2.0, 1.0, 5.0), ('Hg', 'Hg', 'Xr')])])

        catalog = ["A", "B"]

        lines = Catalog.load(catalog, wend=4.2)

        assert np.allclose(lines['pos'], [1.0, 2.0, 3.0, 4.0])
        assert np.alltrue(lines['label'] == ['Ne', 'Hg', 'Xr', 'Ar'])

    def test_load_catalog5(self, mocker):

        mocker.patch('spectrapy.dataio.catalog.Table.read',
                     side_effect=[Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')]),
                                  Table(data=[(2.0, 1.0, 5.0), ('Hg', 'Hg', 'Xr')])])

        catalog = ["A", "B"]

        lines = Catalog.load(catalog, wstart=1.2, wend=4.2)

        assert np.allclose(lines['pos'], [2.0, 3.0, 4.0])
        assert np.alltrue(lines['label'] == ['Hg', 'Xr', 'Ar'])

    def test_load_catalog6(self, mocker):

        mocker.patch('spectrapy.dataio.catalog.Table.read',
                     return_value=Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')]))

        lines = Catalog.load("A", wstart=1.2, wend=4.2)

        assert np.allclose(lines['pos'], [3.0, 4.0])
        assert np.alltrue(lines['label'] == ['Xr', 'Ar'])

    def test_load_catalog7(self, mocker):
        with pytest.raises(Exception) as invalid_catalog:
            Catalog.load(None, wstart=1.2, wend=4.2)

        assert invalid_catalog.type is IOError
        assert invalid_catalog.value.args[0] == "Invalid input catalog"

    def test_load_catalog8(self, mocker):
        catalog = Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')])

        lines = Catalog.load(catalog, wstart=1.2, wend=4.2)

        assert np.allclose(lines['pos'], [3.0, 4.0])
        assert np.alltrue(lines['label'] == ['Xr', 'Ar'])

    def test_load_single_catalog1(self, mocker, tmpdir):
        tmpfile = tmpdir.join('tmpname.dat')
        with open(tmpfile, 'w') as catalog:
            catalog.write("4.0 Ar 1\n1.0 Ne 0\n3.0 Xr 1")

        lines = Catalog.load(os.path.join(tmpfile.dirname, tmpfile.basename))

        assert np.allclose(lines['pos'], [1.0, 3.0, 4.0])
        assert np.alltrue(lines['label'] == ['Ne', 'Xr', 'Ar'])
        assert np.alltrue(lines['flag'] == [0, 1, 1])

    def test_load_single_catalog2(self, mocker, tmpdir):
        tmpfile = tmpdir.join('tmpname.dat')
        with open(tmpfile, 'w') as catalog:
            catalog.write("4.0 Ar\n1.0 Ne\n3.0 Xr")

        lines = Catalog.load(os.path.join(tmpfile.dirname, tmpfile.basename))

        assert np.allclose(lines['pos'], [1.0, 3.0, 4.0])
        assert np.alltrue(lines['label'] == ['Ne', 'Xr', 'Ar'])
        assert np.alltrue(lines['flag'] == [1, 1, 1])

    def test_load_single_catalog3(self, mocker, tmpdir):
        tmpfile = tmpdir.join('tmpname.dat')
        with open(tmpfile, 'w') as catalog:
            catalog.write("4.0\n1.0\n3.0")

        filename = os.path.join(tmpfile.dirname, tmpfile.basename)
        with pytest.raises(Exception) as invalid_catalog:
            Catalog.load(filename)

        assert invalid_catalog.type is IOError
        assert invalid_catalog.value.args[0] == "Invalid input catalog %s" % filename

    def test_set_col_names1(self):
        catalog = Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr')], names=['A', 'B'])

        Catalog.set_col_names(catalog)

        assert catalog.colnames == ['pos', 'label']

    def test_set_col_names2(self):
        catalog = Table(data=[(4.0, 1.0, 3.0), ('Ar', 'Ne', 'Xr'), (1, 0, 1)], names=['A', 'B', 'C'])

        Catalog.set_col_names(catalog)

        assert catalog.colnames == ['pos', 'label', 'flag']

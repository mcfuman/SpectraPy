#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File image_test.py
#
# Created on: Feb 11, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest
from unittest.mock import MagicMock

from spectrapy.dataio.image import Image
from spectrapy.math.interp import BiLinearInterpolator
import spectrapy.dataio.image

from astropy.io import fits


class TestImage(object):
    """Test for Image class"""

    def test_init_1(self):
        """
        """
        data = np.zeros((2,))

        with pytest.raises(Exception) as invalid_data:
            Image(data)

        assert invalid_data.type is TypeError
        assert invalid_data.value.args[0] == "Invalid data shape"

    def test_init_2(self):
        """
        """
        data = np.zeros((2, 2, 2))

        with pytest.raises(Exception) as invalid_data:
            Image(data)

        assert invalid_data.type is TypeError
        assert invalid_data.value.args[0] == "Invalid data shape"

    def test_init_3(self):
        """
        """
        data = np.ones((2, 2))

        image = Image(data)

        assert np.allclose(image.data, data)
        assert np.all(image.start == np.zeros(2, dtype=np.int32))

    def test_init_4(self):
        """
        """
        data = np.random.rand(3, 3)
        flags = (2 * np.random.rand(3, 3)).astype(int)

        image = Image(data, flags=flags)

        assert np.allclose(image.data, data)
        assert np.all(image.data.mask == flags.astype(bool))

    def test_init_5(self):
        """
        """
        data = np.random.rand(3, 3)
        variances = np.random.rand(3, 3)

        image = Image(data, variances=variances)

        assert np.allclose(image.data, data)
        assert np.allclose(image.variances, variances)

    def test_init_6(self):
        """
        """
        data = np.random.rand(3, 3)
        variances = np.random.rand(3, 3)
        flags = (2 * np.random.rand(3, 3)).astype(int)

        image = Image(data, variances=variances, flags=flags)

        assert np.allclose(image.data, data)
        assert np.allclose(image.variances, variances)
        assert np.all(image.data.mask == flags.astype(bool))
        assert np.all(image.variances.mask == flags.astype(bool))

    def test_init_7(self, mocker):

        data = np.random.rand(3, 3)
        primary = fits.hdu.image.PrimaryHDU()
        mocker.patch.object(primary, 'data', new=data)

        image = Image(primary)
        assert np.allclose(image.data, data)

    def test_init_8(self, mocker):

        data = [[1, 1, 2], [3, 2, 1]]

        with pytest.raises(Exception) as invalid_data:
            image = Image(data)

        assert invalid_data.type is TypeError
        assert invalid_data.value.args[0] == "Invalid data type"

    def test_shape(self):
        data = np.zeros((10, 7))

        image = Image(data)

        assert image.shape == data.shape

    def test_header1(self):
        data = np.zeros((10, 7))

        image = Image(data)

        assert image.header is None

    def test_header2(self, mocker):
        data = np.zeros((10, 7))

        primary = fits.hdu.image.PrimaryHDU()
        header = fits.hdu.image.Header()
        #mocker.patch.object(primary, 'header', new=header)
        #mocker.patch.object(primary, 'data', new=data)
        primary.data = data
        primary.header = header

        image = Image(primary)

        assert image.header is not None
        assert len(image.header) == 4
        assert image.header['BITPIX'] == -64
        assert image.header['NAXIS'] == 2
        assert image.header['NAXIS1'] == 7
        assert image.header['NAXIS2'] == 10

    def test_variances1(self, mocker):
        image = Image(np.zeros((2, 2)))

        mocker.patch('spectrapy.dataio.image.Image.is_valid', return_value=False)
        with pytest.raises(Exception) as invalid_variance:
            image.variances = np.zeros((2, 2))

        assert invalid_variance.type is TypeError
        assert invalid_variance.value.args[0] == "Invalid variances"

    def test_variances2(self):
        image = Image(np.zeros((2, 2)))
        var = np.random.rand(2, 2)

        image.variances = var

        assert np.allclose(image.variances, var)
        assert np.all(image.variances.mask == image.data.mask)

    def test_variances3(self):
        image = Image(np.zeros((2, 2)), flags=np.array([[0, 1], [3, 0]]))
        var = np.random.rand(2, 2)

        image.variances = var

        assert np.allclose(image.variances, var)
        assert np.all(image.variances.mask == image.data.mask)

    def test_variances4(self):
        image = Image(np.zeros((2, 2)), flags=np.array([[0, 1], [3, 0]]))

        with pytest.raises(Exception) as no_var:
            image.variances

        assert no_var.type is AttributeError
        assert no_var.value.args[0] == "No variances layer!"

    def test_flags1(self):
        image = Image(np.zeros((2, 2)))

        assert np.all(image.flags == False)
        assert image.flags.shape == image.data.shape
        assert image.flags.dtype == np.bool

    def test_flags2(self):
        flags = np.array([[0, 1], [3, 0]])
        image = Image(np.zeros((2, 2)), flags=flags)

        assert np.all(image.flags == flags.astype(bool))
        assert image.flags.shape == image.data.shape
        assert image.flags.dtype == np.bool

    def test_flags3(self, mocker):
        flags = np.array([[0, 1], [3, 0]])
        image = Image(np.zeros((2, 2)))

        mocker.patch('spectrapy.dataio.image.Image.is_valid', return_value=False)
        with pytest.raises(Exception) as invalid_mask:
            image.flags = np.zeros((2, 2))

        assert invalid_mask.type is TypeError
        assert invalid_mask.value.args[0] == "Invalid flags mask"

    def test_is_valid1(self):
        image = Image(np.zeros((2, 3)))
        assert image.is_valid(None) is False

    def test_is_valid2(self):
        image = Image(np.zeros((2, 3)))
        var = np.random.rand(2,)
        assert image.is_valid(var) is False

    def test_is_valid3(self):
        image = Image(np.zeros((2, 3)))
        var = np.random.rand(2, 2, 3)
        assert image.is_valid(var) is False

    def test_is_valid4(self):
        image = Image(np.zeros((3, 3)))
        var = np.random.rand(2, 3)

        assert image.is_valid(var) is False

    def test_is_valid5(self):
        image = Image(np.zeros((2, 3)))
        var = np.random.rand(2, 3)

        assert image.is_valid(var)

    def test_get_subimage1(self):
        image = Image(np.random.rand(6, 4))

        with pytest.raises(Exception) as invalid_region:
            image.get_subimage(0, 0, 0, 1)

        assert invalid_region.type is IndexError
        assert invalid_region.value.args[0] == "Subimage out of range"

    def test_get_subimage2(self):
        image = Image(np.random.rand(6, 4))

        with pytest.raises(Exception) as invalid_region:
            image.get_subimage(0, 0, 1, 0)

        assert invalid_region.type is IndexError
        assert invalid_region.value.args[0] == "Subimage out of range"

    def test_get_subimage3(self):
        image = Image(np.random.rand(6, 4))

        with pytest.raises(Exception) as invalid_region:
            image.get_subimage(0, 6, 1, 1)

        assert invalid_region.type is IndexError
        assert invalid_region.value.args[0] == "Subimage out of range"

    def test_get_subimage4(self):
        image = Image(np.random.rand(6, 4))

        with pytest.raises(Exception) as invalid_region:
            image.get_subimage(-1, 0, 1, 1)

        assert invalid_region.type is IndexError
        assert invalid_region.value.args[0] == "Subimage out of range"

    def test_get_subimage5(self):
        image = Image(np.random.rand(6, 4))

        with pytest.raises(Exception) as invalid_region:
            image.get_subimage(0, -1, 1, 1)

        assert invalid_region.type is IndexError
        assert invalid_region.value.args[0] == "Subimage out of range"

    def test_get_subimage6(self):
        image = Image(np.random.rand(6, 4))

        with pytest.raises(Exception) as invalid_region:
            image.get_subimage(4, 0, 1, 1)

        assert invalid_region.type is IndexError
        assert invalid_region.value.args[0] == "Subimage out of range"

    def test_get_subimage7(self):
        image = Image(np.random.rand(6, 4))

        sub = image.get_subimage(1, 1, 2, 3)

        assert np.allclose(sub.data, image.data[1:5, 1:4])
        assert np.all(sub.start == [1, 1])

    def test_get_subimage8(self):
        image = Image(np.random.rand(6, 4), variances=np.random.rand(6, 4))

        sub = image.get_subimage(1, 2, 2, 3)

        assert np.allclose(sub.data, image.data[2:6, 1:4])
        assert np.allclose(sub.variances, image.variances[2:6, 1:4])
        assert np.all(sub.start == [1, 2])

    def test_get_subimage9(self):
        flags = (2 * np.random.rand(6, 4)).astype(int)
        image = Image(np.random.rand(6, 4), variances=np.random.rand(6, 4), flags=flags)

        sub = image.get_subimage(1, 1, 2, 3)

        assert np.allclose(sub.data, image.data[1:5, 1:4])
        assert np.allclose(sub.data.mask, image.data[1:5, 1:4].mask)
        assert np.allclose(sub.variances, image.variances[1:5, 1:4])
        assert np.allclose(sub.variances.mask, image.variances[1:5, 1:4].mask)
        assert np.allclose(sub.flags, flags[1:5, 1:4])
        assert np.all(sub.start == [1, 1])

    def test_compute_profile1(self, mocker):
        data = np.random.randn(5, 10)
        var = np.random.randn(5, 10)**2

        image = Image(data, variances=var)

        maxiters = 1
        sigma = 2.0
        axis = 0

        import spectrapy

        mocker.patch('spectrapy.dataio.image.clipped_average', return_value=None)
        image.compute_profile(sigma=sigma, maxiters=maxiters, axis=axis)
        args, kwargs = spectrapy.dataio.image.clipped_average.call_args

        assert np.allclose(args[0].data, data)
        assert np.allclose(kwargs['var'].data, var)
        assert np.allclose(kwargs['sigma'], sigma)
        if 'maxiters' in kwargs:
            assert kwargs['maxiters'] == maxiters
        else:
            assert kwargs['iters'] == maxiters
        assert kwargs['axis'] == axis

    def test_load1(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile', data_hdu=0)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 1
        assert args[0][0][0] == 'mockfile'
        assert args[0][1]['data_hdu'] == 0

    def test_load2(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile', data_hdu=0, var_hdu=1)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 1
        assert args[0][0][0] == 'mockfile'
        assert args[0][1]['data_hdu'] == 0
        assert args[0][1]['var_hdu'] == 1

    def test_load3(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile', data_hdu=0, err_hdu=1)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 1
        assert args[0][0][0] == 'mockfile'
        assert args[0][1]['data_hdu'] == 0
        assert args[0][1]['err_hdu'] == 1

    def test_load4(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile', data_hdu=0, flag_hdu=1)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 1
        assert args[0][0][0] == 'mockfile'
        assert args[0][1]['data_hdu'] == 0
        assert args[0][1]['flag_hdu'] == 1

    def test_load5(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile1', 'mockfile2', data_hdu=0)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list

        assert len(args) == 2
        assert args[0][0][0] == 'mockfile1'
        assert args[0][1]['data_hdu'] == 0
        assert args[1][0][0] == 'mockfile2'
        assert args[1][1]['data_hdu'] == 0

    def test_load6(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile1', 'mockfile2', data_hdu=0, var_hdu=1)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 2
        assert args[0][0][0] == 'mockfile1'
        assert args[0][1]['data_hdu'] == 0
        assert args[0][1]['var_hdu'] == 1
        assert args[1][0][0] == 'mockfile2'
        assert args[1][1]['data_hdu'] == 0
        assert args[1][1]['var_hdu'] == 1

    def test_load7(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile1', 'mockfile2', data_hdu=0, err_hdu=1)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 2
        assert args[0][0][0] == 'mockfile1'
        assert args[0][1]['data_hdu'] == 0
        assert args[0][1]['err_hdu'] == 1
        assert args[1][0][0] == 'mockfile2'
        assert args[1][1]['data_hdu'] == 0
        assert args[1][1]['err_hdu'] == 1

    def test_load8(self, mocker):
        hdulist = fits.HDUList([fits.PrimaryHDU()])

        mocker.patch('spectrapy.dataio.image.Image.load_single_image')

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load('mockfile1', 'mockfile2', data_hdu=0, flag_hdu=1)

        args = spectrapy.dataio.image.Image.load_single_image.call_args_list
        assert len(args) == 2
        assert args[0][0][0] == 'mockfile1'
        assert args[0][1]['data_hdu'] == 0
        assert args[0][1]['flag_hdu'] == 1
        assert args[1][0][0] == 'mockfile2'
        assert args[1][1]['data_hdu'] == 0
        assert args[1][1]['flag_hdu'] == 1

    def test_load_single_image1(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU()])
        hdulist[0].data = np.random.rand(5, 10)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=0)

        assert np.allclose(image.data.data, hdulist[0].data)
        assert np.all(~image.flags)

        assert image.has_variances() is False

        with pytest.raises(Exception) as no_variance:
            image.variances

        assert no_variance.type is AttributeError
        assert no_variance.value.args[0] == "No variances layer!"

    def test_load_single_image2(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU(), fits.ImageHDU()])
        hdulist[0].data = np.random.rand(5, 10)**2
        hdulist[1].data = np.random.rand(5, 10)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=1)
        image.load_variances('mockfile', var_hdu=0)

        assert np.allclose(image.data.data, hdulist[1].data)
        assert image.has_variances() is True
        assert np.allclose(image.variances.data, hdulist[0].data)
        assert np.all(~image.flags)

    def test_load_single_image3(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU(), fits.ImageHDU()])
        hdulist[0].data = np.random.rand(5, 10)
        hdulist[1].data = np.random.rand(5, 10)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=1)
        image.load_variances('mockfile', err_hdu=0)

        assert np.allclose(image.data.data, hdulist[1].data)
        assert np.allclose(image.variances.data, hdulist[0].data**2)
        assert np.all(~image.flags)

    def test_load_single_image4(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU(), fits.ImageHDU(), fits.ImageHDU()])
        hdulist[0].data = np.random.rand(5, 10)
        hdulist[1].data = np.random.rand(5, 10)
        hdulist[2].data = (2 * np.random.rand(5, 10)).astype(int)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=1)
        image.load_variances('mockfile', err_hdu=0)
        image.load_flags('mockfile', flag_hdu=2)

        assert np.allclose(image.data.data, hdulist[1].data)
        assert np.allclose(image.variances.data, hdulist[0].data**2)
        assert np.all(image.flags == hdulist[2].data.astype(bool))

    def test_load_single_image5(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU(), fits.ImageHDU(), fits.ImageHDU()])
        hdulist[0].data = np.random.rand(5, 10)
        hdulist[1].data = np.random.rand(5, 10)
        hdulist[2].data = (2 * np.random.rand(5, 10)).astype(int)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=1, err_hdu=0)

        assert np.allclose(image.data.data, hdulist[1].data)
        assert np.allclose(image.variances.data, hdulist[0].data**2)
        assert np.all(~image.flags)

    def test_load_single_image6(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU(), fits.ImageHDU(), fits.ImageHDU()])
        hdulist[0].data = np.random.rand(5, 10)
        hdulist[1].data = np.random.rand(5, 10)
        hdulist[2].data = (2 * np.random.rand(5, 10)).astype(int)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=1, var_hdu=0)

        assert np.allclose(image.data.data, hdulist[1].data)
        assert np.allclose(image.variances.data, hdulist[0].data)
        assert np.all(~image.flags)

    def test_load_single_image7(self, mocker):

        hdulist = fits.HDUList([fits.PrimaryHDU(), fits.ImageHDU(), fits.ImageHDU()])
        hdulist[0].data = np.random.rand(5, 10)
        hdulist[1].data = np.random.rand(5, 10)
        hdulist[2].data = (2 * np.random.rand(5, 10)).astype(int)

        mocker.patch('spectrapy.dataio.image.fits.open', return_value=hdulist)
        image = Image.load_single_image('mockfile', data_hdu=1, flag_hdu=2)

        assert np.allclose(image.data.data, hdulist[1].data)
        assert np.all(image.flags == hdulist[2].data.astype(bool))

    def test_interpolate1(self):
        image = Image(np.random.rand(6, 4))

        assert image._interpolator is None
        image.interpolate(np.array([1, ]), np.array([2, ]))
        assert isinstance(image._interpolator, BiLinearInterpolator)

    def test_interpolate2(self, mocker):
        image = Image(np.random.rand(6, 4))

        image.interpolate(np.array([1, ]), np.array([2, ]))

        x = np.random.rand(5)
        y = np.random.rand(5)

        mocker.patch.object(image._interpolator, 'compute')

        image.interpolate(x, y)

        args, kwargs = image._interpolator.compute.call_args

        assert args[0] is x
        assert args[1] is y

    def test_interpolate3(self, mocker):
        image = Image(np.random.rand(6, 4))
        image.interpolate(np.array([1, ]), np.array([2, ]))

        x = np.random.rand(5, 2)

        mocker.patch.object(image._interpolator, 'compute')

        image.interpolate(x)

        args, kwargs = image._interpolator.compute.call_args

        assert np.allclose(args[0], x[:, 0])
        assert np.allclose(args[1], x[:, 1])

    def test_iadd1(self):
        data1 = np.random.rand(2, 3)
        image1 = Image(data1)
        data2 = np.random.rand(2, 2)
        image2 = Image(data2)

        with pytest.raises(Exception) as invalid_shapes:
            image1 += image2

        assert invalid_shapes.type is TypeError
        assert invalid_shapes.value.args[0] == "Shapes don't match!"

    def test_iadd2(self):
        data1 = np.random.rand(2, 3)
        var1 = np.random.rand(2, 3)
        flag1 = np.array([[False, False, True], [False, True, False]])
        image1 = Image(data1, variances=var1, flags=flag1)
        data2 = np.random.rand(2, 3)
        var2 = np.random.rand(2, 3)
        flag2 = np.array([[False, True, True], [False, False, False]])
        image2 = Image(data2, variances=var2, flags=flag2)

        image1 += image2

        assert np.allclose(image1._data, data1 + data2)
        assert np.allclose(image1._variances, var1 + var2)
        assert np.allclose(image1._data.mask, [[False, True, True], [False, True, False]])



#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File mask_test.py
#
# Created on: Feb 24, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker

from spectrapy.mask.mask import Mask, MaskSlit
from spectrapy.models.optmodel import OptModel2d


class TestMask(object):
    """Test for Mask class"""

    def test_add_slit1(self):
        mock = {'id': "TEST_ID", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        mask = Mask()

        mask.add_slit(mock)

        assert len(mask._slits) == 1
        assert "TEST_ID" in mask.keys()

    def test_add_slit2(self):
        mock = {'id': "TEST_ID", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        mask = Mask()

        mask.add_slit(mock)

        assert len(mask._slits) == 1
        assert "TEST_ID" in mask.keys()

        with pytest.raises(Exception) as slit_exists:
            mask.add_slit(mock)

        assert slit_exists.type is IndexError
        assert slit_exists.value.args[0] == "Slit TEST_ID already exists! Please edit mask description file"

        assert len(mask._slits) == 1
        assert "TEST_ID" in mask.keys()

    def test_add_slit3(self):
        mock = {'id': "TEST_ID", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        mock2 = mock.copy()
        mock2["id"] = "TEST_ID2"

        mask = Mask()

        mask.add_slit(mock)

        assert len(mask._slits) == 1
        assert "TEST_ID" in mask.keys()

        mask.add_slit(mock2)
        assert len(mask._slits) == 2
        assert "TEST_ID" in mask.keys()
        assert "TEST_ID2" in mask.keys()

    def test_iter(self):
        mock = {'id': "TEST_ID", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        mock2 = {'id': "TEST_ID2", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                 'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                 'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                 'len': 10 * np.random.rand(), 'ref': True}

        mask = Mask()
        mask.add_slit(mock)
        mask.add_slit(mock2)

        i = 0
        for k in mask:
            assert k in ("TEST_ID", "TEST_ID2")
            assert isinstance(mask[k], MaskSlit)
            i += 1

        assert i == 2

    def test__compute_scales1(self):
        # slit length is along X axis
        dimx = 10 * np.random.rand() + 2
        length = 3.2 * dimx
        dimy = np.random.rand()
        width = 4.7 * dimy

        mock = {'id': 123, 'dimx': dimx, 'dimy': dimy, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                'wid': width, 'len': length, 'ref': True}

        mask = Mask()
        mask.add_slit(mock)
        scales = mask.compute_scales()

        assert np.allclose(scales[0], 3.2)
        assert np.allclose(scales[1], 4.7)

    def test__compute_scales2(self):
        # slit length is along Y axis
        dimx = np.random.rand()
        width = 1.7 * dimx
        dimy = 10 * np.random.rand() + 3.
        length = 2.2 * dimy

        mock = {'id': 123, 'dimx': dimx, 'dimy': dimy, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                'wid': width, 'len': length, 'ref': True}

        mask = Mask()
        mask.add_slit(mock)
        scales = mask.compute_scales()

        assert np.allclose(scales[0], 1.7)
        assert np.allclose(scales[1], 2.2)

    def test__compute_scales3(self):
        # slit length is along X axis
        dimx1 = 10 * np.random.rand() + 2
        dimx2 = 10 * np.random.rand() + 2
        dimx3 = 10 * np.random.rand() + 2

        dimy = np.random.rand()

        mock1 = {'id': 1, 'dimx': dimx1, 'dimy': dimy, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                 'wid': 0.0, 'len': 0.0, 'ref': True}
        mock2 = {'id': 2, 'dimx': dimx2, 'dimy': dimy, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                 'wid': 0.0, 'len': 0.0, 'ref': True}
        mock3 = {'id': 3, 'dimx': dimx3, 'dimy': dimy, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                 'wid': 0.0, 'len': 0.0, 'ref': True}

        mask = Mask()
        mask.add_slit(mock1)
        mask.add_slit(mock2)
        mask.add_slit(mock3)

        scales = mask.compute_scales(slit_width=0.8)

        assert np.allclose(scales[0], 0.8 / dimy)
        assert np.allclose(scales[1], 0.8 / dimy)

    def test__compute_scales4(self):
        # slit length is along Y axis
        dimx = np.random.rand()
        dimy1 = 10 * np.random.rand() + 4
        dimy2 = 10 * np.random.rand() + 4
        dimy3 = 10 * np.random.rand() + 4

        mock1 = {'id': 1, 'dimx': dimx, 'dimy': dimy1, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                 'wid': 0.0, 'len': 0.0, 'ref': True}
        mock2 = {'id': 2, 'dimx': dimx, 'dimy': dimy2, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                 'wid': 0.0, 'len': 0.0, 'ref': True}
        mock3 = {'id': 3, 'dimx': dimx, 'dimy': dimy3, 'x': 0.0, 'y': 0.0, 'rot': 0.0,
                 'wid': 0.0, 'len': 0.0, 'ref': True}

        mask = Mask()
        mask.add_slit(mock1)
        mask.add_slit(mock2)
        mask.add_slit(mock3)

        scales = mask.compute_scales(slit_width=0.6)

        assert np.allclose(scales[0], 0.6 / dimx)
        assert np.allclose(scales[1], 0.6 / dimx)

    def test_load1(self, mocker):
        mock = np.array([(1, 10 * np.random.rand(), 10 * np.random.rand(), 10 * np.random.rand(),
                          10 * np.random.rand(), 10 * np.random.rand(), 10 * np.random.rand(),
                          10 * np.random.rand(), False),
                         (2, 10 * np.random.rand(), 10 * np.random.rand(), 10 * np.random.rand(),
                          10 * np.random.rand(), 10 * np.random.rand(), 10 * np.random.rand(),
                          10 * np.random.rand(), True), ], dtype=MaskSlit.dtype)

        mocker.patch('spectrapy.mask.mask.np.loadtxt', return_value=mock)
        mask = Mask.load('mock')

        for key in mask:
            assert key in ("1", "2")

    def test_load2(self, mocker):
        mock = np.array([], dtype=MaskSlit.dtype)

        mocker.patch('spectrapy.mask.mask.np.loadtxt', return_value=mock)

        with pytest.raises(Exception) as invalid_file:
            mask = Mask.load('mock')

        assert invalid_file.type is TypeError
        assert invalid_file.value.args[0] == "No slits in mask file mock"

    def test_opt(self):
        mock = {'id': b"TEST_ID", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        mock2 = {'id': b"TEST_ID2", 'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                 'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                 'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                 'len': 10 * np.random.rand(), 'ref': True}

        mask = Mask()
        mask.add_slit(mock)
        mask.add_slit(mock2)

        assert mask.opt is None
        for k in mask:
            assert mask[k].opt is None

        opt = OptModel2d(2, 2)
        mask.opt = opt

        assert mask.opt is opt
        for k in mask:
            assert mask[k].opt is opt


class TestMaskSlit(object):
    """Test for MaskSlit class"""

    def test__init__(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("A", mock)

        assert np.allclose(slit._center, [mock['x'], mock['y']])
        assert np.allclose(slit._semi_axis, [mock['dimx'] / 2., mock['dimy'] / 2.])
        assert np.allclose(slit._rot, mock['rot'])
        assert np.allclose(slit._wid, mock['wid'])
        assert np.allclose(slit._len, mock['len'])
        assert slit._ref == mock['ref']

    def test_slitid(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert slit.slit_id == "TEST_ID"

    def test_center(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert np.allclose(slit.center, [mock['x'], mock['y']])

    def test_rotation(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert np.allclose(slit.rotation, mock['rot'])

    def test_reference(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert slit.reference == mock['ref']

    def test_semi_axis(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert np.allclose(slit.semi_axis, [mock['dimx'] / 2., mock['dimy'] / 2.])

    def test_width(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert np.allclose(slit.width, mock['wid'])

    def test_length(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 10 * np.random.rand(), 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)

        assert np.allclose(slit.length, mock['len'])

    def test_vertices1(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 0.0, 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'LR'
        opt.xscale = 2.
        opt.yscale = 3
        slit._opt = opt
        vertices = slit.vertices

        assert vertices.shape == (4, 2)

        assert np.allclose(vertices[0], (mock['x'] - mock['dimx'] / 2., mock['y'] + mock['dimy'] / 2.))
        assert np.allclose(vertices[1], (mock['x'] + mock['dimx'] / 2., mock['y'] + mock['dimy'] / 2.))
        assert np.allclose(vertices[2], (mock['x'] + mock['dimx'] / 2., mock['y'] - mock['dimy'] / 2.))
        assert np.allclose(vertices[3], (mock['x'] - mock['dimx'] / 2., mock['y'] - mock['dimy'] / 2.))

    def test_vertices2(self):
        mock = {'dimx': 10 * np.random.rand(), 'dimy': 10 * np.random.rand(),
                'x': 10 * np.random.rand(), 'y': 10 * np.random.rand(),
                'rot': 20.0, 'wid': 10 * np.random.rand(),
                'len': 10 * np.random.rand(), 'ref': True}

        slit = MaskSlit("TEST_ID", mock)
        opt = OptModel2d(1, 1)
        opt.dispersion_direction = 'LR'
        opt.xscale = 2.
        opt.yscale = 3
        slit._opt = opt
        vertices = slit.vertices

        assert vertices.shape == (4, 2)

        shift = np.tan(np.radians(20.)) * mock['dimy'] / 2.

        assert np.allclose(vertices[0], (mock['x'] - mock['dimx'] / 2. + shift, mock['y'] + mock['dimy'] / 2.))
        assert np.allclose(vertices[1], (mock['x'] + mock['dimx'] / 2. + shift, mock['y'] + mock['dimy'] / 2.))
        assert np.allclose(vertices[2], (mock['x'] + mock['dimx'] / 2. - shift, mock['y'] - mock['dimy'] / 2.))
        assert np.allclose(vertices[3], (mock['x'] - mock['dimx'] / 2. - shift, mock['y'] - mock['dimy'] / 2.))

    def test_get_xdisp_edge1(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            pix_slit.xdispersion_edge

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_xdisp_edge2(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)

        for orientation in np.array([[0, 1], [0, -1], [1, 0], [-1, 0]]):
            for sign in np.array([[1, 1], [1, -1], [-1, 1], [-1, -1]]):

                mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
                mocker.patch.object(OptModel2d, 'sign', new=sign)
                semi_axis = np.random.rand(2)
                mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

                pix_slit = MaskSlit("1", mock, opt)

                margins = pix_slit.xdispersion_edge

                assert np.allclose(margins, -1. * orientation * sign * semi_axis)

    def test_get_disp_edge1(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            pix_slit.dispersion_edge

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_disp_edge2(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)

        for orientation in np.array([[0, 1], [0, -1], [1, 0], [-1, 0]]):
            for sign in np.array([[1, 1], [1, -1], [-1, 1], [-1, -1]]):

                mocker.patch.object(OptModel2d, 'dispersion_versor', new=orientation)
                mocker.patch.object(OptModel2d, 'sign', new=sign)
                semi_axis = np.random.rand(2)
                mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

                pix_slit = MaskSlit("1", mock, opt)

                margins = pix_slit.dispersion_edge

                assert np.allclose(margins, -1. * orientation * sign * semi_axis)

    def test_get_pix_left_edge(self):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            margins = pix_slit.get_pix_left_edge()

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_pix_left_edgeLR(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, -1])  # LR dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_left_edge()[0]

        assert np.allclose(margins, [center[0], center[1] + semi_axis[1]])

    def test_get_pix_left_edgeRL(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, 1])  # RL dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_left_edge()[0]

        assert np.allclose(margins, [center[0], center[1] - semi_axis[1]])

    def test_get_pix_left_edgeUD(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([-1, 0])  # UD dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_left_edge()[0]

        assert np.allclose(margins, [center[0] + semi_axis[0], center[1]])

    def test_get_pix_left_edgeDU(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([1, 0])  # DU dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_left_edge()[0]

        assert np.allclose(margins, [center[0] - semi_axis[0], center[1]])

    def test_get_pix_right_edge(self):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            pix_slit.get_pix_right_edge()

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_pix_right_edgeLR(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, -1])  # LR dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_right_edge()[0]

        assert np.allclose(margins, [center[0], center[1] - semi_axis[1]])

    def test_get_pix_right_edgeRL(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, 1])  # RL dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_right_edge()[0]

        assert np.allclose(margins, [center[0], center[1] + semi_axis[1]])

    def test_get_pix_right_edgeUD(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([-1, 0])  # UD dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_right_edge()[0]

        assert np.allclose(margins, [center[0] - semi_axis[0], center[1]])

    def test_get_pix_right_edgeDU(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([1, 0])  # DU dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_right_edge()[0]

        assert np.allclose(margins, [center[0] + semi_axis[0], center[1]])

    def test_get_pix_position(self):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            pix_slit.get_pix_position(0)

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

        with pytest.raises(Exception) as no_opt:
            pix_slit.get_pix_position(1)

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

        with pytest.raises(Exception) as no_opt:
            pix_slit.get_pix_position(0.5)

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_pix_positionLR(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, -1])  # LR dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_position(0)[0]
        assert np.allclose(margins, [center[0], center[1] + semi_axis[1]])

        margins = pix_slit.get_pix_position(1)[0]
        assert np.allclose(margins, [center[0], center[1] - semi_axis[1]])

        margins = pix_slit.get_pix_position(0.5)[0]
        assert np.allclose(margins, [center[0], center[1]])

    def test_get_pix_positionRL(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, 1])  # RL dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_position(0)[0]
        assert np.allclose(margins, [center[0], center[1] - semi_axis[1]])

        margins = pix_slit.get_pix_position(1)[0]
        assert np.allclose(margins, [center[0], center[1] + semi_axis[1]])

        margins = pix_slit.get_pix_position(0.5)[0]
        assert np.allclose(margins, [center[0], center[1]])

    def test_get_pix_positionUD(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([-1, 0])  # UD dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_position(0)[0]
        assert np.allclose(margins, [center[0] + semi_axis[0], center[1]])

        margins = pix_slit.get_pix_position(1)[0]
        assert np.allclose(margins, [center[0] - semi_axis[0], center[1]])

        margins = pix_slit.get_pix_position(0.5)[0]
        assert np.allclose(margins, [center[0], center[1]])

    def test_get_pix_positionDU(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([1, 0])  # DU dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = np.random.rand(2)
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        margins = pix_slit.get_pix_position(0)[0]
        assert np.allclose(margins, [center[0] - semi_axis[0], center[1]])

        margins = pix_slit.get_pix_position(1)[0]
        assert np.allclose(margins, [center[0] + semi_axis[0], center[1]])

        margins = pix_slit.get_pix_position(0.5)[0]
        assert np.allclose(margins, [center[0], center[1]])

    def test_get_pix_center(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)

        px_center = pix_slit.get_pix_center()[0]
        assert np.allclose(center, px_center)

    def test_get_num_rows(self):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            pix_slit.get_num_rows()

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_num_rowsLR(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, -1])  # LR dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        start = pix_slit.get_pix_position(0)[0]
        end = pix_slit.get_pix_position(1)[0]

        assert pix_slit.get_num_rows() == int(round(abs(start[1] - end[1]))) + 1

    def test_get_num_rowsRL(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, 1])  # RL dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        start = pix_slit.get_pix_position(0)[0]
        end = pix_slit.get_pix_position(1)[0]

        assert pix_slit.get_num_rows() == int(round(abs(start[1] - end[1]))) + 1

    def test_get_num_rowsDU(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([1, 0])  # DU dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        start = pix_slit.get_pix_position(0)[0]
        end = pix_slit.get_pix_position(1)[0]

        assert pix_slit.get_num_rows() == int(round(abs(start[0] - end[0]))) + 1

    def test_get_num_rowsUD(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([-1, 0])  # UD dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        start = pix_slit.get_pix_position(0)[0]
        end = pix_slit.get_pix_position(1)[0]

        assert pix_slit.get_num_rows() == int(round(abs(start[0] - end[0]))) + 1

    def test_get_pix_vertices1(self):
        semi_axis = 10 * np.random.rand(2) + 2
        center = 10 * np.random.rand(2) + 100

        mock = {'dimx': semi_axis[0] * 2, 'dimy': semi_axis[1] * 2, 'x': center[0], 'y': center[1],
                'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, opt=None)

        with pytest.raises(Exception) as no_opt:
            pix_slit.get_pix_vertices()

        assert no_opt.type is AttributeError
        assert no_opt.value.args[0] == "Optical Model not set yet!"

    def test_get_pix_vertices2(self):
        semi_axis = 10 * np.random.rand(2) + 2
        center = 10 * np.random.rand(2) + 100

        mock = {'dimx': semi_axis[0] * 2, 'dimy': semi_axis[1] * 2, 'x': center[0], 'y': center[1],
                'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 2.0
        opt.yscale = 3.0

        pix_slit = MaskSlit("1", mock, opt)

        pix_vertices = pix_slit.get_pix_vertices()

        vertices = pix_slit.vertices
        vertices[:, 0] *= 2.
        vertices[:, 1] *= 3.

        assert np.allclose(pix_vertices, vertices)

    def test__get_parameter_valueLR(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, -1])  # LR dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        t = pix_slit._get_parameter_value(center)
        assert np.allclose(t, 0.5)

        t = pix_slit._get_parameter_value(center + np.array([0, semi_axis[1]]))
        assert np.allclose(t, 0.0)

        t = pix_slit._get_parameter_value(center - np.array([0, semi_axis[1]]))
        assert np.allclose(t, 1.0)

    def test__get_parameter_valueRL(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, 1])  # RL dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        t = pix_slit._get_parameter_value(center)
        assert np.allclose(t, 0.5)

        t = pix_slit._get_parameter_value(center - np.array([0, semi_axis[1]]))
        assert np.allclose(t, 0.0)

        t = pix_slit._get_parameter_value(center + np.array([0, semi_axis[1]]))
        assert np.allclose(t, 1.0)

    def test__get_parameter_valueDU(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([1, 0])  # DU dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        t = pix_slit._get_parameter_value(center)
        assert np.allclose(t, 0.5)

        t = pix_slit._get_parameter_value(center - np.array([semi_axis[0], 0]))
        assert np.allclose(t, 0.0)

        t = pix_slit._get_parameter_value(center + np.array([semi_axis[0], 0]))
        assert np.allclose(t, 1.0)

    def test__get_parameter_valueUD(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([-1, 0])  # UD dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = np.random.rand(2)

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        t = pix_slit._get_parameter_value(center)
        assert np.allclose(t, 0.5)

        t = pix_slit._get_parameter_value(center + np.array([semi_axis[0], 0]))
        assert np.allclose(t, 0.0)

        t = pix_slit._get_parameter_value(center - np.array([semi_axis[0], 0]))
        assert np.allclose(t, 1.0)

    def test_deltat(self, mocker):
        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        pix_slit = MaskSlit("1", mock, None)

        nrows = int(10 * np.random.rand() + 2)
        mocker.patch.object(MaskSlit, 'get_num_rows', return_value=nrows)

        assert np.allclose(pix_slit.deltat, 1.0 / (nrows - 1))

    def test_get_slit_pixLR(self, mocker):

        mock = {'dimx': 0, 'dimy': 0, 'x': 0, 'y': 0, 'rot': 0, 'wid': 0, 'len': 0, 'ref': True}

        opt = OptModel2d(1, 1)
        opt.xscale = 1.0
        opt.yscale = 1.0

        orientation = np.array([0, -1])  # LR dispersion
        sign = np.array([1, 1])
        mocker.patch.object(OptModel2d, 'slit_versor', new=orientation)
        mocker.patch.object(OptModel2d, 'sign', new=sign)

        semi_axis = 10 * np.random.rand(2) + 2
        center = 10 * np.random.rand(2) + 50

        pix_slit = MaskSlit("1", mock, opt)
        mocker.patch.object(MaskSlit, 'center', new=center)
        mocker.patch.object(MaskSlit, 'semi_axis', new=semi_axis)

        pixs, _ = pix_slit.get_slit_pix()

        assert np.allclose(pixs[0], pix_slit.get_pix_left_edge()[0])
        assert np.allclose(pixs[-1], pix_slit.get_pix_right_edge()[0])

        assert len(pixs) == pix_slit.get_num_rows()
        assert np.allclose(1.0 / (len(pixs) - 1), pix_slit.deltat)

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File stats_test.py
#
# Created on: Nov 09, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest

import spectrapy

from spectrapy.math.poly import polyfit1d, polyfit2d, polyval2d
from spectrapy.math.stats import clipped_average


class TestClippedAverage(object):

    def test_clipped_average1(self, mocker):
        data = np.random.randn(10)

        maxiters = 1
        sigma = 2.0
        axis = 0

        mocker.patch('spectrapy.math.stats.sigma_clip', return_value=np.ones(10))
        try:
            clipped_average(data, var=None, sigma=sigma, maxiters=maxiters, axis=axis)
        except:
            clipped_average(data, var=None, sigma=sigma, iters=maxiters, axis=axis)

        args, kwargs = spectrapy.math.stats.sigma_clip.call_args

        assert np.allclose(args[0], data)
        assert np.allclose(kwargs['sigma'], sigma)
        if 'iters' in kwargs:
            assert kwargs['iters'] == maxiters
        else:
            assert kwargs['maxiters'] == maxiters
        assert kwargs['axis'] == axis

    def test_clipped_average2(self, mocker):
        data = np.random.rand(10)
        var = 1. + np.random.rand(10)**2

        maxiters = 1
        sigma = 2.0
        axis = 0

        mocker.patch('spectrapy.math.stats.sigma_clip', return_value=np.ones(10))
        try:
            clipped_average(data, var=var, sigma=sigma, maxiters=maxiters, axis=axis)
        except:
            clipped_average(data, var=var, sigma=sigma, iters=maxiters, axis=axis)
        args, kwargs = spectrapy.math.stats.sigma_clip.call_args

        assert np.allclose(args[0], data)
        assert np.allclose(kwargs['sigma'], sigma)
        if 'iters' in kwargs:
            assert kwargs['iters'] == maxiters
        else:
            assert kwargs['maxiters'] == maxiters
        assert kwargs['axis'] == axis

    def test_clipped_average3(self):
        data = np.random.rand(100)
        data[0] = 1000.

        maxiters = 1
        sigma = 2.0
        axis = 0

        try:
            average, variance = clipped_average(data, var=None, sigma=sigma, maxiters=maxiters,
                                                axis=axis)
        except:
            average, variance = clipped_average(data, var=None, sigma=sigma, iters=maxiters, axis=axis)

        assert np.allclose(average, data[1:].mean())
        assert np.allclose(variance, data[1:].var() / (len(data) - 1))

    def test_clipped_average4(self):
        data = np.random.rand(100)
        var = np.random.rand(100)**2
        data[0] = 1000.

        maxiters = 1
        sigma = 2.0
        axis = 0

        try:
            average, variance = clipped_average(data, var=var, sigma=sigma, maxiters=maxiters,
                                                axis=axis)
        except:
            average, variance = clipped_average(data, var=var, sigma=sigma, iters=maxiters, axis=axis)

        w = 1. / var
        assert np.allclose(average, data[1:].dot(w[1:]) / w[1:].sum())
        assert np.allclose(variance, 1 / w[1:].sum())

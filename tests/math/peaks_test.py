#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File peaks_test.py
#
# Created on: Apr 14 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import spectrapy
from spectrapy.math.peaks import _check_variance, find_peak_1d

class TestFindPeak1D(object):

    def test_check_variance1(self):
        assert _check_variance(0.5, 3, np.array([1,2,3])) is False


    def test_check_variance2(self):
        assert _check_variance(0.9, 10, np.array([1,2,3])) is False


    def test_check_variance3(self):
        assert _check_variance(9, 10, np.array([1,2,3])) is False


    def test_check_variance4(self):
        assert _check_variance(7, 10, np.array([1,2,3])) is True


    def test_find_peak_1d1(self):
        data = np.ones(5)
        pos, var = find_peak_1d(data, 6)

        assert pos is None
        assert var is None


    def test_find_peak_1d2(self):
        data = np.ma.array(np.ones(5), mask=[False, False, True, True, False])
        pos, var = find_peak_1d(data, 4)

        assert pos is None
        assert var is None


    def test_find_peak_1d3(self):
        data = np.ma.array(np.ones(5), mask=[False, False, False, True, False])
        pos, var = find_peak_1d(data, 2)

        assert pos is None
        assert var is None


    def test_find_peak_1d4(self):
        data = np.ma.array(np.ones(5), mask=[False, False, False, True, False])
        data[2] = 3
        pos, var = find_peak_1d(data, 2)

        assert pos is None
        assert var is None


    def test_find_peak_1d5(self, mocker):
        data = np.array([0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5])
        mocker.patch('spectrapy.math.peaks._check_variance', return_value = True)

        pos, var = find_peak_1d(data, 2)

        assert np.allclose(pos, 6.0)
        assert var is None


    def test_find_peak_1d6(self, mocker):
        data = np.array([0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5])
        mocker.patch('spectrapy.math.peaks._check_variance', return_value = False)

        pos, var = find_peak_1d(data, 2)

        assert pos is None
        assert var is None


    def test_find_peak_1d7(self, mocker):
        data = np.array([0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5])
        mocker.patch('spectrapy.math.peaks._check_variance', return_value = True)

        pos1, var1 = find_peak_1d(data, 2)
        pos2, var2 = find_peak_1d(data, 2, 0.1*np.ones(len(data)))
        pos3, var3 = find_peak_1d(data, 2, np.ones(len(data)))
        pos4, var4 = find_peak_1d(data, 2, 10*np.ones(len(data)))

        assert np.allclose(pos1, pos2)
        assert np.allclose(pos1, pos3)
        assert np.allclose(pos1, pos4)

        assert var1 is None
        assert np.allclose(var2/var3, 0.1)
        assert np.allclose(var3/var4, 0.1)


    def test_find_peak_1d8(self, mocker):
        data = np.array([0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5, 0.0,
                         0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5])
        mocker.patch('spectrapy.math.peaks._check_variance', return_value = True)

        pos1, var1 = find_peak_1d(data, 2)
        assert np.allclose(pos1, 13.0)

        variance = np.ones(len(data))
        variance[0:13]*=0.1
        variance[13:]*=100.0

        pos2, var2 = find_peak_1d(data, 2, variance)
        assert 5.5 < pos2 < 6.5


    def test_find_peak_1d9(self, mocker):
        data = np.array([0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5, 0.0,
                         0.5, 0.8, 1.,2.,4.,8.,12., 8., 4.,2.,1., 0.8, 0.5])
        mocker.patch('spectrapy.math.peaks._check_variance', return_value = True)

        pos1, var1 = find_peak_1d(data, 2)
        assert np.allclose(pos1, 13.0)

        variance = np.ones(len(data))
        variance[0:14]*=100.0
        variance[14:]*=0.1

        pos2, var2 = find_peak_1d(data, 2, variance)
        assert 19.5 < pos2 < 20.5

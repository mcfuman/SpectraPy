#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File poly_test.py
#
# Created on: Nov 09, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest

import spectrapy

from spectrapy.math.poly import polyfit1d, polyfit2d, polyval2d


class TestPolyFit(object):
    """Test for ResamplingKernel"""

    def test_polyfit1d_1(self):
        """Test polynomial fit 1d, no variance
        """

        x = np.array([-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.0])
        y = np.array([-1, 3, 2.5, 5, 4, 3.5, 2, 4.7, 5, 3.9, 4.0])

        deg = 2
        p, cov = np.polyfit(x, y, deg, full=False, cov=True)

        tmp = polyfit1d(x, y, deg, cov=True)
        p1 = tmp[0][::-1]
        cov1 = tmp[-1][::-1, ::-1]

        assert np.allclose(p, p1)
        assert np.allclose(cov, cov1)

    def test_polyfit1d_2(self):
        """Test polynomial fit 1d, with variance
        """

        x = np.array([-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.0])
        y = np.array([-1, 3, 2.5, 5, 4, 3.5, 2, 4.7, 5, 3.9, 4.0])
        sigma2 = y * y

        deg = 2
        p, cov = np.polyfit(x, y, deg, w=1. / np.sqrt(sigma2), full=False, cov=True)

        tmp = polyfit1d(x, y, deg, var=sigma2, cov=True)
        p1 = tmp[0][::-1]
        cov1 = tmp[-1][::-1, ::-1]

        assert np.allclose(p, p1)
        assert np.allclose(cov, cov1)

    def test_polyfit1d_3(self):
        """Test polynomial fit 1d, with variance
        """

        x = np.array([-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9.0])
        y = np.array([-1, 3, 2.5, 5, 4, 3.5, 2, 4.7, 5, 3.9, 4.0])
        sigma2 = y * y

        deg = 2
        p, residuals, rank, singular_values, rcond = np.polyfit(x, y, deg, w=1. / np.sqrt(sigma2), full=True, cov=False)

        tmp = polyfit1d(x, y, deg, var=sigma2, cov=False)
        p1 = tmp[0][::-1]

        assert np.allclose(p, p1)
        assert tmp[1] * (len(x) - deg - 1) == pytest.approx(residuals[0])

    def test_polyfit2d_1(self):
        with pytest.raises(Exception) as invalid_degs:
            polyfit2d(None, None, None, (1, 2, 3))

        assert invalid_degs.type is TypeError
        assert invalid_degs.value.args[0] == "Invalid degs"

    def test_polyfit2d_2(self):
        with pytest.raises(Exception) as invalid_degs:
            polyfit2d(None, None, None, (2, ))

        assert invalid_degs.type is TypeError
        assert invalid_degs.value.args[0] == "Invalid degs"

    def test_polyfit2d_3(self):
        """
        """

        model = np.array([[1., -0.2, 0.4], [-0.3, 1.7, 2.1]])

        x = np.linspace(-1., 2., 10)
        y = np.linspace(-2., 1., 7)

        x, y = np.meshgrid(x, y)
        x = x.ravel()
        y = y.ravel()

        z = np.polynomial.polynomial.polyval2d(x, y, model)

        tmp = polyfit2d(x, y, z, (1, 2), cov=True)
        p = tmp[0]
        cov = tmp[-1]

        assert np.allclose(p, model)
        assert np.allclose(cov, np.zeros((6, 6)))

    def test_polyfit2d_4(self):
        """
        """

        model = np.array([[1., -0.2, 0.4], [-0.3, 1.7, 2.1]])

        x = np.linspace(-1., 2., 10)
        y = np.linspace(-2., 1., 7)

        x, y = np.meshgrid(x, y)
        x = x.ravel()
        y = y.ravel()

        z = np.polynomial.polynomial.polyval2d(x, y, model)
        err = np.random.random(z.shape) / 100
        z += err

        tmp = polyfit2d(x, y, z, (1, 2), cov=True)

        p = tmp[0]
        assert np.allclose(p, model, atol=1. / 100.)
        cov = tmp[-1]
        assert np.allclose(cov, np.zeros((6, 6)), atol=1. / 10000.)

    def test_polyfit2d_5(self):
        """
        """

        model = np.array([[1., -0.2, 0.4], [-0.3, 1.7, 2.1]])

        x = np.linspace(-1., 2., 10)
        y = np.linspace(-2., 1., 7)

        x, y = np.meshgrid(x, y)
        x = x.ravel()
        y = y.ravel()
        w = y**2
        w[w <= 0] = 1.0
        z = np.polynomial.polynomial.polyval2d(x, y, model)

        tmp = polyfit2d(x, y, z, (1, 2), var=w, cov=True)
        p = tmp[0]
        cov = tmp[-1]

        assert np.allclose(p, model)
        assert np.allclose(cov, np.zeros((6, 6)))

    def test_polyfit2d_6(self):
        """
        """

        model = np.array([[1., -0.2, 0.4], [-0.3, 1.7, 2.1]])

        x = np.linspace(-1., 2., 10)
        y = np.linspace(-2., 1., 7)

        x, y = np.meshgrid(x, y)
        x = x.ravel()
        y = y.ravel()
        err = np.random.random(x.shape) / 100
        z = np.polynomial.polynomial.polyval2d(x, y, model)
        z += err
        tmp = polyfit2d(x, y, z, (1, 2), var=err, cov=True)
        p = tmp[0]
        cov = tmp[-1]
        assert np.allclose(p, model, atol=1. / 100.)
        assert np.allclose(cov, np.ones((6, 6)) / 10000., atol=1.)

    def test_polyval2d_1(self):

        model = np.random.rand(3, 2)
        x, y = np.random.rand(2)

        z = np.polynomial.polynomial.polyval2d(x, y, model)
        z2 = polyval2d((x, y), model)

        assert np.allclose(z, z2[0])

    def test_polyval2d_2(self):
        model = np.random.rand(3, 3)
        x, y = np.random.rand(2)
        cov = np.random.rand(9, 9)

        J = np.array([1.0, y, y**2, x, x * y, x * y * y, x * x, x * x * y, x * x * y * y])

        z = np.polynomial.polynomial.polyval2d(x, y, model)
        zv = J.dot(cov.dot(J))
        z2 = polyval2d((x, y), model, cov=cov)

        assert np.allclose(z, z2[0])
        assert np.allclose(zv, z2[1])




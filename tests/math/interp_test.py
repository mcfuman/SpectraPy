#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File interp_test.py
#
# Created on: Nov 09, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest

from spectrapy.math.interp import BiLinearInterpolator

class TestBiLinearInterpolator(object):

    def test_get_int1(self):

        data = np.array([1.0, 2.1, 3.5, 4.7])
        idata = BiLinearInterpolator._get_int(data)

        assert np.alltrue(idata == np.array([1, 2, 3, 4]))
        assert idata.dtype is np.dtype(np.int64)


    def test_get_int2(self):

        data = 2.1
        idata = BiLinearInterpolator._get_int(data)

        assert idata[0] == 2
        assert idata.dtype is np.dtype(np.int64)


    def test_call1(self):

        data = np.zeros((3,2))

        interpolator = BiLinearInterpolator(data)

        assert interpolator._xsize == 2
        assert interpolator._ysize == 3


    def test_call2(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata, _ = interpolator(np.array([1,2,3]),np.array([1,1,1]))

        assert np.allclose(idata, data[1][1:4])


    def test_call3(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata, _ = interpolator(np.array([1,2,3]),np.array([0,0,0]))

        assert np.allclose(idata, data[0][1:4])


    def test_call4(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata, _ = interpolator(np.array([1,2,3]),np.array([2,2,2]))

        assert np.all(np.isnan(idata))


    def test_call5(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata, _ = interpolator(np.array([0,1,2,3,4]),np.array([1,1,1,1,1]))

        assert np.allclose(idata[0:4], data[1][0:4])
        assert np.isnan(idata[4])


    def test_call6(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata, _ = interpolator(np.array([-1,0,1,2,3,4]),np.array([1,1,1,1,1,1]))

        assert np.allclose(idata[1:5], data[1][0:4])
        assert np.isnan(idata[5])
        assert np.isnan(idata[0])


    def test_call7(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))

        Q0 = [[data[0,1],data[1,1]],[data[0,2],data[1,2]]]
        e0=np.dot([0.8, 0.2], np.dot(Q0, [0.4, 0.6]))
        Q1 = [[data[1,2],data[2,2]],[data[1,3],data[2,3]]]
        e1=np.dot([0.4, 0.6], np.dot(Q1, [0.5, 0.5]))

        assert np.allclose(idata, [e0,e1])


    def test_call8(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata1, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))

        interpolator = BiLinearInterpolator(data, 0.2*np.ones((3, 5)))
        idata2, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(idata1, idata2)

        interpolator = BiLinearInterpolator(data, 2*np.ones((3, 5)))
        idata2, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(idata1, idata2)

        interpolator = BiLinearInterpolator(data, 23.45*np.ones((3, 5)))
        idata2, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(idata1, idata2)


    def test_call9(self):

        data = np.random.rand(3,5)

        interpolator = BiLinearInterpolator(data)
        idata1, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))

        interpolator = BiLinearInterpolator(data, 0.2*np.ones((3, 5)))
        idata2, var2 = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        expected = [(0.8*0.4)**2 + (0.8*0.6)**2 + (0.2*0.4)**2 + (0.2*0.6)**2,
                    2*(0.4*0.5)**2 + 2*(0.6*0.5)**2]
        assert np.allclose(expected, var2)

        interpolator = BiLinearInterpolator(data, 2*np.ones((3, 5)))
        idata2, var2 = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(expected, var2)

        interpolator = BiLinearInterpolator(data, 23.45*np.ones((3, 5)))
        idata2, var2 = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(expected, var2)


    def test_call10(self):

        data = 3.4*np.ones((3,5))

        interpolator = BiLinearInterpolator(data)
        idata1, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))

        assert np.allclose(idata1, [3.4, 3.4])

        interpolator = BiLinearInterpolator(data, 0.2*np.ones((3, 5)))
        idata2, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(idata2, [3.4, 3.4])

        interpolator = BiLinearInterpolator(data, 2*np.ones((3, 5)))
        idata2, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(idata2, [3.4, 3.4])

        interpolator = BiLinearInterpolator(data, 23.45*np.ones((3, 5)))
        idata2, _ = interpolator(np.array([1.2,2.6]),np.array([0.6,1.5]))
        assert np.allclose(idata2, [3.4, 3.4])


    def test_call11(self):

        data = np.arange(15)
        data.shape = (3, 5)

        interpolator = BiLinearInterpolator(data)
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert np.allclose(idata1, [4.0,])

        interpolator = BiLinearInterpolator(data, 0.2*np.ones((3, 5)))
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert np.allclose(idata1, [4.0,])

        interpolator = BiLinearInterpolator(data, 2*np.ones((3, 5)))
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert np.allclose(idata1, [4.0,])

        interpolator = BiLinearInterpolator(data, 23.45*np.ones((3, 5)))
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert np.allclose(idata1, [4.0,])


    def test_call12(self):

        data = np.arange(15)
        data.shape = (3, 5)

        variance = 100*np.ones((3, 5))
        variance[0][1]=0.1
        interpolator = BiLinearInterpolator(data, variance)
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert 0.9 < idata1[0] < 1.1

        variance = 100*np.ones((3, 5))
        variance[0][2]=0.1
        interpolator = BiLinearInterpolator(data, variance)
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert 1.9 < idata1[0] < 2.1

        variance = 100*np.ones((3, 5))
        variance[1][1]=0.1
        interpolator = BiLinearInterpolator(data, variance)
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert 5.9 < idata1[0] < 6.1

        variance = 100*np.ones((3, 5))
        variance[1][2]=0.1
        interpolator = BiLinearInterpolator(data, variance)
        idata1, _ = interpolator(np.array([1.5,]),np.array([0.5,]))
        assert 6.9 < idata1[0] < 7.1

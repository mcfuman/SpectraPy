#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File wavelengthcalib_test.py
#
# Created on: Mar 29, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
from astropy.table import Table
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import spectrapy
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d
from spectrapy.models.crvmodel import CrvModel2d, CrvModel1d
from spectrapy.models.idsmodel import IdsModel2d, IdsModel1d
from spectrapy.models.model import Model2d

from spectrapy.modelscalib.idscalib import IdsModelCalibration
from spectrapy.mask.mask import Mask
from spectrapy.datacalib.wavelengthcalib import WavelengthCalibration


class TestTraceCalibration(object):

    def get_calib(self, mocker, instrument=None, mask=None, opt=None, crv=None, ids=None):
        if instrument is None:
            instrument = Instrument()
        if mask is None:
            mask = Mask()
        if opt is None:
            opt = OptModel2d(1, 1)
        if crv is None:
            crv = CrvModel2d(1, 1, 1)
        if ids is None:
            ids = IdsModel2d(1, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = WavelengthCalibration(instrument, mask, opt, crv, ids)
        calib._ds9 = MagicMock()
        return calib

    def test_init1(self, mocker):

        calib = self.get_calib(mocker)

        assert calib._wstart is None
        assert calib._wend is None

    def test_set_lambda_range1(self, mocker):

        calib = self.get_calib(mocker)

        with pytest.raises(Exception) as invalid_range:
            calib.set_lambda_range(10, 9)

        assert invalid_range.type is ValueError
        assert invalid_range.value.args[0] == "Invalid lambda range: (10, 9)"

    def test_set_lambda_range2(self, mocker):

        calib = self.get_calib(mocker)

        assert calib._wstart is None
        assert calib._wend is None

        calib.set_lambda_range(9, 10)

        assert np.allclose(calib._wstart, 9)
        assert np.allclose(calib._wend, 10)

    def test_set_lambda_range3(self, mocker):

        calib = self.get_calib(mocker)

        with pytest.raises(Exception) as invalid_range:
            calib.set_lambda_range(-9, 10)
        assert invalid_range.type is ValueError
        assert invalid_range.value.args[0] == "Invalid lambda range: blue and red must be positive"

    def test_set_lambda_range4(self, mocker):

        calib = self.get_calib(mocker)

        with pytest.raises(Exception) as invalid_range:
            calib.set_lambda_range(-9, -1)
        assert invalid_range.type is ValueError
        assert invalid_range.value.args[0] == "Invalid lambda range: blue and red must be positive"

    def test_get_waves_range(self, mocker):

        calib = self.get_calib(mocker)
        calib.set_lambda_range(1000, 2000)
        calib._ids.model[0] = 0.1 * np.random.rand(2, 2)

        assert np.allclose(calib._get_waves_range(), np.arange(1000, 2000, 1. / calib._ids.model[0][0][0]))

    def test_new_ids_model(self, mocker):

        calib = self.get_calib(mocker)

        with pytest.raises(Exception) as no_generate:
            calib.new_ids_model(1, 1, 1)

        assert no_generate.type is NotImplementedError

    def test_display_slit(self, mocker):
        exttable = MagicMock()
        table_row1 = MagicMock()
        table_row2 = MagicMock()

        mocker.patch.object(exttable, '__getitem__', return_value=[table_row1, table_row2])

        exttable.__getitem__
        points = np.random.rand(6, 2)

        mocker.patch.object(table_row1, 'compute', return_value=points[:3])
        mocker.patch.object(table_row2, 'compute', return_value=points[3:])

        mocker.patch('spectrapy.datacalib.wavelengthcalib.IdsModelCalibration.data_to_region',
                     side_effect=[points[0], points[1], points[2], points[3], points[4], points[5]])

        calib = self.get_calib(mocker)
        mocker.patch.object(calib._ds9, 'set')
        calib._display_slit(exttable, 'A', None)
        args, kwargs = calib._ds9.set.call_args

        assert args[0] == "regions"
        regions = ""
        for point in points:
            regions += f"image; point {point[0]} {point[1]} #point=cross 5 color=green\n"

        assert args[1] == regions

    def test_compute_real_position1(self, mocker):
        calib = self.get_calib(mocker)
        disp = 5.0
        calib._ids.model[0] = np.zeros((2, 2))
        calib._ids.model[0][0][0] = 1. / disp
        calib.set_lambda_range(100., 1000.)
        waves = calib._get_waves_range()

        fluxes = np.arange(len(waves))
        delta_disp = np.linspace(-200, 400, len(waves))

        margin = 10
        lines = Table(data=([50, 99, 101, 151, 300, 900, 1100], ['Ar'] * 7, [1] * 7),
                      names=['pos', 'label', 'flag'])

        mocker.patch('spectrapy.datacalib.wavelengthcalib.find_peak_1d',
                     side_effect=[(1., None), (10.4, None), (None, None), (10.7, None)])

        pos = WavelengthCalibration.compute_line_positions(waves, delta_disp, fluxes, lines, margin)

        assert np.allclose(pos[0], [101, delta_disp[1]])

        idx = np.nonzero(waves >= 151)[0][0]
        assert np.allclose(pos[1], [151, 0.6 * delta_disp[idx] + 0.4 * delta_disp[idx + 1]])

        assert np.allclose(pos[2][0], 300)
        assert np.isnan(pos[2][1])

        idx = np.nonzero(waves >= 900)[0][0]
        assert np.allclose(pos[3], [900, 0.3 * delta_disp[idx] + 0.7 * delta_disp[idx + 1]])

        assert len(pos) == 4

    def test_compute_real_position2(self, mocker):
        calib = self.get_calib(mocker)
        disp = 5.0
        calib._ids.model[0] = np.zeros((2, 2))
        calib._ids.model[0][0][0] = 1. / disp
        calib.set_lambda_range(100., 1000.)
        waves = calib._get_waves_range()

        fluxes = np.arange(len(waves))
        delta_disp = np.linspace(-200, 400, len(waves))

        margin = 10
        lines = Table(data=([50, 99, 101, 151, 300, 900, 1100], ['Ar'] * 7, [0, 1, 1, 0, 0, 1, 1]),
                      names=['pos', 'label', 'flag'])

        mocker.patch('spectrapy.datacalib.wavelengthcalib.find_peak_1d',
                     side_effect=[(1., None), (10.7, None)])

        pos = WavelengthCalibration.compute_line_positions(waves, delta_disp, fluxes, lines, margin)

        assert np.allclose(pos[0], [101, delta_disp[1]])

        idx = np.nonzero(waves >= 900)[0][0]
        assert np.allclose(pos[1], [900, 0.3 * delta_disp[idx] + 0.7 * delta_disp[idx + 1]])

        assert len(pos) == 2

    def test_fit_slit_ids(self, mocker):
        calib = self.get_calib(mocker)

        pixels = 100 * np.random.rand(3, 2)
        var = 100 * np.random.rand(3, 2)

        ids_values = [(np.random.rand(5), None), (np.random.rand(5), None), (np.random.rand(5), None)]
        ids_models = [MagicMock(return_value=ids_values[0]), MagicMock(return_value=ids_values[1]),
                      MagicMock(return_value=ids_values[2])]

        crv_values = [(np.random.rand(5, 2), None), (np.random.rand(5, 2), None), (np.random.rand(5, 2), None)]
        crv_models = [MagicMock(return_value=crv_values[0]), MagicMock(return_value=crv_values[1]),
                      MagicMock(return_value=crv_values[2])]

        waves = np.random.rand(5)
        lines = MagicMock()

        mocker.patch.object(calib.crv, 'get_local_model', side_effect=crv_models)
        mocker.patch.object(calib, '_fit_slice_ids', side_effect=[ids_models[0], None, ids_models[2]])

        slit_rows = calib._fit_slit_ids(pixels, var, waves, lines, 10)

        assert np.allclose(calib._fit_slice_ids.call_args_list[0][0][0], pixels[0])
        assert np.allclose(calib._fit_slice_ids.call_args_list[0][0][1], waves)
        assert calib._fit_slice_ids.call_args_list[0][0][2] is lines
        assert np.allclose(calib._fit_slice_ids.call_args_list[0][0][3], 10)

        assert np.allclose(calib._fit_slice_ids.call_args_list[1][0][0], pixels[1])
        assert np.allclose(calib._fit_slice_ids.call_args_list[1][0][1], waves)
        assert calib._fit_slice_ids.call_args_list[1][0][2] is lines
        assert np.allclose(calib._fit_slice_ids.call_args_list[1][0][3], 10)

        assert np.allclose(calib._fit_slice_ids.call_args_list[2][0][0], pixels[2])
        assert np.allclose(calib._fit_slice_ids.call_args_list[2][0][1], waves)
        assert calib._fit_slice_ids.call_args_list[2][0][2] is lines
        assert np.allclose(calib._fit_slice_ids.call_args_list[2][0][3], 10)

        assert len(slit_rows) == 3
        assert np.allclose(slit_rows[0][0], pixels[0])
        assert np.allclose(slit_rows[0][1], var[0])
        assert slit_rows[0][2] is crv_models[0]
        assert slit_rows[0][3] is ids_models[0]

        assert np.allclose(slit_rows[1][0], pixels[1])
        assert np.allclose(slit_rows[1][1], var[1])
        assert slit_rows[1][2] is crv_models[1]
        assert np.alltrue(~np.isfinite(slit_rows[1][3].coeffs))

        assert np.allclose(slit_rows[2][0], pixels[2])
        assert np.allclose(slit_rows[2][1], var[2])
        assert slit_rows[2][2] is crv_models[2]
        assert slit_rows[2][3] is ids_models[2]

    def test_fit_slice_ids1(self, mocker):
        calib = self.get_calib(mocker)
        pixels = 100 * np.random.rand(1, 2)

        ids_values = [(np.random.rand(5), None), (np.random.rand(5), None), (np.random.rand(5), None)]
        ids_models = [MagicMock(return_value=ids_values[0]), MagicMock(return_value=ids_values[1]),
                      MagicMock(return_value=ids_values[2])]

        crv_values = [(np.random.rand(5, 2), None), (np.random.rand(5, 2), None), (np.random.rand(5, 2), None)]
        crv_models = [MagicMock(return_value=crv_values[0]), MagicMock(return_value=crv_values[1]),
                      MagicMock(return_value=crv_values[2])]

        mocker.patch('spectrapy.datacalib.wavelengthcalib.WavelengthCalibration.compute_line_positions', return_value=np.zeros((10, 2)))
        mocker.patch.object(calib._ids, 'get_local_model', side_effect=ids_models)
        mocker.patch.object(calib._crv, 'get_local_model', side_effect=crv_models)
        mocker.patch.object(calib._ds9.image, 'interpolate', side_effect=[(np.array([1., 2., 3., 4., 5]), None), ])
        mocker.patch('spectrapy.datacalib.wavelengthcalib.WavelengthCalibration.compute_line_positions', side_effect=[np.random.rand(5, 2), ])
        mocker.patch.object(calib.ids, 'refit_slit', side_effect=Exception())

        waves = np.random.rand(5,)
        ids_model = calib._fit_slice_ids(pixels, waves, None, None)
        assert ids_model is None

    def test_fit_slice_ids2(self, mocker):
        calib = self.get_calib(mocker)
        pixels = 100 * np.random.rand(1, 2)

        ids_values = [(np.random.rand(5), None), (np.random.rand(5), None), (np.random.rand(5), None)]
        ids_models = [MagicMock(return_value=ids_values[0]), MagicMock(return_value=ids_values[1]),
                      MagicMock(return_value=ids_values[2])]

        ids_values2 = [(np.random.rand(5), None), (np.random.rand(5), None), (np.random.rand(5), None)]
        ids_models2 = [MagicMock(return_value=ids_values2[0]), MagicMock(return_value=ids_values2[1]),
                       MagicMock(return_value=ids_values2[2])]

        crv_values = [(np.random.rand(5, 2), None), (np.random.rand(5, 2), None), (np.random.rand(5, 2), None)]
        crv_models = [MagicMock(return_value=crv_values[0]), MagicMock(return_value=crv_values[1]),
                      MagicMock(return_value=crv_values[2])]

        mocker.patch('spectrapy.datacalib.wavelengthcalib.WavelengthCalibration.compute_line_positions', return_value=np.zeros((10, 2)))
        mocker.patch.object(calib._ids, 'get_local_model', side_effect=ids_models)
        mocker.patch.object(calib._crv, 'get_local_model', side_effect=crv_models)
        fluxes = [(np.array([1., 2., 3., 4., 5]), None), ] * 3
        mocker.patch.object(calib._ds9.image, 'interpolate', side_effect=fluxes)
        positions = np.random.rand(5, 2)
        mocker.patch('spectrapy.datacalib.wavelengthcalib.WavelengthCalibration.compute_line_positions', side_effect=[positions, positions, positions])
        mocker.patch.object(calib.ids, 'refit_slit', side_effect=ids_models2)

        waves = np.random.rand(5,)
        lines = np.random.rand(5,)
        margin = np.random.rand()
        ids_model = calib._fit_slice_ids(pixels, waves, lines, margin)
        assert ids_model is ids_models2[1]

        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][0], waves)
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][1], ids_values[0][0])
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][2], fluxes[0][0])
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][3], lines)
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][4], margin)

        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][0], waves)
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][1], ids_values2[0][0])
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][2], fluxes[1][0])
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][3], lines)
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][4], margin)

    def test_fit_slice_ids3(self, mocker):
        calib = self.get_calib(mocker)

        pixels = 100 * np.random.rand(1, 2)

        ids_values = [(np.random.rand(5), None), (np.random.rand(5), None), (np.random.rand(5), None)]
        ids_models = [MagicMock(return_value=ids_values[0]), MagicMock(return_value=ids_values[1]),
                      MagicMock(return_value=ids_values[2])]

        ids_values2 = [(np.random.rand(5), None), (np.random.rand(5), None), (np.random.rand(5), None)]
        ids_models2 = [MagicMock(return_value=ids_values2[0]), MagicMock(return_value=ids_values2[1]),
                       MagicMock(return_value=ids_values2[2])]

        crv_values = [(np.random.rand(5, 2), None), (np.random.rand(5, 2), None), (np.random.rand(5, 2), None)]
        crv_models = [MagicMock(return_value=crv_values[0]), MagicMock(return_value=crv_values[1]),
                      MagicMock(return_value=crv_values[2])]

        mocker.patch('spectrapy.datacalib.wavelengthcalib.WavelengthCalibration.compute_line_positions', return_value=np.zeros((10, 2)))
        mocker.patch.object(calib._ids, 'get_local_model', side_effect=ids_models)
        mocker.patch.object(calib._crv, 'get_local_model', side_effect=crv_models)
        fluxes = [(np.array([1., 2., 3., 4., 5]), None), (np.array([1., np.nan, np.nan, 3., 6]), None),
                  (np.array([1., np.nan, np.nan, np.nan, 5]), None), ]
        mocker.patch.object(calib._ds9.image, 'interpolate', side_effect=fluxes)
        positions1 = np.random.rand(5, 2)
        positions2 = np.random.rand(5, 2)
        positions2[:, 1][[0, 2, 3]] = np.nan
        positions3 = np.random.rand(5, 2)
        positions3[:, 1][[0, 3]] = np.nan

        mocker.patch('spectrapy.datacalib.wavelengthcalib.WavelengthCalibration.compute_line_positions', side_effect=[positions1, positions2, positions3])
        mocker.patch.object(calib.ids, 'refit_slit', side_effect=ids_models2)

        waves = np.random.rand(5,)
        lines = np.random.rand(5,)
        margin = np.random.rand()
        ids_model = calib._fit_slice_ids(pixels, waves, lines, margin)
        assert ids_model is ids_models2[1]

        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][0], waves)
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][1], ids_values[0][0])
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][2], fluxes[0][0])
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][3], lines)
        assert np.allclose(calib.compute_line_positions.call_args_list[0][0][4], margin)

        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][0], waves[[0, 3, 4]])
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][1], ids_values2[0][0][[0, 3, 4]])
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][2], fluxes[1][0][[0, 3, 4]])
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][3], lines)
        assert np.allclose(calib.compute_line_positions.call_args_list[1][0][4], margin)

    def test_compute_spectra_wave1(self, mocker):
        calib = self.get_calib(mocker)

        with pytest.raises(Exception) as no_range:
            calib.compute_spectra_wave(None)

        assert no_range.type is AttributeError
        assert no_range.value.args[0] == "Lambda range not set yet!"

    def test_compute_spectra_wave2(self, mocker):
        calib = self.get_calib(mocker)
        calib.set_lambda_range(10, 20)

        lines = MagicMock()
        waves = MagicMock()
        rows = [[[10, 20], [30, 40], [1, None]],
                [[50, 60, 70], [2, 3, None], [4, 5, None], [6, 7, None], [80, 90, 100]]]

        mocker.patch.object(calib, '_load_catalog', return_value=lines)
        mocker.patch.object(calib, '_get_waves_range', return_value=waves)
        mocker.patch.object(calib, '_fit_slit_ids', side_effect=rows)
        mocker.patch.object(calib, '_display_slit', side_effect=rows)

        mocker.patch('spectrapy.datacalib.wavelengthcalib.ExtTable.add_row')
        mocker.patch('spectrapy.datacalib.wavelengthcalib.ExtTable.create_region_str')

        slits = [MagicMock(), MagicMock(), MagicMock()]

        slits[0].reference = False
        mocker.patch.object(slits[0], 'get_slit_pix', return_value=(1, 2))
        slits[1].reference = True
        slits[2].reference = False
        mocker.patch.object(slits[2], 'get_slit_pix', return_value=(3, 4))

        mask = Mask()
        mask._slits = {'A': slits[0], 'B': slits[1], 'C': slits[2]}
        calib._mask = mask

        calib.compute_spectra_wave(None)

        assert calib._fit_slit_ids.call_args_list[0][0][0] == 1
        assert calib._fit_slit_ids.call_args_list[0][0][1] == 2
        assert calib._fit_slit_ids.call_args_list[0][0][2] is waves
        assert calib._fit_slit_ids.call_args_list[0][0][3] is lines

        assert calib._fit_slit_ids.call_args_list[1][0][0] == 3
        assert calib._fit_slit_ids.call_args_list[1][0][1] == 4
        assert calib._fit_slit_ids.call_args_list[1][0][2] is waves
        assert calib._fit_slit_ids.call_args_list[1][0][3] is lines

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[0][0][0] == 'A'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[0][0][1] == 10
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[0][0][2] == 20
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[0][0]) == 3

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[1][0][0] == 'A'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[1][0][1] == 30
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[1][0][2] == 40
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[1][0]) == 3

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[2][0][0] == 'A'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[2][0][1] == 1
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[2][0][2] is None
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[1][0]) == 3

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[3][0][0] == 'C'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[3][0][1] == 50
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[3][0][2] == 60
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[3][0][3] == 70
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[3][0]) == 4

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[4][0][0] == 'C'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[4][0][1] == 2
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[4][0][2] == 3
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[4][0][3] is None
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[4][0]) == 4

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[5][0][0] == 'C'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[5][0][1] == 4
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[5][0][2] == 5
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[5][0][3] is None
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[5][0]) == 4

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[6][0][0] == 'C'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[6][0][1] == 6
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[6][0][2] == 7
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[6][0][3] is None
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[6][0]) == 4

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[7][0][0] == 'C'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[7][0][1] == 80
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[7][0][2] == 90
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[7][0][3] == 100
        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list[7][0]) == 4

        assert len(spectrapy.datacalib.wavelengthcalib.ExtTable.add_row.call_args_list) == 8

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.create_region_str.call_args_list[0][0][1] == 'A'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.create_region_str.call_args_list[0][0][0] is not lines

        assert spectrapy.datacalib.wavelengthcalib.ExtTable.create_region_str.call_args_list[1][0][1] == 'C'
        assert spectrapy.datacalib.wavelengthcalib.ExtTable.create_region_str.call_args_list[1][0][0] is not lines

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File datacalib_test.py
#
# Created on: Mar 25, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import spectrapy
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d
from spectrapy.models.crvmodel import CrvModel2d
from spectrapy.models.model import Model2d

from spectrapy.modelscalib.crvcalib import CrvModelCalibration
from spectrapy.mask.mask import Mask
from spectrapy.datacalib.tracingcalib import TraceCalibration

from spectrapy.dataio.image import Image


class TestTraceCalibration(object):

    def get_calib(self, mocker):
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()
        instrument = Instrument()
        opt = OptModel2d(1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv, engine="DTanH")
        calib._ds9 = MagicMock()
        return calib

    def test_init1(self, mocker):
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        opt = OptModel2d(1, 1, instrument=instrument)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        assert calib.instrument is instrument
        assert calib.mask is mask
        assert calib.opt is opt
        assert calib.crv is crv

        assert calib._red is None
        assert calib._blue is None
        assert calib._engine.down_step is True
        assert calib._engine.axis == 1

    def test_init2(self, mocker):
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        opt = OptModel2d(1, 1, instrument=instrument)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        assert calib.instrument is instrument
        assert calib.mask is mask
        assert calib.opt is opt
        assert calib.crv is crv

        assert calib._red is None
        assert calib._blue is None
        assert calib._engine.down_step is False
        assert calib._engine.axis == 1

    def test_init3(self, mocker):
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()
        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        opt = OptModel2d(1, 1, instrument=instrument)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        assert calib.instrument is instrument
        assert calib.mask is mask
        assert calib.opt is opt
        assert calib.crv is crv

        assert calib._red is None
        assert calib._blue is None
        assert calib._engine.down_step is False
        assert calib._engine.axis == 0

    def test_init4(self, mocker):
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()
        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1, instrument=instrument)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        assert calib.instrument is instrument
        assert calib.mask is mask
        assert calib.opt is opt
        assert calib.crv is crv

        assert calib._red is None
        assert calib._blue is None
        assert calib._engine.down_step is True
        assert calib._engine.axis == 0

    def test_init5(self, mocker):
        crv = CrvModel2d(1, 1, 1)
        mask = Mask()
        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        opt = OptModel2d(1, 1, instrument=instrument)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        with pytest.raises(Exception) as invalid_engine:
            calib = TraceCalibration(instrument, mask, opt, crv, engine="mock")

        assert invalid_engine.type is ValueError
        assert invalid_engine.value.args[0] == "Invalid method mock"

    def test_new_crv_model(self, mocker):
        instrument = Instrument()
        mask = Mask()
        opt = OptModel2d(1, 1)
        crv = CrvModel2d(1, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        with pytest.raises(NotImplementedError) as no_method:
            calib.new_crv_model(None, None, None)

        assert no_method.type is NotImplementedError

    def test_reload_traces_from_regions(self, mocker):
        instrument = Instrument()
        mask = Mask()
        opt = OptModel2d(1, 1)
        crv = CrvModel2d(1, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        with pytest.raises(NotImplementedError) as no_method:
            calib.reload_traces_from_regions()

        assert no_method.type is NotImplementedError

    def test_get_spectrum_area(self, mocker):
        instrument = Instrument()
        mask = Mask()
        opt = OptModel2d(1, 1)
        crv = CrvModel2d(1, 1, 1)

        mocker.patch('spectrapy.modelscalib.optcalib.Display')
        calib = TraceCalibration(instrument, mask, opt, crv)

        with pytest.raises(NotImplementedError) as no_method:
            calib._get_spectrum_area(None)

        assert no_method.type is NotImplementedError

    def test_get_subimage_shape1(self, mocker):
        calib = self.get_calib(mocker)
        calib._opt.dispersion_direction = 'LR'

        shape = calib._get_subimage_shape(10, 20)

        assert shape[0] == 20
        assert shape[1] == 10

    def test_get_subimage_shape2(self, mocker):
        calib = self.get_calib(mocker)
        calib._opt.dispersion_direction = 'UB'

        shape = calib._get_subimage_shape(10, 20)

        assert shape[0] == 10
        assert shape[1] == 20

    def test_get_subimage_shape3(self, mocker):
        calib = self.get_calib(mocker)
        calib._opt.dispersion_direction = 'RL'

        shape = calib._get_subimage_shape(10, 20)

        assert shape[0] == 20
        assert shape[1] == 10

    def test_get_subimage_shape4(self, mocker):
        calib = self.get_calib(mocker)
        calib._opt.dispersion_direction = 'BU'

        shape = calib._get_subimage_shape(10, 20)

        assert shape[0] == 10
        assert shape[1] == 20

    def test_get_delta_dispersions1(self, mocker):
        calib = self.get_calib(mocker)
        calib.crv.dispersion_direction = 'LR'

        with pytest.raises(Exception) as no_edges:
            calib._get_delta_dispersions(20)

        assert no_edges.type is AttributeError
        assert no_edges.value.args[0] == "Red/blue pixels not set yet!"

    def test_get_delta_dispersions2(self, mocker):
        calib = self.get_calib(mocker)
        calib.crv.dispersion_direction = 'LR'

        calib.set_trace_limits(100, 200)

        deltas = calib._get_delta_dispersions(20)

        assert np.allclose(deltas, np.arange(-100, 200, 20))

    def test_get_delta_dispersions3(self, mocker):
        calib = self.get_calib(mocker)
        calib.crv.dispersion_direction = 'RL'

        calib.set_trace_limits(100, 200)

        deltas = calib._get_delta_dispersions(20)

        assert np.allclose(deltas, np.arange(-100, 200, 20))

    def test_get_delta_dispersions4(self, mocker):
        calib = self.get_calib(mocker)
        calib.crv.dispersion_direction = 'BU'

        calib.set_trace_limits(100, 200)

        deltas = calib._get_delta_dispersions(20)

        assert np.allclose(deltas, np.arange(-100, 200, 20))

    def test_get_delta_dispersions5(self, mocker):
        calib = self.get_calib(mocker)
        calib.crv.dispersion_direction = 'UB'

        calib.set_trace_limits(100, 200)

        deltas = calib._get_delta_dispersions(20)

        assert np.allclose(deltas, np.arange(-100, 200, 20))

    def test_get_subimage_center(self, mocker):
        calib = self.get_calib(mocker)

        slit = MagicMock()
        calib.mask._slits = {'A': slit}
        pos = np.random.rand(2)
        mocker.patch.object(slit, 'get_pix_position', return_value=(pos, None))

        poss = 100 * np.random.rand(3, 2)
        mocker.patch.object(calib._crv, 'compute', return_value=(poss, None))

        centers = calib._get_subimage_center('A', np.zeros(3))

        assert np.allclose(centers[0], pos + poss[0])
        assert np.allclose(centers[1], pos + poss[1])
        assert np.allclose(centers[2], pos + poss[2])

    def test_compute_spectra_traces1(self, mocker, caplog):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib._ds9, 'reset_background')

        mocker.patch.object(calib, '_get_delta_dispersions')
        mocker.patch.object(calib, '_get_subimage_shape')

        slit1 = MagicMock()
        slit2 = MagicMock()
        calib._mask._slits = {'A': slit1, 'B': slit2}

        centers = np.random.rand(5, 2)

        mocker.patch.object(calib, '_get_subimage_center', side_effect=[centers[0:3], centers[3:]])

        edges = np.random.rand(3, 2)
        var = np.random.rand(3)

        mocker.patch.object(calib._engine, 'compute_slice_edge', side_effect=[(edges[0], var[0]),
                                                                              (edges[1], var[1]),
                                                                              (None, None),
                                                                              (edges[2], var[2]),
                                                                              (None, None)])

        mocker.patch.object(calib, '_reg_plot_crv_regions')

        calib.compute_spectra_traces(var=True)

        assert caplog.record_tuples[-1][2] == "Var paramter is experimental. "\
            "In case of faluire try to turn if off"

        assert len(calib._traces) == 1
        assert '0.00' in calib._traces
        assert 'A' in calib._traces['0.00']
        assert 'B' in calib._traces['0.00']

        assert len(calib._variances) == 1
        assert '0.00' in calib._variances
        assert 'A' in calib._variances['0.00']
        assert 'B' in calib._variances['0.00']

        assert np.allclose(calib._traces['0.00']['A'], [edges[0], edges[1]])
        assert np.allclose(calib._traces['0.00']['B'], [edges[2], ])

        assert np.allclose(calib._variances['0.00']['A'], [var[0], var[1]])
        assert np.allclose(calib._variances['0.00']['B'], [var[2], ])

        args, kwargs = calib._reg_plot_crv_regions.call_args

        assert len(args[0]) == 1
        assert np.allclose(args[0]['0.00']['A'], centers[2])
        assert np.allclose(args[0]['0.00']['B'], centers[4])

    def test_compute_spectra_traces2(self, mocker, caplog):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib._ds9, 'reset_background')

        mocker.patch.object(calib, '_get_delta_dispersions')
        mocker.patch.object(calib, '_get_subimage_shape')

        slit1 = MagicMock()
        slit2 = MagicMock()
        calib._mask._slits = {'A': slit1, 'B': slit2}

        lcenters = np.random.rand(5, 2)
        rcenters = np.random.rand(5, 2)

        mocker.patch.object(calib, '_get_subimage_center', side_effect=[lcenters[0:3], lcenters[3:],
                                                                        rcenters[0:3], rcenters[3:]])

        ledges = np.random.rand(3, 2)
        lvar = np.random.rand(3)
        redges = np.random.rand(3, 2)
        rvar = np.random.rand(3)

        mocker.patch.object(calib._engine, 'compute_slice_edge', side_effect=[(ledges[0], lvar[0]),
                                                                              (ledges[1], lvar[1]),
                                                                              (None, None),
                                                                              (ledges[2], lvar[2]),
                                                                              (None, None),

                                                                              (redges[0], rvar[0]),
                                                                              (None, None),
                                                                              (redges[1], rvar[1]),
                                                                              (redges[2], rvar[2]),
                                                                              (None, None)])

        mocker.patch.object(calib, '_reg_plot_crv_regions')

        calib.compute_spectra_traces(var=True, right=True)

        assert caplog.record_tuples[-1][2] == "Var paramter is experimental. "\
            "In case of faluire try to turn if off"

        assert len(calib._traces) == 2
        assert '0.00' in calib._traces
        assert '1.00' in calib._traces
        assert 'A' in calib._traces['0.00']
        assert 'B' in calib._traces['0.00']
        assert 'A' in calib._traces['1.00']
        assert 'B' in calib._traces['1.00']

        assert len(calib._variances) == 2
        assert '0.00' in calib._variances
        assert '1.00' in calib._variances
        assert 'A' in calib._variances['0.00']
        assert 'B' in calib._variances['0.00']
        assert 'A' in calib._variances['1.00']
        assert 'B' in calib._variances['1.00']

        assert np.allclose(calib._traces['0.00']['A'], [ledges[0], ledges[1]])
        assert np.allclose(calib._traces['0.00']['B'], [ledges[2], ])
        assert np.allclose(calib._traces['1.00']['A'], [redges[0], redges[1]])
        assert np.allclose(calib._traces['1.00']['B'], [redges[2], ])

        assert np.allclose(calib._variances['0.00']['A'], [lvar[0], lvar[1]])
        assert np.allclose(calib._variances['0.00']['B'], [lvar[2], ])
        assert np.allclose(calib._variances['1.00']['A'], [rvar[0], rvar[1]])
        assert np.allclose(calib._variances['1.00']['B'], [rvar[2], ])

    def test_compute_spectra_traces3(self, mocker, caplog):
        calib = self.get_calib(mocker)
        mocker.patch.object(calib._ds9, 'reset_background')

        mocker.patch.object(calib, '_get_delta_dispersions')
        mocker.patch.object(calib, '_get_subimage_shape')

        slit1 = MagicMock()
        slit2 = MagicMock()
        calib._mask._slits = {'A': slit1, 'B': slit2}

        lcenters = np.random.rand(5, 2)
        rcenters = np.random.rand(5, 2)

        mocker.patch.object(calib, '_get_subimage_center', side_effect=[lcenters[0:3], lcenters[3:],
                                                                        rcenters[0:3], rcenters[3:]])

        ledges = np.random.rand(3, 2)
        redges = np.random.rand(3, 2)

        mocker.patch.object(calib._engine, 'compute_slice_edge', side_effect=[(ledges[0], None),
                                                                              (ledges[1], None),
                                                                              (None, None),
                                                                              (ledges[2], None),
                                                                              (None, None),

                                                                              (redges[0], None),
                                                                              (None, None),
                                                                              (redges[1], None),
                                                                              (redges[2], None),
                                                                              (None, None)])

        mocker.patch.object(calib, '_reg_plot_crv_regions')

        calib.compute_spectra_traces(var=True, right=True)

        assert caplog.record_tuples[-1][2] == "Var paramter is experimental. "\
            "In case of faluire try to turn if off"

        assert len(calib._traces) == 2
        assert '0.00' in calib._traces
        assert '1.00' in calib._traces
        assert 'A' in calib._traces['0.00']
        assert 'B' in calib._traces['0.00']
        assert 'A' in calib._traces['1.00']
        assert 'B' in calib._traces['1.00']

        assert len(calib._variances) == 0

        assert np.allclose(calib._traces['0.00']['A'], [ledges[0], ledges[1]])
        assert np.allclose(calib._traces['0.00']['B'], [ledges[2], ])
        assert np.allclose(calib._traces['1.00']['A'], [redges[0], redges[1]])
        assert np.allclose(calib._traces['1.00']['B'], [redges[2], ])

    def test_writeto(self, mocker):

        calib = self.get_calib(mocker)

        mocker.patch.object(calib.crv, 'writeto', return_value=None)

        calib.writeto(1, overwrite=2)

        args, kwargs = calib.crv.writeto.call_args
        assert args[0] == 1
        assert kwargs['overwrite'] == 2

    def test_load_image(self, mocker):
        calib = self.get_calib(mocker)
        image = Image(data=np.zeros((2, 2)))
        calib._ds9._image = image

        mocker.patch('spectrapy.datacalib.tracingcalib.CrvModelCalibration.load_image')

        calib.load_image("mock")
        args = spectrapy.datacalib.tracingcalib.CrvModelCalibration.load_image.call_args_list[0]
        assert args[0][0] == calib
        assert args[0][1] == "mock"


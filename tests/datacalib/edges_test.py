#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File edges_test.py
#
# Created on: Feb 08 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import numpy as np
import pytest
from pytest_mock import mocker
from unittest.mock import MagicMock

import spectrapy
from spectrapy.datacalib.edges import *
from spectrapy.dataio.image import Image


class TestEdgeEngine(object):
    def test_init1(self):
        engine = EdgeEngine()
        assert engine.image is None

    def test_init2(self, mocker):
        image = MagicMock()
        engine = EdgeEngine()
        engine.image = image

        assert engine._image is image


class TestTDoubleTanH(object):

    def test_init1(self):
        engine = DoubleTanH()

        assert engine._popt is None
        assert engine._pcov is None
        assert len(engine._p0) == 0

        assert engine._f['right'] == DoubleTanH.right
        assert engine._f['left'] == DoubleTanH.left
        assert engine._f['double'] == DoubleTanH.double

        assert engine._df['right'] == DoubleTanH.dright
        assert engine._df['left'] == DoubleTanH.dleft
        assert engine._df['double'] == DoubleTanH.ddouble

    def test_has_fit1(self):
        engine = DoubleTanH()

        assert engine._has_fit() is False

    def test_has_fit2(self):
        engine = DoubleTanH()
        engine._popt = np.zeros(3)
        assert engine._has_fit() is True

    def test_double(self):
        x = np.linspace(0, 10, 1000)
        A = 20 * np.random.rand(3)
        pos = 10 * np.random.rand(2)
        steep = 5 * np.random.rand(2)

        expected = A[0] * (np.tanh(-steep[0] * (x - pos[0])) / 2. + .5) + \
            A[1] * (np.tanh(steep[1] * (x - pos[1])) / 2. + 0.5) + A[2]

        assert np.allclose(expected, DoubleTanH.double(x, A[0], A[1], pos[0], pos[1], steep[0], steep[1], A[2]))

    def test_left(self):
        x = np.linspace(0, 10, 1000)
        A = 20 * np.random.rand(2)
        pos = 10 * np.random.rand(1)
        steep = 5 * np.random.rand(1)

        expected = A[0] * (np.tanh(-steep[0] * (x - pos[0])) / 2. + .5) + A[1]

        assert np.allclose(expected, DoubleTanH.left(x, A[0], pos[0], steep[0], A[1]))

    def test_right(self):
        x = np.linspace(0, 10, 1000)
        A = 20 * np.random.rand(2)
        pos = 10 * np.random.rand(1)
        steep = 5 * np.random.rand(1)

        expected = A[0] * (np.tanh(steep[0] * (x - pos[0])) / 2. + .5) + A[1]

        assert np.allclose(expected, DoubleTanH.right(x, A[0], pos[0], steep[0], A[1]))

    def test_ddouble(self):
        x = np.linspace(0, 10, 1000)
        A = 20 * np.random.rand(3) + 1
        pos = 8 * np.random.rand(2) + 1
        steep = 5 * np.random.rand(2) + 1

        jac = DoubleTanH.ddouble(x, A[0], A[1], pos[0], pos[1], steep[0], steep[1], A[2])

        dA0 = np.tanh(-steep[0] * (x - pos[0])) / 2. + .5
        assert np.allclose(jac[:, 0], dA0)

        dA1 = np.tanh(steep[1] * (x - pos[1])) / 2. + 0.5
        assert np.allclose(jac[:, 1], dA1)

        dA2 = np.ones(1000)
        assert np.allclose(jac[:, 6], dA2)

        dpos0 = A[0] * steep[0] * (1 - np.tanh(-steep[0] * (x - pos[0]))**2) / 2.
        assert np.allclose(jac[:, 2], dpos0)

        dpos1 = -A[1] * steep[1] * (1 - np.tanh(steep[1] * (x - pos[1]))**2) / 2.
        assert np.allclose(jac[:, 3], dpos1)

        dsteep0 = -A[0] * (x - pos[0]) * (1 - np.tanh(-steep[0] * (x - pos[0]))**2) / 2.
        assert np.allclose(jac[:, 4], dsteep0)

        dsteep1 = A[1] * (x - pos[1]) * (1 - np.tanh(steep[1] * (x - pos[1]))**2) / 2.
        assert np.allclose(jac[:, 5], dsteep1)

    def test_dleft(self):
        x = np.linspace(0, 10, 1000)
        A = 20 * np.random.rand(2) + 1
        pos = 8 * np.random.rand(1) + 1
        steep = 5 * np.random.rand(1) + 1

        jac = DoubleTanH.dleft(x, A[0], pos[0], steep[0], A[1])

        dA = np.tanh(-steep[0] * (x - pos[0])) / 2. + .5
        assert np.allclose(jac[:, 0], dA)

        dL = np.ones(1000)
        assert np.allclose(jac[:, 3], dL)

        dpos = A[0] * steep[0] * (1 - np.tanh(-steep[0] * (x - pos[0]))**2) / 2.
        assert np.allclose(jac[:, 1], dpos)

        dsteep = -A[0] * (x - pos[0]) * (1 - np.tanh(-steep[0] * (x - pos[0]))**2) / 2.
        assert np.allclose(jac[:, 2], dsteep)

    def test_dright(self):
        x = np.linspace(0, 10, 1000)
        A = 20 * np.random.rand(2) + 1
        pos = 8 * np.random.rand(1) + 1
        steep = 5 * np.random.rand(1) + 1

        jac = DoubleTanH.dright(x, A[0], pos[0], steep[0], A[1])

        dA = np.tanh(steep[0] * (x - pos[0])) / 2. + .5
        assert np.allclose(jac[:, 0], dA)

        dL = np.ones(1000)
        assert np.allclose(jac[:, 3], dL)

        dpos = -A[0] * steep[0] * (1 - np.tanh(steep[0] * (x - pos[0]))**2) / 2.
        assert np.allclose(jac[:, 1], dpos)

        dsteep = A[0] * (x - pos[0]) * (1 - np.tanh(steep[0] * (x - pos[0]))**2) / 2.
        assert np.allclose(jac[:, 2], dsteep)

    def test_compute_p0_1(self, mocker):
        engine = DoubleTanH()
        engine._compute_p0(None, None, None, sigma=-0.1)

        assert engine._has_fit() is False

    def test_compute_p0_2(self, mocker):
        engine = DoubleTanH()
        engine._compute_p0(None, None, None, sigma=5.1)

        assert engine._has_fit() is False

    def test_compute_p0_3(self, mocker):
        engine = DoubleTanH()

        mocker.patch('spectrapy.datacalib.edges.sigma_clip',
                     return_value=np.ma.array([1, 2, 3], mask=[True] * 3))

        engine._compute_p0(None, None, None, sigma=0.1)

        assert engine._has_fit() is False

    def test_compute_p0_4(self, mocker):
        engine = DoubleTanH()

        xdata = np.arange(10)
        ydata = np.array([30., 30., 30., 30., 10., 10., 10., 20., 20., 20., 20.])
        edges = np.zeros(10)
        edges[3] = -1000.
        edges[7] = 1000.

        engine._compute_p0(xdata, ydata, edges)

        assert np.allclose(engine._p0['double'], [30., 20., 2., 9., 3, 3, 10.])
        assert len(engine._p0) == 1

    def test_compute_p0_5(self, mocker):
        engine = DoubleTanH()

        xdata = np.arange(10)
        ydata = np.array([30., 30., 30., 30., 10., 10., 10., 10., 10., 10., 10.])
        edges = np.zeros(10)
        edges[3] = -1000.

        engine._compute_p0(xdata, ydata, edges)

        assert np.allclose(engine._p0['left'], [30., 2., 3, 10.])
        assert len(engine._p0) == 1

    def test_compute_p0_6(self, mocker):
        engine = DoubleTanH()

        xdata = np.arange(10)
        ydata = np.array([10., 10., 10., 10., 30., 30., 30., 30., 30., 30., 30.])
        edges = np.zeros(10)
        edges[3] = 1000.

        engine._compute_p0(xdata, ydata, edges)

        assert np.allclose(engine._p0['right'], [30., 2., 3, 10.])
        assert len(engine._p0) == 1

    def test_compute_p0_7(self, mocker):
        engine = DoubleTanH()

        xdata = np.arange(10)
        ydata = np.array([30., 30., 30., 30., 10., 10., 10., 12., 12., 12., 12.])
        edges = np.zeros(10)
        edges[3] = -1000.
        edges[7] = 10.

        engine._compute_p0(xdata, ydata, edges)

        assert np.allclose(engine._p0['double'], [30., 12., 2., 9., 3, 3, 10.])
        assert np.allclose(engine._p0['left'], [30., 2., 3, 11.])

        assert len(engine._p0) == 2

    def test_compute_p0_8(self, mocker):
        engine = DoubleTanH()

        xdata = np.arange(10)
        ydata = np.array([12., 12., 12., 12., 10., 10., 10., 30., 30., 30., 30.])
        edges = np.zeros(10)
        edges[3] = -10.
        edges[7] = 1000.

        engine._compute_p0(xdata, ydata, edges)

        assert np.allclose(engine._p0['double'], [12., 30., 2., 9., 3, 3, 10.])
        assert np.allclose(engine._p0['right'], [30., 6, 3, (12 * 4 + 10 * 3) / 7.])

        assert len(engine._p0) == 2

    def test_compute_fg(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'marco': None}
        mocker.patch.object(engine, '_compute_p0')
        engine.compute_fg(None, None, None)

        assert len(engine._p0) == 0

    def test_compute1(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'right': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', return_value=[1, 2, 3])

        engine._compute(None, None, None)
        assert engine._popt == 1
        assert engine._pcov == 2

    def test_compute2(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'left': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', return_value=[1, 2, 3])

        engine._compute(None, None, None)
        assert engine._popt == 1
        assert engine._pcov == 2

    def test_compute3(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'right': None, 'left': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', side_effect=([1, 2, 3], [4, 5, 6]))

        engine._compute(None, None, None)
        assert engine._popt == 1
        assert engine._pcov == 2
        assert len(engine._p0) == 1
        assert 'right' in engine._p0

    def test_compute4(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'right': None, 'left': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', side_effect=([1, 2, 3], [4, 5, .6]))

        engine._compute(None, None, None)
        assert engine._popt == 4
        assert engine._pcov == 5
        assert len(engine._p0) == 1
        assert 'left' in engine._p0

    def test_compute5(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'double': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', return_value=[1, 2, 3])

        engine._compute(None, None, None)
        assert engine._popt == 1
        assert engine._pcov == 2
        assert len(engine._p0) == 1
        assert 'double' in engine._p0

    def test_compute6(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'double': None, 'right': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', side_effect=([1, 2, 3], [4, 5, 0.6]))

        engine._compute(None, None, None)
        assert engine._popt == 4
        assert engine._pcov == 5
        assert len(engine._p0) == 1
        assert 'double' in engine._p0

    def test_compute7(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'double': None, 'left': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', side_effect=([1, 2, 3], [4, 5, 0.6]))

        engine._compute(None, None, None)
        assert engine._popt == 4
        assert engine._pcov == 5
        assert len(engine._p0) == 1
        assert 'double' in engine._p0

    def test_compute8(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'double': None, 'right': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', side_effect=([1, 2, 3], [4, 5, 6]))

        engine._compute(None, None, None)
        assert engine._popt == 1
        assert engine._pcov == 2
        assert len(engine._p0) == 1
        assert 'right' in engine._p0

    def test_compute9(self, mocker):
        engine = DoubleTanH()

        mocker.patch.object(engine, 'compute_fg')
        engine._p0 = {'double': None, 'left': None}
        mocker.patch('spectrapy.datacalib.edges.DoubleTanH._interpolate_data',
                     return_value=[None] * 4)

        mocker.patch.object(engine, '_compute_fit', side_effect=([1, 2, 3], [4, 5, 6]))

        engine._compute(None, None, None)
        assert engine._popt == 1
        assert engine._pcov == 2
        assert len(engine._p0) == 1
        assert 'left' in engine._p0

    def test_compute_fit1(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'right': 3}

        mocker.patch('spectrapy.datacalib.edges.curve_fit', side_effect=RuntimeError("Failed"))
        values = engine._compute_fit(1, 2, 'right')

        assert values[0] is None
        assert values[1] is None
        assert np.isinf(values[2])

        args, kwargs = spectrapy.datacalib.edges.curve_fit.call_args
        assert args[0] is DoubleTanH.right
        assert args[1] == 1
        assert args[2] == 2
        assert kwargs['p0'] == 3
        assert kwargs['jac'] is DoubleTanH.dright

    def test_compute_fit2(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'left': 3}

        mocker.patch('spectrapy.datacalib.edges.curve_fit', side_effect=RuntimeError("Failed"))
        values = engine._compute_fit(1, 2, 'left')

        assert values[0] is None
        assert values[1] is None
        assert np.isinf(values[2])

        args, kwargs = spectrapy.datacalib.edges.curve_fit.call_args
        assert args[0] is DoubleTanH.left
        assert args[1] == 1
        assert args[2] == 2
        assert kwargs['p0'] == 3
        assert kwargs['jac'] is DoubleTanH.dleft

    def test_compute_fit3(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'double': 3}

        mocker.patch('spectrapy.datacalib.edges.curve_fit', side_effect=RuntimeError("Failed"))
        values = engine._compute_fit(1, 2, 'double')

        assert values[0] is None
        assert values[1] is None
        assert np.isinf(values[2])

        args, kwargs = spectrapy.datacalib.edges.curve_fit.call_args
        assert args[0] is DoubleTanH.double
        assert args[1] == 1
        assert args[2] == 2
        assert kwargs['p0'] == 3
        assert kwargs['jac'] is DoubleTanH.ddouble

    def test_compute_fit4(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'right': None}

        popt = 10 * np.random.rand(4)
        pcov = 10 * np.random.rand(4, 4)
        xdata = np.linspace(1, 10, 10)
        ydata = 20 * np.random.rand(10)
        mocker.patch('spectrapy.datacalib.edges.curve_fit', return_value=[popt, pcov])
        values = engine._compute_fit(xdata, ydata, 'right')

        assert np.allclose(values[0], popt)
        assert np.allclose(values[1], pcov)
        assert np.allclose(values[2], np.sum((ydata - DoubleTanH.right(xdata, *popt))**2))

    def test_compute_fit5(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'left': None}

        popt = 10 * np.random.rand(4)
        pcov = 10 * np.random.rand(4, 4)
        xdata = np.linspace(1, 10, 10)
        ydata = 20 * np.random.rand(10)
        mocker.patch('spectrapy.datacalib.edges.curve_fit', return_value=[popt, pcov])
        values = engine._compute_fit(xdata, ydata, 'left')

        assert np.allclose(values[0], popt)
        assert np.allclose(values[1], pcov)
        assert np.allclose(values[2], np.sum((ydata - DoubleTanH.left(xdata, *popt))**2))

    def test_compute_fit6(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'double': None}

        popt = 10 * np.random.rand(7)
        pcov = 10 * np.random.rand(7, 7)
        xdata = np.linspace(1, 10, 10)
        ydata = 20 * np.random.rand(10)
        mocker.patch('spectrapy.datacalib.edges.curve_fit', return_value=[popt, pcov])
        values = engine._compute_fit(xdata, ydata, 'double')

        assert np.allclose(values[0], popt)
        assert np.allclose(values[1], pcov)
        assert np.allclose(values[2], np.sum((ydata - DoubleTanH.double(xdata, *popt))**2))

    def test_compute_fit7(self, mocker):

        engine = DoubleTanH()
        engine._p0 = {'double': None}

        popt = 10 * np.random.rand(7)
        pcov = 10 * np.random.rand(7, 7)
        xdata = np.linspace(1, 10, 10)
        ydata = 20 * np.random.rand(10)
        vdata = 20 * np.random.rand(10)
        mocker.patch('spectrapy.datacalib.edges.curve_fit', return_value=[popt, pcov])
        values = engine._compute_fit(xdata, ydata, 'double', vdata=vdata)

        args, kwargs = spectrapy.datacalib.edges.curve_fit.call_args

        assert args[0] is DoubleTanH.double
        assert args[1] is xdata
        assert args[2] is ydata
        assert kwargs['p0'] is None
        assert kwargs['jac'] is DoubleTanH.ddouble
        assert np.allclose(kwargs['sigma'], np.sqrt(vdata))

        assert np.allclose(values[0], popt)
        assert np.allclose(values[1], pcov)
        assert np.allclose(values[2], np.sum((ydata - DoubleTanH.double(xdata, *popt))**2 / vdata))

    def test_get_bottom_step(self, mocker):
        engine = DoubleTanH()
        mocker.patch.object(engine, 'get_step')
        engine.get_bottom_step()

        args, kwargs = engine.get_step.call_args

        assert len(args) == 0
        assert kwargs['bottom'] is True

    def test_get_up_step(self, mocker):
        engine = DoubleTanH()
        mocker.patch.object(engine, 'get_step')
        engine.get_up_step()

        args, kwargs = engine.get_step.call_args

        assert len(args) == 0
        assert kwargs['bottom'] is False

    def test_get_step1(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'left': None}

        edge, var = engine.get_step(bottom=False)

        assert edge is None
        assert var is None

    def test_get_step2(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'right': None}

        edge, var = engine.get_step(bottom=True)

        assert edge is None
        assert var is None

    def test_get_step3(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'left': None}
        popt = np.zeros(4)
        pcov = np.zeros((4, 4))
        popt[1] = 2.0
        popt[2] = 1.0

        pcov[1][1] = 3.0
        pcov[2][2] = .7
        pcov[1][2] = pcov[2][1] = .5

        engine._popt = popt
        engine._pcov = pcov

        edge, var = engine.get_step(bottom=True)

        assert np.allclose(edge, DoubleTanH.EDGE_FLEX + 2.0)
        assert np.allclose(var, 0.7 * DoubleTanH.EDGE_FLEX**2 + 3.0 - 2. * DoubleTanH.EDGE_FLEX * 0.5)

    def test_get_step4(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'right': None}
        popt = np.zeros(4)
        pcov = np.zeros((4, 4))
        popt[1] = 2.0
        popt[2] = 1.0

        pcov[1][1] = 3.0
        pcov[2][2] = .7
        pcov[1][2] = pcov[2][1] = .5

        engine._popt = popt
        engine._pcov = pcov

        edge, var = engine.get_step(bottom=False)

        assert np.allclose(edge, -DoubleTanH.EDGE_FLEX + 2.0)
        assert np.allclose(var, 0.7 * DoubleTanH.EDGE_FLEX**2 + 3.0 + 2. * DoubleTanH.EDGE_FLEX * 0.5)

    def test_get_step5(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'double': None}
        popt = np.zeros(7)
        pcov = np.zeros((7, 7))
        popt[2] = 2.0
        popt[4] = 1.0

        pcov[2][2] = 3.0
        pcov[4][4] = .7
        pcov[2][4] = pcov[4][2] = .5

        engine._popt = popt
        engine._pcov = pcov

        edge, var = engine.get_step(bottom=True)

        assert np.allclose(edge, DoubleTanH.EDGE_FLEX + 2.0)
        assert np.allclose(var, 0.7 * DoubleTanH.EDGE_FLEX**2 + 3.0 - 2. * DoubleTanH.EDGE_FLEX * 0.5)

    def test_get_step6(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'double': None}
        popt = np.zeros(7)
        pcov = np.zeros((7, 7))
        popt[3] = 2.0
        popt[5] = 1.0

        pcov[3][3] = 3.0
        pcov[5][5] = .7
        pcov[3][5] = pcov[5][3] = .5

        engine._popt = popt
        engine._pcov = pcov

        edge, var = engine.get_step(bottom=False)

        assert np.allclose(edge, -DoubleTanH.EDGE_FLEX + 2.0)
        assert np.allclose(var, 0.7 * DoubleTanH.EDGE_FLEX**2 + 3.0 + 2. * DoubleTanH.EDGE_FLEX * 0.5)

    def test_get_step7(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'left': None}
        popt = np.zeros(4)
        pcov = np.zeros((4, 4))
        popt[1] = 2.0
        popt[2] = 1.0

        pcov[1][1] = 3.0
        pcov[2][2] = .7
        pcov[1][2] = pcov[2][1] = -100000000

        engine._popt = popt
        engine._pcov = pcov

        edge, var = engine.get_step(bottom=True)

        assert edge is None
        assert var is None

    def test_get_step8(self, mocker):
        engine = DoubleTanH()
        engine._p0 = {'left': None}
        popt = np.zeros(4)
        pcov = np.zeros((4, 4))
        popt[1] = 2.0
        popt[2] = 1.0

        pcov[1][1] = 3.0
        pcov[2][2] = .7
        pcov[1][2] = pcov[2][1] = np.inf

        engine._popt = popt
        engine._pcov = pcov

        edge, var = engine.get_step(bottom=True)

        assert edge is None
        assert var is None

    def test_interpolate_data1(self):
        xdata = np.linspace(1, 2, 10)
        ydata = np.random.rand(10)
        edges = np.random.rand(10)

        a, b, c, d = DoubleTanH._interpolate_data(1, xdata, ydata, edges)

        assert np.allclose(xdata, a)
        assert np.allclose(ydata, b)
        assert np.allclose(edges, c)
        assert d is None

    def test_interpolate_data2(self):
        xdata = np.linspace(1, 2, 5)
        ydata = np.random.rand(5)
        edges = np.random.rand(5)

        a, b, c, d = DoubleTanH._interpolate_data(3, xdata, ydata, edges)

        assert np.allclose(np.linspace(1, 2, 12 + 1), a)
        assert np.allclose(ydata, b[::3])
        assert np.allclose(edges, c[::3])
        assert d is None

    def test_interpolate_data3(self):
        xdata = np.linspace(1, 2, 5)
        ydata = np.random.rand(5)
        edges = np.random.rand(5)
        vdata = np.random.rand(5)

        a, b, c, d = DoubleTanH._interpolate_data(3, xdata, ydata, edges, vdata)

        assert np.allclose(np.linspace(1, 2, 12 + 1), a)
        assert np.allclose(ydata, b[::3])
        assert np.allclose(edges, c[::3])
        assert np.allclose(vdata, d[::3])

    def test_get_profile_edges1(self, mocker):
        sub_image = MagicMock()
        sub_image.start = [0, 0]
        engine = DoubleTanH('BU')

        data = np.ma.array(np.random.rand(10, 20), mask=np.zeros((10, 20), dtype=bool))
        sub_image.data = data
        edges = np.random.rand(10, 20)

        mocker.patch.object(engine, '_get_slice', return_value=sub_image)

        mocker.patch('spectrapy.datacalib.edges.ndimage.sobel', return_value=edges)
        mocker.patch('spectrapy.datacalib.edges.np.ma.array', return_value=np.zeros((3, 3)))
        mocker.patch('spectrapy.datacalib.edges.np.ma.sum', return_value=np.zeros(3))

        engine._get_slice_edges(1, 2)

        args, kwargs = engine._get_slice.call_args
        assert args[0] == 1
        assert args[1] == 2

        args, kwargs = spectrapy.datacalib.edges.ndimage.sobel.call_args
        assert args[0] is data
        assert kwargs['axis'] == 1

        args, kwargs = spectrapy.datacalib.edges.np.ma.array.call_args
        assert args[0] is edges
        assert np.allclose(kwargs['mask'], data.mask)

        args, kwargs = spectrapy.datacalib.edges.np.ma.sum.call_args
        assert kwargs['axis'] == 0

    def test_get_profile_edges2(self, mocker):
        sub_image = MagicMock()
        sub_image.start = [0, 0]
        engine = DoubleTanH('LR')
        data = np.ma.array(np.random.rand(10, 20), mask=np.zeros((10, 20), dtype=bool))
        sub_image.data = data
        edges = np.random.rand(10, 20)

        mocker.patch.object(engine, '_get_slice', return_value=sub_image)

        mocker.patch('spectrapy.datacalib.edges.ndimage.sobel', return_value=edges)
        mocker.patch('spectrapy.datacalib.edges.np.ma.array', return_value=np.zeros((3, 3)))
        mocker.patch('spectrapy.datacalib.edges.np.ma.sum', return_value=np.zeros(3))

        engine._get_slice_edges(1, 2)

        args, kwargs = engine._get_slice.call_args
        assert args[0] == 1
        assert args[1] == 2

        args, kwargs = spectrapy.datacalib.edges.ndimage.sobel.call_args
        assert args[0] is data
        assert kwargs['axis'] == 0

        args, kwargs = spectrapy.datacalib.edges.np.ma.array.call_args
        assert args[0] is edges
        assert np.allclose(kwargs['mask'], data.mask)

        args, kwargs = spectrapy.datacalib.edges.np.ma.sum.call_args
        assert kwargs['axis'] == 1

    def test_get_profile_edges3(self, mocker):
        sub_image = MagicMock()
        sub_image.start = [0, 0]
        engine = DoubleTanH('LR')
        data = np.ma.array(10 * np.random.rand(10, 20), mask=np.zeros((10, 20), dtype=bool))
        sub_image.data = data
        edges = np.random.rand(10, 20)

        mocker.patch.object(engine, '_get_slice', return_value=sub_image)

        mocker.patch('spectrapy.datacalib.edges.ndimage.sobel', return_value=edges)
        mocker.patch('spectrapy.datacalib.edges.np.ma.array', return_value=np.zeros((3, 3)))
        mocker.patch('spectrapy.datacalib.edges.np.ma.sum', return_value=np.zeros(3))

        engine._get_slice_edges(1, 2, logp=True)

        args, kwargs = engine._get_slice.call_args
        assert args[0] == 1
        assert args[1] == 2

        args, kwargs = spectrapy.datacalib.edges.ndimage.sobel.call_args

        offset = max(0, -data.min())
        assert np.allclose(args[0], np.log(data + offset + 1))
        assert kwargs['axis'] == 0

        args, kwargs = spectrapy.datacalib.edges.np.ma.array.call_args
        assert args[0] is edges
        assert np.allclose(kwargs['mask'], data.mask)

        args, kwargs = spectrapy.datacalib.edges.np.ma.sum.call_args
        assert kwargs['axis'] == 1

    def test_compute_slice_edge1(self, mocker):
        engine = DoubleTanH()
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_get_slice_edges', return_value=[xprofile, edges])

        mocker.patch.object(engine, '_compute')

        center = MagicMock()
        shape = MagicMock()
        edge, _ = engine.compute_slice_edge(center, shape)

        assert edge is None
        args, kwargs = engine._get_slice_profile.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = engine._compute.call_args
        assert args[0] is xprofile
        assert args[1] is yprofile
        assert args[2] is edges

    def test_compute_slice_edge2(self, mocker):
        engine = DoubleTanH('LR')

        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_get_slice_edges', return_value=[xprofile, edges])

        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)
        mocker.patch.object(engine, 'get_step', return_value=[None, None])

        edge, _ = engine.compute_slice_edge(None, None)

        assert edge is None

        args, kwargs = engine.get_step.call_args
        assert kwargs['bottom'] is True

    def test_compute_slice_edge3(self, mocker):
        engine = DoubleTanH('RL')
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_get_slice_edges', return_value=[xprofile, edges])

        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)
        mocker.patch.object(engine, 'get_step', return_value=[None, None])

        edge, _ = engine.compute_slice_edge(None, None)

        assert edge is None

        args, kwargs = engine.get_step.call_args
        assert kwargs['bottom'] is False

    def test_compute_slice_edge4(self, mocker):
        engine = DoubleTanH('RL')
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_get_slice_edges', return_value=[xprofile, edges])

        edge = 100 * np.random.rand(1)
        var = 10 * np.random.rand(1)
        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)
        mocker.patch.object(engine, 'get_step', return_value=[edge, var])
        mocker.patch.object(engine, 'plot')

        center = 10 * np.random.rand(2)
        edge_pos, edge_var = engine.compute_slice_edge(center, None, plot=True)

        args, kwargs = engine.plot.call_args

        assert args[0] is xprofile
        assert args[1] is yprofile
        assert args[2] is edges
        assert kwargs['edge'] is edge

    def test_compute_slice_edge5(self, mocker):
        engine = DoubleTanH('BU')
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_get_slice_edges', return_value=[xprofile, edges])

        edge = 100 * np.random.rand()
        var = 10 * np.random.rand()
        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)
        mocker.patch.object(engine, 'get_step', return_value=[edge, var])

        center = 10 * np.random.rand(2)
        edge_pos, edge_var = engine.compute_slice_edge(center, None, plot=False)

        assert np.allclose(edge_pos, (edge, center[1]))
        assert np.allclose(edge_var, var)

    def test_compute_slice_edge6(self, mocker):
        engine = DoubleTanH('LR')

        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_get_slice_edges', return_value=[xprofile, edges])

        edge = 100 * np.random.rand()
        var = 10 * np.random.rand()
        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)
        mocker.patch.object(engine, 'get_step', return_value=[edge, var])

        center = 10 * np.random.rand(2)
        edge_pos, edge_var = engine.compute_slice_edge(center, None, plot=False)

        assert np.allclose(edge_pos, (center[0], edge))
        assert np.allclose(edge_var, var)

    def test_get_slice_profile1(self, mocker):
        engine = DoubleTanH('BU')
        data = 100 * np.random.rand(10, 16)
        image = Image(data, start=[3, 4])
        mocker.patch.object(engine, '_get_slice', return_value=image)

        profile = data.sum(axis=0)
        mocker.patch.object(image, 'compute_profile', return_value=(profile, None))

        center = MagicMock()
        shape = MagicMock()

        x, y, _ = engine._get_slice_profile(center, shape)

        args, kwargs = engine._get_slice.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = image.compute_profile.call_args
        assert len(args) == 0
        assert kwargs['axis'] == 0

        assert np.allclose(y, profile)
        assert np.allclose(x, np.arange(3, 16 + 3))

    def test_get_slice_profile2(self, mocker):
        engine = DoubleTanH('LR')
        data = 100 * np.random.rand(10, 16)
        image = Image(data, start=[3, 4])
        mocker.patch.object(engine, '_get_slice', return_value=image)

        profile = data.sum(axis=1)
        mocker.patch.object(image, 'compute_profile', return_value=(profile, None))

        center = MagicMock()
        shape = MagicMock()

        x, y, _ = engine._get_slice_profile(center, shape)

        args, kwargs = engine._get_slice.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = image.compute_profile.call_args
        assert len(args) == 0
        assert kwargs['axis'] == 1

        assert np.allclose(y, profile)
        assert np.allclose(x, np.arange(4, 10 + 4))

    def test_get_slice_profile3(self, mocker):
        engine = DoubleTanH('LR')
        data = 100 * np.random.rand(10, 16) - 50.
        image = Image(data, start=[3, 4])
        mocker.patch.object(engine, '_get_slice', return_value=image)

        profile = data = 100 * np.random.rand(10) - 50.
        if profile.min() < 0:
            log_profile = profile - profile.min() + 1
        else:
            log_profile = profile + 1
        log_profile = np.log(log_profile)

        mocker.patch.object(image, 'compute_profile', return_value=(profile, None))

        center = MagicMock()
        shape = MagicMock()

        _, y, _ = engine._get_slice_profile(center, shape, logp=True)

        args, kwargs = engine._get_slice.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = image.compute_profile.call_args
        assert len(args) == 0
        assert kwargs['axis'] == 1

        assert np.allclose(y, log_profile)

    def test_get_slice1(self, mocker, caplog):
        engine = DoubleTanH()
        engine._image = MagicMock()
        mocker.patch.object(engine._image, 'get_subimage', return_value=np.zeros((2, 2)))

        sub = engine._get_slice(np.array([10, 10]), np.array([2, 2]))

        assert sub is None

    def test_get_slice2(self, mocker, caplog):
        engine = DoubleTanH()
        engine._image = MagicMock()
        mocker.patch.object(engine._image, 'get_subimage', return_value=np.zeros((2, 2)))

        engine._get_slice(np.array([10.2, 20.2]), np.array([4, 2]))

        args, kwargs = engine._image.get_subimage.call_args

        assert args[0] == 8
        assert args[1] == 19
        assert args[2] == 5
        assert args[3] == 3

    def test_get_slice3(self, mocker, caplog):
        engine = DoubleTanH()
        engine._image = MagicMock()
        mocker.patch.object(engine._image, 'get_subimage', return_value=np.zeros((2, 2)))

        engine._get_slice(np.array([10.6, 20.8]), np.array([4, 2]))

        args, kwargs = engine._image.get_subimage.call_args

        assert args[0] == 8
        assert args[1] == 19
        assert args[2] == 5
        assert args[3] == 3

    def test_get_slice4(self, mocker, caplog):
        engine = DoubleTanH()
        engine._image = MagicMock()
        mocker.patch.object(engine._image, 'get_subimage', return_value=np.zeros((2, 2)))

        engine._get_slice(np.array([10.6, 20.8]), np.array([5, 3]))

        args, kwargs = engine._image.get_subimage.call_args

        assert args[0] == 8
        assert args[1] == 19
        assert args[2] == 5
        assert args[3] == 3

    def test_get_slice5(self, mocker, caplog):
        engine = DoubleTanH()
        engine._image = MagicMock()
        mocker.patch.object(engine._image, 'get_subimage', return_value=np.zeros((20, 20)))

        _slice = engine._get_slice(np.array([10.6, 20.8]), np.array([5, 3]))

        args, kwargs = engine._image.get_subimage.call_args

        assert args[0] == 8
        assert args[1] == 19
        assert args[2] == 5
        assert args[3] == 3

        assert np.allclose(_slice, np.zeros((20, 20)))


class TestSobelEngine(object):
    def test_init1(self):
        engine = Sobel()
        assert engine._down_step is None
        assert engine._up_step is None

    def test_init2(self, mocker):
        image = MagicMock()
        image.data = MagicMock()
        data = np.random.rand(10, 20)

        image.data.data = data
        engine = Sobel('BU')

        engine.image = image

        assert np.allclose(engine._image.data.data, ndimage.sobel(data, axis=1))
        assert np.allclose(engine._orig.data.data, data)
        assert engine.image is not image

    def test_init3(self, mocker):
        image = MagicMock()
        image.data = MagicMock()
        data = np.random.rand(10, 20)

        image.data.data = data
        engine = Sobel('LR')

        engine.image = image

        assert np.allclose(engine._image.data.data, ndimage.sobel(data, axis=0))
        assert np.allclose(engine._orig.data.data, data)
        assert engine.image is not image

    def test_has_fit1(self):
        engine = Sobel()

        assert engine._has_fit() is False

    def test_has_fit2(self):
        engine = Sobel()
        engine._down_step = 1

        assert engine._has_fit() is False

    def test_has_fit3(self):
        engine = Sobel()
        engine._up_step = 1

        assert engine._has_fit() is False

    def test_has_fit4(self):
        engine = Sobel()
        engine._down_step = 1
        engine._up_step = 1

        assert engine._has_fit() is True

    def test_get_step1(self):
        engine = Sobel()
        engine._down_step = 2
        engine._up_step = 3

        value, var = engine.get_step(bottom=False)
        assert var is None
        assert value == 3

    def test_get_step2(self):
        engine = Sobel()
        engine._down_step = 2
        engine._up_step = 3

        value, var = engine.get_step(bottom=True)
        assert var is None
        assert value == 2

    def test_compute_slice_edge1(self, mocker):
        engine = Sobel('BU')
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)

        mocker.patch.object(engine, 'get_step', return_value=(10.2, None))
        center = 10 * np.random.rand(2)
        shape = MagicMock()
        edge, var = engine.compute_slice_edge(center, shape)

        args, kwargs = engine._get_slice_profile.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = engine._compute.call_args
        assert args[0] is xprofile
        assert args[1] is yprofile

        assert np.allclose(edge[0], 10.2)
        assert np.allclose(edge[1], center[1])
        assert var is None

    def test_compute_slice_edge2(self, mocker):
        engine = Sobel('LR')
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=True)

        mocker.patch.object(engine, 'get_step', return_value=(10.2, None))
        center = 10 * np.random.rand(2)
        shape = MagicMock()
        edge, var = engine.compute_slice_edge(center, shape)

        args, kwargs = engine._get_slice_profile.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = engine._compute.call_args
        assert args[0] is xprofile
        assert args[1] is yprofile

        assert np.allclose(edge[1], 10.2)
        assert np.allclose(edge[0], center[0])
        assert var is None

    def test_compute_slice_edge3(self, mocker):
        engine = Sobel('LR')
        xprofile = np.arange(12, 32)
        yprofile = np.random.rand(19)
        edges = np.random.rand(19)

        mocker.patch.object(engine, '_get_slice_profile', return_value=[xprofile, yprofile, None])
        mocker.patch.object(engine, '_compute')
        mocker.patch.object(engine, '_has_fit', return_value=False)

        mocker.patch.object(engine, 'get_step', return_value=(10.2, None))
        center = 10 * np.random.rand(2)
        shape = MagicMock()
        edge, var = engine.compute_slice_edge(center, shape)

        args, kwargs = engine._get_slice_profile.call_args
        assert args[0] is center
        assert args[1] is shape

        args, kwargs = engine._compute.call_args
        assert args[0] is xprofile
        assert args[1] is yprofile

        assert edge is None
        assert var is None

    def test_compute1(self, mocker):
        engine = Sobel()

        xprofile = np.arange(7, 17)
        yprofile = 10 * (np.random.rand(10) - 0.5)

        pos_profile = np.where(yprofile > 0, yprofile, 0)
        neg_profile = np.where(yprofile > 0, 0, -yprofile)

        mocker.patch('spectrapy.datacalib.edges.find_peak_1d', side_effect=[(3.4, 2.3), (5.6, 7.8)])

        engine._compute(xprofile, yprofile)

        args = spectrapy.datacalib.edges.find_peak_1d.call_args_list[0]
        assert np.allclose(args[0][0], pos_profile)
        assert np.allclose(args[0][1], 2)

        args = spectrapy.datacalib.edges.find_peak_1d.call_args_list[1]
        assert np.allclose(args[0][0], neg_profile)
        assert np.allclose(args[0][1], 2)

        assert np.allclose(engine._up_step, 7 + 3.4)
        assert np.allclose(engine._down_step, 7 + 5.6)

    def test_compute2(self, mocker):
        engine = Sobel()

        xprofile = np.arange(7, 17)
        yprofile = 10 * (np.random.rand(10) - 0.5)

        pos_profile = np.where(yprofile > 0, yprofile, 0)
        neg_profile = np.where(yprofile > 0, 0, -yprofile)

        mocker.patch('spectrapy.datacalib.edges.find_peak_1d', side_effect=[(None, 2.3), (None, 7.8)])

        engine._compute(xprofile, yprofile)

        args = spectrapy.datacalib.edges.find_peak_1d.call_args_list[0]
        assert np.allclose(args[0][0], pos_profile)
        assert np.allclose(args[0][1], 2)

        args = spectrapy.datacalib.edges.find_peak_1d.call_args_list[1]
        assert np.allclose(args[0][0], neg_profile)
        assert np.allclose(args[0][1], 2)

        assert np.allclose(engine._up_step, 7 + np.argmax(yprofile))
        assert np.allclose(engine._down_step, 7 + np.argmin(yprofile))

    def test_compute3(self, mocker):
        engine = Sobel()

        xprofile = np.arange(7, 10)
        yprofile = 10 * (np.random.rand(3) - 0.5)

        mocker.patch('spectrapy.datacalib.edges.find_peak_1d')

        engine._compute(xprofile, yprofile)

        assert len(spectrapy.datacalib.edges.find_peak_1d.call_args_list) == 0

        assert np.allclose(engine._up_step, 7 + np.argmax(yprofile))
        assert np.allclose(engine._down_step, 7 + np.argmin(yprofile))

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File exttable_test.py
#
# Created on: Feb 23, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
from astropy.io import fits

import pytest
from unittest.mock import MagicMock
from pytest_mock import mocker

import spectrapy
from spectrapy.datacalib.extractiontable import ExtTable, ExtTableSlit, ExtTableRow
from spectrapy.models.crvmodel import CrvModel1d
from spectrapy.models.idsmodel import IdsModel1d
from spectrapy.mask.mask import Mask
from spectrapy.models.instrument import Instrument
from spectrapy.models.optmodel import OptModel2d


class TestExtTable(object):
    """Test for ExtTable class: the extraction table"""

    def test_init(self):
        ext = ExtTable()
        assert ext._slits == {}

    def test_add_row1(self):
        ext = ExtTable()
        assert len(ext) == 0

        ext.add_row("A", None, None, None, None)
        assert len(ext) == 1
        assert len(ext["A"]) == 1

    def test_add_row2(self):
        ext = ExtTable()
        assert len(ext) == 0

        ext.add_row("A", None, None, None, None)
        assert len(ext) == 1
        assert len(ext["A"]) == 1

        ext.add_row("A", None, None, None, None)
        assert len(ext) == 1
        assert len(ext["A"]) == 2

    def test_add_row3(self):
        ext = ExtTable()
        assert len(ext) == 0

        ext.add_row("A", None, None, None, None)
        assert len(ext) == 1
        assert len(ext["A"]) == 1

        ext.add_row("A", None, None, None, None)
        assert len(ext) == 1
        assert len(ext["A"]) == 2

        ext.add_row("B", None, None, None, None)
        assert len(ext) == 2
        assert len(ext["A"]) == 2
        assert len(ext["B"]) == 1

    def test_keys1(self):
        ext = ExtTable()
        assert len(ext.keys()) == 0

    def test_keys2(self):
        ext = ExtTable()
        ext.add_row("A", None, None, None, None)
        ext.add_row("B", None, None, None, None)

        assert len(ext.keys()) == 2
        assert "A" in ext.keys()
        assert "B" in ext.keys()

    def test__iter__(self):
        ext = ExtTable()
        ext.add_row("A", None, None, None, None)
        ext.add_row("B", None, None, None, None)

        i = 0
        for slit in ext:
            assert slit in ("A", "B")
            i += 1

        assert i == 2

    def test_writeto1(self, mocker, tmpdir):
        mocker.patch('os.path.isfile', return_value=True)
        ext = ExtTable()

        with pytest.raises(Exception) as already_exists:
            ext.writeto('mock', overwrite=False)

        assert already_exists.type is IOError
        assert already_exists.value.args[0] == "Extraction table mock already exists!"

    def test_writeto2(self, mocker, tmpdir):
        tmpfile = tmpdir.join('tmpname.dat')

        ext = ExtTable()

        pos_a1 = np.random.rand(2)
        posvar_a1 = np.random.rand(2)
        crv_a1 = CrvModel1d(np.random.rand(3), (np.random.rand(2, 2) * 100 - 50).astype(int))
        ids_a1 = IdsModel1d(np.random.rand(5), np.random.rand())
        ext.add_row("A", pos_a1, posvar_a1, crv_a1, ids_a1)

        pos_a2 = np.random.rand(2)
        posvar_a2 = np.random.rand(2)
        crv_a2 = CrvModel1d(np.random.rand(3), (np.random.rand(2, 2) * 100 - 50).astype(int))
        ids_a2 = IdsModel1d(np.random.rand(5), np.random.rand())
        ext.add_row("A", pos_a2, posvar_a2, crv_a2, ids_a2)

        pos_b1 = np.random.rand(2)
        posvar_b1 = np.random.rand(2)
        crv_b1 = CrvModel1d(np.random.rand(3), (np.random.rand(2, 2) * 100 - 50).astype(int))
        ids_b1 = IdsModel1d(np.random.rand(5), 10 * np.random.rand())
        ext.add_row("B", pos_b1, posvar_a2, crv_b1, ids_b1)

        tmpfile = tmpdir.join('tmpname.exr')
        ext.writeto(str(tmpfile), overwrite=True)

        ext2 = ExtTable.load(str(tmpfile))

        assert len(ext) == len(ext2)
        assert len(ext["A"]) == len(ext2["A"])
        assert len(ext["B"]) == len(ext2["B"])

        for key in ext:
            for i in range(len(ext[key])):
                row1 = ext[key][i]
                row2 = ext2[key][i]

                assert np.allclose(row1["DET_POS"], row2["DET_POS"])
                assert np.allclose(row1["POS_VAR"], row2["POS_VAR"])
                assert np.allclose(row1["CRV_COEFFS"], row2["CRV_COEFFS"])
                assert np.allclose(row1["CRV_VAR"], row2["CRV_VAR"])
                assert np.allclose(row1["IDS_COEFFS"], row2["IDS_COEFFS"])
                assert np.allclose(row1["IDS_VAR"], row2["IDS_VAR"])
                assert np.allclose(row1["ROTATION"], row2["ROTATION"])
                assert row2["ROTATION"].shape == (2, 2)
                assert np.allclose(row1["REF_LAMBDA"], row2["REF_LAMBDA"])

    def test_writeto3(self, mocker, tmpdir):
        tmpfile = tmpdir.join('tmpname.dat')

        ext = ExtTable()

        pos_a1 = np.random.rand(2)
        posvar_a1 = np.random.rand(2)
        crv_a1 = CrvModel1d(np.random.rand(3), (np.random.rand(2, 2) * 100 - 50).astype(int))
        ids_a1 = IdsModel1d(np.random.rand(5), np.random.rand())
        ext.add_row("A", pos_a1, posvar_a1, crv_a1, ids_a1)

        pos_a2 = np.random.rand(2)
        posvar_a2 = np.random.rand(2)
        crv_a2 = CrvModel1d(np.random.rand(3), (np.random.rand(2, 2) * 100 - 50).astype(int))
        ids_a2 = IdsModel1d(np.full(5, np.nan), np.random.rand())
        ext.add_row("A", pos_a2, posvar_a2, crv_a2, ids_a2)

        pos_b1 = np.random.rand(2)
        posvar_b1 = np.random.rand(2)
        crv_b1 = CrvModel1d(np.random.rand(3), (np.random.rand(2, 2) * 100 - 50).astype(int))
        ids_b1 = IdsModel1d(np.random.rand(5), 10 * np.random.rand())
        ext.add_row("B", pos_b1, posvar_a2, crv_b1, ids_b1)

        tmpfile = tmpdir.join('tmpname.exr')
        ext.writeto(str(tmpfile), overwrite=True)

        ext2 = ExtTable.load(str(tmpfile))

        assert len(ext) == len(ext2)
        assert len(ext["A"]) == len(ext2["A"])
        assert len(ext["B"]) == len(ext2["B"])

        for key in ext:
            for i in range(len(ext[key])):
                row1 = ext[key][i]
                row2 = ext2[key][i]

                assert np.allclose(row1["DET_POS"], row2["DET_POS"])
                assert np.allclose(row1["POS_VAR"], row2["POS_VAR"])
                assert np.allclose(row1["CRV_COEFFS"], row2["CRV_COEFFS"])
                assert np.allclose(row1["CRV_VAR"], row2["CRV_VAR"])
                assert np.allclose(row1["IDS_COEFFS"], row2["IDS_COEFFS"], equal_nan=True)
                assert np.allclose(row1["IDS_VAR"], row2["IDS_VAR"])
                assert np.allclose(row1["ROTATION"], row2["ROTATION"])
                assert row2["ROTATION"].shape == (2, 2)
                assert np.allclose(row1["REF_LAMBDA"], row2["REF_LAMBDA"])

    def test_get_spectrum_area1(self, mocker):

        ext = ExtTable()

        slit1 = MagicMock()
        slit2 = MagicMock()
        slit3 = MagicMock()

        ext._slits = {'A': [slit1, slit2, slit3]}

        box = 100 * np.random.rand(4, 2)

        mocker.patch.object(slit1, 'compute', side_effect=box[0:2])
        mocker.patch.object(slit2, 'compute', side_effect=-box[2:4] / 100)
        mocker.patch.object(slit3, 'compute', side_effect=box[2:4])

        area = ext.get_spectrum_area('A', None, None)

        xmin = np.floor(box[:, 0].min()) - 2
        ymin = np.floor(box[:, 1].min()) - 2
        xmax = np.floor(box[:, 0].max()) + 2
        ymax = np.floor(box[:, 1].max()) + 2

        assert area[0][0] == xmin
        assert area[0][1] == ymin

        assert area[1][0] == xmax - xmin + 1
        assert area[1][1] == ymax - ymin + 1

    def test_get_spectrum_area2(self, mocker):

        ext = ExtTable()

        slit1 = MagicMock()
        slit2 = MagicMock()
        slit3 = MagicMock()

        ext._slits = {'A': [slit1, slit2, slit3]}

        box = 100 * np.random.rand(4, 2)

        mocker.patch.object(slit1, 'compute', side_effect=box[0:2])
        mocker.patch.object(slit2, 'compute', side_effect=-box[2:4] / 100)
        mocker.patch.object(slit3, 'compute', side_effect=box[2:4])

        area = ext.get_spectrum_area('A', None, None, px_margin=1)

        xmin = np.floor(box[:, 0].min()) - 1
        ymin = np.floor(box[:, 1].min()) - 1
        xmax = np.floor(box[:, 0].max()) + 1
        ymax = np.floor(box[:, 1].max()) + 1

        assert area[0][0] == xmin
        assert area[0][1] == ymin

        assert area[1][0] == xmax - xmin + 1
        assert area[1][1] == ymax - ymin + 1

    def test_create_region_str1(self, mocker):
        exttable = ExtTable()  # MagicMock()
        row1 = MagicMock()
        row2 = MagicMock()
        row3 = MagicMock()

        exttable._slits = {'A': [row1, row2, row3], 'B': None}

        # mocker.patch.object(exttable, '__getitem__', return_value = [table_row1, table_row2])

        points = np.random.rand(6, 2)

        mocker.patch.object(row1, 'compute', return_value=points[0:2])
        mocker.patch.object(row2, 'compute', return_value=points[2:4])
        mocker.patch.object(row3, 'compute', return_value=points[4:6])

        mocker.patch('spectrapy.datacalib.wavelengthcalib.IdsModelCalibration.data_to_region',
                     side_effect=[points[0], points[1], points[2], points[3], points[4], points[5]])

        ds9_str = exttable.create_region_str(None, 'A')

        regions = ""
        for point in points:
            regions += f"image; point {point[0]} {point[1]} #point=cross 5 color=green\n"

        assert ds9_str == regions

    def test_create_region_str2(self, mocker):
        exttable = ExtTable()
        row1 = MagicMock()
        row2 = MagicMock()
        row3 = MagicMock()

        exttable._slits = {'A': [row1, row2, row3], 'B': None}

        points = np.random.rand(6, 2)

        mocker.patch.object(row1, 'compute', return_value=points[0:2])
        mocker.patch.object(row2, 'compute', return_value=points[2:4])
        mocker.patch.object(row3, 'compute', return_value=points[4:6])

        mocker.patch('spectrapy.datacalib.wavelengthcalib.IdsModelCalibration.data_to_region',
                     side_effect=[points[0], points[1], points[4], points[5]])

        ds9_str = exttable.create_region_str(None, 'A', rows=[0, 2])

        regions = ""
        regions += f"image; point {points[0][0]} {points[0][1]} #point=cross 5 color=green\n"
        regions += f"image; point {points[1][0]} {points[1][1]} #point=cross 5 color=green\n"
        regions += f"image; point {points[4][0]} {points[4][1]} #point=cross 5 color=green\n"
        regions += f"image; point {points[5][0]} {points[5][1]} #point=cross 5 color=green\n"

        assert ds9_str == regions

    def test_create_region_str3(self, mocker):
        exttable = ExtTable()
        row1 = MagicMock()
        row2 = MagicMock()
        row3 = MagicMock()

        exttable._slits = {'A': [row1, row2, row3], 'B': None}

        points = np.random.rand(6, 2)
        limits = np.random.rand(6, 2)

        mocker.patch.object(row1, 'compute', side_effect=(points[0:2], limits[0:2]))
        mocker.patch.object(row2, 'compute', return_value=(points[2:4], limits[2:4]))
        mocker.patch.object(row3, 'compute', return_value=(points[4:6], limits[4:6]))

        mocker.patch('spectrapy.datacalib.wavelengthcalib.IdsModelCalibration.data_to_region',
                     side_effect=[points[0], points[1], limits[0], limits[1],
                                  points[4], points[5], limits[4], limits[5]])

        ds9_str = exttable.create_region_str(None, 'A', rows=[0, 2], wrange=1.)

        regions = ""
        regions += f"image; point {points[0][0]} {points[0][1]} #point=cross 5 color=green\n"
        regions += f"image; point {points[1][0]} {points[1][1]} #point=cross 5 color=green\n"
        regions += f"image; point {limits[0][0]} {limits[0][1]} #point=diamond 5 color=green\n"
        regions += f"image; point {limits[1][0]} {limits[1][1]} #point=diamond 5 color=green\n"

        regions += f"image; point {points[4][0]} {points[4][1]} #point=cross 5 color=green\n"
        regions += f"image; point {points[5][0]} {points[5][1]} #point=cross 5 color=green\n"
        regions += f"image; point {limits[4][0]} {limits[4][1]} #point=diamond 5 color=green\n"
        regions += f"image; point {limits[5][0]} {limits[5][1]} #point=diamond 5 color=green\n"

        assert ds9_str == regions

    def test_instrument1(self):
        exttable = ExtTable()

        assert exttable.instrument is None

    def test_instrument2(self):
        exttable = ExtTable()

        with pytest.raises(Exception) as invalid_instrument:
            exttable.instrument = 7

        assert invalid_instrument.type is TypeError
        assert invalid_instrument.value.args[0] == "Invalid instrument instance"

    def test_instrument3(self):
        exttable = ExtTable()
        instrument = Instrument()

        exttable.instrument = instrument

        assert exttable.instrument is instrument

    def test_add_instrument_info(self, mocker):
        mock = Instrument()
        mock.pixel_scale = 1
        mock.xpixels = 2
        mock.ypixels = 3
        mock.dispersion_direction = 'LR'
        mock.linear_dispersion = 5
        mock.reference_lambda = 6
        mock.data_hdu = 7
        mock.var_hdu = 8
        mock.err_hdu = 9
        mock.flag_hdu = 9

        exttable = ExtTable()
        exttable.instrument = mock

        primary = fits.PrimaryHDU()
        exttable._add_instrument_info(primary)

        assert primary.header['PIXSCALE'] == mock.pixel_scale
        assert primary.header['XPIXELS'] == mock.xpixels
        assert primary.header['YPIXELS'] == mock.ypixels
        assert primary.header['DISPDIR'] == mock.dispersion_direction
        assert primary.header['LINDISP'] == mock.linear_dispersion
        assert primary.header['REFLAMB'] == mock.reference_lambda
        assert primary.header['DATAHDU'] == mock.data_hdu
        assert primary.header['VARHDU'] == mock.data_hdu
        assert primary.header['ERRHDU'] == mock.err_hdu
        assert primary.header['FLAGHDU'] == mock.flag_hdu

    def test_load_instrument(self):
        primary = fits.PrimaryHDU()

        primary.header['PIXSCALE'] = 1
        primary.header['XPIXELS'] = 2
        primary.header['YPIXELS'] = 3
        primary.header['DISPDIR'] = 'RL'
        primary.header['LINDISP'] = 5
        primary.header['REFLAMB'] = 6
        primary.header['DATAHDU'] = 7
        primary.header['VARHDU'] = 8
        primary.header['ERRHDU'] = 9
        primary.header['FLAGHDU'] = 10

        instrument = ExtTable.load_instrument(primary)

        assert instrument.pixel_scale == 1
        assert instrument.xpixels == 2
        assert instrument.ypixels == 3
        assert instrument.dispersion_direction == 'RL'
        assert instrument.linear_dispersion == 5
        assert instrument.reference_lambda == 6
        assert instrument.data_hdu == 7
        assert instrument.var_hdu == 8
        assert instrument.err_hdu == 9
        assert instrument.flag_hdu == 10

    def test_rectify(self, mocker):
        exttable = ExtTable()

        exttable._slits = {'A': ExtTableSlit('A'), 'B': ExtTableSlit('B'), 'C': ExtTableSlit('C')}
        mocker.patch.object(exttable['A'], 'rectify', return_value=None)
        mocker.patch.object(exttable['B'], 'rectify', return_value=None)
        mocker.patch.object(exttable['C'], 'rectify', return_value=None)

        exttable.rectify(3, 4)

        assert exttable['A'].rectify.call_args_list[0][1]['deg'] == 3
        assert exttable['A'].rectify.call_args_list[0][1]['margin'] == 4

        assert exttable['B'].rectify.call_args_list[0][1]['deg'] == 3
        assert exttable['B'].rectify.call_args_list[0][1]['margin'] == 4

        assert exttable['C'].rectify.call_args_list[0][1]['deg'] == 3
        assert exttable['C'].rectify.call_args_list[0][1]['margin'] == 4

    def test_rectify_slit(self, mocker):
        exttable = ExtTable()
        # exttable.add_row(slit_id, pos, var_pos, crv, ids)
        ids = IdsModel1d(np.array([1, 2, 3]), 1000)
        for k in range(6):
            exttable.add_row('A', None, None, None, ids)

        x, y, yvar = np.ma.arange(10, 16), 10 * np.random.rand(6, 3), 5 * np.random.rand(6, 3)
        x.mask = [False] * len(x)

        mocker.patch.object(exttable['A'], 'get_ids_values', return_value=[x, y, yvar])

        vals = [np.random.rand(3), np.random.rand(3), np.random.rand(3)]
        covs = [np.random.rand(3, 3), np.random.rand(3, 3), np.random.rand(3, 3)]
        mocker.patch('spectrapy.datacalib.extractiontable.np.ma.polyfit',
                     side_effect=[(vals[0], covs[0]), (vals[1], covs[1]), (vals[2], covs[2])])
        vanders = [np.random.rand(6, 3), np.random.rand(6, 3), np.random.rand(6, 3)]
        mocker.patch('spectrapy.datacalib.extractiontable.np.vander',
                     side_effect=vanders)

        # mocker.patch('spectrapy.datacalib.extractiontable.IdsModel1d')

        exttable['A'].rectify(deg=2, margin=1)

        args_list = spectrapy.datacalib.extractiontable.np.ma.polyfit.call_args_list
        assert np.allclose(x, args_list[0][0][0])
        assert x[1] == -2  # Check shift

        assert np.alltrue(args_list[0][0][0].mask == [True, False, False, False, False, True])
        assert np.allclose(y[:, 0], args_list[0][0][1])
        assert np.allclose(1. / np.sqrt(yvar[:, 0]), args_list[0][1]['w'])

        assert np.allclose(x, args_list[1][0][0])
        assert np.alltrue(args_list[1][0][0].mask == [True, False, False, False, False, True])
        assert np.allclose(y[:, 1], args_list[1][0][1])
        assert np.allclose(1. / np.sqrt(yvar[:, 1]), args_list[1][1]['w'])

        assert np.allclose(x, args_list[2][0][0])
        assert np.alltrue(args_list[2][0][0].mask == [True, False, False, False, False, True])
        assert np.allclose(y[:, 2], args_list[2][0][1])
        assert np.allclose(1. / np.sqrt(yvar[:, 2]), args_list[2][1]['w'])

        coeffs = []
        coeffs.append(np.poly1d(vals[0])(x.data))
        coeffs.append(np.poly1d(vals[1])(x.data))
        coeffs.append(np.poly1d(vals[2])(x.data))
        coeffs = np.array(coeffs)
        _vars = []
        _vars.append(vanders[0].dot(covs[0].diagonal()))
        _vars.append(vanders[1].dot(covs[1].diagonal()))
        _vars.append(vanders[2].dot(covs[2].diagonal()))
        _vars = np.array(_vars)
        for k in range(6):
            assert np.allclose(exttable._slits['A'][k].ids.coeffs, coeffs[:, k])
            assert np.allclose(exttable._slits['A'][k].ids.reference_lambda, 1000)
            assert np.allclose(exttable._slits['A'][k].ids.variance, _vars[:, k])

    def test_get_ids_values(self, mocker):
        exttable = ExtTable()
        # exttable.add_row(slit_id, pos, var_pos, crv, ids)
        pos = []
        ids = []
        lamb = []
        cov = []

        ids.append(np.random.rand(5))
        tmp = np.random.rand(5, 5)
        cov.append(tmp.dot(tmp.T))
        lamb.append(np.random.rand())
        pos.append(np.random.rand(2,))

        ids.append(np.full((5,), np.nan))
        tmp = np.random.rand(5, 5)
        cov.append(tmp.dot(tmp.T))
        lamb.append(np.random.rand())
        pos.append(np.random.rand(2,))

        ids.append(np.random.rand(5))
        tmp = np.random.rand(5, 5)
        cov.append(tmp.dot(tmp.T))
        lamb.append(np.random.rand())
        pos.append(np.random.rand(2,))

        crv = CrvModel1d(np.zeros((1,)), np.array([[0, -1], [1, 0]]))
        for k in range(3):
            model = IdsModel1d(ids[k], lamb[k], cov[k])
            exttable.add_row('A', pos[k], None, crv, model)

        poss, coeffs, coeffs_var = exttable['A'].get_ids_values()

        assert np.allclose(poss[0], pos[0][0])
        assert np.allclose(poss.mask[1], True)
        assert np.allclose(poss[2], pos[2][0])

        assert np.allclose(coeffs[0], ids[0])
        assert np.alltrue(coeffs.mask[1] == [True] * 5)
        assert np.allclose(coeffs[2], ids[2])

        assert np.allclose(coeffs_var[0], cov[0].diagonal())
        assert np.alltrue(coeffs_var.mask[1] == [True] * 5)
        assert np.allclose(coeffs_var[2], cov[2].diagonal())


class TestExtTableSlit(object):
    """Test for ExtTableSlit class: the extraction table slit"""

    def test_init(self):
        """
        """
        slit = ExtTableSlit("3.14")

        assert slit._slit_id == "3.14"
        assert len(slit._rows) == 0
        assert len(slit._model) == 0

    def test_get_positions(self, mocker):
        slit = ExtTableSlit("3.14")

        positions = np.random.rand(3, 2)

        slit.add_row(positions[0], "var_pos", "crv", "ids")
        slit.add_row(positions[1], "var_pos", "crv", "ids")
        slit.add_row(positions[2], "var_pos", "crv", "ids")

        poss = slit._get_positions()
        assert np.allclose(positions, poss)

    def test_compute_t1(self, mocker):
        x = np.ma.array(100 * np.random.rand(10))
        x.sort()
        slit = ExtTableSlit("3.14")

        assert len(slit._model) == 0

        t = slit._compute_t(x)
        assert np.isclose(t.data[0], 0.0)
        assert np.isclose(t.data[-1], 1.0)
        assert t.mask[0] == True
        assert t.mask[-1] == True
        assert np.isclose(x[0], slit._model['a'])
        assert np.isclose(x[-1], slit._model['b'])
        assert np.allclose((x.data - slit._model['a']) / (slit._model['b'] - slit._model['a']),
                           t.data)

    def test_compute_t2(self, mocker):
        x = np.ma.array(100 * np.random.rand(10))
        x.sort()
        slit = ExtTableSlit("3.14")

        assert len(slit._model) == 0

        t = slit._compute_t(x, margin=2)
        assert np.isclose(t.data[0], 0.0)
        assert np.isclose(t.data[-1], 1.0)
        assert t.mask[0] == True
        assert t.mask[1] == True
        assert t.mask[-2] == True
        assert t.mask[-1] == True
        assert np.alltrue(~t.mask[2:-2])

        assert np.isclose(x[0], slit._model['a'])
        assert np.isclose(x[-1], slit._model['b'])
        assert np.allclose((x.data - slit._model['a']) / (slit._model['b'] - slit._model['a']),
                           t.data)


class TestExtTableRow(object):
    """Test for ExtRow class: the extraction table row"""

    def test_init(self):
        """
        """
        row = ExtTableRow("slit_id", "pos", "var_pos", "crv", "ids")

        assert row.items["SLIT"] == "slit_id"
        assert row.items["DET_POS"] == "pos"
        assert row.items["POS_VAR"] == "var_pos"
        assert row.items["CRV"] == "crv"
        assert row.items["IDS"] == "ids"

    def test_compute(self, mocker):

        pos = np.random.rand(1, 2)
        ids = IdsModel1d(np.zeros(2), 0.0)
        crv = CrvModel1d(np.zeros(2), 'LR')

        crv_results = np.random.rand(2, 2)
        mocker.patch.object(crv, 'compute', return_value=crv_results)

        row = ExtTableRow("123", pos, None, crv, ids)

        value = row.compute(1.0)
        expected = pos + crv_results[0]

        assert value.shape == (1, 2)
        assert np.allclose(value, expected)

    def test__getitem__1(self):
        row = ExtTableRow("123", None, None, None, None)

        assert row["SLIT"] == "123"

    def test__getitem__2(self):
        pos = np.random.rand(1, 2)
        row = ExtTableRow(None, pos, None, None, None)

        assert np.allclose(row["DET_POS"], pos)

    def test__getitem__3(self):
        var = np.random.rand(1, 2)
        row = ExtTableRow(None, None, var, None, None)

        assert np.allclose(row["POS_VAR"], var)

    def test__getitem__4(self):
        coeffs = np.random.rand(2)
        crv = CrvModel1d(coeffs, np.zeros((2, 2)))
        row = ExtTableRow(None, None, None, crv, None)

        assert np.allclose(row["CRV_COEFFS"], coeffs)

    def test__getitem__5(self):
        coeffs = np.random.rand(2)
        crv = CrvModel1d(coeffs, np.zeros((2, 2)), coeffs**2)
        row = ExtTableRow(None, None, None, crv, None)

        assert np.allclose(row["CRV_VAR"], coeffs**2)

    def test__getitem__6(self):
        coeffs = np.random.rand(3)
        ids = IdsModel1d(coeffs, 0)
        row = ExtTableRow(None, None, None, None, ids)

        assert np.allclose(row["IDS_COEFFS"], coeffs)

    def test__getitem__7(self):
        coeffs = np.random.rand(2)
        ids = IdsModel1d(coeffs, 0, coeffs**2)
        row = ExtTableRow(None, None, None, None, ids)

        assert np.allclose(row["IDS_VAR"], coeffs**2)

    def test__getitem__8(self):
        coeffs = np.random.rand(2)
        crv = CrvModel1d(coeffs, np.random.rand(2, 2))
        row = ExtTableRow(None, None, None, crv, None)

        assert np.allclose(row["ORIENTATION"], crv.slit_versor)

    def test_rotation(self):
        crv = CrvModel1d(np.zeros(2), np.random.rand(2, 2))
        row = ExtTableRow(None, None, None, crv, None)

        assert np.allclose(crv.rotation, row["ROTATION"])

    def test_reference_lambda(self):
        ids = IdsModel1d(np.zeros(2), np.random.rand())
        row = ExtTableRow(None, None, None, None, ids)

        assert np.allclose(ids.reference_lambda, row["REF_LAMBDA"])

    def test_ids1(self):
        ids = IdsModel1d(np.zeros(2), np.random.rand())
        row = ExtTableRow(None, None, None, None, None)

        assert row.ids is None

    def test_ids2(self):
        ids = IdsModel1d(np.zeros(2), np.random.rand())
        row = ExtTableRow(None, None, None, None, None)

        row.ids = ids
        assert row.ids is ids

    def test_crv1(self, mocker):
        row = ExtTableRow(None, None, None, None, None)

        assert row.crv is None

    def test_crv2(self, mocker):
        crv = MagicMock()
        row = ExtTableRow(None, None, None, crv, None)

        assert row.crv is crv

    def test_center(self, mocker):
        reference_lambda = np.random.rand() * 1000
        ids = IdsModel1d(np.zeros(2), reference_lambda)
        row = ExtTableRow(None, None, None, None, ids)

        center = np.random.rand() * 10
        mocker.patch.object(row, 'compute', return_value=center)

        c = row.center

        args_list = row.compute.call_args_list[0]
        assert np.allclose(reference_lambda, args_list[0][0])
        assert np.allclose(c, center)

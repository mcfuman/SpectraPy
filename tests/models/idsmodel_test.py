#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File idsmodel_test.py
#
# Created on: Feb 10, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from numpy.polynomial import polynomial

from spectrapy.models.instrument import Instrument

from spectrapy.models.idsmodel import IdsModel2d, IdsModel1d
from spectrapy.models.crvmodel import CrvModel2d, CrvModel1d


class TestIdsModel2d(object):
    """Test for IdsModel2d class"""

    def test_init1(self, mocker):
        """
        """

        instrument = Instrument()
        instrument.linear_dispersion = np.random.rand() * 10
        instrument.reference_lambda = np.random.rand() * 10000.

        mocker.patch('spectrapy.models.idsmodel.Instrument.load', return_value=instrument)
        ids = IdsModel2d(1, 1, 1, 'mock')

        assert np.allclose(instrument.linear_dispersion, 1. / ids.inv_dispersion)

    def test_init2(self, mocker):
        """
        """

        instrument = Instrument()
        instrument.linear_dispersion = -1.
        instrument.reference_lambda = np.random.rand() * 10000.

        with pytest.raises(Exception) as invalid_dispersion:
            ids = IdsModel2d(1, 1, 1, instrument)

        assert invalid_dispersion.type is ValueError
        assert invalid_dispersion.value.args[0] == f"Invalid linear dispersion value -1.0"

    def test_reference_lambda(self):
        """
        """
        instrument = Instrument()
        instrument.linear_dispersion = np.random.rand() * 10
        instrument.reference_lambda = np.random.rand() * 10000.

        ids = IdsModel2d(1, 1, 1, instrument)

        assert np.allclose(instrument.reference_lambda, ids.reference_lambda)

    def test_get_delta_dispersion1(self):
        pos = np.random.rand(2)
        xy = np.random.rand(10, 2) * 100

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        instrument.linear_dispersion = np.random.rand() * 10
        instrument.reference_lambda = np.random.rand() * 10000.

        ids = IdsModel2d(1, 1, 1, instrument)

        deltas = ids.get_delta_dispersion(pos, xy)

        assert np.allclose(deltas, xy[:, 0] - pos[0])

    def test_get_delta_dispersion2(self):
        pos = np.random.rand(2) * 0
        xy = np.random.rand(10, 2) * 100

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        instrument.linear_dispersion = np.random.rand() * 10
        instrument.reference_lambda = np.random.rand() * 10000.

        ids = IdsModel2d(1, 1, 1, instrument)

        deltas = ids.get_delta_dispersion(pos, xy)

        assert np.allclose(-deltas, xy[:, 1] - pos[1])

    def test_get_delta_dispersion3(self):
        pos = np.random.rand(2)
        xy = np.random.rand(10, 2) * 100

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        instrument.linear_dispersion = np.random.rand() * 10
        instrument.reference_lambda = np.random.rand() * 10000.

        ids = IdsModel2d(1, 1, 1, instrument)

        deltas = ids.get_delta_dispersion(pos, xy)

        assert np.allclose(-deltas, xy[:, 0] - pos[0])

    def test_get_delta_dispersion4(self):
        pos = np.random.rand(2) * 0
        xy = np.random.rand(10, 2) * 100

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'
        instrument.linear_dispersion = np.random.rand() * 10
        instrument.reference_lambda = np.random.rand() * 10000.

        ids = IdsModel2d(1, 1, 1, instrument)

        deltas = ids.get_delta_dispersion(pos, xy)

        assert np.allclose(deltas, xy[:, 1] - pos[1])

    def test_init3(self):
        """
        """
        with pytest.raises(Exception) as invalid_deg:
            CrvModel2d(1, 1, -1)

        assert invalid_deg.type is ValueError
        assert invalid_deg.value.args[0] == "Invalid ydeg -1 value"

    def test_init4(self):
        """
        """

        crv = CrvModel2d(2, 1, 2)
        assert crv.deg == 2
        assert crv._model[0].shape == (2, 3)
        assert crv._model[1].shape == (2, 3)
        assert crv._model[2].shape == (2, 3)

        assert len(crv._cov) == 3
        assert crv._cov[0].shape == (6, 6)
        assert crv._cov[1].shape == (6, 6)
        assert crv._cov[2].shape == (6, 6)

    def test_init4(self, mocker):
        """
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        mocker.patch('spectrapy.models.crvmodel.Instrument.load', return_value=instrument)
        crv = CrvModel2d(1, 1, 1, 'mock')
        assert crv.dispersion_direction == 'LR'

    def test_get_local_coeffs(self):
        crv = CrvModel2d(2, 1, 2)
        a = np.random.rand(2, 3)
        b = np.random.rand(2, 3)
        c = np.random.rand(2, 3)
        crv._model[0] = a
        crv._model[1] = b
        crv._model[2] = c
        x = 10.5
        y = 7.2
        expected = [polynomial.polyval2d(x, y, a), polynomial.polyval2d(x, y, b), polynomial.polyval2d(x, y, c)]
        coeffs = crv._get_local_coeffs((x, y))

        assert np.allclose(coeffs['values'], expected) is True

    def test_get_local_coeffs2(self):
        crv = CrvModel2d(2, 1, 2)
        a = np.random.rand(2, 3)
        b = np.random.rand(2, 3)
        c = np.random.rand(2, 3)
        crv._model[0] = a
        crv._model[1] = b
        crv._model[2] = c
        va = np.random.rand(6, 6)
        vb = np.random.rand(6, 6)
        vc = np.random.rand(6, 6)
        va += va.T
        vb += vb.T
        vc += vc.T
        crv._cov[0] = va
        crv._cov[1] = vb
        crv._cov[2] = vc

        x = 10.5
        y = 7.2

        coeffs = crv._get_local_coeffs((x, y))
        expected = [polynomial.polyval2d(x, y, a), polynomial.polyval2d(x, y, b), polynomial.polyval2d(x, y, c)]
        assert np.allclose(coeffs['values'], expected) is True

        J = polynomial.polyvander2d(x, y, (1, 2))[0]
        expected = [J.dot(va.dot(J.T)), J.dot(vb.dot(J.T)), J.dot(vc.dot(J.T))]
        assert np.allclose(coeffs['variances'], expected) is True

    def test_get_local_model(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv = CrvModel2d(2, 1, 2, instrument)

        coeffs = np.array([(1., 4), (2, 5), (3, 6)], dtype=IdsModel1d.dtype)

        mocker.patch.object(CrvModel2d, '_get_local_coeffs', return_value=coeffs)
        crv1d = crv.get_local_model(None)

        assert np.allclose(crv1d.coeffs, coeffs['values'])
        assert np.allclose(crv1d._vars, coeffs['variances'])

    def test_compute(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 1, 2, instrument)

        crv1d = CrvModel1d([1, 2], crv2d.rotation, cov=[[2, 3], [3, 4]])

        delta_disp = np.array([-100.0, ])

        mocker.patch.object(CrvModel2d, 'get_local_model', return_value=crv1d)
        value2d, var2d = crv2d.compute(None, delta_disp)
        value1d, var1d = crv1d(delta_disp)

        assert np.allclose(value2d, value1d)
        assert np.allclose(var2d, var1d)

    def test_call(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(2, 1, 2, instrument)

        crv1d = CrvModel1d([1, 4], crv2d.rotation, cov=[[2, 3], [3, 4]])

        delta_disp = np.array([-50.0, ])

        mocker.patch.object(CrvModel2d, 'get_local_model', return_value=crv1d)
        value2d, var2d = crv2d.compute(None, delta_disp)
        value1d, var1d = crv1d(delta_disp)

        assert np.allclose(value2d, value1d)
        assert np.allclose(var2d, var1d)

    def test_refit_trace1(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([2, 1])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata += pos[0]
        ydata += pos[1]

        crv1d = crv2d.refit_trace(pos, xdata, ydata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit_trace2(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([1, 2])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata -= pos[1]
        ydata += pos[0]

        crv1d = crv2d.refit_trace(pos, ydata, -xdata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit_trace3(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([1, 2])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata -= pos[0]
        ydata -= pos[1]

        crv1d = crv2d.refit_trace(pos, -xdata, -ydata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit_trace4(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([1, 2])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata += pos[1]
        ydata -= pos[0]

        crv1d = crv2d.refit_trace(pos, -ydata, xdata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit1(self, mocker):

        crv = CrvModel2d(4, 2, 1)

        model = np.random.rand(4, 3, 2)
        cov = np.random.rand(4, 6, 6)

        crv.model = model
        crv.covariance = cov

        results = ((np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)))

        mocker.patch('spectrapy.models.crvmodel.polyfit2d', side_effect=results)
        crv.refit(None, None, np.zeros((2, 4)))

        assert np.allclose(crv.model[0], results[0][0])
        assert np.allclose(crv.model[1], results[1][0])
        assert np.allclose(crv.model[2], results[2][0])
        assert np.allclose(crv.model[3], results[3][0])

        assert np.allclose(crv.covariance[0], results[0][3])
        assert np.allclose(crv.covariance[1], results[1][3])
        assert np.allclose(crv.covariance[2], results[2][3])
        assert np.allclose(crv.covariance[3], results[3][3])

    def test_refit2(self, mocker):

        crv = CrvModel2d(4, 2, 1)

        model = np.random.rand(4, 3, 2)
        cov = np.random.rand(4, 6, 6)

        crv.model = model
        crv.covariance = cov

        results = ((np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)))

        mocker.patch('spectrapy.models.crvmodel.polyfit2d', side_effect=results)
        crv.refit(None, None, np.zeros((2, 4)), np.zeros((2, 4)))

        assert np.allclose(crv.model[0], results[0][0])
        assert np.allclose(crv.model[1], results[1][0])
        assert np.allclose(crv.model[2], results[2][0])
        assert np.allclose(crv.model[3], results[3][0])

        assert np.allclose(crv.covariance[0], results[0][3])
        assert np.allclose(crv.covariance[1], results[1][3])
        assert np.allclose(crv.covariance[2], results[2][3])
        assert np.allclose(crv.covariance[3], results[3][3])

    def test_load1(self, mocker):

        model = ['LR', np.random.randn(2, 3, 4)]

        mocker.patch('spectrapy.models.crvmodel.Model2d.load', return_value=model)
        crv = CrvModel2d.load('mock')

        assert crv.dispersion_direction == model[0]
        assert np.allclose(crv.model, model[1])
        assert np.allclose(crv.covariance, np.zeros((12, 12)))

    def test_load2(self, mocker):

        model = ['LR', np.random.randn(2, 3, 4), np.random.randn(2, 12, 12)]

        mocker.patch('spectrapy.models.crvmodel.Model2d.load', return_value=model)
        crv = CrvModel2d.load('mock')

        assert crv.dispersion_direction == model[0]
        assert np.allclose(crv.model, model[1])
        assert np.allclose(crv.covariance, model[2])

    def test_get_dispersion_component1(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[0], component)

    def test_get_dispersion_component2(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[0], component)

    def test_get_dispersion_component3(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[1], component)

    def test_get_dispersion_component4(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[1], component)

    def test_load1(self, mocker):
        model = ['LR', np.random.randn(2, 3, 4), 10.4]

        mocker.patch('spectrapy.models.idsmodel.Model2d.load', return_value=model)
        ids = IdsModel2d.load('mock')

        assert ids.dispersion_direction == model[0]
        assert np.allclose(ids.model, model[1])
        assert np.allclose(ids.reference_lambda, model[2])
        assert np.allclose(ids.covariance, np.zeros((12, 12)))

    def test_load2(self, mocker):
        cov = np.random.rand(2, 12, 12)
        model = ['LR', np.random.rand(2, 3, 4), 10.4, [cov[0].dot(cov[0].T), cov[1].dot(cov[1].T)]]

        mocker.patch('spectrapy.models.idsmodel.Model2d.load', return_value=model)
        ids = IdsModel2d.load('mock')

        assert ids.dispersion_direction == model[0]
        assert np.allclose(ids.model, model[1])
        assert np.allclose(ids.reference_lambda, model[2])
        assert np.allclose(ids.covariance, model[3])

    def test_writeto1(self, mocker, tmpdir):
        mocker.patch('os.path.isfile', return_value=True)
        model = IdsModel2d(0, 0, 0)

        tmpfile = tmpdir.join('tmpname.dat')
        with pytest.raises(Exception) as invalid_file:
                model.writeto(str(tmpfile), overwrite=False)

        assert invalid_file.type is IOError
            # Rivisita con il mocker
            # assert invalid_file.value.args[0] == f"{str(tmpfile)} already exists!"

    def test_writeto2(self, mocker, tmpdir):
        mocker.patch('os.path.isfile', return_value=True)
        model = IdsModel2d(0, 0, 0)

        tmpfile = tmpdir.join('tmpname.dat')
        with pytest.raises(Exception) as invalid_model:
            model.writeto(str(tmpfile), overwrite=True)

        assert invalid_model.type is ArithmeticError
        assert invalid_model.value.args[0] == "Invalid dispersion direction"

    def test_refit_slit(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        instrument.reference_lambda = 12010.
        instrument.linear_dispersion = 1.0

        poly1d = np.poly1d((-0.01, 0.3, 4.2))

        lambdas = np.linspace(11000., 13000., 20)
        delta_disp = poly1d(lambdas - instrument.reference_lambda)

        model = IdsModel2d(2, 0, 0, instrument)

        new_model = model.refit_slit(lambdas, delta_disp)

        assert np.allclose(new_model.coeffs, poly1d)
        assert np.allclose(new_model.reference_lambda, instrument.reference_lambda)

    def test_get_local_model(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        instrument.reference_lambda = 12010.0
        instrument.linear_dispersion = 1.0

        ids = IdsModel2d(2, 1, 2, instrument)

        coeffs = np.array([(1, 4), (2, 5), (3, 6)], dtype=IdsModel1d.dtype)

        mocker.patch.object(IdsModel2d, '_get_local_coeffs', return_value=coeffs)
        ids1d = ids.get_local_model(None)

        assert np.allclose(ids1d.coeffs, coeffs['values'])
        assert np.allclose(ids1d._vars, coeffs['variances'])
        assert np.allclose(ids1d.reference_lambda, instrument.reference_lambda)

    def test_compute(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'
        instrument.dispersion_direction = 'LR'
        instrument.reference_lambda = 12010.0
        instrument.linear_dispersion = 1.0

        ids2d = IdsModel2d(2, 1, 2, instrument)

        ids1d = IdsModel1d([1, 2], instrument.reference_lambda, cov=[[2, 3], [3, 4]])

        lambdas = np.array([13000.5])

        mocker.patch.object(IdsModel2d, 'get_local_model', return_value=ids1d)
        value2d, var2d = ids2d.compute(None, lambdas)
        value1d, var1d = ids1d(lambdas)

        assert np.allclose(value2d, value1d)
        assert np.allclose(var2d, var1d)

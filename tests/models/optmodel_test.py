#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File optmodel_test.py
#
# Created on: Nov 09, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import tempfile
import pytest
from pytest_mock import mocker

from spectrapy.models.optmodel import OptModel2d

from spectrapy.models.instrument import Instrument
from spectrapy.mask.mask import Mask


class TestOptModel2d(object):
    """Test for OptModel2d class"""

    def test_init1(self):
        """
        """
        with pytest.raises(Exception) as invalid_deg:
            OptModel2d(0, 1)

        assert invalid_deg.type is ValueError
        assert invalid_deg.value.args[0] == "Invalid Optical Model x/y degs"

    def test_init2(self):
        """
        """
        with pytest.raises(Exception) as invalid_deg:
            OptModel2d(1, 0)

        assert invalid_deg.type is ValueError
        assert invalid_deg.value.args[0] == "Invalid Optical Model x/y degs"

    def test_init3(self):
        """
        """

        opt = OptModel2d(1, 2)
        assert len(opt._model) == 2
        assert opt._model[0].shape == (2, 3)
        assert opt._model[1].shape, (2, 3)

        assert len(opt._cov) == 2
        assert opt._cov[0].shape == (6, 6)
        assert opt._cov[1].shape == (6, 6)

        assert opt.dispersion_direction == 'LR'

    def test_init4(self):
        """
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'
        instrument.xpixels = 1000
        instrument.ypixels = 2000

        opt = OptModel2d(1, 1, instrument)
        assert opt.dispersion_direction == 'RL'
        assert opt.xshift == pytest.approx(500)
        assert opt.yshift == pytest.approx(1000)
        assert opt.xscale == pytest.approx(0.0)
        assert opt.yscale == pytest.approx(0.0)

    def test_init5(self):
        """
        """
        instrument = Instrument()

        instrument.dispersion_direction = 'LR'
        instrument.xpixels = 1000
        instrument.ypixels = 2000
        instrument.pixel_scale = 0.18

        opt = OptModel2d(1, 1, instrument, (20., 40.))
        assert opt.dispersion_direction == 'LR'
        assert opt.xshift == pytest.approx(500)
        assert opt.yshift == pytest.approx(1000)
        assert opt.xscale == pytest.approx(20. / 0.18)
        assert opt.yscale == pytest.approx(40. / 0.18)

    def test_init6(self, mocker):
        """
        """
        instrument = Instrument()

        instrument.dispersion_direction = 'LR'
        instrument.xpixels = 1000
        instrument.ypixels = 2000
        instrument.pixel_scale = 0.18

        mocker.patch('spectrapy.models.optmodel.Instrument.load', return_value=instrument)
        opt = OptModel2d(1, 1, instrument='mock')

        assert opt.dispersion_direction == 'LR'
        assert opt.xshift == pytest.approx(500)
        assert opt.yshift == pytest.approx(1000)

    def test_init7(self, mocker):
        """
        """
        instrument = Instrument()

        instrument.dispersion_direction = 'LR'
        instrument.xpixels = 1000
        instrument.ypixels = 2000
        instrument.pixel_scale = 0.18

        mocker.patch('spectrapy.models.optmodel.Instrument.load', return_value=instrument)
        opt = OptModel2d(1, 1, 'mock', (20., 40.))

        assert opt.dispersion_direction == 'LR'
        assert opt.xshift == pytest.approx(500)
        assert opt.yshift == pytest.approx(1000)
        assert opt.xscale == pytest.approx(20. / 0.18)
        assert opt.yscale == pytest.approx(40. / 0.18)

    def test_xmodel(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[0] = model
        assert np.allclose(opt.xmodel, model)

    def test_ymodel(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[1] = model
        assert np.allclose(opt.ymodel, model)

    def test_xshift(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[0] = model
        assert opt.xshift == pytest.approx(model[0][0])

    def test_xshift2(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[0] = model
        assert opt.xshift == pytest.approx(model[0][0])
        opt.xshift = model[1][1]
        assert opt.xshift == pytest.approx(model[1][1])

    def test_yshift(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[1] = model
        assert opt.yshift == pytest.approx(model[0][0])

    def test_yshift2(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[1] = model
        assert opt.yshift == pytest.approx(model[0][0])
        opt.yshift = model[1][1]
        assert opt.yshift == pytest.approx(model[1][1])

    def test_shift1(self):
        opt = OptModel2d(2, 1)
        xmodel = np.random.rand(3, 2)
        ymodel = np.random.rand(3, 2)
        opt._model[0] = xmodel
        opt._model[1] = ymodel
        assert np.allclose(opt.shift, (xmodel[0][0], ymodel[0][0]))

    def test_shift2(self):
        opt = OptModel2d(2, 1)
        with pytest.raises(Exception) as invalid_shift:
            opt.shift = 7

        assert invalid_shift.type is TypeError
        # FIXME!!!
        #assert invalid_shift.value.args[0] == "Invalid shift"

    def test_shift3(self):
        opt = OptModel2d(2, 1)
        with pytest.raises(Exception) as invalid_shift:
            opt.shift = [7, 2, 3]

        assert invalid_shift.type is TypeError
        assert invalid_shift.value.args[0] == "Invalid shift type"

    def test_shift4(self):
        opt = OptModel2d(2, 1)
        with pytest.raises(Exception) as invalid_shift:
            opt.shift = np.array([[7, 2], [2, 3]])

        assert invalid_shift.type is TypeError
        assert invalid_shift.value.args[0] == "Invalid shift type"

    def test_shift5(self):
        opt = OptModel2d(2, 1)
        opt.shift = (3.2, -2)
        assert np.allclose(opt.shift, (3.2, -2.0))

    def test_xscale1(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[0] = model
        assert opt.xscale == pytest.approx(model[1][0])

    def test_xscale2(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[0] = model
        assert opt.xscale == pytest.approx(model[1][0])
        opt.xscale = model[0][0]
        assert opt.xscale == pytest.approx(model[0][0])

    def test_yscale1(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[1] = model
        assert opt.yscale == pytest.approx(model[0][1])

    def test_yscale2(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[1] = model
        assert opt.yscale == pytest.approx(model[0][1])
        opt.yscale = model[0][0]
        assert opt.yscale == pytest.approx(model[0][0])

    def test_flipx(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[0] = np.array(model)
        assert opt.xscale == pytest.approx(model[1][0])
        opt.flipx()
        assert opt.xscale == pytest.approx(-1.0 * model[1][0])

    def test_flipy(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt._model[1] = np.array(model)
        assert opt.yscale == pytest.approx(model[0][1])
        opt.flipy()
        assert opt.yscale == pytest.approx(-1.0 * model[0][1])

    def test_sign1(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt.xscale = 7.
        opt.yscale = 5.
        assert np.allclose(opt.sign, (1, 1))

    def test_sign2(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt.xscale = -7.
        opt.yscale = 5.
        assert np.allclose(opt.sign, (-1, 1))

    def test_sign3(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt.xscale = 7.
        opt.yscale = -5.
        assert np.allclose(opt.sign, (1, -1))

    def test_sign4(self):
        opt = OptModel2d(2, 1)
        model = np.random.rand(3, 2)
        opt.xscale = -7.
        opt.yscale = -5.
        assert np.allclose(opt.sign, (-1, -1))

    def test_call1(self):
        opt = OptModel2d(2, 1)
        xmodel = np.random.rand(3, 2)
        ymodel = np.random.rand(3, 2)

        opt._model[0] = xmodel
        opt._model[1] = ymodel

        x, y = np.random.rand(2)
        value1, var1 = opt.compute((x, y))
        value2, var2 = opt.__call__((x, y))

        assert np.allclose(value1, value2)
        assert np.allclose(var1, var2)

    def test_compute1(self):
        opt = OptModel2d(2, 1)
        xmodel = np.random.rand(3, 2)
        ymodel = np.random.rand(3, 2)

        opt._model[0] = xmodel
        opt._model[1] = ymodel

        x, y = np.random.rand(2)
        value, var = opt.compute((x, y))

        xexp = np.dot([1, x, x * x], xmodel.dot([1, y]))
        assert xexp == pytest.approx(value[0])

        yexp = np.dot([1, x, x * x], ymodel.dot([1, y]))
        assert yexp == pytest.approx(value[1])

        assert var[0] == pytest.approx(0.0)
        assert var[1] == pytest.approx(0.0)

    def test_compute2(self):
        opt = OptModel2d(2, 1)
        xmodel = np.random.rand(3, 2)
        ymodel = np.random.rand(3, 2)

        xcov = np.random.rand(6, 6)
        xcov = xcov.dot(xcov.T)

        ycov = np.random.rand(6, 6)
        ycov = ycov.dot(ycov.T)

        opt._model[0] = xmodel
        opt._model[1] = ymodel

        opt._cov[0] = xcov
        opt._cov[1] = ycov

        x, y = np.random.rand(2)
        value, covariance = opt.compute((x, y))

        xexp = np.dot([1, x, x * x], xmodel.dot([1, y]))
        assert xexp == pytest.approx(value[0])

        yexp = np.dot([1, x, x * x], ymodel.dot([1, y]))
        assert yexp == pytest.approx(value[1])

        # VARIANCE COMPUTE CHECK
        J = np.array([1.0, y, x, x * y, x * x, x * x * y])
        assert J.dot(xcov.dot(J)) == covariance[0]
        assert J.dot(ycov.dot(J)) == covariance[1]

    def test_load1(self):
        opt = OptModel2d(2, 1)
        xmodel = np.random.rand(3, 2)
        ymodel = np.random.rand(3, 2)
        xcov = np.random.rand(6, 6)
        ycov = np.random.rand(6, 6)

        opt._model[0] = xmodel
        opt._model[1] = ymodel
        opt._cov[0] = xcov
        opt._cov[1] = ycov

        opt.dispersion_direction = 'LR'

        tmp = tempfile.NamedTemporaryFile(delete=True)
        try:
            opt.writeto(tmp.name, overwrite=True)
        finally:
            opt2 = OptModel2d.load(tmp.name)
            tmp.close()

        assert np.allclose(opt2._model[0], xmodel)
        assert np.allclose(opt2._model[1], ymodel)
        assert np.allclose(opt2._cov[0], xcov)
        assert np.allclose(opt2._cov[1], ycov)
        assert opt2.dispersion_direction == 'LR'

    def test_load2(self, mocker):

        model = ['LR', np.random.randn(2, 3, 4)]

        mocker.patch('spectrapy.models.optmodel.Model2d.load', return_value=model)
        opt = OptModel2d.load('mock')

        assert opt.dispersion_direction == model[0]
        assert np.allclose(opt.xmodel, model[1][0])
        assert np.allclose(opt.ymodel, model[1][1])
        assert np.allclose(opt._cov[0], np.zeros((12, 12)))
        assert np.allclose(opt._cov[1], np.zeros((12, 12)))

    def test_load3(self, mocker):

        model = ['LR', np.random.randn(2, 3, 4), np.random.randn(2, 12, 12)]

        mocker.patch('spectrapy.models.optmodel.Model2d.load', return_value=model)
        opt = OptModel2d.load('mock')

        assert opt.dispersion_direction == model[0]
        assert np.allclose(opt.xmodel, model[1][0])
        assert np.allclose(opt.ymodel, model[1][1])
        assert np.allclose(opt._cov[0], model[2][0])
        assert np.allclose(opt._cov[1], model[2][1])

    def test_refit(self, mocker):

        opt = OptModel2d(2, 1)
        xmodel = np.random.rand(3, 2)
        ymodel = np.random.rand(3, 2)
        xcov = np.random.rand(6, 6)
        ycov = np.random.rand(6, 6)

        opt._model[0] = xmodel
        opt._model[1] = ymodel
        opt._cov[0] = xcov
        opt._cov[1] = ycov

        xresults = (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6))
        yresults = (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6))

        mocker.patch('spectrapy.models.optmodel.polyfit2d', side_effect=(xresults, yresults))
        opt.refit(np.zeros((3, 2)), np.zeros((3, 2)))

        assert np.allclose(opt.xmodel, xresults[0])
        assert np.allclose(opt.xmodel, yresults[0]) is False

        assert np.allclose(opt.ymodel, yresults[0])
        assert np.allclose(opt.ymodel, xresults[0]) is False

        assert np.allclose(opt._cov[0], xresults[-1])
        assert np.allclose(opt._cov[0], yresults[-1]) is False

        assert np.allclose(opt._cov[1], yresults[-1])
        assert np.allclose(opt._cov[1], xresults[-1]) is False

    def test_slit_orientation1(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'LR'
        assert np.allclose(opt.slit_versor, [0, -1])

    def test_slit_orientation2(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'RL'
        assert np.allclose(opt.slit_versor, [0, 1])

    def test_slit_orientation3(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'BU'
        assert np.allclose(opt.slit_versor, [1, 0])

    def test_slit_orientation4(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'UB'
        assert np.allclose(opt.slit_versor, [-1, 0])

    def test_dispersion_versor1(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'LR'
        assert np.allclose(opt.dispersion_versor, [1, 0])

    def test_dispersion_versor2(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'RL'
        assert np.allclose(opt.dispersion_versor, [-1, 0])

    def test_dispersion_versor3(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'BU'
        assert np.allclose(opt.dispersion_versor, [0, 1])

    def test_dispersion_versor4(self):
        opt = OptModel2d(2, 1)
        opt.dispersion_direction = 'UB'
        assert np.allclose(opt.dispersion_versor, [0, -1])





#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File model_test.py
#
# Created on: Nov 09, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np

import pytest
from pytest_mock import mocker
from spectrapy.models.model import Model2d, Model1d


class TestModel2d(object):
    """Test for OptModel class"""

    def test_init(self):
        """
        """
        m = Model2d()
        assert m._model is None
        assert m._cov is None
        with pytest.raises(Exception) as no_dispersion:
            m.rotation

        assert no_dispersion.type is AttributeError
        assert no_dispersion.value.args[0] == f"Dispersion direction not set yet!"

    def test_writeto1(self, mocker, tmpdir):
        """
        """
        mocker.patch('os.path.isfile')
        mocker.return_value = True
        m = Model2d()

        tmpfile = tmpdir.join('tmpname.dat')

        with pytest.raises(Exception) as file_exists:
            m.writeto(str(tmpfile), overwrite=False)

        assert file_exists.type is IOError
        assert file_exists.value.args[0] == f"Ouput file {tmpfile} already exists!"

    def test_writeto2(self, mocker, tmpdir):
        """
        """
        mocker.patch('os.path.isfile')
        mocker.return_value = True
        m = Model2d()

        tmpfile = tmpdir.join('tmpname.dat')
        with pytest.raises(Exception) as no_model:
            m.writeto(str(tmpfile), overwrite=True)

        assert no_model.type is ArithmeticError
        assert no_model.value.args[0] == "Invalid dispersion direction"

    def test_writeto3(self, mocker, tmpdir):
        """
        """
        mocker.patch('os.path.isfile')
        mocker.return_value = False
        m = Model2d()

        tmpfile = tmpdir.join('tmpname.dat')
        with pytest.raises(Exception) as no_model:
            m.writeto(str(tmpfile), overwrite=False)

        assert no_model.type is IOError
        assert no_model.value.args[0] == f"Ouput file {tmpfile} already exists!"

    def test_writeto4(self, mocker, tmpdir):
        """
        """
        mocker.patch('os.path.isfile')
        mocker.return_value = False
        m = Model2d()
        m.model = np.zeros((3, 2, 2))
        m.ones = np.zeros((3, 4, 4))

        tmpfile = tmpdir.join('tmpname.dat')
        with pytest.raises(Exception) as file_exists:
            m.writeto(str(tmpfile), overwrite=False)

        assert file_exists.type is IOError
        assert file_exists.value.args[0] == f"Ouput file {tmpfile} already exists!"

    def test_load1(self, tmpdir):
        """
        """
        m = Model2d()
        m.model = [np.array([[1, 2], [3, 4]])]
        m.covariance = [np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 16]])]
        m.dispersion_direction = 'LR'

        tmpfile = tmpdir.join('tmpname.dat')
        m.writeto(str(tmpfile), overwrite=True)

        n = Model2d.load(str(tmpfile))

        assert m.dispersion_direction == n[0]
        assert np.allclose(m.model, n[1])
        assert np.allclose(m.covariance, n[2])

    def test_dispersion_direction1(self):
        with pytest.raises(Exception) as no_dispersion:
            m = Model2d()
            m.dispersion_direction

        assert no_dispersion.type is ArithmeticError
        assert no_dispersion.value.args[0] == "Invalid dispersion direction"

    def test_dispersion_direction2(self):
        m = Model2d()
        m._rotation = np.array([[1, 0], [0, 1]])
        assert m.dispersion_direction == 'LR'

    def test_dispersion_direction3(self):
        m = Model2d()
        m._rotation = np.array([[-1, 0], [0, -1]])
        assert m.dispersion_direction == 'RL'

    def test_dispersion_direction4(self):
        m = Model2d()
        m._rotation = np.array([[0, 1], [-1, 0]])
        assert m.dispersion_direction == 'UB'

    def test_dispersion_direction5(self):
        m = Model2d()
        m._rotation = np.array([[0, -1], [1, 0]])
        assert m.dispersion_direction == 'BU'

    def test_dispersion_direction6(self):
        m = Model2d()
        m.dispersion_direction = 'LR'
        assert np.allclose(m.rotation, [[1, 0], [0, 1]])

    def test_dispersion_direction7(self):
        m = Model2d()
        m.dispersion_direction = 'RL'
        assert np.allclose(m.rotation, [[-1, 0], [0, -1]])

    def test_dispersion_direction8(self):
        m = Model2d()
        m.dispersion_direction = 'UB'
        assert np.allclose(m.rotation, [[0, 1], [-1, 0]])

    def test_dispersion_direction9(self):
        m = Model2d()
        m.dispersion_direction = 'BU'
        assert np.allclose(m.rotation, [[0, -1], [1, 0]])

    def test_dispersion_direction10(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_dispersion:
            m.dispersion_direction = 'X'

        assert invalid_dispersion.type is ValueError
        assert invalid_dispersion.value.args[0] == "Invalid dispersion direction"

    def test_model1(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_model:
            m.model = []

        assert invalid_model.type is TypeError
        assert invalid_model.value.args[0] == "Empty model"

    def test_model2(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_model:
            m.model = 7

        assert invalid_model.type is TypeError
        #assert invalid_model.value.args[0] == ""

    def test_model3(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_model:
            m.model = [np.zeros(7)]

        assert invalid_model.type is TypeError
        assert invalid_model.value.args[0] == "Invalid model shape"

    def test_model4(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_model:
            m.model = [np.zeros(7, 2, 3)]

        assert invalid_model.type is TypeError
        #assert invalid_model.value.args[0] == "data type not understood"

    def test_model5(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_model:
            m.model = [np.zeros(3, 3), np.zeros(3, 3), np.zeros(3, 2)]

        assert invalid_model.type is TypeError
        #assert invalid_model.value.args[0] == "data type not understood"

    def test_model6(self):
        m = Model2d()
        model = [np.random.rand(3, 3), np.random.rand(3, 3), np.random.rand(3, 3)]
        m.model = model

        assert np.allclose(m.model[0], model[0])
        assert np.allclose(m.model[1], model[1])
        assert np.allclose(m.model[2], model[2])
        assert len(m.model) == 3

    def test_red_direction1(self):
        m = Model2d()
        m.dispersion_direction = 'LR'
        assert m.red_direction == 1
        assert m.blue_direction == -1

    def test_red_direction2(self):
        m = Model2d()
        m.dispersion_direction = 'BU'
        assert m.red_direction == 1
        assert m.blue_direction == -1

    def test_red_direction3(self):
        m = Model2d()
        m.dispersion_direction = 'RL'
        assert m.red_direction == -1
        assert m.blue_direction == 1

    def test_red_direction4(self):
        m = Model2d()
        m.dispersion_direction = 'UB'
        assert m.red_direction == -1
        assert m.blue_direction == 1

    def test_covariance1(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = 7

        assert invalid_covariance.type is AttributeError
        assert invalid_covariance.value.args[0] == "Model not yet set"

    def test_covariance2(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = [np.zeros(7)]

        assert invalid_covariance.type is AttributeError
        assert invalid_covariance.value.args[0] == "Model not yet set"

    def test_covariance3(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = [np.zeros(7, 2, 3)]

        assert invalid_covariance.type is TypeError
        #assert invalid_covariance.value.args[0] == "data type not understood"

    def test_covariance4(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = [np.zeros(3, 3), np.zeros(3, 3), np.zeros(3, 2)]

        assert invalid_covariance.type is TypeError
        #assert invalid_covariance.value.args[0] == "data type not understood"

    def test_covariance5(self):
        m = Model2d()
        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = [np.random.rand(4, 4), np.random.rand(4, 4), np.random.rand(4, 4)]

        assert invalid_covariance.type is AttributeError
        assert invalid_covariance.value.args[0] == "Model not yet set"

    def test_covariance6(self):
        m = Model2d()
        m.model = [np.zeros((2, 2))] * 3
        covariance = [np.random.rand(4, 4), np.random.rand(4, 4), np.random.rand(4, 4)]
        m.covariance = covariance

        assert np.allclose(m.covariance[0], covariance[0])
        assert np.allclose(m.covariance[1], covariance[1])
        assert np.allclose(m.covariance[2], covariance[2])
        assert len(m.covariance) == 3

    def test_covariance7(self):
        m = Model2d()
        model = [np.random.rand(2, 3)] * 3
        m.model = model
        m.covariance = []
        assert np.allclose(m.covariance[0], np.zeros((6, 6)))
        assert np.allclose(m.covariance[1], np.zeros((6, 6)))
        assert np.allclose(m.covariance[2], np.zeros((6, 6)))
        assert len(m.covariance) == 3

    def test_covariance8(self):
        m = Model2d()
        model = [np.random.rand(2, 3)] * 3
        covariance = [np.random.rand(6, 6)] * 2
        m.model = model

        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = covariance

        assert invalid_covariance.type is TypeError
        assert invalid_covariance.value.args[0] == "Invalid covariance matrixes number"

    def test_covariance9(self):
        m = Model2d()
        model = [np.random.rand(2, 3)] * 3
        covariance = [np.random.rand(5, 5)] * 3
        m.model = model

        with pytest.raises(Exception) as invalid_covariance:
            m.covariance = covariance

        assert invalid_covariance.type is TypeError
        assert invalid_covariance.value.args[0] == "Invalid covariance matrix shape"

    def test_xdeg1(self):
        m = Model2d()
        with pytest.raises(Exception) as no_model:
            m.xdeg

        assert no_model.type is AttributeError
        assert no_model.value.args[0] == "Model missing"

    def test_xdeg2(self):
        m = Model2d()

        m.model = [np.random.rand(2, 3)] * 3
        assert m.xdeg == 1

    def test_xdeg3(self):
        m = Model2d()

        model = np.random.rand(3, 3, 4)
        m.model = model.copy()
        assert m.xdeg == 2

        with pytest.raises(Exception) as no_model:
            m.xdeg = -1

        assert no_model.type is ValueError
        assert no_model.value.args[0] == "Invalid negative deg"

    def test_xdeg4(self):
        m = Model2d()

        model = np.random.rand(3, 3, 4)
        m.model = model.copy()
        assert m.xdeg == 2

        m.xdeg = 2
        assert np.allclose(m.model, model)

    def test_xdeg5(self):
        m = Model2d()

        model = np.random.rand(3, 3, 4)
        m.model = model.copy()
        assert m.xdeg == 2

        m.xdeg = 1
        for i in range(3):
            assert np.allclose(m.model[i], model[i][0:2, :])

    def test_xdeg6(self):
        m = Model2d()

        model = np.random.rand(3, 3, 4)
        m.model = model.copy()
        assert m.xdeg == 2

        m.xdeg = 3
        for i in range(3):
            assert np.allclose(m.model[i][0:3, :], model[i])
            assert np.allclose(m.model[i][3, :], np.zeros(4))

    def test_ydeg1(self):
        m = Model2d()
        with pytest.raises(Exception) as no_model:
            m.ydeg

        assert no_model.type is AttributeError
        assert no_model.value.args[0] == "Model missing"

    def test_ydeg2(self):
        m = Model2d()

        m.model = [np.random.rand(2, 3)] * 3
        assert m.ydeg == 2

    def test_ydeg3(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.ydeg == 3

        with pytest.raises(Exception) as no_model:
            m.ydeg = -1

        assert no_model.type is ValueError
        assert no_model.value.args[0] == "Invalid negative deg"

    def test_ydeg4(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.ydeg == 3

        m.ydeg = 3
        assert np.allclose(m.model, model)

    def test_ydeg5(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.ydeg == 3

        m.ydeg = 2
        for i in range(3):
            assert np.allclose(m.model[i], model[i][:, 0:3])

    def test_ydeg6(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.ydeg == 3

        m.ydeg = 4
        for i in range(3):
            assert np.allclose(m.model[i][:, 0:4], model[i])
            assert np.allclose(m.model[i][:, 4], np.zeros(2))

    def test_deg1(self):
        m = Model2d()
        with pytest.raises(Exception) as no_model:
            m.deg

        assert no_model.type is AttributeError
        assert no_model.value.args[0] == "Model missing"

    def test_deg2(self):
        m = Model2d()

        m.model = [np.random.rand(2, 3)] * 3
        assert m.deg == 2

    def test_deg3(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.deg == 2

        with pytest.raises(Exception) as no_model:
            m.deg = -1

        assert no_model.type is ValueError
        assert no_model.value.args[0] == "Invalid negative deg"

    def test_deg4(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.deg == 2

        m.deg = 2
        assert np.allclose(m.model, model)

    def test_deg5(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        cov = np.random.rand(3, 8, 8)
        m.model = model.copy()
        m.covariance = cov
        assert m.deg == 2

        m.deg = 1
        for i in range(1, 3):
            assert np.allclose(m.model[i - 1], model[i])
            assert np.allclose(m.covariance[i - 1], cov[i])
        assert len(m.model) == 2

    def test_deg6(self):
        m = Model2d()

        model = np.random.rand(3, 2, 4)
        m.model = model.copy()
        assert m.deg == 2

        m.deg = 3
        for i in range(1, 4):
            assert np.allclose(m.model[i], model[i - 1])

        assert np.allclose(m.model[0], np.zeros((2, 4)))
        assert len(m.model) == 4


class TestModel1d(object):
    """Test for Model1d class"""

    def test_init1(self):
        """
        """
        m = Model1d([1., 2.])
        assert np.allclose(m.coeffs, [1., 2.])
        assert m.covariance is None

    def test_init2(self):
        """
        """
        with pytest.raises(Exception) as invalid_covariance:
            m = Model1d([1., 2.], np.ones((3, 3)))

        assert invalid_covariance.type is TypeError
        assert invalid_covariance.value.args[0] == "Coefficients length and covariance sizes didn't match"

    def test_init3(self):
        """
        """
        with pytest.raises(Exception) as invalid_covariance:
            m = Model1d([1., 2., 3], np.ones((2, 2)))

        assert invalid_covariance.type is TypeError
        assert invalid_covariance.value.args[0] == "Coefficients length and covariance sizes didn't match"

    def test_init4(self):
        """
        """
        cov = np.ones((2, 2))
        cov[1][0] += 1
        with pytest.raises(Exception) as invalid_covariance:
            m = Model1d([1., 2.], cov)

        assert invalid_covariance.type is TypeError
        assert invalid_covariance.value.args[0] == "Covariance is not simmetric"

    def test_init5(self):
        """
        """
        cov = np.ones((2, 2))
        cov[1][0] = 2
        cov[0][1] = 2
        m = Model1d([1., 2.], cov)
        assert np.allclose(m.coeffs, [1., 2.])
        assert np.allclose(m.covariance, cov)

    def test_init6(self):
        """
        """
        m = Model1d([1., 2.], cov=[3, 4.])

        assert np.allclose(m.coeffs, [1., 2.])
        assert np.allclose(m.covariance, [[3., 0.], [0., 4.]])
        assert np.allclose(m.variance, [3., 4.])

    def test_variance(self):
        """
        """
        cov = np.ones((2, 2))
        cov[1][1] = 3

        m = Model1d([1., 2.], cov)
        assert np.allclose(m.variance, [1, 3])


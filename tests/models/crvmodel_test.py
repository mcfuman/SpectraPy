#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File crvmodel_test.py
#
# Created on: Nov 12, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import numpy as np
import pytest
from pytest_mock import mocker
from numpy.polynomial import polynomial

from spectrapy.models.instrument import Instrument

from spectrapy.models.crvmodel import CrvModel2d, CrvModel1d


class TestCrvModel2d(object):
    """Test for CrvModel2d class"""

    def test_init1(self):
        """
        """
        with pytest.raises(Exception) as invalid_deg:
            CrvModel2d(-1, 1, 1)

        assert invalid_deg.type is ValueError
        assert invalid_deg.value.args[0] == "Invalid deg -1 value"

    def test_init2(self):
        """
        """
        with pytest.raises(Exception) as invalid_deg:
            CrvModel2d(1, -1, 1)

        assert invalid_deg.type is ValueError
        assert invalid_deg.value.args[0] == "Invalid xdeg -1 value"

    def test_init3(self):
        """
        """
        with pytest.raises(Exception) as invalid_deg:
            CrvModel2d(1, 1, -1)

        assert invalid_deg.type is ValueError
        assert invalid_deg.value.args[0] == "Invalid ydeg -1 value"

    def test_init4(self):
        """
        """

        crv = CrvModel2d(2, 1, 2)
        assert crv.deg == 2
        assert crv._model[0].shape == (2, 3)
        assert crv._model[1].shape == (2, 3)
        assert crv._model[2].shape == (2, 3)

        assert len(crv._cov) == 3
        assert crv._cov[0].shape == (6, 6)
        assert crv._cov[1].shape == (6, 6)
        assert crv._cov[2].shape == (6, 6)

    def test_init4(self, mocker):
        """
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        mocker.patch('spectrapy.models.crvmodel.Instrument.load', return_value=instrument)
        crv = CrvModel2d(1, 1, 1, 'mock')
        assert crv.dispersion_direction == 'LR'

    def test_get_local_coeffs(self):
        crv = CrvModel2d(2, 1, 2)
        a = np.random.rand(2, 3)
        b = np.random.rand(2, 3)
        c = np.random.rand(2, 3)
        crv._model[0] = a
        crv._model[1] = b
        crv._model[2] = c
        x = 10.5
        y = 7.2
        expected = [polynomial.polyval2d(x, y, a), polynomial.polyval2d(x, y, b), polynomial.polyval2d(x, y, c)]
        coeffs = crv._get_local_coeffs((x, y))

        assert np.allclose(coeffs['values'], expected) is True

    def test_get_local_coeffs2(self):
        crv = CrvModel2d(2, 1, 2)
        a = np.random.rand(2, 3)
        b = np.random.rand(2, 3)
        c = np.random.rand(2, 3)
        crv._model[0] = a
        crv._model[1] = b
        crv._model[2] = c
        va = np.random.rand(6, 6)
        vb = np.random.rand(6, 6)
        vc = np.random.rand(6, 6)
        va += va.T
        vb += vb.T
        vc += vc.T
        crv._cov[0] = va
        crv._cov[1] = vb
        crv._cov[2] = vc

        x = 10.5
        y = 7.2

        coeffs = crv._get_local_coeffs((x, y))
        expected = [polynomial.polyval2d(x, y, a), polynomial.polyval2d(x, y, b), polynomial.polyval2d(x, y, c)]
        assert np.allclose(coeffs['values'], expected) is True

        J = polynomial.polyvander2d(x, y, (1, 2))[0]
        expected = [J.dot(va.dot(J.T)), J.dot(vb.dot(J.T)), J.dot(vc.dot(J.T))]
        assert np.allclose(coeffs['variances'], expected) is True

    def test_get_local_model(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv = CrvModel2d(2, 1, 2, instrument)

        coeffs = np.array([(1, 4), (2, 5), (3, 6)], dtype=CrvModel1d.dtype)

        mocker.patch.object(CrvModel2d, '_get_local_coeffs', return_value=coeffs)
        crv1d = crv.get_local_model(None)

        assert np.allclose(crv1d.coeffs, coeffs['values'])
        assert np.allclose(crv1d._vars, coeffs['variances'])

    def test_compute1(self, mocker):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 1, 2, instrument)

        crv1d = CrvModel1d([1, 2], crv2d.rotation, cov=[[2, 3], [3, 4]])

        delta_disp = np.array([-100.0, ])

        mocker.patch.object(CrvModel2d, 'get_local_model', return_value=crv1d)
        value2d, var2d = crv2d.compute(None, delta_disp)
        value1d, var1d = crv1d(delta_disp)

        assert np.allclose(value2d, value1d)
        assert np.allclose(var2d, var1d)

    def test_compute2(self, mocker):
        """Test deg reshape
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 1, 3, instrument)

        model = np.random.rand(3, 2, 4)
        cov = np.random.rand(3, 8, 8)

        crv2d.model = model
        crv2d.covariance = cov

        crv1d = crv2d.get_local_model([1, 1])

        crv2d.deg = 3
        crv1d_ = crv2d.get_local_model([1, 1])

        assert np.allclose(crv1d.coeffs, crv1d_.coeffs[1:])
        assert np.allclose(crv1d.covariance, crv1d_.covariance[1:, 1:])

        value1d, var1d = crv1d(100.)
        value1d_, var1d_ = crv1d_(100.)

        assert np.allclose(value1d, value1d_)
        assert np.allclose(var1d, var1d_)

    def test_compute3(self, mocker):
        """Test deg reshape
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 1, 3, instrument)

        model = np.random.rand(3, 2, 4)
        cov = np.random.rand(3, 8, 8)

        crv2d.model = model
        crv2d.covariance = cov

        crv1d = crv2d.get_local_model([1, 1])

        crv2d.deg = 1
        crv1d_ = crv2d.get_local_model([1, 1])

        assert np.allclose(crv1d.coeffs[1:], crv1d_.coeffs)
        assert np.allclose(crv1d.covariance[1:, 1:], crv1d_.covariance)

        crv2d_ = CrvModel2d(1, 1, 3, instrument)

        crv2d_.model = model[1:]
        crv2d_.covariance = cov[1:]

        crv1d__ = crv2d.get_local_model([1, 1])

        value1d, var1d = crv1d_(100.)
        value1d_, var1d_ = crv1d__(100.)

        assert np.allclose(value1d, value1d_)
        assert np.allclose(var1d, var1d_)

    def test_compute4(self, mocker):
        """Test deg reshape
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 1, 3, instrument)

        model = np.random.rand(3, 2, 4)
        cov = np.random.rand(3, 8, 8)

        crv2d.model = model
        crv2d.covariance = cov

        crv1d = crv2d.get_local_model([1, 1])

        crv2d.xdeg = 2

        assert crv2d.model.shape == (3, 3, 4)

        crv1d_ = crv2d.get_local_model([1, 1])
        assert np.allclose(crv1d.coeffs, crv1d_.coeffs)
        assert np.allclose(crv1d.covariance, crv1d_.covariance)

        value1d, var1d = crv1d(100.)
        value1d_, var1d_ = crv1d_(100.)

        assert np.allclose(value1d, value1d_)
        assert np.allclose(var1d, var1d_)

    def test_compute5(self, mocker):
        """Test deg reshape
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 2, 3, instrument)

        model = np.random.rand(3, 3, 4)
        cov = np.random.rand(3, 12, 12)

        crv2d.model = model
        crv2d.covariance = cov

        crv1d = crv2d.get_local_model([1, 1])
        crv2d.xdeg = 1

        assert crv2d.model.shape == (3, 2, 4)

        crv1d_ = crv2d.get_local_model([1, 1])

        crv2d_ = CrvModel2d(2, 2, 3, instrument)
        crv2d_.model = model[:, :2, :]
        crv2d_.covariance = cov[:, :8, :8]
        crv1d__ = crv2d_.get_local_model([1, 1])

        assert np.allclose(crv1d_.coeffs, crv1d__.coeffs)
        assert np.allclose(crv1d_.covariance, crv1d__.covariance)

        value1d_, var1d_ = crv1d_(100.)
        value1d__, var1d__ = crv1d__(100.)

        assert np.allclose(value1d_, value1d__)
        assert np.allclose(var1d_, var1d__)

    def test_compute6(self, mocker):
        """Test deg reshape
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 1, 3, instrument)

        model = np.random.rand(3, 2, 4)
        cov = np.random.rand(3, 8, 8)

        crv2d.model = model
        #crv2d.covariance = cov

        crv1d = crv2d.get_local_model([1, 1])

        crv2d.ydeg = 4

        assert crv2d.model.shape == (3, 2, 5)

        crv1d_ = crv2d.get_local_model([1, 1])
        assert np.allclose(crv1d.coeffs, crv1d_.coeffs)
        #assert np.allclose(crv1d.covariance, crv1d_.covariance)

        value1d, var1d = crv1d(100.)
        value1d_, var1d_ = crv1d_(100.)

        assert np.allclose(value1d, value1d_)
        #assert np.allclose(var1d, var1d_)

    def test_compute7(self, mocker):
        """Test deg reshape
        """
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 2, 3, instrument)

        model = np.random.rand(3, 3, 4)
        cov = np.random.rand(3, 12, 12)

        crv2d.model = model
        #crv2d.covariance = cov

        crv1d = crv2d.get_local_model([1, 1])
        crv2d.ydeg = 2

        assert crv2d.model.shape == (3, 3, 3)

        crv1d_ = crv2d.get_local_model([1, 1])

        crv2d_ = CrvModel2d(2, 2, 2, instrument)
        crv2d_.model = model[:, :, :3]
        #crv2d_.covariance = cov[:, :8, :8]
        crv1d__ = crv2d_.get_local_model([1, 1])

        assert np.allclose(crv1d_.coeffs, crv1d__.coeffs)
        #assert np.allclose(crv1d_.covariance, crv1d__.covariance)

        value1d_, var1d_ = crv1d_(100.)
        value1d__, var1d__ = crv1d__(100.)

        assert np.allclose(value1d_, value1d__)
        #assert np.allclose(var1d_, var1d__)

    def test_call(self, mocker):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(2, 1, 2, instrument)

        crv1d = CrvModel1d([1, 4], crv2d.rotation, cov=[[2, 3], [3, 4]])

        delta_disp = np.array([-50.0, ])

        mocker.patch.object(CrvModel2d, 'get_local_model', return_value=crv1d)
        value2d, var2d = crv2d.compute(None, delta_disp)
        value1d, var1d = crv1d(delta_disp)

        assert np.allclose(value2d, value1d)
        assert np.allclose(var2d, var1d)

    def test_refit_trace1(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([2, 1])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata += pos[0]
        ydata += pos[1]

        crv1d = crv2d.refit_trace(pos, xdata, ydata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit_trace2(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([1, 2])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata -= pos[1]
        ydata += pos[0]

        crv1d = crv2d.refit_trace(pos, ydata, -xdata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit_trace3(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([1, 2])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata -= pos[0]
        ydata -= pos[1]

        crv1d = crv2d.refit_trace(pos, -xdata, -ydata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit_trace4(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(3, 0, 0, instrument)

        pos = np.array([1, 2])

        xdata = np.linspace(-10, 10, 11)
        poly1d = np.poly1d([0.01, 0.0, -0.02, -0.5])

        ydata = poly1d(xdata)
        xdata += pos[1]
        ydata -= pos[0]

        crv1d = crv2d.refit_trace(pos, -ydata, xdata)

        assert np.allclose(crv1d.poly, poly1d)

    def test_refit1(self, mocker):

        crv = CrvModel2d(4, 2, 1)

        model = np.random.rand(4, 3, 2)
        cov = np.random.rand(4, 6, 6)

        crv.model = model
        crv.covariance = cov

        results = ((np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)))

        mocker.patch('spectrapy.models.crvmodel.polyfit2d', side_effect=results)
        crv.refit(None, None, np.zeros((2, 4)))

        assert np.allclose(crv.model[0], results[0][0])
        assert np.allclose(crv.model[1], results[1][0])
        assert np.allclose(crv.model[2], results[2][0])
        assert np.allclose(crv.model[3], results[3][0])

        assert np.allclose(crv.covariance[0], results[0][3])
        assert np.allclose(crv.covariance[1], results[1][3])
        assert np.allclose(crv.covariance[2], results[2][3])
        assert np.allclose(crv.covariance[3], results[3][3])

    def test_refit2(self, mocker):

        crv = CrvModel2d(4, 2, 1)

        model = np.random.rand(4, 3, 2)
        cov = np.random.rand(4, 6, 6)

        crv.model = model
        crv.covariance = cov

        results = ((np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)),
                   (np.random.rand(3, 2), 0.0, 0.0, np.random.rand(6, 6)))

        mocker.patch('spectrapy.models.crvmodel.polyfit2d', side_effect=results)
        crv.refit(None, None, np.zeros((2, 4)), np.zeros((2, 4)))

        assert np.allclose(crv.model[0], results[0][0])
        assert np.allclose(crv.model[1], results[1][0])
        assert np.allclose(crv.model[2], results[2][0])
        assert np.allclose(crv.model[3], results[3][0])

        assert np.allclose(crv.covariance[0], results[0][3])
        assert np.allclose(crv.covariance[1], results[1][3])
        assert np.allclose(crv.covariance[2], results[2][3])
        assert np.allclose(crv.covariance[3], results[3][3])

    def test_load1(self, mocker):

        model = ['LR', np.random.randn(2, 3, 4)]

        mocker.patch('spectrapy.models.crvmodel.Model2d.load', return_value=model)
        crv = CrvModel2d.load('mock')

        assert crv.dispersion_direction == model[0]
        assert np.allclose(crv.model, model[1])
        assert np.allclose(crv.covariance, np.zeros((12, 12)))

    def test_load2(self, mocker):

        model = ['LR', np.random.randn(2, 3, 4), np.random.randn(2, 12, 12)]

        mocker.patch('spectrapy.models.crvmodel.Model2d.load', return_value=model)
        crv = CrvModel2d.load('mock')

        assert crv.dispersion_direction == model[0]
        assert np.allclose(crv.model, model[1])
        assert np.allclose(crv.covariance, model[2])

    def test_get_dispersion_component1(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[0], component)

    def test_get_dispersion_component2(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[0], component)

    def test_get_dispersion_component3(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[1], component)

    def test_get_dispersion_component4(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(0, 0, 0, instrument)

        direction = np.random.rand(2)

        component = crv2d._get_dispersion_component(direction)

        assert np.allclose(direction[1], component)

    def test_getSpectraEdges1(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv2d.model = np.zeros((3, 1, 1))
        a = 0.001
        b = -0.02
        c = 7.2

        crv2d.model[0][0][0] = a
        crv2d.model[1][0][0] = b
        crv2d.model[2][0][0] = c

        edges = crv2d.get_trace_edges(np.array([40, 30]), (100, 120))

        assert edges[0].shape == (2,)
        assert edges[1].shape == (2,)
        assert np.allclose(edges[0], [0, 30 + a * (0 - 40)**2 + b * (0 - 40) + c])
        assert np.allclose(edges[1], [99, 30 + a * (99 - 40)**2 + b * (99 - 40) + c])

    def test_getSpectraEdges2(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv2d.model = np.zeros((3, 1, 1))
        a = 0.001
        b = -0.02
        c = 7.2

        crv2d.model[0][0][0] = a
        crv2d.model[1][0][0] = b
        crv2d.model[2][0][0] = c

        edges = crv2d.get_trace_edges(np.array([40, 30]), (100, 120))

        assert edges[0].shape == (2,)
        assert edges[1].shape == (2,)
        assert np.allclose(edges[0], [99, 30 - (a * (40 - 99)**2 + b * (40 - 99) + c)])
        assert np.allclose(edges[1], [0, 30 - (a * (40)**2 + b * (40) + c)])

    def test_getSpectraEdges3(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv2d.model = np.zeros((3, 1, 1))
        a = 0.001
        b = -0.02
        c = 7.2

        crv2d.model[0][0][0] = a
        crv2d.model[1][0][0] = b
        crv2d.model[2][0][0] = c

        edges = crv2d.get_trace_edges(np.array([40, 30]), (100, 120))

        assert edges[0].shape == (2,)
        assert edges[1].shape == (2,)
        assert np.allclose(edges[0], [40 - (a * (0 - 30)**2 + b * (0 - 30) + c), 0])
        assert np.allclose(edges[1], [40 - (a * (119 - 30)**2 + b * (119 - 30) + c), 119])

    def test_getSpectraEdges4(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv2d.model = np.zeros((3, 1, 1))
        a = 0.001
        b = -0.02
        c = 7.2

        crv2d.model[0][0][0] = a
        crv2d.model[1][0][0] = b
        crv2d.model[2][0][0] = c

        edges = crv2d.get_trace_edges(np.array([40, 30]), (100, 120))

        assert edges[0].shape == (2,)
        assert edges[1].shape == (2,)
        assert np.allclose(edges[0], [40 + (a * (30 - 119)**2 + b * (30 - 119) + c), 119])
        assert np.allclose(edges[1], [40 + (a * (30)**2 + b * (30) + c), 0])

    def test_dispersion_vector1(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 0, 0, instrument)

        assert np.all(crv2d.dispersion_vector == [1, 0])

    def test_dispersion_vector2(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(2, 0, 0, instrument)

        assert np.all(crv2d.dispersion_vector == [-1, 0])

    def test_dispersion_vector3(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(2, 0, 0, instrument)

        assert np.all(crv2d.dispersion_vector == [0, 1])

    def test_dispersion_vector4(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(2, 0, 0, instrument)

        assert np.all(crv2d.dispersion_vector == [0, -1])

    def test_rotation(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'UB'
        rot = np.random.rand(2, 2)
        crv1d = CrvModel1d(np.random.rand(2), rot)

        assert np.allclose(crv1d.rotation, rot)

    def test_slit_orientation1(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv1d = CrvModel1d(np.random.rand(2), crv2d._rotation)

        assert np.allclose(crv1d.slit_versor, [0, -1])

    def test_slit_orientation2(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv1d = CrvModel1d(np.random.rand(2), crv2d._rotation)

        assert np.allclose(crv1d.slit_versor, [0, 1])

    def test_slit_orientation3(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv1d = CrvModel1d(np.random.rand(2), crv2d._rotation)

        assert np.allclose(crv1d.slit_versor, [1, 0])

    def test_slit_orientation4(self):
        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(2, 0, 0, instrument)
        crv1d = CrvModel1d(np.random.rand(2), crv2d._rotation)

        assert np.allclose(crv1d.slit_versor, [-1, 0])

    def test_disperion_vector1(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'RL'

        crv2d = CrvModel2d(1, 0, 0, instrument)
        model = CrvModel1d(np.zeros(2,), crv2d._rotation)

        assert np.allclose(model.dispersion_vector, [-1, 0])

    def test_disperion_vector2(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'LR'

        crv2d = CrvModel2d(1, 0, 0, instrument)
        model = CrvModel1d(np.zeros(2,), crv2d._rotation)

        assert np.allclose(model.dispersion_vector, [1, 0])

    def test_disperion_vector3(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'BU'

        crv2d = CrvModel2d(1, 0, 0, instrument)
        model = CrvModel1d(np.zeros(2,), crv2d._rotation)

        assert np.allclose(model.dispersion_vector, [0, 1])

    def test_disperion_vector4(self):

        instrument = Instrument()
        instrument.dispersion_direction = 'UB'

        crv2d = CrvModel2d(1, 0, 0, instrument)
        model = CrvModel1d(np.zeros(2,), crv2d._rotation)

        assert np.allclose(model.dispersion_vector, [0, -1])

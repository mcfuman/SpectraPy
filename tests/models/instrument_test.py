#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File instrument_test.py
#
# Created on: Nov 11, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import configparser

import pytest
from pytest_mock import mocker

from spectrapy.models.instrument import Instrument


class TestInstrument(object):
    """Test for OptModel class"""

    def test_init(self):
        """
        """
        ins = Instrument()

        assert ins._pixel_scale == pytest.approx(0.0)
        assert ins._pixel_size == pytest.approx(0.0)
        assert ins._xpixels == 0
        assert ins._ypixels == 0
        assert ins.dispersion_direction == 'LR'

        assert ins._linear_dispersion == pytest.approx(0.0)
        assert ins._reference_lambda == pytest.approx(0.0)
        assert ins._data_hdu == 0
        assert ins._var_hdu is None
        assert ins._flag_hdu is None

    def test_load1(self, mocker):
        """
        """
        mocker.patch('os.path.isfile', return_value=False)

        with pytest.raises(Exception) as invalid_file:
            Instrument.load('mock')

        assert invalid_file.type is IOError
        assert invalid_file.value.args[0] == "Invalid instrument file"

    def test_load2(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {'data_hdu': 2,  # 'Primary',
                                 'var_hdu': 'VARIANCE',
                                 'flag_hdu': 'MASK'}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)

        instrument = Instrument.load('mock')

        assert instrument.pixel_scale == pytest.approx(mock_config['Detector']['pixel_scale'])
        assert instrument.xpixels == mock_config['Detector']['xpixels']
        assert instrument.ypixels == mock_config['Detector']['ypixels']

        assert instrument.dispersion_direction == mock_config['Grism']['dispersion_direction'].upper()
        assert instrument.linear_dispersion == pytest.approx(mock_config['Grism']['linear_dispersion'])
        assert instrument.reference_lambda == pytest.approx(mock_config['Grism']['reference_lambda'])

        assert instrument.data_hdu == mock_config['Files']['data_hdu']
        assert instrument.var_hdu == mock_config['Files']['var_hdu']
        assert instrument.flag_hdu == mock_config['Files']['flag_hdu']

    def test_load3(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {'data_hdu': 2,  # 'Primary',
                                 'flag_hdu': 'MASK'}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)

        instrument = Instrument.load('mock')

        assert instrument.pixel_scale == pytest.approx(mock_config['Detector']['pixel_scale'])
        assert instrument.xpixels == mock_config['Detector']['xpixels']
        assert instrument.ypixels == mock_config['Detector']['ypixels']

        assert instrument.dispersion_direction == mock_config['Grism']['dispersion_direction'].upper()
        assert instrument.linear_dispersion == pytest.approx(mock_config['Grism']['linear_dispersion'])
        assert instrument.reference_lambda == pytest.approx(mock_config['Grism']['reference_lambda'])

        assert instrument.data_hdu == mock_config['Files']['data_hdu']
        assert instrument.var_hdu is None
        assert instrument.flag_hdu == mock_config['Files']['flag_hdu']

    def test_load4(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {'data_hdu': 2,  # 'Primary',
                                 'var_hdu': 'VARIANCE'}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)

        instrument = Instrument.load('mock')

        assert instrument.pixel_scale == pytest.approx(mock_config['Detector']['pixel_scale'])
        assert instrument.xpixels == mock_config['Detector']['xpixels']
        assert instrument.ypixels == mock_config['Detector']['ypixels']

        assert instrument.dispersion_direction == mock_config['Grism']['dispersion_direction'].upper()
        assert instrument.linear_dispersion == pytest.approx(mock_config['Grism']['linear_dispersion'])
        assert instrument.reference_lambda == pytest.approx(mock_config['Grism']['reference_lambda'])

        assert instrument.data_hdu == mock_config['Files']['data_hdu']
        assert instrument.var_hdu == mock_config['Files']['var_hdu']
        assert instrument.flag_hdu is None

    def test_load5(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {'data_hdu': 2,  # 'Primary',
                                 'err_hdu': 'ERROR'}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)

        instrument = Instrument.load('mock')

        assert instrument.pixel_scale == pytest.approx(mock_config['Detector']['pixel_scale'])
        assert instrument.xpixels == mock_config['Detector']['xpixels']
        assert instrument.ypixels == mock_config['Detector']['ypixels']

        assert instrument.dispersion_direction == mock_config['Grism']['dispersion_direction'].upper()
        assert instrument.linear_dispersion == pytest.approx(mock_config['Grism']['linear_dispersion'])
        assert instrument.reference_lambda == pytest.approx(mock_config['Grism']['reference_lambda'])

        assert instrument.data_hdu == mock_config['Files']['data_hdu']
        assert instrument.err_hdu == mock_config['Files']['err_hdu']
        assert instrument.flag_hdu is None

    def test_load6(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {'data_hdu': 2,  # 'Primary',
                                 'err_hdu': 'ERROR',
                                 'var_hdu': 'VARIANCE'}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)

        with pytest.raises(Exception) as invalid_conf:
            instrument = Instrument.load('mock')

        assert invalid_conf.type is ValueError
        assert invalid_conf.value.args[0] == "Invalid instrument file: use or var_hdu either err_hdu, not both!"

    def test_load7(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)

        instrument = Instrument.load('mock')
        assert instrument.data_hdu == 0
        assert instrument._var_hdu is None
        assert instrument._err_hdu is None
        assert instrument._flag_hdu is None

    def test_load8(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {'data_hdu': 3},
                       'Description': {'Name': 'Marco',
                                       'Value': 77}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)
        mocker.patch.object(configparser.ConfigParser, '__contains__', new=mock_config.__contains__)

        instrument = Instrument.load('mock')
        assert len(instrument.description) == 2
        assert instrument.description['Name'] == 'Marco'
        assert instrument.description['Value'] == 77

    def test_pixel_scale1(self):
        """
        """
        ins = Instrument()
        ins.pixel_scale = 7.14
        assert ins.pixel_scale == pytest.approx(7.14)

    def test_pixel_scale2(self):
        """
        """
        ins = Instrument()
        ins.pixel_scale = 7
        assert ins.pixel_scale == pytest.approx(7.0)
        assert isinstance(ins.pixel_scale, float) is True

    def test_xpixels1(self):
        """
        """
        ins = Instrument()
        ins.xpixels = 1024
        assert ins.xpixels == 1024

    def test_xpixels2(self):
        """
        """
        ins = Instrument()
        ins.xpixels = 1024.0
        assert ins.xpixels == 1024
        assert isinstance(ins.xpixels, int) is True

    def test_xpixels3(self):
        """
        """
        ins = Instrument()
        ins.xpixels = '1024'
        assert ins.xpixels == 1024
        assert isinstance(ins.xpixels, int) is True

    def test_xpixels4(self):
        """
        """
        ins = Instrument()
        with pytest.raises(Exception) as invalid_npixel:
            ins.xpixels = 1024.1

        assert invalid_npixel.type is ValueError
        assert invalid_npixel.value.args[0] == "Invalid number of x pixels 1024.1"

    def test_ypixels1(self):
        """
        """
        ins = Instrument()
        ins.ypixels = 1024
        assert ins.ypixels == 1024

    def test_ypixels2(self):
        """
        """
        ins = Instrument()
        ins.ypixels = 1024.0
        assert ins.ypixels == 1024
        assert isinstance(ins.ypixels, int) is True

    def test_ypixels3(self):
        """
        """
        ins = Instrument()
        ins.ypixels = '1024'
        assert ins.ypixels == 1024
        assert isinstance(ins.ypixels, int) is True

    def test_ypixels4(self):
        """
        """
        ins = Instrument()
        with pytest.raises(Exception) as invalid_npixel:
            ins.ypixels = 1024.1

        assert invalid_npixel.type is ValueError
        assert invalid_npixel.value.args[0] == "Invalid number of y pixels 1024.1"

    def test_dispersiondirection1(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = "lr"
        assert ins.dispersion_direction == 'LR'

    def test_dispersiondirection2(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = "rl"
        assert ins.dispersion_direction == 'RL'

    def test_dispersiondirection3(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = "bu"
        assert ins.dispersion_direction == 'BU'

    def test_dispersiondirection4(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = "ub"
        assert ins.dispersion_direction == 'UB'

    def test_dispersiondirection5(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = 'LR'
        assert ins.dispersion_direction == 'LR'

    def test_dispersiondirection6(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = 'RL'
        assert ins.dispersion_direction == 'RL'

    def test_dispersiondirection7(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = 'BU'
        assert ins.dispersion_direction == 'BU'

    def test_dispersiondirection8(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = 'UB'
        assert ins.dispersion_direction == 'UB'

    def test_dispersiondirection9(self):
        """
        """
        ins = Instrument()
        ins.dispersion_direction = " Ub    "
        assert ins.dispersion_direction == 'UB'

    def test_dispersiondirection10(self):
        """
        """
        ins = Instrument()
        with pytest.raises(Exception) as invalid_disperion:
            ins.dispersion_direction = "xUb    "

        assert invalid_disperion.type is ValueError
        assert invalid_disperion.value.args[0] == "Invalid grism dispersion direction"

    def test_linear_dispersion1(self):
        """
        """
        ins = Instrument()
        ins.linear_dispersion = 10.3
        assert ins.linear_dispersion == pytest.approx(10.3)

    def test_linear_dispersion2(self):
        """
        """
        ins = Instrument()
        ins.linear_dispersion = "10.3"
        assert ins.linear_dispersion == pytest.approx(10.3)
        assert isinstance(ins.linear_dispersion, float) is True

    def test_reference_lambda(self):
        pass

    def test_data_hdu1(self):
        ins = Instrument()
        assert ins.data_hdu == 0

    def test_data_hdu2(self):
        ins = Instrument()
        ins.data_hdu = 1
        assert ins.data_hdu == 1

    def test_data_hdu3(self):
        ins = Instrument()
        ins.data_hdu = 'EXR2D'
        assert ins.data_hdu == 'EXR2D'

    def test_data_hdu4(self):
        ins = Instrument()
        ins.data_hdu = ' Primary '
        assert ins.data_hdu == 0

    def test_data_hdu5(self):
        ins = Instrument()
        ins.data_hdu = '0'
        assert ins.data_hdu == '0'

    def test_data_hdu6(self):
        ins = Instrument()
        with pytest.raises(Exception) as invalid_hdu:
            ins.data_hdu = 1.0

        assert invalid_hdu.type is TypeError
        assert invalid_hdu.value.args[0] == "Invalid hdu 1.0"

    def test_var_hdu1(self):
        ins = Instrument()
        assert ins.var_hdu is None

    def test_var_hdu2(self):
        ins = Instrument()
        ins.var_hdu = 1
        assert ins.var_hdu == 1

    def test_var_hdu3(self):
        ins = Instrument()
        ins.var_hdu = 'EXR2D'
        assert ins.var_hdu == 'EXR2D'

    def test_var_hdu4(self):
        ins = Instrument()
        ins.var_hdu = ' Primary '
        assert ins.var_hdu == 0

    def test_var_hdu5(self):
        ins = Instrument()
        ins.var_hdu = '0'
        assert ins.var_hdu == '0'

    def test_var_hdu6(self):
        ins = Instrument()
        with pytest.raises(Exception) as invalid_hdu:
            ins.var_hdu = 1.0

        assert invalid_hdu.type is TypeError
        assert invalid_hdu.value.args[0] == "Invalid hdu 1.0"

    def test_flag_hdu1(self):
        ins = Instrument()
        assert ins.flag_hdu is None

    def test_flag_hdu2(self):
        ins = Instrument()
        ins.flag_hdu = 1
        assert ins.flag_hdu == 1

    def test_flag_hdu3(self):
        ins = Instrument()
        ins.flag_hdu = 'EXR2D'
        assert ins.flag_hdu == 'EXR2D'

    def test_flag_hdu4(self):
        ins = Instrument()
        ins.flag_hdu = ' Primary '
        assert ins.flag_hdu == 0

    def test_flag_hdu5(self):
        ins = Instrument()
        ins.flag_hdu = '0'
        assert ins.flag_hdu == '0'

    def test_flag_hdu6(self):
        ins = Instrument()
        with pytest.raises(Exception) as invalid_hdu:
            ins.flag_hdu = 1.0

        assert invalid_hdu.type is TypeError
        assert invalid_hdu.value.args[0] == "Invalid hdu 1.0"

    def test_print(self, mocker):
        """
        """
        import numpy as np
        mocker.patch('os.path.isfile', return_value=True)

        mock_config = {'Detector': {'pixel_scale': np.random.rand(),
                                    'xpixels': int(1000 * np.random.rand()),
                                    'ypixels': int(1000 * np.random.rand())},
                       'Grism': {'dispersion_direction': "lr",
                                 'linear_dispersion': np.random.rand(),
                                 'reference_lambda': np.random.rand()},
                       'Files': {},
                       'Description': {'Name': 'Marco',
                                       'Instrument_type': 'Luci LoRes_200HK',
                                       'CentralWave': 77}}

        mocker.patch.object(configparser.ConfigParser, '__getitem__', new=mock_config.__getitem__)
        mocker.patch.object(configparser.ConfigParser, '__contains__', new=mock_config.__contains__)

        instrument = Instrument.load('mock')
        expected = "Name: Marco\n"
        expected += "Instrument type: Luci LoRes_200HK\n"
        expected += "CentralWave: 77\n"

        assert expected == instrument.__str__()


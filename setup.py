#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2019-2020 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File setup.py
#
# Created on: Mar 15, 2020
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

import os
import sys
import shutil
from glob import glob

import numpy as np

from distutils.core import setup
#from Cython.Build import cythonize
#import Cython.Compiler.Options
#Cython.Compiler.Options.annotate = True
#ext_modules=cythonize("spectrapy/extraction/cresamplingkernel.pyx", annotate=True),

from spectrapy import __version__

from distutils.extension import Extension
from Cython.Distutils import build_ext


# Compile the module using proper python3 includes
conda_dir = os.path.abspath(os.path.join(os.path.dirname(sys.executable), '..'))
include_dirs = [os.path.join(conda_dir, 'lib/python3.7/site-packages/numpy/core/include/'),
                np.get_include()]

ext_modules = [Extension("spectrapy.extraction.cresamplingkernel",
                         include_dirs=include_dirs,
                         sources=["spectrapy/extraction/cresamplingkernel.pyx", ])]
cmdclass = {'build_ext': build_ext}

setup(
    name='spectrapy',
    version=__version__,
    author='Marco Fumana',
    author_email='marco.fumana@inaf.it',
    description="This is the Python library to handle astronomical spectra",
    packages=['spectrapy', 'spectrapy.datacalib', 'spectrapy.dataio', 'spectrapy.extraction',
              'spectrapy.mask', 'spectrapy.math', 'spectrapy.models', 'spectrapy.modelscalib',
              'spectrapy.check'],
    scripts=glob('scripts/*'),
    requires=['astropy', 'pyds9', 'pyregions', 'cython'],
    license='GPLv3',
    cmdclass=cmdclass,
    ext_modules=ext_modules,
    python_requires='>=3.6',
)

# Copy in the tar in case user uses spectra from tar root
libso = glob("build/lib*/spectrapy/extraction/cresamplingkernel*.so")
if len(libso) > 0:
    shutil.copy(libso[0], "spectrapy/extraction/")


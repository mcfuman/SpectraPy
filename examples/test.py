"""from spectrapy.extraction.resamplingkernel import ResamplingKernel
from spectrapy.extraction.fastresamplingkernel import FastResamplingKernel
import numpy as np
data = np.ma.array(np.random.rand(20, 20) * 1000, mask=np.zeros((20, 20), dtype=bool))
pos = np.array([4.3, 7.])

k = ResamplingKernel()
k.interpolate(pos, data)

fk = FastResamplingKernel()
fk.interpolate(pos, data)
"""


from spectrapy.datacalib.extractiontable import ExtTable
from spectrapy.extraction.exponentialfilter import ExponentialFilter

engine = ExponentialFilter(2, 1000)
#engine = FastExponentialFilter()

engine.set_extraction_range(6000., 9000., 0.8)
objlist = ('55', )
mods_arc = "examples/data/mods1r/mods1r.20190301.0074.fits"

exr = ExtTable.load("examples/tmp/ID501202.exr")

import time
start = time.time()
spectra2d = engine.extract(exr, mods_arc, objlist=objlist)  # , row_start=10, row_end=20)
end = time.time()
print("Elapsed (after compilation) = %s" % (end - start))
spectra2d.writeto("/tmp/fast.fits", overwrite=True)
# spectra2d.join()

.. SpectraPy documentation master file, created by
   sphinx-quickstart on Wed Apr 24 07:49:11 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SpectraPy's documentation!
=====================================


.. toctree::
   :maxdepth: 4

   spectrapy
   install
   instrument-configuration
   mask-description
   models
   catalogs
   models-calib
   data-calib
   spectra-extraction


SpectraPy API
=============
.. toctree::
    api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. |astropy| image:: images/astropy_logo.svg
   :width: 100pt
   :align: middle

.. |python| image:: images/python_logo.svg
   :width: 100pt
   :align: middle

.. |matplotlib| image:: images/matplotlib_logo.png
   :width: 100pt
   :align: middle


+----------+-----------+--------------+
| |python| | |astropy| | |matplotlib| |
+----------+-----------+--------------+

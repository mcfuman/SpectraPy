.. _Models calib:

==================
Models calibration
==================
In the following sections, we will see how to use SpectraPy to obtain the final 2D extracted spectra. As training example we will use a MODS1R MOS frame.
In some cases one longslit  LUCI frame is also used to highlight the differences between MOS and LS cases.


.. include:: ./opt-model-calibration.rst

.. include:: ./crv-model-calibration.rst

.. include:: ./ids-model-calibration.rst

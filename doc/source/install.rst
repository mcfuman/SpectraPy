================================
SpectraPy installation procedure
================================

Installing Prerequisites
------------------------

* `Python3.7 <https://www.python.org/>`_ (or greater)
* `Astopy <https://www.astropy.org/>`_
* `Matplotlib <https://matplotlib.org/>`_
* `PyDS9 <http://hea-www.harvard.edu/RD/pyds9/>`_
* `pyregion <https://github.com/astropy/pyregion>`_
* `Cython <https://cython.org/>`_
* `DS9 <http://ds9.si.edu/site/Home.html>`_ (8.1 or greater)

We suggest to download and install `Anaconda Python Distribution <https://www.anaconda.com/distribution/>`_
and update your ``$PATH`` environment variable to use the proper python

In the ``BASH`` shell

.. code-block:: tcsh

  > export PATH=${CONDA_INSTAL_DIR}/bin:$PATH


or in the ``TCSH`` shell

.. code-block:: tcsh

  > setenv PATH ${CONDA_INSTAL_DIR}/bin:$PATH


And install the additional packages

.. code-block:: tcsh

  > ${CONDA_INSTAL_DIR}/bin/pip install pyds9
  > ${CONDA_INSTAL_DIR}/bin/pip install pyregion

Astropy and Cython are already included in the `conda distribution`.


Install SpectraPy
-----------------

Extract files from the tar package

.. code-block:: tcsh

  > tar -xvzf spectrapy-0.11.3.tar.gz

and use the proper python3 to install it


.. code-block:: tcsh

  > python3 setup.py install

SpectraPy is now installed and can be used from the python3 console

.. note::

   To work with spectrapy, we suggest to use the `ipython3 <https://ipython.org/>`_ console (available in the conda distribution previously installed).
   This is a powerful interactive shell, which provides a lot of facilities.

======================
SpectraPy introduction
======================

SpectraPy is a `Python3 <https://www.python.org/>`_ library, which collects algorithms and methods for data reduction of astronomical spectra obtained by a through slits spectrograph.

The library is designed to be **spectrograph independent**. It comes with a set of already configured spectrographs, but it can be easily configured to reduce data of other instruments.

Current implementation of SpectraPy is focused on the extraction of 2D spectra: it produces wavelength calibrated spectra, rectified for instrument distortion.
The library can be used on both longslit (LS) and multi object spectrograph (MOS) data.

To achieve the spectra extraction, the main components used by the library are:
  #. the :ref:`Instrument configuration` file
  #. the :ref:`Mask description` file
  #. one set of geometrical :ref:`Models`, which describes the spectra geometry on the raw frames.

Using these components the library is able to follow spectra distortions and extract rectified wavelength calibrated 2D spectra.

The extraction steps, can be roughly summarized in:
  #. :ref:`Models calib`: the calibration of the geometrical models. **If the instrument is stable enough**, this step is done once for all, for a given instrument configuration.
  #. :ref:`Data calib`: the geometrical models must be adjusted on the current data. This steps performs slightly adjustments on the data in use
  #. :ref:`Spectra extraction`: the extraction of the 2D spectra.

In this tutorial we will show how to create your :ref:`Models` from scratch, apply them to your data and obtain 2D spectra extracted.

We will also illustrate how to handle different issues related to MOS ans LS data.


Acknowledging or citing SpectraPy
=================================

If you use SpectraPy in your work, we would be grateful if you could include an acknowledgment in papers and/or presentations

In publications
---------------
If you use SpectraPy for research presented in a publication, we ask that you please cite the SpectraPy DOI `10.20371/inaf/sw/2021_00001 <https://www.ict.inaf.it/index.php/31-doi/133-sw-2021-01>`_

In projects or presentations
----------------------------

If you are using SpectraPy as part of a code project, or if you are giving a presentation/talk featuring work/research that makes use of SpectraPy and would like to acknowledge SpectraPy, we suggest using this badge

.. image:: images/powered_by-SpectraPy-B18904.svg

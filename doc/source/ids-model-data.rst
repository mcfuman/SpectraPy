.. _IDSDataCalib:


Inverse Dispersion Solution data tuning
=======================================

The wavelength solution is tuned on real data, searching the *real* line positions on frames.

The Optical Model gives us the slit extension on the frame along the cross dispersion direction, i.e. for each slit we know how large is the 2D spectrum.

To compute the wavelength solution, SpectraPy cuts N slices of 2D spectrum: one slice for each pixel along the cross dispersion direction. Namely, if the 2D spectrum is 23 pixels large, SpectraPy will create 23 slice of the 2D spectrum. SpectraPy can move along the slice following the Curvature Model Solution and it handles each slice like a 1D spectrum. Moving along the slice it computes the **real** line positions (relying on one line catalog). Then it computes the **slice wavelength solution** using these measured lines positions.

In details, for each slice:
  * SpectraPy picks nominal line positions from the input catalog
  * It uses the :ref:`IDSModel` solution (coming from :ref:`Models calib`) as first guess to move on the **expected** line position
  * It measures the **real** line position.
  * for each slice the library refits the 1d polynomial using these **real** positions.

The ``WavelengthCalibration`` class, is the tool used to achieve this calibration.
The final product of this procedure is a structure called **Extraction Table**, saved as FITS table


.. code-block:: python3

  >>> # The last CRV Module
  >>> crv = "examples/tmp/MODS1R.data.crv"

  >>> opt = "examples/data/mods1r/models/MODS1R.opt"
  >>> mods_mask = "examples/data/mods1r/ID532016.mdf"
  >>> mods1r = "conf/instruments/mods_G670L.icf"

  >>> # The first guess
  >>> ids = "examples/data/mods1r/models/MODS1R.ids"

  >>> from spectrapy.datacalib.wavelengthcalib import WavelengthCalibration
  >>> wave_calib = WavelengthCalibration(mods1r, mods_mask, opt, crv, ids)

Now we must define the range of lambda we want to calibrate (in Angstrom)

.. code-block:: python3

  >>> wave_calib.set_lambda_range(5000., 9000.)

and load a line catalog. Since in the MOS example we are using an arc frame as calibrator, we select the proper line catalog

.. code-block:: python3

  >>> mods_arc = "examples/data/mods1r/mods1r.20180121.0073.fits.bz2"
  >>> wave_calib.load_image(mods_arc)
  >>> NeHg_cat="conf/catalogs/NeHg_hr.dat"

  >>> # Do the computation
  >>> ID532016_exr = wave_calib.compute_spectra_wave(NeHg_cat)

During this computation we will see appear crosses in the DS9 viewer on the **measured** lines positions: one cross for each line, each slit and each slice.

.. image:: images/ids_data_after.png
  :width: 1000

.. note:: Since this is a completely automatic process, only reliable catalog lines, i.e. lines with flag 1 (see Catalog section for details) will be used.

In case we are satisfied by the results, we can save it

.. code-block:: python3

  >>> ID532016_exr.writeto("examples/tmp/ID532016.exr", overwrite=True)




The longslit case
-----------------

For longslit data, the procedure is the same. We just reports the list of commands, but there are not differences compared to the MOS case.


.. code-block:: python3

  >>> luci_mask = "conf/masks/luci_LS_0.75.mdf"
  >>> luci = "conf/instruments/luci_G200LoRes_1.93_1.8.icf"
  >>> sc_file = "examples/data/luci1LoRes/luci1.20180202.0181.fits.bz2"

  >>> crv = "examples/tmp/LUCI1.data.crv"

  >>> opt = "examples/tmp/LUCI1.opt"
  >>> ids = "examples/tmp/LUCI1.ids"

  >>> from spectrapy.datacalib.wavelengthcalib import WavelengthCalibration
  >>> wave_calib = WavelengthCalibration(luci, luci_mask, opt, crv, ids)
  >>> wave_calib.set_lambda_range(15000., 22000.)
  >>> wave_calib.load_image(sc_file)
  >>> sky_cat = "conf/catalogs/sky_lr.dat"

  >>> LS075_exr = wave_calib.compute_spectra_wave(sky_cat)


.. image:: images/luci_ids_data_after.png
  :width: 1000

and save the extraction table


.. code-block:: python3

   >>> LS075_exr.writeto("examples/tmp/LS075.exr",overwrite=True)


.. note::
  The Extraction Table contains the solution **for a given mask**, i.e. it is no more a generic model like OPTModel, CRVModel or IDSModel. It contains the 3 models applied to the current mask. If we want to extract spectra acquired with the same instrument configuration, but with another mask, we can tune the valid models on our new data, and we **must create a new Extraction Table**.

.. _Catalogs:

========
Catalogs
========

Catalog files are ASCII files containing expected lines positions, lines starting with # are ignored. These file are used both during :ref:`IDSCalibration` and during :ref:`IDSDataCalib` to know where emission lines should be.

In the directory ``conf/catalogs`` there are a set of already prepared catalogs, or we can create our custom catalog, according to our needs.

The catalog file, must contain 3 columns:

  * **pos**: the expected line position (in Angstrom)

  * **label**: the line name, a label to identify it (just for human purposes)

  * **flag**: the line flag internally used by SpectraPy (positive integer)

Here, as example, a part of a line catalog provided by SpectraPy ::

  11591.684       sky     1
  11627.846       sky     1
  11650.746       sky     1
  11696.348       sky     1
  11716.151       sky     1
  12007.078       sky     0
  12030.885       sky     0
  12055.878       sky     1
  12121.5         double  2
  12196.386       sky     1


The ``flag`` is an integer number used by SpectraPy, in the current version of SpectraPy, just 3 flags are allow: 0, 1, 2.

  - **0** if the line is faint or very close to a stronger line.
  - **1** these are bright isolated lines
  - **2** these are lines very close and not resolved by the instrument, but they can be useful to anchor the solution in regions of the spectrum with few lines

During the :ref:`IDSCalibration`, the positions of the lines in the frame are manually decided by user. At this stage we can use all the lines in the catalog, in case of doubts we can remove the DS9 regions related with the ambiguous line. Different flags are plotted with different colors:

  - 0 (faint lines): **pink**
  - 1 (reliable lines): **green**
  - 2 (not resolved lines): **red**

The :ref:`IDSCalibration` procedure is totally automatic, at this stage **only** the reliable lines (the lines with flag=1) will be used. SpectraPy automatically discards other lines, to obtain one solution as much reliable as possible.

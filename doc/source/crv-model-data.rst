Curvature Model data tuning
===========================

The accurate calibration of the spectra curvatures is performed by SpectraPy fitting the edges of the spectra on the frames.
These are the main steps of the edges computing:

  1. SpectraPy uses, as first guesses for the edge location, the solution of :ref:`CRVModel` obtained by the :ref:`Models calib` procedures

  2. then it moves along this curve, cutting thumbnails containing the edge of the spectrum (blue boxes in the figure)

  3. the library collapses these thumbnails along the dispersion direction and computes edge profile (the red curve in the figure) of each thumbnail

  4. these edge values are used by SpectraPy to fit the model


.. image:: images/edge_profile_full.png
  :width: 1000


The ``TraceCalibration`` class is the object used by SpectraPy, to fit the edge of the spectra trace


.. code-block:: python3

  >>> mods_mask = "examples/data/mods1r/ID532016.mdf"
  >>> mods1r = "conf/instruments/mods_G670L.icf"

  >>> mods_flat = "examples/data/mods1r/mods1r.20180121.0067.fits.bz2"

  >>> opt = "examples/data/mods1r/models/MODS1R.opt"
  >>> crv = "examples/data/mods1r/models/MODS1R.crv"

  >>> from spectrapy.datacalib.tracingcalib import TraceCalibration
  >>> trace_calib = TraceCalibration(mods1r, mods_mask, opt, crv)


Now we must define the limits of the tracing computations, and the sizes of the thumbnails

.. code-block:: python3

  >>> # Define the trace limits
  >>> trace_calib.set_trace_limits(1500, 3000)

  >>> # Load the image
  >>> trace_calib.load_image(mods_flat)

  >>> # Define the thumbnails sizes and perform the computation
  >>> trace_calib.compute_spectra_traces(slit_win=20, pix_bin=50, var=False)


In this example we selected a range of 1500 pixels in the blue and 3000 in the red. We decided to cut thumbnails every 50 pixels along the dispersion direction (the ``pix_bin`` parameter) and each thumbnail is 20 pixels large (along the cross dispersion direction). For each thumbnail the library creates the profile and computes the edge of this profile.

.. image:: images/crv_data_after.png

For visual check, SpectraPy displays in DS9 the computed edges. It places green crosses on the computed edge positions. It also shows red crosses in case of failures.


Like we did during :ref:`Models calib`, in case we are satisfied by the results, we can use these positions to recompute the CRV model (the red crosses will be excluded from computation)

.. code-block:: python3

  >>> trace_calib.fit_crv_model()


check the result

.. code-block:: python3

  >>> trace_calib.plot_crv_model(50)

and save the new model

.. code-block:: python3

  >>> trace_calib.writeto("examples/tmp/MODS1R.data.crv", overwrite=True)


.. note::
  In case your instrument is **stable enough**, i.e. **through slits flats match science data**, we suggest to use through slits flats to compute spectra curvatures, because the spectra edges are sharper with respect to science data



The longslit case
-----------------

Like we did during :ref:`Models calib`, in the longslit case we want to fit both edges. This is done using the ``right`` flag in the ``compute_spectra_traces`` call.

.. code-block:: python3

  >>> luci_mask = "conf/masks/luci_LS_0.75.mdf"
  >>> luci = "conf/instruments/luci_G200LoRes_1.93_1.8.icf"
  >>> sc_file = "examples/data/luci1LoRes/luci1.20180202.0181.fits.bz2"

  >>> opt = "examples/tmp/LUCI1.opt"
  >>> crv = "examples/tmp/LUCI1.crv"

  >>> from spectrapy.datacalib.tracingcalib import TraceCalibration
  >>> trace_calib = TraceCalibration(luci, luci_mask, opt, crv)

  >>> trace_calib.set_trace_limits(950, 1000)

  >>> # We are using a science frame
  >>> trace_calib.load_image(sc_file)

  >>> # The right flag is now set True
  >>> trace_calib.compute_spectra_traces(slit_win=20, pix_bin=50, right=True)


In this example, we also show how to to use a science frame to fit the trace edges instead of trough slit flat.

.. code-block:: python3

  >>> trace_calib.fit_crv_model()


Like in the MOS case we can check the results and save them

.. code-block:: python3

  >>> trace_calib.plot_crv_model(100, pos=(0, 0.325, 1))
  >>> trace_calib.writeto("examples/tmp/LUCI1.data.crv", overwrite=True)


.. note::
  The ``right=True`` flag, could be used also in the MOS case.
  It could be useful when we are working with crowded masks, where slits are very close one to the other, and the edges are not well defined.
  The ``right`` flag, doubles the traces (for each slit we have 2 edges now), increasing the model constraints, and this could help the fitting procedure.

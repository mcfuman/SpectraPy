.. _IDSCalibration:

Inverse Dispersion Solution calibration
=======================================

Once the spectra have been located on detector by the :ref:`OPTModel` and geometrically described by the :ref:`CrvModel`, the wavelength calibration of the 2D spectra can be carried out.

The :ref:`IDSModel` is the model which, following the spectrum curvatures, gives us a relation between wavelength (in Angstrom) and detector pixels.

Like for the ``CRVModel``, the ``IDSModel`` can be calibrated either using the previous ``ModelsCalibration`` instance or initializing a new class which loads the already saved ``OPTModel`` and ``CRVModel`` instances.

.. code-block:: python3

  >>> #The variables already initialized
  >>> mods_mask = "examples/data/mods1r/ID532016.mdf"
  >>> mods1r = "conf/instruments/mods_G670L.icf"
  >>> mods_arc = "examples/data/mods1r/mods1r.20180121.0073.fits.bz2"
  >>> from spectrapy.modelscalib.calib import ModelsCalibration

  >>> opt = "examples/tmp/MODS1R.opt"
  >>> crv = "examples/tmp/MODS1R.crv"
  >>> calib = ModelsCalibration(mods1r, mask=mods_mask, opt=opt, crv=crv)

The mathematical description of the ``IDSModel`` is similar to the ``CRVModel``: for each slit, one mono dimensional polynomial locates the wavelength positions along the curves described by the CRVModel . The FOV variations of the coefficients of this polynomial are described by a 2D polynomial.

We start creating the ``IDSModel`` from scratch

.. code-block:: python3

  >>> ids = calib.new_ids_model(3, 2, 2)

In this way, we created a model described locally by a 3th order polynomial and globally by 2x2 bi-dimensional polynomial.

In this example for the ``IDSModel`` calibration, we use the arc frame

.. code-block:: python3

  >>> calib.load_image(mods_arc)
  >>> NeHg_cat="conf/catalogs/NeHg_hr.dat"

In addition to the arc frame, we need a **line catalog** to know the expected arc lines position.
Catalogs are ASCII files containing line positions, their formats is described the :ref:`Catalogs` paragraph.

.. note:: Each arcs lamps, can be acquired in different frames separately. During IDS model calibration, it could be useful to add together frames of different arc lamps, in order to span a wider wavelength range. In case we provide a list of files to the ``load_image`` method, SpectraPy sums them together and displays the stacked image in DS9.

The global mode
---------------

Now we can start with the model calibration. First of all we will show the global approach, by this strategy we will calibrate all the MOS slits at the same time.


.. code-block:: python3

  >>> calib.plot_ids_model(NeHg_cat)

As we can see in DS9 we will see the expected DS9 lines positions. Since lines catalogs are related to a specific instrument configuration, they can span a wavelength range larger than range covered by the data, this is the reason why we see line region positions of of the frame.

Spectrapy allow us to load just a part of the catalog, using only lines in the relevant range. This is done by the ``wstart`` and ``wend`` parameter. In this case we ca limit the lines to the MODS red regions

.. code-block:: python3

  >>> calib.plot_ids_model(NeHg_cat, wstart=5000., wend=10000.)


.. image:: images/ids_before.png
   :width: 1200

The method  ``plot_ids_model`` displays the frame and over it, green arrows on the expected line positions. For each list, we must move the arrowhead in the proper position and then refit the model. [#]_ In this figure we can see how arrow positions don't match the line positions. The adjustment is not a simple global shift, we are suppose to adjust the model slit by slit, since the distortions are different along the FOV. Moreover each arrow position on the slit, must be moved of a different amount.

.. note:: During this adjustment, don't be afraid to delete lines which can not be properly adjusted, e.g. lines which fall out of frame.

.. code-block:: python3

  >>> calib.fit_ids_model()

After the fit recomputing, the line positions according with the new model will be plotted.

.. image:: images/ids_after.png
   :width: 1200


In case the new computed model well fits the line positions, we can save it.

.. code-block:: python3

  >>> ids.writeto("examples/tmp/MODS1R.ids",  overwrite=True)

Otherwise we can change the orders of the model and repeat the previous steps.

.. [#] Already prepared region file is available in ``examples/data/mods1r/regions/ids_global.reg``


Spectra slicing
+++++++++++++++

The first time we are dealing with new data, wavelength calibration can be a very tricky exercise. We have to label emission lines in the raw frame, associating to each of them the proper wavelength value. In literature an on the web, we can find very useful references to complete this task .

For the optical range we can suggest `atomic data tables <https://physics.nist.gov/PhysRefData/Handbook/atomic_number.htm>`_ (National Institute of Standards and Technology) or for the near infrared the `Rousselot et al. <https://ui.adsabs.harvard.edu/abs/2000A&A...354.1134R>`_ article could be a very useful reference for the OH lines positions.

We can find plots of lamp spectra already calibrated, like this on the `LBTO page <https://sites.google.com/a/lbto.org/mods/>`_.

.. image:: images/mods_lines.png
   :width: 600

The comparison of this plots with 2D raw frames can be not so straightforward. For this reason SpectraPy allow us to obtain a 1D slice of a single slit using the ``plot_slice`` method. We each slit we can cut a slice passing the slit ID to the ``ModelsCalibration.plot_slice`` method. In the following example we are plotting a slice for the slit **49**.


.. code-block:: python3

  >>> %pylab
  >>> calib.plot_slice('49')


.. image:: images/ids_calib_slice.png
   :width: 600

In this case this auxiliary plot shows the 1D slice of the slit 49 allowing us to compare the reference plot, with this slice and adjust properly the regions on the raw frame.


The interactive mode
--------------------

In some cases, adjusting the entire mask could be very tricky. For example if we are dealing with a very crowded mask, this approach could not be the best choice.

For this reason, beside this **global approach**, the SpectraPy library provides also an **interactive mode**. This mode allow us to calibrate the model slit by slit.

The **interactive mode** starts from the slit closest to center of the FOV (where the distortions should be smaller), so we can focus only on this slit and adjust the solution for it; once done we can switch to the next slit, the slit closest to the current one.
Moving to the next slit, SpectraPy will apply to the next slit the solution computed on the previous slit. Since the 2 slits are close each other, the new solution is normally reasonable even for this slit. In this way the manual adjustments to be made are quite small.

We will show this methodology by an example. We starts reading the already computed model and creating the IDS model from scratch, like we did in the global approach.

.. code-block:: python3

  >>> calib = ModelsCalibration(mods1r, mask=mods_mask, opt=opt, crv=crv)
  >>> calib.load_image(mods_arc)
  >>> NeHg_cat="conf/catalogs/NeHg_hr.dat"
  >>> ids = calib.new_ids_model(3, 2, 2)


and start the iteration process

.. code-block:: python3

  >>> calib.ids_iter(NeHg_cat, wstart=5000.)

SpectraPy will display in DS9 only the central slit.

.. image:: images/ids_iter1_before.png
   :width: 1200

We must adjust the solution for this slit [#]_; once done, we can switch to the next slit, by calling the ``next`` method

.. code-block:: python3

  >>> calib.next()

Calling ``next`` SpectraPy performs the following actions:

  -  it fits the solution for the current slit already adjust

  -  it applies this new solution to the next slit

  -  it splits DS9 in 2 frames: in upper frame (or left in case dispersion is bottom-up) there is the slit already adjusted as reference, in the lower frame the new slit to adjust

.. image:: images/ids_iter2_before.png
   :width: 1200

We must work on this second frame and adjust the solution for this new slit. Once done we can iterate the process calling again the ``next()`` method.

When we reach the last slit and adjusted it, we can stop the iteration process and fit the overall model. This is done calling the ``stop_iter`` method.

.. code-block:: python3

  >>> calib.stop_iter()

Calling ``stop_iter`` we automatically perform the fit of the IDSModel and restore the visualization with the whole single frame

.. image:: images/ids_iter_stop.png
   :width: 1200

If we are satisfied of the solution, we can save the model.

.. code-block:: python3

  >>> ids.writeto('/tmp/MODS1R.ids',  overwrite=True)


.. note:: In case we need to go back and refine the solution of some slit, the ``prev`` method is also available .

.. note:: Every time we go back and forward with the ``prev`` and ``next`` method, the current slit solution is refitted by default. In case we may want to browse the slits solutions, **without refit their solutions**, we can call call ``prev`` and ``next`` methods with the parameter ``fit=False``.

.. note:: In case we want stop the iteration without refit the model, we can also call the ``stop_iter`` method with the parameter ``fit=False``.

.. [#] The already prepared region file is available in ``examples/data/mods1r/regions/ids120.reg``. One file for each region is available in the same directory.


The longslit case
-----------------

Unlike the MOS case, in the longlist case the slit can be quite long, mask flexures and instrument distortions can produce spectrum with curved lines like in frame below.
That means the solution of the IDSModel is different in the center of the slit with respect of to the edges.

To address this issue, SpectraPy allows to describe the dispersion solutions, defining the IDSModel solution in several parts of the slits .

.. code-block:: python3

  >>> luci_mask = "conf/masks/luci_LS_0.75.mdf"
  >>> luci1 = "conf/instruments/luci_G200LoRes_1.93_1.8.icf"
  >>> luci_file = "examples/data/luci1LoRes/luci1.20180202.0181.fits.bz2"
  >>> from spectrapy.modelscalib.calib import ModelsCalibration

  >>> opt="examples/tmp/LUCI1.opt"
  >>> crv="examples/tmp/LUCI1.crv"
  >>> calib = ModelsCalibration(luci1, mask=luci_mask, opt=opt, crv=crv)

We decide to generate a local IDSModel described by 3rd order polynomial, which does not change along the X axis (we have just 1 slit), but which changes along the cross dispersion direction since we want to follow the line curvatures

.. code-block:: python3

  >>> ids = calib.new_ids_model(3, 0, 2)

Now we can start the calibration procedure, splitting the slit in many pieces (7 in this example)

.. code-block:: python3

  >>> sky_cat = "conf/catalogs/sky_lr.dat"
  >>> calib.load_image(luci_file)
  >>> calib.plot_ids_model(sky_cat, wstart=15000., nsplit=7)

.. image:: images/luci_ids_before.png
   :width: 1200

The same line position is showed 7 times along the slit. Adjusting the line position for each piece, we will instruct the model to follow the lines curvatures [#]_


.. image:: images/luci_ids_adjust.png
   :width: 1200

Once done, we refit the model and check the solution on the frame. We can also increase the number of slits for a better check.


.. code-block:: python3

  >>> calib.fit_ids_model()
  >>> calib.plot_ids_model(sky_cat, nsplit=20)


.. image:: images/luci_ids_after.png
   :width: 1200


And save the model

.. code-block:: python3

  >>> ids.writeto("examples/tmp/LUCI1.ids", overwrite=True)


.. [#] An already prepared region file is available in ``examples/data/luci1/regions/ids.reg``

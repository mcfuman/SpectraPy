.. _Data calib:

=================
Model Data Tuning
=================

In the ideal case, well calibrated models are ready to be used on real data. In the real world, instrument distortions, mask position uncertainty and other instrument effects affect the data in slightly different ways, producing slightly different distortions on a night by night basis. For this reason models must be adjusted on real data, this is what we call **models data tuning**.

Unlike before, now the computations of the spectra edges as well the line positions for the wavelength calibration, are automatically performed on real data and no more on the base of region files defined by the user.

Since we are working only with dispersed data, this kind of tuning **can not** be performed on the :ref:`OPTModel`, but only on :ref:`CRVModel` and :ref:`IDSModel`.

In these section we will see how SpectraPy performs this tuning on real data.


.. include:: ./crv-model-data.rst

.. include:: ./ids-model-data.rst

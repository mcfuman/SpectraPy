import pylab
from scipy import ndimage
from astropy.io import fits
from spectrapy.math.stats import clipped_average


data=fits.open('/cassandra/fumana/sobel_float.fits')[0].data

d=data[2424:2470,3605:3645]
p=clipped_average(d, axis=1, sigma=2.0, maxiters=5)


fig, ax=pylab.subplots()
pylab.plot(np.arange(2424,2470), p[0], 'red')
ax.set_xlabel('Pixel positions (cross dispersion direction)')
ax.set_ylabel('Clipped mean values (dispersion direction)')


pylab.show()

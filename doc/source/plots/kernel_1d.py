from spectrapy.extraction.kernelprofile import KernelProfile
import numpy as np

l=1000
x=KernelProfile(2, l)

import matplotlib.pyplot as plt

fig = plt.figure()
ax1 = fig.add_subplot(121)

steep=5
f=np.linspace(-2,2,4*l+1)

def H(f):
  return ((np.tanh(steep * (f + 0.5)) + 1.0) / 2.0) * \
               ((np.tanh(steep * (-f + 0.5)) + 1.0) / 2.0)

ax1.set_xlabel('frequencies')

ax1.set_title(r'$H_5(f)$')
ax1.plot(f, H(f))




ax2 = fig.add_subplot(122)

p=x.profile
y=np.concatenate((p[::-1],p[1:]))

x=np.linspace(-2,2,4*l+1)

ax2.set_title(r'$h_5(x)$')
ax2.plot(x, y)

ax2.set_xlabel('kernel radius [pixels]')
plt.subplots_adjust(wspace = 0)
ax1.set_ylim([-0.1,1.1])
ax2.set_ylim([-0.1,1.1])
ax2.set_yticklabels([])
ax1.grid(True, linestyle='--')
ax2.grid(True, linestyle='--')

#plt.axis('equal')
plt.show()

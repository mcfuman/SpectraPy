import numpy as np
from matplotlib import pyplot as plt

badw,badf = np.loadtxt("bad_wave.dat")
goodw,goodf = np.loadtxt("good_wave.dat")

ax1=plt.subplot(111)
ax1.set_xlim(6650,6750)
ax1.set_ylim(0,7000)

ax1.plot(badw,badf,color='gray',linestyle='--', label='1D spectrum slice')

#idx = np.nonzero((badw>=6660) & (badw<=6690))
idx = np.nonzero((badw>=6678-15) & (badw<=6678+15))

ax1.plot(badw[idx],badf[idx],color='red', label='1D spectrum window')

ax1.set_xlabel("wavelength [$\AA$]")
ax1.set_ylabel("counts")

ax1.vlines([6678.280,], 0, 7000, color='k', linestyle='dotted', label="Ne line position")
ax1.scatter(6678.280-2.,2000., color='blue', s=100, marker='o', label="line barycenter")


ax1.legend()
plt.show()



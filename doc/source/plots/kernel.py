from spectrapy.extraction.kernelprofile import KernelProfile
from matplotlib import cm

l=100
x=KernelProfile(2, l)

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
p=x.profile
y=np.concatenate((p[::-1],p[1:]))

Z=np.tensordot(y,y,axes=0)

print(Z.max())

x=np.linspace(-2,2,4*l+1)
y=np.linspace(-2,2,4*l+1)


X,Y=np.meshgrid(x,y)
ax.plot_wireframe(X,Y,Z)

ax.set_xlabel('X [pixels]')
ax.set_ylabel('Y [pixels]')
ax.set_zlabel('Kernel value')

plt.show()


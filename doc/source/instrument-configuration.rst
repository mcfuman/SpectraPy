.. _Instrument configuration:

Instrument configuration
========================

SpectraPy requires basic information about the instrument and the format of the data files. All these information are collected in the **instrument configuration file** (``icf``), the file is formalized by one `ini file <https://en.wikipedia.org/wiki/INI_file>`_ organized in 4 different sessions:

  #. :ref:`Instrument Detector`
  #. :ref:`Instrument Grism`
  #. :ref:`Instrument Files`
  #. :ref:`Instrument Description` (optional)

.. _Instrument Detector:

Detector
--------
The ``detector`` section contains information related to the detectors geometry. The following information are required by the library:
    * ``pixel_scale`` ["/pixel]: the nominal pixel scale of the instrument
    * ``xpixels``: the number of pixels along the X axis
    * ``ypixels``: the number of pixels along the Y axis


.. _Instrument Grism:

Grism
-----
The ``grism`` section defines the properties of the dispersion element:
  * ``dispersion_direction``: this keyword defines the dispersion direction, which goes from bluest regions of the frame to reddest. Possible values of this keyword are:
     #. ``LR``: dispersion along X axis, from left to right
     #. ``RL``: dispersion along X axis, from right to left
     #. ``BU``: dispersion along Y axis, from bottom to top
     #. ``LR``: dispersion along Y axis, from top to bottom

  * ``linear_dispersion`` [A/pixel]: the nominal linear dispersion of the grism
  * ``reference_lambda``: wavelength position of a bright isolate line near the nominal central wavelength of the grism [#RefLambda]_.

  .. image:: images/grism_orientations.png
     :width: 350

.. [#RefLambda] Strictly speaking the ``reference_lambda`` is not a property of the grism, but it is a value required by the **SpectraPy  wavelength calibration algorithm**. During the calibration process, we will choose a bright isolated line (sky line or arc line) as *reference point* for the slit. Since the real slit position is not visible on dispersed frames, this line will be used by SpectraPy as *"ideal"* slit position, and as reference point for the models.


.. _Instrument Files:

Files
-----
Every instrument produces data in several FITS formats and data can be stored in extensions that are not the Primary HDU.
The ``files`` section is used by the library to retrieve data from the proper extension. The extensions can be defined by name or by number [#Primary]_.

  * ``data_hdu``: extension containing science data. If not specified, the primary extension is used
  * ``var_hdu``: extension containing variance on data. If not specified, no variance is used.
  * ``err_hdu``: this is used when no variance is associated with data, but an error layer is provided [#Error_layer]_.
  * ``flag_hdu``: extension containing bit mask on data. Pixels with mask value > 0, will be masked out by SpectraPy  library during data calibration or data extraction. This layer can be used to mask out bad pixels, cosmic ray, ...


.. [#Primary] The Primary extension is 0 (like `astropy.io.fits <https://docs.astropy.org/en/stable/io/fits/>`_ does)
.. [#Error_layer] SpectraPy assumes var_hdu = err_hdu :superscript:`2`


.. _Instrument Description:

Description
-----------
The ``Description`` section is not mandatory and it used just for human purposes.
It just contains a description for user


The icf files
-------------

In the ``conf/instruments`` directory there are a set of already prepared instrument configuration files. Here we can see an example of MODS1R/MODS2R instrument configuration file used in this manual. ::

  [Description]
  instrument = MODS1R/2R
  grism = G670L

  [Detector]
  pixel_scale = 0.123
  pixel_size = 0.015
  xpixels = 8288
  ypixels = 3088

  [Grism]
  dispersion_direction = RL
  #A/pixels
  linear_dispersion = 0.8
  #Ne line
  reference_lambda = 6929.47

  [Files]
  data_hdu = Primary

.. _Mask description:

Mask description
================

The mask description files (``mdf`` files) are used by SpectraPy to know the geometry of the slits mask and to locate spectra on the raw frames.
The `mdf` file is an ASCII file, containing the following columns [#mask0]_:

  #. ``ID`` the **unique** slit ID [#ID]_.

  #. ``DIMX`` and ``DIMY`` the dimensions in millimeters of the slit.

  #. ``X`` and ``Y`` the position (always in millimeters) of the **center** of the slit in the field of view. [#mask1]_

  #. ``ROT`` the rotation angle of the slit. Rotation angle is clockwise and starts from cross-dispersion direction.

  #. ``WID`` and ``LEN`` the width and length of the slit in arcsec. These entries are not mandatory, some instruments don't provides them. In case these information are not available the value must be 0. [#mask2]_

  #. ``REF`` the reference slit flag. This value is 1 in case the slit is a reference. Reference slits can be ignored during data calibration and extraction

Here an example of one ``mdf`` of a MODS mask ::

  #ID     DIMX    DIMY    X       Y       ROT     WID    LEN     REF
  49      0.720   3.600   -73.62  -86.11   0.0    1.2     6.0    0
  85      0.720   6.000   -40.32  -91.09   0.0    1.2    10.0    0
  119     0.720   3.600   -86.62  -48.47  40.0    1.2     6.0    0
  Ref1    2.400   2.400   -20.70   74.29   0.0    4.0     4.0    1



.. [#mask0] No empty entries are allowed

.. [#ID] SpectraPy handles IDs as strings.

.. [#mask1] The library assumes the center of this coordinates system in the center of the FOV.

.. [#mask2] unlike the ``DIMX`` and ``DIMY`` parameters ``WID`` and ``LEN`` are not oriented along the X,Y axes, but they are oriented according with the grism dispersion direction: ``LEN`` is the slit length along the cross-dispersion direction, ``WID`` is the width along the dispersion direction


Mask utility scripts
--------------------

SpectraPy provides a set of scripts (stored in the ``scripts`` directory) to automatically create mdf files for VIMOS, LUCI and MODS data.
Available scripts are:

  #. ``vimos2mdf.py``
  #. ``luci2mdf.py``
  #. ``mods2mdf.py``

The ``vimos2mdf.py`` and ``luci2mdf.py`` require in input the fits data file, because in the VIMOS and LUCI cases, mask geometry is described in the header of the files.

Since no mask information is available in the header of MODS files, MODS scripts takes in input the ``mms`` (the file obtained by the MODS mask preparation tool).

Finally, for LUCI and MODS spectrograph, a set of already prepared LS masks is available in the ``conf/masks`` directory.

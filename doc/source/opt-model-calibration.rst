Optical Model calibration
=========================

In order to define the distortions map of the FOV, we must initialize the :ref:`OPTModel`. This step requires: the instrument configuration file and the mask description file.
We will start by loading an arc lamp frame for MODS1R

.. code-block:: python3

  >>> # The MODS instrument configuration file
  >>> mods1r = "conf/instruments/mods_G670L.icf"
  >>> # The mask description file
  >>> mods_mask = "examples/data/mods1r/ID532016.mdf"
  >>> mods_arc = "examples/data/mods1r/mods1r.20180121.0073.fits.bz2"

We must initialize the ``ModelsCalibration`` class: **this is the class used to calibrate all the models**.

.. code-block:: python3

  >>> from spectrapy.modelscalib.calib import ModelsCalibration
  >>> calib = ModelsCalibration(mods1r, mask=mods_mask)

The last call opens a `DS9 <http://ds9.si.edu/site/Home.html>`_ instance used by SpectraPy to display images and regions.
All the calibration processes consist in moving and adjusting regions on the frame in order to compute proper models.
The idea is that the ``ModelsCalibration`` instance shows us the current models solutions plotting regions on the DS9 frame. We can adjust these regions and refit the model using the new regions positions.

First of all, we must create a new Optical Model from scratch, because no OPTModel is yet available for this instrument configuration. In this case, we choose to describe the OPTModel with a polynomial of order 2 in both (:math:`x` and :math:`y`) directions.

.. code-block:: python3

  >>> opt = calib.new_opt_model(2, 2)

OPTModel assumes the mask oriented with the :math:`X` axis horizontal (from left to right) and :math:`Y` axis vertical (from bottom to up).
In the next figure we show on the left a dispersed frame, on the right the mask as described by the Optical Model.

.. image:: images/flip1.png
   :width: 800


As we can see, in this case the mask and the image don't match, slits are not in the expected position, we can check this discrepancy looking at the tilted slit or at the reference square slit.

Due to instrument particularities and optical reflections, this initial assumption (with the axes oriented like in the picture above) can be not true for all the instruments. To solve this problem, the OPTModel can be flipped on both directions by the methods: ``flipx`` and ``flipy``. In this MODS1R case, we flipped the model vertically.

.. code-block:: python3

  >>> # For optical reasons we must (at least in MODS) flip the model along the Y axis
  >>> opt.flipy()

.. image:: images/flip2.png
   :width: 800


The ``OPTModel`` locates the mask slits on the FOV. Since we are working with dispersed images, slits are not visible on the frame. For this reason we will use an arc frame to tune the model: we will use the lambda reference position on the frame as *virtual slit*.

.. note::    As lambda reference position we suggest to choose a bright isolated line in the mid region of the dispersion range.


.. code-block:: python3

  >>> calib.load_image(mods_arc)
  >>> calib.plot_opt_model()

The arc image and the expected slits as green boxes will be displayed in the DS9 viewer.

.. image:: images/opt_before.png
   :width: 600

The first time, usually slits will not be in the proper position, we have to tune the model moving [#]_ these slits on the expected reference line position [#]_, using the standard DS9 regions commands

.. image:: images/opt_adjust.png
   :width: 600

Once we have moved all the boxes over the correct lines, we must recompute the model solution, refitting it. The library reads from ``DS9`` the current position of the lists, and adjust the model according with these positions.

.. note:: In case we don't want to use some slit and discard it from the fits, we can just remove this region from DS9.

.. code-block:: python3

  >>> calib.fit_opt_model()


We can visually check the results, plotting again of the recomputed model on the frame in use.

.. code-block:: python3

  >>> calib.plot_opt_model()

.. image:: images/opt_after.png
   :width: 600


If slits regions, still remain in the proper position, the model is good and we can save it.

.. code-block:: python3

  >>> opt.writeto("examples/tmp/MODS1R.opt", overwrite=True)

In case the model does not match the proper slit positions, we can try to increase the polynomial order of the model and repeat the previous operations.


.. [#] In case regions are frozen and you are not able to move them, select the ``Region`` option in the DS9 ``Edit`` menu.

.. [#] In case you want follow the exercise, without losing time moving regions, you can use already prepared region file in ``examples/data/mods1r/regions/opt.reg``. You must delete all current regions and replace them with regions contained into the file.



The longslit case
-----------------

In this section, we will calibrate the optical model for a longlist case. For this example we will use LUCI1 frames acquired with a slit  0.75" width and 60" long, using the low resolution grism. The main difference is that the number of slits in the FOV is just 1.

.. code-block:: python3

  >>> luci_mask = "conf/masks/luci_LS_0.75.mdf"
  >>> luci1 = "conf/instruments/luci_G200LoRes_1.93_1.8.icf"
  >>> luci_file = "examples/data/luci1LoRes/luci1.20180202.0181.fits.bz2"

In this case we are using a science frame, since in the instrument configuration file we choose an OH sky line as reference position.

Again we create the OPTModel from scratch and display the slit.

.. code-block:: python3

  >>> from spectrapy.modelscalib.calib import ModelsCalibration
  >>> calib = ModelsCalibration(luci1, mask=luci_mask)
  >>> calib.load_image(luci_file)
  >>> opt = calib.new_opt_model(1, 1)
  >>> calib.plot_opt_model(edit=True)

.. image:: images/luci_opt_before.png
   :width: 1200

In this case we have just one slit, and the slit mutal positions can not be used to derive the scaling factor of the Optical Model. You can see how the nominal scale factor is not enough accurate since the slit size (green box on the frame) doesn't fit the size on the dispersed frame.

So we have to adjust both the slit position and the slit dimensions. This is the reason why we set the parameter ``edit=True``. With this flag ON we can move also the slit corners to fit the *real* slit position. [#]_

.. note:: Be careful: slit is described by a polygon. **DON'T ADD** corners to this polygon, just **move the already existing corners**, otherwise the fit will fail!


This resizing allows us to calibrate the OPTModel scale. Once done we can refit the models

.. code-block:: python3

  >>> calib.fit_opt_model()


And save the fit result

.. code-block:: python3

  >>> opt.writeto("examples/tmp/LUCI1.opt", overwrite=True)


.. [#] Already prepared region file is available in examples/data/luci1LoRes/regions/opt.reg

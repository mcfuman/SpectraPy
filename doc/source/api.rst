.. _SpectraPy_API:

=============
SpectraPy API
=============


Mathematical utilities
======================

.. automodule:: spectrapy.math.interp
  :members:

.. automodule:: spectrapy.math.peaks
  :members:

.. automodule:: spectrapy.math.poly
  :members:

.. automodule:: spectrapy.math.stats
  :members:


Data I/O utilities
==================

.. automodule:: spectrapy.dataio.catalog
  :members:

.. automodule:: spectrapy.dataio.display
  :members:

.. automodule:: spectrapy.dataio.image
  :members:

.. automodule:: spectrapy.dataio.spectra
  :members:


The mathematical models
=======================

.. automodule:: spectrapy.models.instrument
  :members:

.. automodule:: spectrapy.models.model
  :members:

.. automodule:: spectrapy.models.optmodel
  :members:

.. automodule:: spectrapy.models.crvmodel
  :members:

.. automodule:: spectrapy.models.idsmodel
  :members:


Masks geometry
==============

  .. automodule:: spectrapy.mask.mask
    :members:


Models Calibration
==================

.. automodule:: spectrapy.modelscalib.calib
  :members:

.. automodule:: spectrapy.modelscalib.optcalib
  :members:

.. automodule:: spectrapy.modelscalib.crvcalib
  :members:

.. automodule:: spectrapy.modelscalib.idscalib
  :members:

.. automodule:: spectrapy.modelscalib.model1diter
  :members:


Calibration on Data
===================

.. automodule:: spectrapy.datacalib.tracingcalib
  :members:

.. automodule:: spectrapy.datacalib.edges
  :members:

.. automodule:: spectrapy.datacalib.wavelengthcalib
  :members:

.. automodule:: spectrapy.datacalib.extractiontable
  :members:


Spectra Extraction
==================

.. automodule:: spectrapy.extraction.kernelprofile
  :members:

.. automodule:: spectrapy.extraction.resamplingbox
  :members:

.. automodule:: spectrapy.extraction.resamplingkernel
  :members:

  .. automodule:: spectrapy.extraction.exponentialfilter
    :members:

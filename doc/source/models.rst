.. _Models:

Models
======
SpectraPy algorithms rely on 3 geometrical model:
  #. The :ref:`OPTModel`, used to describe the geometrical distortions along the FOV
  #. The :ref:`CRVModel`, used to describe the spectra displacement with respect to the ideal dispersion direction, perfectly aligned along pixels
  #. The :ref:`IDSModel`, which describes the wavelength to pixel relation


.. _OPTModel:

Optical Model
-------------

The Optical Model (aka ``OPTModel``) describes the optical distortion in the FOV and it converts mask slit positions (in millimeters) into pixels on the detector.
The Optical model does not dependent on the grism in use, since it just locates the reference lambda (the virtual slit for SpectraPy) on the detector ignoring any information about dispersion.

The model is defined by a pair of global 2D polynomials: one for the :math:`x` and one for the :math:`y`.

In the current implementation :math:`X` and :math:`Y` polynomials have the same shape.

.. math::

    x_{[pix]} = \sum_{i=0}^N \sum_{j=0}^M X_{i,j} x_{[mm]}^i y_{[mm]}^j

    y_{[pix]} = \sum_{i=0}^N \sum_{j=0}^M Y_{i,j} x_{[mm]}^i y_{[mm]}^j


.. image:: images/opt_model.png
       :width: 400

This model is used to describe the distorted images in a rectified coordinate system.


.. _CRVModel:

Curvature Model
---------------

Due to optical distortions on real data, spectra are not perfectly aligned along the pixels and these distortions change within the FOV.

The Curvature Model (aka ``CRVModel``) describes the deviation of the spectra traces, with respect to the perfect straight line (horizontal in the case of LR or RL dispersion directions and vertical in the case of BU or UB dispersion directions). The model estimates this displacement (in pixel) along the cross dispersion direction.

For each slit, one mono dimensional polynomial is used to describe the displacement along the cross dispersion direction :math:`\Delta c`, starting from the slit reference position (located by the Optical Model)


.. math::

    \Delta c_{[pix]} = \sum_{i=0}^N c_{x,y,i} \Delta d_{[pix]}^i


The :math:`\Delta d` is the displacement with respect to the reference lambda position in pixel :math:`(x,y)`.

.. image:: images/crv_model_part.png
   :width: 400

Each slit has own set of :math:`c_{x,y,i}` coefficients, different slit by slit, because each slit is in a difference position in the FOV.

SpectraPy uses a global model to describe coefficients variation along the FOV.

The local :math:`c_{x,y,i}` coefficients are obtained by the evaluation of this global model at the reference lambda position on detector.

.. math::
    c_{x,y,i} = \sum_{h=0}^N \sum_{k=0}^M C_{i,h,k} x_{[pix]}^h y_{[pix]}^k


.. note:: This approach has the advantage to be **mask independent**. Once we have calibrated the global CRVModel, if the instrument is stable, we can apply the same model to describe every masks.


.. _IDSModel:

Inverse Dispersion Solution Model
---------------------------------

Once the spectra are located on Detector (:ref:`OPTModel`) and geometrically described (:ref:`CRVModel`) the wavelength calibration of the 2D can be carried out.

The Invese Dispersion Solutions Model (aka ``IDSModel``) is the model used to obtain the relation between pixel positions and wavelengths. It moves along the curve described by the combination of OptModel and CRVModel and it associates expected wavelength value to pixels of this curve.

The IDSModel mathematical description is quite similar to the CRVModel: for each slit, one mono dimensional polynomial locates the wavelength position . Since each slit is in a difference position in the FOV, and distortions changes within the FOV, each slit has own set of :math:`d_{x,y,i}` coefficients.


.. math::
    d_{x,y,i} = \sum_{h=0}^N \sum_{k=0}^M D_{i,h,k} x_{[pix]}^h y_{[pix]}^k


The set of :math:`d_{x,y,i}` coefficients are used to measure the wavelength with respect to the reference lambda position.

.. math::

    \Delta d_{[pix]} = \sum_{i=0}^N d_{x,y,i} (\lambda - \lambda_{ref})^i


The :math:`\Delta d` is the displacement with respect to the reference lambda position located at the pixel :math:`(x,y)`.

Even in this case a global 2D polynomial is used to describe the coefficients variation along the FOV and the local :math:`c_{x,y,i}` are obtained by the evaluation of this global model

Curvature Model calibration
===========================

Once the ``OPTModel`` has been calibrated, i.e. the reference lambda is properly located for each list, we can handle the spectra curvatures.

The Curvature Model calibration can be performed either using the previous ``ModelsCalibration`` class instance or initializing a new class, loading the saved OPTModel.

.. code-block:: python3

  >>> #The variables already initialized
  >>> mods_mask = "examples/data/mods1r/ID532016.mdf"
  >>> mods1r = "conf/instruments/mods_G670L.icf"
  >>> mods_arc = "examples/data/mods1r/mods1r.20180121.0073.fits.bz2"
  >>> from spectrapy.modelscalib.calib import ModelsCalibration

  >>> opt = "examples/tmp/MODS1R.opt"
  >>> calib = ModelsCalibration(mods1r, mask=mods_mask, opt=opt)

Like in the ``OPTModel`` case, we create a :ref:`CRVModel` from scratch and display it

.. code-block:: python3

  >>> crv=calib.new_crv_model(1, 2, 2)

The previous line of code creates a curvature model which is locally described by a straight line, i.e. each spectra trace is described by one 1st order polynomial. The coefficients of these lines change along the FOV, i.e. the curvature of each trace is slightly different spectrum by spectrum. In this example we decide to describe **this variation** by a 2D polynomial of order 2 by 2.

Since we want to follow the trace of the spectra, the best frame to use for the ``CRVModel`` is a through slit flat, which shows clearly the trace edges of the spectra.

.. code-block:: python3

  >>> mods_flat="examples/data/mods1r/mods1r.20180121.0067.fits.bz2"
  >>> calib.load_image(mods_flat)
  >>> calib.set_trace_limits(800, 800)

The  ``CRVModel``, starting from the reference position defined by the ``OPTModel``, follows the geometry of the spectra both in the blue and the red directions.

The last line of code defines (in pixels) the tracing range we are going to calibrate. In this case we decide to calibrate 800 pixels (both directions) along the dispersion direction around the reference line [#]_.

.. code-block:: python3

  >>> calib.plot_crv_model(9)

In this tutorial we decided to use 9 points (marked by green crosses) equally spaced along these 1600 pixels.


.. image:: images/crv_before.png
   :width: 1200

``DS9`` will show the slits position (the blue region) and 9 crosses for each slits along the expected position of the spectra traces. Like we did for the ``OPTModel``, we must adjust in ``DS9`` the crosses along the *left side* the spectra traces. [#]_

By default only the *left* edges of the spectra are used to calibrate the CRVModel. The *left* and *right* edges of the spectra are defined by SpectraPy on frames with ``BU`` dispersion direction (frames where the dispersion direction goes from bottom to up). According with that: in the frames with ``UB`` the dispersion direction the *left* edges for SpectraPy are the right edges on the frames, if the dispersion direction is ``LR`` is the upper side is the *left* edge and in the ``RL`` case the *left* edge is the lower edge on the frame.

.. image:: images/left_edges.png
   :width: 200

.. note:: Don't be afraid to delete some crosses if they follow out of frame or they are in a region where the spectrum signal is too faint, the fitting procedure will use only available crosses

.. code-block:: python3

  >>> calib.fit_crv_model()
  >>> calib.plot_crv_model(20)

Once done, we can refit the model and check it again. During check we can increase the number of points (20 in this case) to better visualize the new solution


.. image:: images/crv_after.png
   :width: 1200

If we are satisfied by this solution, we can save it pass on to the IDSModel calibration.

.. code-block:: python3

  >>> crv.writeto("examples/tmp/MODS1R.crv", overwrite=True)

Otherwise we can increase the degree of the polynomial and repeat the previous steps.

.. note:: The degree of the local polynomial (1 in this example) can be increased according with the number of crosses used for each slit.

.. note:: The degrees of the global 2D polynomial is strictly related to the number of slit in the FOV and we MUST take into account of the slit number when we decide its degree.


.. [#] The first parameter of the ``set_trace_limits`` defines the number of pixels in the blue direction, the second parameter the pixels in the red

.. [#] An already prepared region file is available in ``examples/data/mods1r/regions/crv.reg``


The longslit case
-----------------

The longlist case shows a single slit very extended along the cross dispersion direction. Due to optical distortions, the spectra produced by this kind of slit could have different sizes in the blue and in the red area. Namely the distance between the left and right edge in blue region slightly differs from the one in the red region.

.. image:: images/spectrum_sizes.png
   :width: 600


SpectraPy allow us to address this issue fitting both edges of the spectra.

.. code-block:: python3

  >>> luci_mask = "conf/masks/luci_LS_0.75.mdf"
  >>> luci1 = "conf/instruments/luci_G200LoRes_1.93_1.8.icf"
  >>> luci_file = "examples/data/luci1LoRes/luci1.20180202.0181.fits.bz2"
  >>> from spectrapy.modelscalib.calib import ModelsCalibration

  >>> opt = "examples/tmp/LUCI1.opt"
  >>> calib = ModelsCalibration(luci1, mask=luci_mask, opt=opt)
  >>> crv=calib.new_crv_model(2, 0, 1)

The last call defines a local curvature model of the 2nd order, described by a global 2D model of:
  * order 0 on X axis: we have just a single slit, it can not change in the FOV moving along X axis
  * order 1 on Y axis: we want a model capable of fitting both edges of the spectrum, i.e. this model can change the spectra curvature along the cross dispersion direction (the Y axis)

.. code-block:: python3

  >>> calib.set_trace_limits(1000, 1000)
  >>> calib.load_image(luci_file)
  >>> calib.plot_crv_model(7,  pos=(0, 1))

.. image:: images/luci_crv_before.png
   :width: 1200

Unlike the MOS case, we **want** to plot both edges of the spectra, this is achieved by the ``pos`` parameter of the ``plot_crv_model`` methos.

SpectraPy describes the slit with a `Bezier curve <https://en.wikipedia.org/wiki/B%C3%A9zier_curve>`_ parameterized by a real value *t* with goes from 0 up to 1.
So the left edge of the slit is the slit at *t=0* and the right edges is the slit at *t=1*. The parameter ``pos=(0,1)`` in the ``plot_crv_model`` call, define which region of the slit we want trace (the edges in this case).

.. note:: `pos` parameter can be any number between 0 and 1, that means SpectraPy can show you tracing on every point of the slit. This feature can be useful to work out with very problematic data.

In this case we must adjust both edges of the slit and refit the models [#]_

.. code-block:: python3

  >>> calib.fit_crv_model()

And check the results

.. code-block:: python3

  >>> calib.plot_crv_model(100, pos=(0, 0.325, 1))

And finally, as usual, save the model

.. code-block:: python3

  >>> crv.writeto("examples/tmp/LUCI1.crv", overwrite=True)

.. [#] An already prepared region file is available in ``examples/data/luci1LoRes/regions/crv.reg``

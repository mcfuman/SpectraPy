#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File wavelengthcalibplot.py
#
# Created on: Mar 28, 2020
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the class the plot the check results computed by the WavelengthCalibCheck"""

import numpy as np
from astropy import log
from astropy.stats import sigma_clipped_stats

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.gridspec import GridSpec
from matplotlib.lines import Line2D


class WavelengthCalibPlot:
    """Class to browse results obtained by WavelengthCalibCheck"""

    def __init__(self, ext_table, lines, positions, row_start: int = 0):
        """Initialize the class to display results obtained by WavelengthCalibCheck

        Parameters
        ----------
        ext_table: dict
          Dictionary containg the selection of slits checked by WavelengthCalibCheck

        lines: np.array, shape(N,)
          The lines list of catalog used by WavelengthCalibCheck

        positions: dict
          Dictionary containg the results of WavelengthCalibCheck

        row_start: int, optional
          The row stating value used by WavelengthCalibCheck
        """
        self._ext_table = ext_table
        self._lines = lines
        self._positions = positions
        self._slits = list(positions.keys())
        self._rainbow = cm.rainbow(np.linspace(0, 1, len(lines)))
        self._index = 0
        self._row_start = row_start

        self.panel = None
        self.legend = None
        self._axes = None
        self._create_legend = False
        self._scatters = None
        self._xpos = None

        plt.ion()

    def onpress(self, event):
        """The action to perform in case key is pressed

        Parameters
        ----------
        event: matplotlib.backend_bases.KeyEvent
          The press key event
        """
        if event.key == 'n':
            try:
                self._next_slit()
            except IndexError as error:
                log.warning(error.args[0])
                return False
        elif event.key == 'p':
            try:
                self._prev_slit()
            except IndexError as error:
                log.warning(error.args[0])
                return False
        else:
            return True

        return self._plot_slit()

    def onpick(self, event):
        """The action to perform in case key is pressed

        Parameters
        ----------
        event: matplotlib.backend_bases.MouseEvent
          The mouse event
        """
        # Col could be different from event.ind if scatter plot contains NaN value
        # We retrieve column using line values
        #col = event.ind[0]

        if self._create_legend is True and event.artist in self.legend.get_lines():
            self.goto_slit(event.artist.get_label())
            return

        col = np.argmin(np.abs(self._lines - event.mouseevent.xdata))
        row = self._scatters.index(event.artist)

        if event.mouseevent.button == 1:  # and event.mouseevent.dblclick is True:
            # every arc line of this row
            self._update_row_subplot(row)

        if event.mouseevent.button == 3:
            # every row of this lamp line
            self._update_line_subplot(row, col)

    def _update_row_subplot(self, row: int):
        """Update the plot lines positions along the single slit row

        Parameters
        ----------
        row: int
          Slit row number
        """
        slit = self.current_slit
        abs_row = row + self._row_start
        axis = self._axes['row']
        row_positions = self._positions[slit][row, :, :]
        WavelengthCalibPlot.log_stats(row_positions[:, 0], f"Row {abs_row}")

        axis.cla()
        axis.set_title(r"$\Delta\lambda$ for row %d" % abs_row)
        axis.set_xlabel(r"Line positions [$\AA$]")
        axis.set_ylabel(r"Line displacement [$\AA$]")
        # ax.errorbar(self._lines, row_positions[:, 0], yerr=row_positions[:, 1], fmt='none',
        #            ecolor='darkgray')
        axis.scatter(self._lines, row_positions[:, 0], color=self._rainbow)
        WavelengthCalibPlot._plot_base_line(axis)

        self.panel.canvas.draw()

    def _update_line_subplot(self, row: int, col: int):
        """Update plot of single line solution for each row

        Parameters
        ----------
        row: int
          The slit row number

        col: int
          The column position of the line to check
        """
        slit = self.current_slit
        line_name = self._lines[col]
        axis = self._axes['line']
        line_positions = self._positions[slit][:, col, :]
        WavelengthCalibPlot.log_stats(line_positions[:, 0], f"Line {line_name}")

        axis.cla()
        axis.set_title(r"$\Delta\lambda$ for line %.2f" % line_name)

        axis.scatter(self._xpos, line_positions[:, 0], color='orange')
        axis.scatter(self._xpos[row], line_positions[row, 0], color='olive')
        axis.set_xlabel("Image position [pix]")
        axis.set_ylabel(r"Line displacement [$\AA$]")

        WavelengthCalibPlot._plot_base_line(axis)

        self.panel.canvas.draw()

    @staticmethod
    def show():
        """Shows the plot tools
        """
        plt.show()

    @staticmethod
    def log_stats(positions, label: str, sigma=3.0):
        """Writes the statistic of a subset of result positions

        Parameters
        ----------
        positions: np.array
          Array of positions computed by WavelengthCalibCheck

        label: str
          The label associated with this set of positions

        sigma: float, optional
          The sigma threshold for the stats computations
        """
        stats = sigma_clipped_stats(positions, sigma=sigma)
        log.info(f"{label} stats : mean {stats[0]:.3f}, median {stats[1]:.3f}, "
                 f"standard deviation {stats[2]:.3f}")
        return stats

    def plot(self, legend=True):
        """Initializes the canvas plot of the tool
        """
        self.panel = plt.figure(f"Extraction Table check")
        self.panel.canvas.mpl_connect('pick_event', self.onpick)
        self.panel.canvas.mpl_connect('key_press_event', self.onpress)

        grid = GridSpec(2, 3)
        self._axes = {}
        self._axes['main'] = self.panel.add_subplot(grid[0, :])
        self._axes['row'] = self.panel.add_subplot(grid[1, :2])
        self._axes['line'] = self.panel.add_subplot(grid[1, 2])
        self._index = 0
        self._create_legend = legend
        plt.tight_layout()
        if self._create_legend is True:
            plt.subplots_adjust(left=0.15)
        self._plot_slit()

    @property
    def current_slit(self):
        """Gets the current slit
        """
        return self._slits[self._index]

    def _next_slit(self):
        """Moves the cursor to the next slit
        """
        if len(self._positions.keys()) > self._index + 1:
            self._index += 1
        else:
            raise IndexError(f"No more slits")

    def _prev_slit(self):
        """Moves the cursor to the previous slit
        """
        if self._index > 0:
            self._index -= 1
        else:
            raise IndexError(f"First slit")

    def goto_slit(self, slit: str):
        """Moves to the selected slit

        Parameters
        ----------
        slit: str
          The slit ID
        """
        if slit not in self._positions.keys():
            raise IndexError(f"No slit {slit}")

        new_index = self._slits.index(slit)
        if new_index == self._index:
            # Nothing to updated
            return

        self._index = new_index
        self._plot_slit()

    def _plot_slit(self):
        """Plots the current slit
        """
        slit = self.current_slit
        # Stats of the entire slit: every row, every column, position values
        WavelengthCalibPlot.log_stats(self._positions[slit][:, :, 0], f"Slit {slit}")
        self._compute_xpositions(slit)

        for ax_name in self._axes:
            self._axes[ax_name].cla()

        self._scatters = []
        axis = self._axes['main']
        axis.set_title(r"Slit %s" % slit)
        axis.set_xlabel(r"Line positions [$\AA$]")
        axis.set_ylabel(r"Line displacement [$\AA$]")
        for row in self._positions[slit]:
            y = row[:, 0]
            #yerr = row[:, 1]
            # ax.errorbar(self._lines, y, yerr=yerr, fmt='none', ecolor='darkgray')
            scatter = axis.scatter(self._lines, y, color=self._rainbow, alpha=0.5, picker=0.1)
            self._scatters.append(scatter)

        # Plot failures
        for col, line in enumerate(self._lines):
            if np.alltrue(np.isnan(self._positions[slit][:, col, 0])):
                scatter = axis.scatter(line, 0, color='red', marker='x', )

        self.create_legend()

        WavelengthCalibPlot._plot_base_line(axis)
        self.panel.canvas.draw()

    def _compute_xpositions(self, slit: str):
        """Computes the pixels positions of the slit on the image.

        Parameters
        ----------
        slit: str
          The slit ID
        """
        xpos = []
        for row in self._ext_table[slit]:
            xpos.append(row.crv.slit_versor.dot(row.center))
        self._xpos = np.array(xpos)

    @staticmethod
    def _plot_base_line(axis):
        """Plots the dotted base line on 0 as reference line.

        Parameters
        ----------
        axis: matplotlib.axes.Axes
          The axis where the base line must be plotted
        """
        hmin, hmax = axis.get_xlim()
        axis.hlines(0, hmin, hmax, linestyles='dashed', alpha=0.4, color='gray')

    def create_legend(self):
        """Creates a clickable mock line to browse the slits
        """
        if self._create_legend is False:
            return
        leglines = []
        for index, key in enumerate(self._positions):
            if index == self._index:
                alpha = 1
            else:
                alpha = 0.4
            leglines.append(Line2D([], [], color='blue', lw=0, marker='o', markersize=10,
                                   markerfacecolor='blue', label=key, alpha=alpha))

        self.legend = self._axes['main'].legend(handles=leglines, loc='upper left', fancybox=True,
                                                shadow=True, title='Slits',
                                                bbox_to_anchor=(-0.15, 1))

        self.legend.get_frame().set_alpha(0.4)
        for legline in self.legend.get_lines():
            legline.set_picker(5)  # 5 pts tolerance

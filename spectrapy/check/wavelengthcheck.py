#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File wavelengthcalib.py
#
# Created on: Mar 28, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the class the check the wavelenght solution on data"""

import numpy as np
from astropy import log
from astropy.utils.console import ProgressBar

from spectrapy.math.peaks import find_peak_1d
from spectrapy.dataio.catalog import Catalog
from spectrapy.dataio.display import Display
from spectrapy.datacalib.extractiontable import ExtTable

from .wavelengthcalibplot import WavelengthCalibPlot


class WavelengthCalibCheck(object):
    """Class to check and adjust wavelength calibration"""
    #fitter = fitting.LevMarLSQFitter()

    def __init__(self, ext_table):
        """Creates the wavelength calibration check class

        Parameters
        ext_table: ExtTable instance
          The extraction table to check
        """
        self._ext_table = None
        self.ext_table = ext_table
        self._ds9 = Display()

    @property
    def ext_table(self):
        """Gets the Extraction table to check
        """
        return self._ext_table

    @ext_table.setter
    def ext_table(self, value):
        """Sets the extraction table to check

        Parameters
        ----------
        value: str or ExtTable
          The Extraction Table file or instance
        """
        if isinstance(value, str):
            self._ext_table = ExtTable.load(value)

        elif isinstance(value, ExtTable):
            self._ext_table = value

        else:
            raise IOError(f"Invalid value type {type(value)}")

    @property
    def instrument(self):
        """Gets the instrument related to the Extraction Table
        """
        if self._ext_table is None:
            raise AttributeError("Extraction Table not set yet!")

        return self.ext_table.instrument

    def plot_table_regions(self, catalog, wrange=None, slits=None, color='green'):
        """Plot extraction table regions on the raw image.

        Parameters
        ----------
        catalog: str or array_like, shape (N,)
          The line catalog

        wrange: tuple, optional
          The wavelength range for the catalog

        slists: str or array_like, shape(N,)
          The slit list to use for check. In case is None all available slits in table are used.

        color: str, optional
          Color of the regions
        """
        if self._ds9.has_image() is False:
            raise AttributeError("Data not set yet.")

        if slits is None:
            slits = self.ext_table._slits.keys()

        if wrange is not None:
            lines = Catalog.load(catalog, wstart=wrange[0], wend=wrange[1])
            wrange = np.array(wrange)
        else:
            lines = Catalog.load(catalog)

        for slit in slits:
            ds9_str = self.ext_table.create_region_str(lines, slit, wrange=wrange, color=color)
            self._ds9.set("regions", ds9_str)

    def load_image(self, *filenames):
        """Loads the raw image used as regions background or to compute image slices for
        calibration checks.

        Parameters
        ----------
        data_file: str
          The image file name

        var_file: str, optional
          The variance file name

        flags_file: str, optional
          The flags file name
        """
        if self.instrument.data_hdu is not None:
            data_hdu = self.instrument.data_hdu
        else:
            data_hdu = 0

        self._ds9.load_image(*filenames, data_hdu=data_hdu, var_hdu=self.instrument.var_hdu,
                             err_hdu=self.instrument.err_hdu, flag_hdu=self.instrument.flag_hdu)

        self._ds9.reset_background()

    def check_ext_table(self, catalog, wradius: float = 10., wrange=None, wstep: float = 1.0,
                        slits=None, row_start: int = 0, row_end: int = None):
        """Checks the wavelength solution contained in the extraction table.
        The function computes a slice of of loaded image, according with Extraction Table solution,
        measures the line positions and compares the measured positions with the expected positions.

        Parameters
        ----------
        exttable: ExtTable instance
          The extraction table containg the model solutions

        catalog: str or array_like, shape(N,)
          The lines where the solution is checked

        wradius: float, optional
          The radius for position computation around the expected position [Angstrom]

        wrange: array_like, shape(2,)
          The wavelegth range for check. In case is None, it uses the range defined by the catalog

        wstep: float, optional
          The wavelenghts step for image slice rebinning [Angstrom]

        slits: array_like, shape(N,), optional
          The slit names used in check.
          If no slits are provided the solution is checked on all available slits

        row_start: int, optional
          Row starting point for check

        row_end: int, optional
          Row ending point for check

        """
        if self._ds9.has_image() is False:
            raise AttributeError("Data not set yet.")

        if wrange is not None:
            lines = Catalog.load(catalog, wstart=wrange[0], wend=wrange[1])
        else:
            lines = Catalog.load(catalog)

        lines = lines[lines['flag'] == Catalog.STRONG]['pos']

        waves = np.arange(lines[0] - wradius, lines[-1] + wradius, wstep)

        if slits is None:
            slits = self.ext_table.keys()

        if row_end is not None:
            row_end += 1

        positions = {}
        tables = {}

        with ProgressBar(len(slits)) as pbar:
            for slit in slits:
                # Defines positions container
                if row_end is not None:
                    if row_end > len(self.ext_table[slit]):
                        log.warning(f"Row end too large. Keep until the end of the slit")
                        row_end = len(self.ext_table[slit])

                tables[slit] = self.ext_table[slit][row_start:row_end]

                # n row, n lines, 2=(pos, error)
                positions[slit] = np.zeros((len(tables[slit]), len(lines), 2))

                for r, row in enumerate(tables[slit]):
                    fluxes, _ = self._ds9.image.interpolate(row.compute(waves))

                    good_fluxes = np.isfinite(fluxes)
                    good_waves = waves[good_fluxes]
                    fluxes = fluxes[good_fluxes]

                    positions[slit][r] = \
                        WavelengthCalibCheck.compute_delta_positions(good_waves, fluxes, lines, 10)
                pbar.update()

        return WavelengthCalibPlot(tables, lines, positions, row_start=row_start)

    @staticmethod
    def compute_delta_positions(waves, fluxes, lines, margin: int):
        """Compute delta position of lines respect to the ideal position

        Parameters
        ----------
        waves: np.array, shape(N,)
          Wavelength positions

        fluxes: np.array, shape(N,)
          Fluxes values

        lines: np.array, shape(N,)
          The lines positions

        margin: int
          The line position margin (in pixels) used in line position computation

        Returns
        -------
        positions: np.array, shape(N, 2)
          The couple of positions and error on position for each line
        """
        lambda_and_error = []

        # delta_line = margin / self._ids.inv_dispersion

        # Uses only lines with flag == 1
        for line in lines:
            idx = np.flatnonzero(waves >= line)
            if len(idx) == 0 or idx[0] == 0:
                continue
            idx = np.arange(max(0, idx[0] - margin), min(len(waves), idx[0] + margin))

            # delta_disp are not uniformelly sampled
            # pos represents the float index
            pos, _ = find_peak_1d(fluxes[idx], 2)
            if pos is not None:
                frac = pos - int(pos)
                value = (1.0 - frac) * waves[idx][int(pos)] + \
                    frac * waves[idx][int(pos) + 1]
                lambda_and_error.append((value - line, np.nan))
            else:
                lambda_and_error.append((np.nan, np.nan))

        return np.array(lambda_and_error)

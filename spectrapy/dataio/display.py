#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File display.py
#
# Created on: Mar 21, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class to handle FITS images"""

import os
from astropy import log

import numpy as np
from pyds9 import DS9
import pyregion

from spectrapy.dataio.image import Image
from spectrapy import __version__


class Display(DS9):
    """Class which wraps DS9 class, used to display data and regions"""

    def __init__(self):
        """Initializes DS9.
        """
        DS9.__init__(self, target=f'SpectraPy_{__version__}')
        self.clear()
        self._image = None

        self._row_start = 0
        self._col_start = 0

    def clear(self):
        """Clears ds9 frames.
        """
        self.set('frame delete all')
        self.set('frame new')

    def load_image(self, *filenames, data_hdu=0, var_hdu=None, err_hdu=None, flag_hdu=None):
        """Loads image from file

        Parameters
        ----------
        filenames: list
          Image file names list

        data_hdu: str, int
          Fits HDU containing data
        """
        self._image = Image.load(*filenames, data_hdu=data_hdu, var_hdu=var_hdu, err_hdu=err_hdu,
                                 flag_hdu=flag_hdu)

    @staticmethod
    def region_to_data(region) -> np.array:
        """Ds9 pixels starts from 1, data from 0. This method removes the offset from ds9 regions

        Parameters
        ----------
        regions: pyregion
          Region instance

        Returns
        -------
        array_like
          Data values
        """
        return np.array(region.coord_list) - 1

    @staticmethod
    def data_to_region(data: np.array) -> np.array:
        """Adds 1 pixel offset to data coordinates to display them
        (in the right position) as DS9 region.

        Parameters
        ----------
        data: array_like
          Input data coordinates

        Returns
        -------
        array_like
          The offsets data
        """
        return data + 1

    @property
    def regions(self):  # pragma: no cover
        """Gets ds9 regions
        """
        header = self._image.header
        try:
            return pyregion.parse(self.get("regions")).as_imagecoord(header)
        except Exception as error:
            log.warning("Error parsing regions")
            raise error

    def reset_background(self):
        """Clears DS9 and displays again the whole image.
        """

        if self.has_image() is False:
            raise AttributeError("Image not loaded yet!")

        if self._image.datafile is None:
            self.set_np2arr(self._image.data.data)
        else:
            self.set(f"fits {self._image.datafile}[{self._image.data_hdu}]")

        self._row_start = 0
        self._col_start = 0

    def join_frames(self):
        """Removes the 2nd frame from DS9 display
        """
        self.set('frame 2')
        self.set('frame delete')

    def split_frames(self, axis: int = 0):
        """Splits ds9 into 2 frames.

        Parameters
        ----------
        axis: int
          If axis == 0 creates 2 frames rows alligned. Otherwise columns alligned
        """
        self.set('frame new')
        self.set('tile yes')
        if axis == 0:
            self.set('tile row')
        else:
            self.set('tile column')

    def trim_image(self, x: int, y: int, lenght: int, width: int):
        """Displays a sub image.

        Parameters
        ----------
        x: int
          Image x starting pixel

        y: int
          Image y starting pixel

        lenght: int
          Image x lenght

        width: int
          Image y width
        """
        try:
            sub_image = self.get_subimage(x, y, lenght, width)
        except:
            raise IndexError("Sub image out of range")

        self._row_start = y
        self._col_start = x

        self.set_np2arr(sub_image.data.data)

    def get_subimage(self, x: int, y: int, length: int, width: int):
        """Returns the sub image

        Parameters
        ----------
        x: int
          The X starting pixel

        y: int
          The Y starting pixel

        length: int
          The image X length

        width: int
          The image Y length

        Returns
        -------
        spectrapy.dataio.image
          The Image subimage
        """
        return self._image.get_subimage(x, y, length, width)

    @property
    def image_dims(self):
        """Returns the X and Y image dimensions

        Returns
        -------
        array_like, shape(2, )
          The image X and Y dimensions
        """
        return self._image.data.shape[::-1]

    @property
    def xstart(self) -> int:
        """In case a sub image is displayed returns the X offset respect to the whole image

        Returns
        -------
        int
          The X offset of the subimage
        """
        return self._col_start

    @property
    def ystart(self) -> int:
        """In case a sub image is displayed returns the Y offset respect to the whole image

        Returns
        -------
        int
          The Y offset of the subimage
        """
        return self._row_start

    @property
    def image(self):
        """The loaded image"""
        if self._image is None:
            raise AttributeError("No image")

        return self._image

    def replace_regions(self, filename: str):
        """Loads regions from file and replace the current DS9 regions

        Parameters
        ----------
        filename: str
          The name of the file containg the new regions
        """
        if os.path.isfile(filename) is False:
            raise IOError(f"Invalid region file {filename}")

        self.set("regions delete all")
        self.set(f"regions load {filename}")

    def has_image(self):
        """Checks if image has been displayed"""
        return self._image is not None

    @property
    def xlen(self) -> int:
        """Gets the image X lenght if is image is loaded"""
        if self.has_image():
            return self._image.data.shape[1]
        return 0

    @property
    def ylen(self) -> int:
        """Gets the image Y lenght if is image is loaded"""
        if self.has_image():
            return self._image.data.shape[0]
        return 0

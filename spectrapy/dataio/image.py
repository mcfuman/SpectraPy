#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File image.py
#
# Created on: Nov 01, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class to handle FITS images"""

import os
import numpy as np
from astropy import log
from astropy.io import fits
from spectrapy.math.stats import clipped_average
from spectrapy.math.interp import BiLinearInterpolator


class Image(object):
    """Class to handle image data, variances and flags mask"""

    def __init__(self, data, variances: np.array = None, flags: np.array = None,
                 start: np.array = None):
        """
        Image constructor.

        Parameters
        ----------
        data: numpy.array 2d or fits.ImageHDU
          `data` contains science data

        variances: numpy.array, optional
          `variances` is a 2d array containing variances value on science data

        flags: numpy.array, optional
          `flags` is a 2d array containing interger flag: 0 good pixels, >0 problematic pixels
          (bad, cosmic, persistance, ...)

        start: numpy.array, optional
          The X,Y offsets in case of subimage
        """

        self._header = None
        self._variances = None
        self.datafile = None
        self.data_hdu = None
        self.filename = None

        if isinstance(data, np.ndarray):
            self._data = np.ma.array(data, dtype=np.float64).copy()

        elif isinstance(data, (fits.hdu.image.PrimaryHDU, fits.hdu.image.ImageHDU)):
            self._data = np.ma.array(data.data, dtype=np.float64).copy()
            self._header = data.header

        else:
            raise TypeError("Invalid data type")

        if len(self._data.shape) != 2:
            raise TypeError(f"Invalid data shape")

        if variances is not None:
            self.variances = variances.copy()

        if flags is not None:
            self.flags = flags.copy()
        else:
            self.flags = False

        if start is None:
            self._start = np.zeros(2, dtype=np.int32)
        else:
            self._start = np.array(start, dtype=np.int32)

        self._interpolator = None

    @property
    def shape(self):
        """The image array 2d shape.
        """
        return self._data.shape

    @property
    def start(self):
        """The image X, Y offset in case is a subimage
        """
        return self._start

    @property
    def data(self):
        """Scientific data
        """
        return self._data

    @property
    def header(self):
        """Gets Image header (if data comes from Fits file)
        """
        return self._header

    @property
    def variances(self):
        """Variances on scientific data

        Raises
        ------
        Exception
          If variances are not yet set.
        """
        if self._variances is None:
            raise AttributeError("No variances layer!")

        return self._variances

    @variances.setter
    def variances(self, value):
        """Set variances values on scientific data

        Parameters
        ----------
        value: numpy.array
          `value` is a 2d array containing variances science data

        Raises
        ------
        Exception
          If value: whether data is missing, or if is not None or if is not a 2d numpy.array
        """

        if self.is_valid(value) is False:
            raise TypeError("Invalid variances")

        self._variances = np.ma.array(value, mask=self._data.mask, dtype=np.float64)

    def has_variances(self):
        """Checks if variance exits"""
        return self._variances is not None

    @property
    def flags(self):
        """Gets data flags"""
        return self._data.mask

    @flags.setter
    def flags(self, value: np.array):
        """Set flags matrix on scientific data

        Parameters
        ----------
        value: numpy.array
          `value` is a 2d array of integers containing flag values: 0 good data, >0 otherwise

        Raises
        ------
        Exception
          If value: whether data is missing, or if is not None or if is not a 2d numpy.array
        """
        if isinstance(value, bool):
            self._data.mask = value

        elif self.is_valid(value) is False:
            raise TypeError("Invalid flags mask")

        else:
            self._data.mask = np.array(value, dtype=bool)

        if self._variances is not None:
            self._variances.mask = self._data.mask

    def is_valid(self, value):
        """Checks if the value layer is compliant with data layer (type and shape).

        Parameters
        ----------
        value: array_like, shape(2,)
          The new values layer

        Returns
        -------
        bool
          Returns True in case is compliant
        """
        if isinstance(value, np.ndarray) is False:
            log.debug(f"Invalid data type: {type(value)}")
            return False

        elif len(value.shape) != 2:
            log.debug(f"Invalid data shape: {value.shape}")
            return False

        elif self._data.shape != value.shape:
            log.debug(f"Input layer shape doesn't match data shape")
            return False

        return True

    @staticmethod
    def load(*datafiles, data_hdu=0, var_hdu=None, err_hdu=None, flag_hdu=None):
        """Loads image from Fits file.

        Parameters
        ----------
        datafiles: list
          File name containing science data
        data_hdu: int or str, optional
          Name or number of Fits extention containg science data
        var_hdu: int or str, optional
          Name or number of Fits extention containg variances on data
        err_hdu: int or str, optional
          Name or number of Fits extention containg errors on data
        flag_hdu: int or str, optional
          Name or number of Fits extention containg flags on data pixels

        Returns
        -------
        Image
          New instance of image
        """

        image = Image.load_single_image(datafiles[0], data_hdu=data_hdu, var_hdu=var_hdu,
                                        err_hdu=err_hdu, flag_hdu=flag_hdu)
        for datafile in datafiles[1:]:
            image += Image.load_single_image(datafile, data_hdu=data_hdu, var_hdu=var_hdu,
                                             err_hdu=err_hdu, flag_hdu=flag_hdu)

        return image

    @staticmethod
    def load_single_image(filename: str, data_hdu=0, var_hdu=None, err_hdu=None, flag_hdu=None):
        """Loads image from Fits file.

        Parameters
        ----------
        filename: str
          File name containing science data
        data_hdu: int or str, optional
          Name or number of Fits extention containg science data
        var_hdu: int or str, optional
          Name or number of Fits extention containg variances on data
        err_hdu: int or str, optional
          Name or number of Fits extention containg errors on data
        flag_hdu: int or str, optional
          Name or number of Fits extention containg flags on data pixels

        Returns
        -------
        Image
          New instance of image
        """
        with fits.open(filename) as hdul:
            image = Image(hdul[data_hdu])
            image.filename = os.path.abspath(filename)
            image.data_hdu = data_hdu

        if var_hdu is not None or err_hdu is not None:
            image.load_variances(filename, var_hdu=var_hdu, err_hdu=err_hdu)

        if flag_hdu is not None:
            image.load_flags(filename, flag_hdu=flag_hdu)

        return image

    def load_variances(self, filename: str, var_hdu=0, err_hdu=None):
        """Loads variances (or errors) from file and append them to the current image

        Parameters
        ----------
        filename: str
          The file containg variances

        var_hdu: str or int, optional
          The fits HDU containg variances

        err_hdu: str or int, optional
          The fits HDU containg errors. In case it is not None the var_hdu parameter is ignored
        """
        with fits.open(filename) as hdul:
            if err_hdu is not None:
                log.info(f"Loading variances from error hdu")
                # pylint: disable=E1101
                self.variances = (hdul[err_hdu].data)**2
            else:
                # pylint: disable=E1101
                self.variances = (hdul[var_hdu].data)

    def load_flags(self, filename: str, flag_hdu=0):
        """Loads image flags from file and append them to the current image

        Parameters
        ----------
        flagsfile: str
          File name containing flags on data
        flag_hdu: int or str, optional
          Name or number of Fits extention containg flags on data
        """
        with fits.open(filename) as hdul:
            self.flags = hdul[flag_hdu].data

    def compute_profile(self, sigma: float = 3.0, maxiters: int = 1, axis: int = 0):
        """Compute image profile along `axis`.
        Profile is computed computing a sigma clipped average in the direction orthogonal to the
        axis direction.

        Parameters
        ----------
        sigma: float, optional
          The number of standard deviations to use for both the lower and upper clipping limit.

        maxiters: int, optional
          The max number of iterations to perform sigma clipping.

        axis: int, optional
          Clip along the given axis.
        """
        return clipped_average(self._data, var=self._variances, sigma=sigma, maxiters=maxiters,
                               axis=axis)

    def get_subimage(self, xstart: int, ystart: int, xlen: int, ylen: int):
        """Extracts a sub image.

        Parameters
        ----------
        xstart: int
          Pixel x starting point

        ystart: int
          Pixel y starting point

        xlen: int
          Size along X in pixel of the sub image

        ylen: int
          Size along Y in pixel of the sub image

        Returns
        -------
        Image
          A new sub image

        Raises
        ------
        Exception:
          If ylen <=0
          If xlen <=0
          If sub image is totally of of image
        """

        if ylen <= 0 or ystart + ylen <= 0:
            raise IndexError("Subimage out of range")
        if ystart >= self.shape[0]:
            raise IndexError("Subimage out of range")
        ystart = max(ystart, 0)
        yend = min(ystart + ylen, self._data.shape[0]) + 1

        if xlen <= 0 or xstart + xlen <= 0:
            raise IndexError("Subimage out of range")
        if xstart >= self.shape[1]:
            raise IndexError("Subimage out of range")
        xstart = max(xstart, 0)
        xend = min(xstart + xlen, self._data.shape[1]) + 1

        _data = self._data[ystart:yend, xstart:xend]
        if self._variances is None:
            _var = None
        else:
            _var = self._variances[ystart:yend, xstart:xend]

        return Image(data=_data, variances=_var, flags=_data.mask, start=[xstart, ystart])

    def interpolate(self, xpos, ypos=None):
        """Estimates the values of image (and variance if exists) in float x,y positions.

        Parameters
        ----------
        xpos: array_like, shape(N,)
          Array of x positions
          In case number of column is 2, is the array of x and y positions

        ypos: array_like, shape(N,)
          Array of y positions
        """
        if self._interpolator is None:
            self._interpolator = BiLinearInterpolator(self._data, variance=self._variances)

        if ypos is None:
            return self._interpolator(xpos[:, 0], xpos[:, 1])

        return self._interpolator(xpos, ypos)

    def __iadd__(self, other):
        """Add in images in place. Data, variances are added. Masks are updated.

        Parameters
        ----------
        other: Image
          The image to add
        """
        if self.shape != other.shape:
            raise TypeError("Shapes don't match!")

        mask = np.logical_or(self._data.mask, other.flags)

        self._data += other.data

        self._data.mask = mask

        if self.has_variances() and other.has_variances():
            self._variances += other.variances

        return self

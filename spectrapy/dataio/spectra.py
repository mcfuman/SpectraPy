#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File spectra.py
#
# Created on: May 26, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class to extracted 2D spectra"""

import numpy as np
from astropy.io import fits
from astropy import log


class Spectra(fits.HDUList):
    """Class to handle extracted spectra"""
    DATA = 'DATA'
    VARIANCE = 'VARIANCE'
    QUALITY = 'QUALITY'

    def __init__(self, hdus=None, file=None):
        """Initializes the class

        Parameters
        ----------
        hdus: fits.HDUList
          In case is not None, postpone the hdus the a new PrimaryHDU

        file: str
          File name to load
        """
        if hdus is None:
            hdulist = [fits.PrimaryHDU(), ]
        elif isinstance(hdus[0], fits.PrimaryHDU) is False:
            hdulist = [fits.PrimaryHDU()] + hdus
        else:
            hdulist = hdus

        fits.HDUList.__init__(self, hdus=hdulist, file=file)
        self._spectra_ids = []
        self._waves = {}

        for hdu in hdulist:
            if isinstance(hdu, fits.ImageHDU) is False:
                continue
            if 'ETYPE' not in hdu.header:
                continue
            if hdu.header['ETYPE'] != Spectra.DATA:
                continue
            slit_id = hdu.header['SLIT_ID']
            self._spectra_ids.append(slit_id)
            self._waves[slit_id] = [hdu.header['CRVAL1'],
                                    hdu.header['CDELT1'],
                                    hdu.data.shape[1]]

    @staticmethod
    def open(name, mode='readonly', memmap=None, save_backup=False,
             cache=True, lazy_load_hdus=None, **kwargs):
        """Opens the fits file and returns the Spectra instance"""
        return Spectra(fits.open(name, mode=mode, memmap=memmap, save_backup=save_backup,
                                 cache=cache, lazy_load_hdus=lazy_load_hdus, **kwargs))

    @property
    def spectra_ids(self):
        """Gets the IDs of spectra in the class"""
        return self._spectra_ids

    def add_dataset(self, data, wstart: float, wstride: float, slit_id: str, content):
        """Adds a new spectrum dataset extracted and linearly wavelegth calibrated

        Parameters
        ----------
        data: array_like, shape(3, N, M)
          The spectrum dataset containg: data, variance and quality

        wstart: float
          The wavelegth starting value

        wstride: float
          The wavelength step value

        slit_id: str
          The spectrum ID
        """
        waves = (wstart, wstride, data.shape[2])
        if slit_id not in self._waves:
            self._waves[slit_id] = waves
            self._spectra_ids.append(slit_id)
        else:
            raise IndexError(f"Dataset {slit_id} already exists!")

        image = fits.ImageHDU(data[0])
        Spectra._set_header(image, wstart, wstride, slit_id, content, Spectra.DATA)
        self.append(image)

        if ~np.alltrue(np.isnan(data[1])):
            image = fits.ImageHDU(data[1])
            Spectra._set_header(image, wstart, wstride, slit_id, content, Spectra.VARIANCE)
            self.append(image)

        if ~np.alltrue(np.isnan(data[2])):
            image = fits.ImageHDU(data[2])
            Spectra._set_header(image, wstart, wstride, slit_id, content, Spectra.QUALITY)
            self.append(image)

    def get_wavelengths(self, slit_id: str):
        """Gets the wavelengths range for the required spectrum

        Parameters
        ----------
        slid_id: str
          The spectrum ID required
        """
        if slit_id not in self._waves:
            raise IndexError(f"No spectrum {slit_id}!")

        wstart, wstride, length = self._waves[slit_id]
        return np.arange(wstart, wstart + (length - 0.5) * wstride, wstride)

    @staticmethod
    def _set_header(image, wstart: float, wstride: float, slit_id: str, content, etype):
        """Sets the header for the spectra

        Parameters
        ----------
        image: fits.ImaheHdu
          The Image HDU to update

        wstart: float
          The wavelength starting point

        wstride: float
          The wavelength step

        slit_id: str
          The spectrum ID

        etype: str
          The sepctrum data type
        """
        image.header['EXTNAME'] = f'{slit_id}.{content}.{etype}'
        image.header['SLIT_ID'] = f'{slit_id}'
        image.header['ECONTENT'] = content
        image.header['ETYPE'] = etype

        image.header['CTYPE1'] = 'WAVE'
        image.header['CTYPE2'] = 'PIXEL'

        image.header['CUNIT1'] = "Angstrom"
        image.header['CUNIT2'] = "pix"

        image.header['CRPIX1'] = 1.0
        image.header['CRPIX2'] = 1.0

        image.header['CRVAL1'] = wstart
        image.header['CRVAL2'] = 1.0

        image.header['CDELT1'] = wstride
        image.header['CDELT2'] = 1.0

    def get_dataset(self, slit_id: str, content):
        """Gets spectrum dataset

        Parameters
        ----------
        slit_id: str
          The spectrum ID

        Returns
        -------
        np.array, shape(3, N, M)
          The spectrum dataset containg: data, variance and quality
        """
        data = [None, None, None]
        for hdu in self[1:]:
            if isinstance(hdu, fits.ImageHDU) is False:
                continue
            elif 'SLIT_ID' not in hdu.header or hdu.header['SLIT_ID'] != slit_id:
                continue
            elif 'ECONTENT' not in hdu.header or hdu.header['ECONTENT'] != content:
                continue
            elif 'ETYPE' not in hdu.header:
                continue
            elif hdu.header['ETYPE'] == Spectra.DATA:
                data[0] = hdu.data
            elif hdu.header['ETYPE'] == Spectra.VARIANCE:
                data[1] = hdu.data
            elif hdu.header['ETYPE'] == Spectra.QUALITY:
                data[2] = hdu.data
            else:
                log.debug("Invalid HDU")

        if data[0] is None:
            raise IndexError(f'No {content} for object {slit_id}')

        for i in (1, 2):
            if data[i] is None:
                data[i] = np.full_like(data[0], np.nan)

        return np.array(data)

    def dump_join(self, filename: str, **kargs):
        """Join al spectra 2D and save them into file in a unique image.
        Usefull to compare wavelength calibrations between slits.

        Parameters
        ----------
        filename: str
          The output file name

        kargs: dict, optional
          The keyword optional parameters required by HDUlist.writeto method
        """
        data = []
        header = None
        for hdu in self:
            if 'ETYPE' not in hdu.header or hdu.header['ETYPE'] != Spectra.DATA:
                continue
            if header is None:
                header = hdu.header
                header['EXTNAME'] = "JOINED"
                del header['SLIT_ID']
                del header['ECONTENT']
                del header['ETYPE']

            data.append(hdu.data)
            data.append(np.full((1, hdu.data.shape[1]), np.nan))

        # Remove the last useless NAN row
        exr2d = fits.ImageHDU(data=np.concatenate(data[:-1]), header=header)
        exr2d.writeto(filename, **kargs)

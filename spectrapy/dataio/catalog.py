#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File catalog.py
#
# Created on: May 31, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class to handle lines catalog"""

import numpy as np
from astropy.table import Table, vstack, unique
from astropy import log


class Catalog(object):
    """Class to handle line catalogs used in IDSModel calibration"""
    WEAK = 0
    STRONG = 1
    DOUBLE = 2

    @staticmethod
    def load(catalog, wstart: float = None, wend: float = None):
        """Loads line catalog.
        Lines in the catalog are sorted and the duplicated lines will be removed

        Parameters
        ----------
        catalog: array_like or str
          catalog parameter could be:
            - Table of lines
            - file name of the catalog
            - list of catalog file names in case you are loading several catalogs

        wstart: float, optional
          If wstart is not None, loads lines >= wstart

        wend: float, optional
          If wend is not None, loads lines <= wend

        Returns
        -------
        Catalog
          The catalog
        """

        if isinstance(catalog, str):
            lines = Catalog._load_single_catalog(catalog)

        elif isinstance(catalog, Table):
            lines = catalog

        elif hasattr(catalog, '__iter__') and len(catalog)>0 and isinstance(catalog[0], str):
            lines = Catalog._load_multi_catalog(catalog)

        else:
            raise IOError("Invalid input catalog")

        # Remove duplicateds
        Catalog.set_col_names(lines)
        lines = unique(lines, keys='pos')
        lines.sort('pos')

        log.debug(f"Loaded {len(lines)} lines")

        if wstart is not None:
            lines = lines[lines['pos'] >= wstart]

        if wend is not None:
            lines = lines[lines['pos'] <= wend]

        return lines

    @staticmethod
    def _load_single_catalog(filename: str):
        """Loads just a single catalog an returns it

        Parameters
        ----------
        filename: str
          File name of the catalog

        Returns
        -------
        Catalog
          The catalog
        """

        try:
            return Table.read(filename, format='ascii', names=['pos', 'label', 'flag'])
        except Exception:
            pass

        try:
            catalog = Table.read(filename, format='ascii', names=['pos', 'label'])
        except Exception:
            raise IOError(f"Invalid input catalog {filename}")

        catalog.add_column(Table.Column(np.ones(len(catalog), dtype=np.float64)), name='flag')
        return catalog

    @staticmethod
    def _load_multi_catalog(filenames):
        """Loads multiple catalogs files and stacks them together

        Parameters
        ----------
        filenames: list
          The list of catalog file names

        Returns
        -------
        Catalog
          The stacked catalog
        """
        lines = []
        for single_catalog in filenames:
            lines.append(Catalog._load_single_catalog(single_catalog))

        return vstack(lines)

    @staticmethod
    def set_col_names(catalog):
        """Sets the catalog column names

        Parameters
        ----------
        catalog: Table
          The catalog instance
        """
        if catalog.colnames[0] != 'pos':
            catalog.rename_column(catalog.colnames[0], 'pos')

        if catalog.colnames[1] != 'label':
            catalog.rename_column(catalog.colnames[1], 'label')

        if len(catalog.colnames) > 2 and catalog.colnames[2] != 'flag':
            catalog.rename_column(catalog.colnames[2], 'flag')

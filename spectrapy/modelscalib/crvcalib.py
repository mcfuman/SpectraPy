#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File crvcalib.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the classes to calibrate the spectra tracing"""

import numpy as np
from astropy import log

from spectrapy.models.crvmodel import CrvModel2d

from .optcalib import OptModelCalibration


class CrvModelCalibration(OptModelCalibration):
    """Class to calibrate the CrvModel"""
    CRV_REGION_TYPE = "point"

    def __init__(self, instrument, mask=None, opt=None, crv=None):
        """
        Initialize the calibrator: set the model and open DS9
         Parameters
        ----------
        instrument: Instrument, str
           The instrument related to the calibration models

        mask: Mask, str, optional
          The instrument Mask in use

        opt: OptModel2d, str, optional
          The OptModel2d to calibrate

        crv: CrvModel2d, str, optional
          The CrvModel2d to calibrate
        """
        OptModelCalibration.__init__(self, instrument, mask, opt)
        self._crv = None

        self.crv = crv

        # Store position of trace spectra and their variances
        self._traces = {}
        self._variances = {}

        self._red = None
        self._blue = None

    def set_trace_limits(self, blue, red):
        """Given the slit reference position moves blue a given amount of pixels
        in the blue direction and in the red direction.

        Parameters
        ----------
        blue: int
          The amount of pixel in the blue direction

        red: int
          The amount of pixel in the red direction
        """
        if blue < 0.:
            raise ValueError("The blue amount of pixels must be positive")

        if red < 0.:
            raise ValueError("The red amount of pixels must be positive")

        self._red = red
        self._blue = -1.0 * blue

    def _auto_set_trace_limits(self):
        """Set trace limits automatically on the base if image shape
        """
        if self._crv.dispersion_direction in ('LR', 'RL'):
            npix = self._ds9.xlen // 2
        else:
            npix = self._ds9.ylen // 2
        log.warning(f"Set trace limits automatically ({npix}, {npix}). Use set_trace_limits() to "
                    f"costumize them")
        return self.set_trace_limits(npix, npix)

    @property
    def crv(self):
        """Gets the Curvature model instance
        """
        if self._crv is None:
            raise AttributeError("CrvModel not set yet!")
        return self._crv

    @crv.setter
    def crv(self, value):
        """Sets the Curvature model instance

        Parameters
        ----------
        value: str or CrvModel2d
          The Curvature Model instance
        """
        if value is None or isinstance(value, CrvModel2d):
            self._crv = value

        elif isinstance(value, str):
            self._crv = CrvModel2d.load(value)

        else:
            raise IOError("Invalid input CrvModel")

    def new_crv_model(self, deg: int, xdeg: int, ydeg: int):
        """Sets up a new model

        Parameters
        ----------
        deg: int
          The local degree of the curvature model

        xdeg: int
          The global X degree

        ydeg: int
          The global Y degree
        """
        self.crv = CrvModel2d(deg, xdeg, ydeg, self.instrument)
        return self.crv

    def plot_crv_model(self, npixels=10, pos=(0, )):
        """Plots DS9 regions of slits described by mask geometry
        """

        self._ds9.reset_background()

        if self._red is None:
            self._auto_set_trace_limits()

        for _id in self._mask:
            self._reg_plot_spectrum_trace(self._mask[_id], npixels, pos=pos)
            self._reg_plot_slit(self._mask[_id], color=OptModelCalibration.BK_REGION_COLOR)

    def _reg_plot_spectrum_trace(self, slit, npixels, color=OptModelCalibration.REGION_COLOR,
                                 pos=(0,)):
        """Gets the pixels trace and plots them in DS9
        """
        for t in pos:
            points = self._get_pix_trace(slit, t, npixels)
            self._reg_set_pix_trace(points, slit.slit_id, t=t, color=color)

    def _reg_set_pix_trace(self, points, slit_id: str, t: float = 0,
                           color: str = OptModelCalibration.REGION_COLOR):
        """Creates DS9 regions for points of the curvature and plots them in DS9

        Parameters
        ----------
        points: array_like, shape(N, 2)
          The curvature points

        slit_id: str
          The slit ID

        t: float, optional
          The bezier parameter along the slit

        color: str, optional
          The DS9 regions color
        """
        regions = ""

        edge = f"{t:.2f}"

        for point in points:
            _point = OptModelCalibration.data_to_region(point)
            # Just for display resons at the edge
            if edge == "0.00":
                self._px_center_to_left_border(_point)
            elif edge == "1.00":
                self._px_center_to_right_border(_point)

            regions += f"image; {CrvModelCalibration.CRV_REGION_TYPE} {_point[0]} {_point[1]} "
            regions += "#point=cross 20 tag={%s} tag={t=%s} text={%s} color=%s\n" % \
                (slit_id, edge, slit_id, color)

        self._ds9.set("regions", regions)

    def _reg_plot_crv_regions(self, regions, color=OptModelCalibration.REGION_COLOR):
        """Plots curvatures regions in DS9

        Parameters
        ----------
        regions: dict
          The parsed regions

        color: str, optional
          The DS9 regions color
        """
        for edge in regions:
            for slit_id in regions[edge]:
                points = regions[edge][slit_id]
                self._reg_set_pix_trace(points, slit_id, t=float(edge), color=color)

    def _get_pix_trace(self, slit, t, npixels: int):
        """Returns vertices of slit in pixels

        Parameters
        ----------
        slit: MaskSlit
          The slit geometric description

        t: float
          The bezier paramter along the slit. It goes from 0 up to 1

        npixels: int
          The number of points between blue and red limits

        Returns
        -------
        array_like, shape(npixels, 2)
          The trace pixels
        """

        pix_slit_margin, _ = slit.get_pix_position(t)

        try:
            npixels = int(npixels)
        except:
            raise TypeError("Invalid npixels parameter value")

        if npixels < self.crv.deg + 1:
            raise ArithmeticError("Parameter value npixels too low")

        delta_disp = np.linspace(self._blue, self._red, npixels)
        return pix_slit_margin + self._crv.compute(pix_slit_margin, delta_disp)[0]

    def _reg_parse_crv_regions(self):
        """Parses DS9 region file and return a dictinary containg new slit curvature in pixels
        """

        pixel_pos = {}
        for region in self._ds9.regions:
            if region.name != CrvModelCalibration.CRV_REGION_TYPE:
                continue

            for tag in region.attr[1]['tag']:
                if tag.startswith("t="):
                    edge = tag[2:]
                    if 0 <= float(edge) <= 1.0:
                        break
            else:
                raise ValueError("Error in region file. No valid edge tag")

            if edge not in pixel_pos:
                pixel_pos[edge] = {}

            slit_id = region.attr[1]['text']
            if slit_id not in pixel_pos[edge]:
                pixel_pos[edge][slit_id] = []

            pixels = OptModelCalibration.region_to_data(region)

            if edge == "0.00":
                self._left_border_to_px_center(pixels)
            elif edge == "1.00":
                self._right_border_to_px_center(pixels)

            if region.attr[1]['color'] == OptModelCalibration.FAILURE_COLOR:
                log.warning(f"Ignoring point ({pixels[0]:.1f}, {pixels[1]:.1f})")
                continue

            pixel_pos[edge][slit_id].append(pixels)

        for edge in pixel_pos:
            for slit_id in pixel_pos[edge]:
                pixel_pos[edge][slit_id] = np.array(pixel_pos[edge][slit_id])

        return pixel_pos

    def reload_traces_from_regions(self):
        """In case user adjusts regions by hand, this function reloads region positions from DS9
        and ignores variances on region positions.
        """
        log.info("Reloading trace positions from regions")

        self._traces = self._reg_parse_crv_regions()
        self._variances = dict([(edge, {}) for edge in self._traces])

    def fit_crv_model(self, weight: bool = True, filename: str = None):
        """Reads adjusted slit curvature in pixel and refit the CrvModel

        Parameters
        ----------
        weight: bool, optional
          If weight is True, compute a weighted fit where weights are 1/sqrt(variances)

        filename: str, optional
          If not None, loads regions from file
        """

        if filename is not None:
            self._ds9.replace_regions(filename)

        if len(self._traces) == 0:
            self.reload_traces_from_regions()

        ntraces = 0
        for edge in self._traces:
            ntraces += len(self._traces[edge])

        if ntraces == 0:
            raise ValueError("No traces")

        x = []
        y = []
        z = []
        z_var = []

        for edge in self._traces:
            for _id in self._traces[edge]:
                if len(self._traces[edge][_id]) == 0:
                    log.info(f"No valid tracing point for slit {_id}. Skip it")
                    continue

                xdata, ydata = self._traces[edge][_id].T
                pix_slit_margin, _ = self._mask[_id].get_pix_position(float(edge))

                if weight is True and edge in self._variances and _id in self._variances[edge]:
                    wdata = 1. / np.sqrt(self._variances[edge][_id])
                else:
                    wdata = None

                model = self.crv.refit_trace(pix_slit_margin, xdata, ydata, wdata=wdata)
                model.log_coeffs()

                z.append(model.coeffs)
                z_var.append(model.variance)

                x.append(pix_slit_margin[0])
                y.append(pix_slit_margin[1])

        self.crv.refit(x, y, z, var=z_var)

        self._traces = {}
        self._variances = {}

    def _get_spectrum_area(self, slit_id: str, left_margin: int = 0, right_margin: int = 0):
        """Extracts sub image containing the current slit. If left_margin or right_margin are >0
        adds extra padding round the slit

        Parameters
        ----------
        slit_id: str, optional
          The slit ID

        left_margin: int, optional
          The extra padding on the left edge of the slit

        right_margin: int, optional
          The extra padding on the right edge of the slit

        Returns
        -------
        array_like, shape(4,)
          X and Y pixel start of the sub image
          X and Y pixels length of the sub image
        """

        slit = self._mask[slit_id]

        slit_pix, _ = slit.get_slit_pix(left_margin, right_margin)

        verteces = np.zeros((4, 2))

        verteces[0], verteces[1] = self.crv.get_trace_edges(slit_pix[0], self._ds9.image_dims)
        verteces[2], verteces[3] = self.crv.get_trace_edges(slit_pix[-1], self._ds9.image_dims)

        # Check verteces are inside the image
        row_start = int(min(verteces[:, 1]))
        row_end = int(max(verteces[:, 1]))
        col_start = int(min(verteces[:, 0]))
        col_end = int(max(verteces[:, 0]))

        return col_start, row_start, col_end - col_start, row_end - row_start

    def display_single_slit(self, slit_id: str):
        """Displays just a single slit trace.

        Parameters
        ----------
        slit_id: str
          The slit ID to display
        """
        # Plot just a single slit
        if slit_id not in self._mask:
            raise IndexError(f"Invalid slit {slit_id}")

        # Trim image slit
        slit = self._mask[slit_id]
        x, y, lenght, width = self._get_spectrum_area(slit_id)
        self._ds9.trim_image(x, y, lenght, width)

        # Offset the slit vertices to the image thumbnail
        vertices = slit.get_pix_vertices()
        vertices[:, 0] -= self._ds9.xstart
        vertices[:, 1] -= self._ds9.ystart
        self._set_slit_regions(vertices, slit_id, color=CrvModelCalibration.BK_REGION_COLOR)

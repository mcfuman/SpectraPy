#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File optcalib.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains classes to calibrate the Optical Model"""

import numpy as np
from astropy import log

from spectrapy.mask.mask import Mask
from spectrapy.models.optmodel import OptModel2d
from spectrapy.models.instrument import Instrument

from spectrapy.dataio.display import Display


class OptModelCalibration(object):
    """Class to calibrate the OptModel"""
    OPT_REGION_TYPE = "polygon"
    REGION_COLOR = "green"
    BK_REGION_COLOR = "blue"
    FAILURE_COLOR = "red"

    def __init__(self, instrument, mask=None, opt=None):
        """
        Initializes the calibrator: sets the model and opens DS9

        Parameters
        ----------
        instrument: str or Instrument
          The instrument configuration file or istance to calibrate

        mask: str or Mask, optional
          The mask descriptrion used into calibration procedure

        opt: str or OptModel2d, optional
          The optical model file or instance to calibrate
        """
        self._instrument = None
        self._mask = None
        self._opt = None

        self.instrument = instrument
        self.mask = mask
        self.opt = opt

        self._ds9 = Display()

    @property
    def instrument(self):
        """The instrument instance"""
        return self._instrument

    @instrument.setter
    def instrument(self, value):
        """Sets the new instrument

        Parameters
        ----------
        value: Instrument or str
          The new instrument to set
        """
        if isinstance(value, Instrument):
            self._instrument = value

        elif isinstance(value, str):
            self._instrument = Instrument.load(value)

        else:
            raise TypeError("Invalid input Instrument type")

    @property
    def opt(self):
        """Gets the optical model"""
        if self._opt is None:
            raise AttributeError("OptModel not set yet!")
        return self._opt

    @opt.setter
    def opt(self, value):
        """The new Optical Model

        Parameters
        ----------
        value: str ot OptModel2d
          The new Optical Model
        """
        if value is None:
            self._opt = None
            if self._mask is not None:
                self._mask.opt = None
            return

        elif isinstance(value, OptModel2d):
            self._opt = value

        elif isinstance(value, str):
            self._opt = OptModel2d.load(value)

        else:
            raise TypeError("Invalid input OptModel type")

        if self._opt.dispersion_direction != self.instrument.dispersion_direction:
            raise AttributeError("OptModel and Instrument dispersion directions don't match")

        if self._mask:
            self._mask.opt = self.opt

    def new_opt_model(self, xdeg: int, ydeg: int):
        """Creates a new model from scratch

        Parameters
        ----------
        xdeg: int
          The X degree of the global 2D model

        ydeg: int
          The Y degree of the global 2D model
        """
        if self._mask is not None:
            fov_scales = self._mask.compute_scales()
        else:
            fov_scales = None
        self.opt = OptModel2d(xdeg, ydeg, self.instrument, fov_scales)
        if self._mask is not None:
            self._mask.opt = self.opt
        return self.opt

    def plot_opt_model(self, color: str = BK_REGION_COLOR, edit: bool = False):
        """Plots DS9 regions of slits described by mask geometry

        Parameters
        ----------
        color: str, optional
          The color of the center of the model

        edit: bool, optional
          If False slits region can be just moved on the frame.
          If True slit geomtries can also be modified. Usefull in LS case
        """

        self._ds9.reset_background()

        pixel = OptModelCalibration.data_to_region(self.opt.shift)
        self._ds9.set("regions", f"image; point {pixel[0]} {pixel[1]} "
                      f"#point=cross 40 tag={{OptModelCenter}} color={color}")

        if self._mask is None:
            return

        for _id in self._mask:
            self._reg_plot_slit(self._mask[_id], edit=edit)

    def _reg_plot_slit(self, slit, color: str = REGION_COLOR, edit: bool = False):
        """Plots DS9 regions of a single slit.

        Parameters
        ----------
        slit: MaskSlit
          The slit instance

        color: str, optional
          The color string

        edit: bool, optional
          If True the slit edges can be changed
        """
        vertices = slit.get_pix_vertices()
        self._set_slit_regions(vertices, slit.slit_id, color=color, edit=edit)

    def _set_slit_regions(self, vertices, slit_id: str, color: str = REGION_COLOR,
                          edit: bool = False):
        """Adds region file for the current slit
        """
        pixels = OptModelCalibration.data_to_region(vertices)
        self._px_center_to_left_border(pixels)
        polygon = " ".join(map(str, pixels.ravel()))
        self._ds9.set("regions", f"image; {OptModelCalibration.OPT_REGION_TYPE}({polygon})"
                      f"#text={{{slit_id}}} color={color} edit={int(edit)}")

    @staticmethod
    def region_to_data(region):
        """Ds9 pixels starts from 1, data from 0. This method removes the offset from ds9 regions
        """
        return np.array(region.coord_list) - 1

    @staticmethod
    def data_to_region(data):
        """Add 1 pixel offset to data coordinate to display them
        (in the right position) as DS9 region.
        """
        return data + 1

    def _px_center_to_left_border(self, pixels):
        """Both the software and DS9 assume the pixel position in the center of the pixel.
        For display reason we want plot some regions (slit position and tracing)
        on the border of the pixel.
        This function add the 0.5 offset in the approprated direction.
        """
        # In this way we can handle both single point or an array of points
        pixels.ravel()[::2] -= 0.5 * self._opt.slit_versor[0]
        pixels.ravel()[1::2] -= 0.5 * self._opt.slit_versor[1]

    def _px_center_to_right_border(self, pixels):
        """Both the software and DS9 assume the pixel position in the center of the pixel.
        For display reason we want plot some regions (slit position and tracing)
        on the border of the pixel.
        This function add the 0.5 offset in the approprated direction.
        """
        # In this way we can handle both single point or an array of points
        pixels.ravel()[::2] += 0.5 * self._opt.slit_versor[0]
        pixels.ravel()[1::2] += 0.5 * self._opt.slit_versor[1]

    def _left_border_to_px_center(self, pixels):
        """Both the software and DS9 assume the pixel position in the center of the pixel.
        For display reason we want plot some regions (slit position and tracing) on the border
        of the pixel.
        This function add the 0.5 offset in the approprated direction.
        """
        # In this way we can handle both single point or an array of points
        pixels.ravel()[::2] += 0.5 * self._opt.slit_versor[0]
        pixels.ravel()[1::2] += 0.5 * self._opt.slit_versor[1]

    def _right_border_to_px_center(self, pixels):
        """Both the software and DS9 assume the pixel position in the center of the pixel.
        For display reason we want plot some regions (slit position and tracing) on the border
        of the pixel.
        This function add the 0.5 offset in the approprated direction.
        """
        # In this way we can handle both single point or an array of points
        pixels.ravel()[::2] -= 0.5 * self._opt.slit_versor[0]
        pixels.ravel()[1::2] -= 0.5 * self._opt.slit_versor[1]

    def _parse_opt_regions(self):
        """Parses DS9 region file and returns a dictionary containg new slit positions in pixels
        """

        pixel_pos = {}
        for region in self._ds9.regions:
            if region.name != OptModelCalibration.OPT_REGION_TYPE:
                continue
            _id = region.attr[1]['text']

            pixels = OptModelCalibration.region_to_data(region)
            self._left_border_to_px_center(pixels)
            pixel_pos[_id] = pixels

        return pixel_pos

    def fit_opt_model(self):
        """Reads adjusted slit position in pixel from DS9 regions and refits the OptModel
        """

        # Parse ds9 regione file
        pixel_pos = self._parse_opt_regions()
        if len(pixel_pos) == 0:
            Exception("No regions")

        log.info("Fitting mask [mm] positions vs region [pixel] positions")
        mm_pos = []
        pix_pos = []
        for _id in pixel_pos:
            mm_pos.extend(self._mask[_id].vertices.ravel())
            pix_pos.extend(pixel_pos[_id].ravel())

        mm_pos = np.array(mm_pos)
        mm_pos.shape = (len(mm_pos) // 2, 2)
        pix_pos = np.array(pix_pos)
        pix_pos.shape = (len(pix_pos) // 2, 2)

        self.opt.refit(mm_pos, pix_pos)

    @property
    def mask(self):
        """Gets the mask in use"""
        if self._mask is None:
            raise AttributeError("Mask not set yet!")
        return self._mask

    @mask.setter
    def mask(self, value):
        """Sets a new mask to use in Optical Model calibration

        Parameters
        ----------
        value: str or Mask instance
          The new mask or the file name containg the new mask to load
        """

        if value is None:
            self._mask = None
        elif isinstance(value, str):
            self._mask = Mask.load(value)
        elif isinstance(value, Mask):
            self._mask = value
        else:
            raise TypeError("Invalid input Mask type")

    def load_image(self, *filenames):
        """Loads image data from file

        Parameters
        ----------
        filenames: list
          The input list of file names containg data
        """
        self._ds9.load_image(*filenames, data_hdu=self.instrument.data_hdu)

    def _sort_slit_by_distances(self, use_ref: bool = False):
        """Sorts mask slits from the center of the FOV up to the edges

        Parameters
        ----------
        use_ref: bool, optional
          If True uses also the reference slits
        """
        if use_ref is False:
            slit_ids = [_id for _id in self.mask if not self.mask[_id].reference]
        else:
            slit_ids = list(self.mask.keys())
        slit_ids = np.array(slit_ids)

        distances = np.zeros(len(slit_ids))
        for k, slit_id in enumerate(slit_ids):
            center = self.mask[slit_id].get_pix_center()[0] - self.opt.shift
            distances[k] = center[0] * center[0] + center[1] * center[1]

        return slit_ids[np.argsort(distances)]

    def restart_display(self):
        """Restart DS9 displat in case it is closed accidentally
        """
        self._ds9 = Display()

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File idscalib.py
#
# Created on: Aug 28, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class the calibrate IDS model"""


import numpy as np
from astropy import log
import matplotlib.pyplot as plt

from spectrapy.dataio.catalog import Catalog
from spectrapy.models.idsmodel import IdsModel2d

from .crvcalib import CrvModelCalibration


class IdsModelCalibration(CrvModelCalibration):
    """Class to calibrate the IdsModel"""

    IDS_REGION_TYPE = "line"
    LINE_MARKER_SIZE = 10
    LINE_COLOR = 'green'
    FAINT_LINE_COLOR = 'pink'
    DOUBLE_LINE_COLOR = 'red'

    # pylint: disable=too-many-arguments

    def __init__(self, instrument, mask=None, opt=None, crv=None, ids=None):
        """
        Initialize the calibrator: set the model and open DS9

        Parameters
        ----------
        instrument: Instrument,str
           The instrument related to the calibration models

        mask: Mask, str, optional
          The instrument Mask in use

        opt: OptModel2d, str, optional
          The OptModel2d to calibrate

        crv: CrvModel2d, str, optional
          The CrvModel2d to calibrate

        ids: IdsModel2d, str, optional
          The IdsModel2d to calibrate
        """
        CrvModelCalibration.__init__(self, instrument, mask, opt, crv)

        self._ids = None
        self.ids = ids

        plt.ion()

    @property
    def ids(self):
        """The IDSModel instance"""
        if self._ids is None:
            raise AttributeError("IdsModel not set yet!")
        return self._ids

    @ids.setter
    def ids(self, value):
        """Sets the IDS model instance or load it from file

        Parameters
        ----------
        value: IdsModel2d instance or str
          The IDS model 2D instance of the file name containg the IDS model
        """
        if value is None or isinstance(value, IdsModel2d):
            self._ids = value

        elif isinstance(value, str):
            self._ids = IdsModel2d.load(value)

        else:
            raise TypeError("Invalid input Ids Model")

    def new_ids_model(self, deg: int, xdeg: int, ydeg: int):
        """Creates new IDS model from scratch

        Parameters
        ----------
        deg: int
          The degree of the local IDS 1D polynomial

        xdeg: int
          The degree along the X axis of the global IDS polynomial

        ydeg: int
          The degree along the Y axis of the global IDS polynomial
        """
        self.ids = IdsModel2d(deg, xdeg, ydeg, self.instrument)
        return self.ids

    def _fit_1d_model(self, slit, line_pos, t):
        pos, _ = slit.get_pix_position(t)

        delta_disp = self.ids.get_delta_dispersion(pos, line_pos[:, 1:])
        return self.ids.refit_slit(line_pos[:, 0], delta_disp)

    def _refit_single_slit(self, slit, regions):
        """Refits the single slit solution"""
        if len(regions) == 0:
            raise ValueError("No regions!")

        log.info("Fitting single slit solution...")

        models = {}
        for t in regions:
            models[t] = self._fit_1d_model(slit, regions[t], float(t))
            models[t].log_coeffs()

        # Updated global model using just the last (in many case the only one) model
        # pylint: disable=undefined-loop-variable
        model = models[t]
        self.ids.model *= 0.0

        for idx, coeff in enumerate(model.coeffs):
            self.ids.model[idx][0][0] = coeff

        log.info("Updated model")
        return models

    def _load_catalog(self, catalog, wstart: float = None, wend: float = None):
        """Loads line catalog if required.
        Lines catalog are sorted and duplicated lines removed

        Parameters
        ----------
        catalog: array_like or str
          catalog parameter could be:
            - list of catalog lines
            - file name of the catalog
            - list of catalog file names in case you are loading several catalogs

        wstart: float, optional
          Removes from catalog all lines before wstart

        wend:  float, optional
          Removes from catalog all lines after wend

        col: int, optional
          Column number to read
        """

        lines = Catalog.load(catalog, wstart=wstart, wend=wend)

        if len(lines) <= self.ids.deg:
            raise ArithmeticError("Number of lines must be > deg of IdsModel")

        return lines

    def plot_ids_model(self, catalog, *, wstart: float = None, wend: float = None,
                       slit_id: str = None, nsplit: int = 1, use_ref: bool = False):
        """Plots DS9 regions of slits described by mask structure

        Parameters
        ----------
        catalog: str
          The lines catalog

        wstart: float, optional
          The starting point of the lines catalog

        wend: float, optional
          The ending point of the lines catalog

        slit_id: str, optional
          In case is not None display only this slit

        nsplit: int, optional
          The number of points for each slit
        """

        if self._ds9.has_image() is False:
            raise AttributeError("Image not loaded yet!")

        self._ds9.reset_background()

        # Load catalog
        catalog = self._load_catalog(catalog, wstart=wstart, wend=wend)

        if slit_id is not None:
            self.display_single_slit(slit_id)

        # Single slit or overall mask
        if slit_id is None:
            slit_ids = self._mask.keys()
        else:
            slit_ids = (slit_id, )

        for _id in slit_ids:
            slit = self._mask[_id]
            self._set_slit_regions(slit.get_pix_vertices(), _id,
                                   color=CrvModelCalibration.BK_REGION_COLOR)

            if use_ref is False and self._mask[_id].reference is True:
                continue
            self._plot_catalog_regions(catalog, _id, nsplit)

    def _plot_catalog_regions(self, catalog, slit_id: str, nsplit: int = 1):
        for line in catalog:
            pieces = self._get_line_pixels(self._mask[slit_id], line, nsplit)
            # If single slit shift the pieces
            for t in pieces:
                pieces[t][0::2] -= self._ds9.xstart
                pieces[t][1::2] -= self._ds9.ystart

            self._set_line_regions(pieces, line, slit_id)

    def _set_line_regions(self, pieces, line, slit_id: str):
        """Sets region for a given line in DS9.

        Parameters
        ----------
        pieces: np.array, shape(N, )
          The pieces along the slit: bezier values from 0 up to 1

        line: Table
          The catalog row for a given line

        slit_id: str
          The slit ID
        """
        if line['flag'] == Catalog.WEAK:
            color = IdsModelCalibration.FAINT_LINE_COLOR
        elif line['flag'] == Catalog.DOUBLE:
            color = IdsModelCalibration.DOUBLE_LINE_COLOR
        else:
            color = IdsModelCalibration.LINE_COLOR
        for t in pieces:
            pixels = IdsModelCalibration.data_to_region(pieces[t])
            self._ds9.set("regions", f"image; {IdsModelCalibration.IDS_REGION_TYPE} "
                          f"{pixels[0]} {pixels[1]} {pixels[2]} {pixels[3]} "
                          f"# line=1 0 tag={{{line['pos']}}} tag={{{slit_id}:{t}}} "
                          f"text={{{line['pos']}}} color={color}")

    def _get_line_pixels(self, slit, line, nsplit: int = 1):
        """Returns vertices of slit in pixels

        Parameters
        ----------
        slit: MaskSlit
          The slit geometric description

        line: Table
          The catalog row for the line

        nsplit: int, optional
          The number of split of the slit

        Returns
        -------
        dict
          The positions of the line for each position
        """
        line_positions = {}

        # Slit slit into segments
        for t in np.linspace(0, 1, nsplit):
            pos, _ = slit.get_pix_position(t)
            delta_disp, _ = self.ids.compute(pos, line['pos'])
            delta_curve, _ = self.crv.compute(pos, delta_disp)

            xstart, ystart = pos + delta_curve
            xend = xstart - IdsModelCalibration.LINE_MARKER_SIZE * self._opt.slit_versor[0]
            yend = ystart - IdsModelCalibration.LINE_MARKER_SIZE * self._opt.slit_versor[1]
            line_positions[f"{t:.2f}"] = np.array([xstart, ystart, xend, yend])

        return line_positions

    def _parse_ids_regions(self):
        """Parses DS9 region file and return a dictinary containg new slit curvature in pixels
        """

        line_pixs = {}
        for region in self._ds9.regions:
            if region.name != IdsModelCalibration.IDS_REGION_TYPE:
                continue

            tags = list(region.attr[1]['tag'])
            if ':' in tags[0]:
                line = tags[1]
                slit_id, piece = tags[0].split(':')
            else:
                line = tags[0]
                slit_id, piece = tags[1].split(':')

            if slit_id not in line_pixs:
                line_pixs[slit_id] = {}
            if piece not in line_pixs[slit_id]:
                line_pixs[slit_id][piece] = []

            # Vertical??
            pixel = IdsModelCalibration.region_to_data(region)

            line_pixs[slit_id][piece].append((float(line), pixel[0], pixel[1]))

        for slit_id in line_pixs:
            for piece in line_pixs[slit_id]:
                line_pixs[slit_id][piece] = np.array(line_pixs[slit_id][piece])
                line_pixs[slit_id][piece][:, 1] += self._ds9.xstart
                line_pixs[slit_id][piece][:, 2] += self._ds9.ystart

        return line_pixs

    def fit_ids_model(self, single_slit=False, filename=None):
        """Reads adjusted slit curvature in pixel and refit the CrvModel

        Parameters
        ----------
        single_slit: bool, optional
          Fit the solution for a single slit

        filename: str, optional
          If it is not None, reload regions from filename
        """

        if filename is not None:
            self._ds9.replace_regions(filename)

        # Parse ds9 regione file
        dispersions = self._parse_ids_regions()
        if len(dispersions) == 0:
            raise IOError("No regions")

        if single_slit is True and len(dispersions) == 1:
            # Fit solution just for a single slit
            slit_id = list(dispersions.keys())[0]
            self._refit_single_slit(self._mask[slit_id], dispersions[slit_id])
            return

        xy = []
        z = []
        zvar = []

        for slit_id in dispersions:
            for piece in dispersions[slit_id]:

                line_pos = dispersions[slit_id][piece]
                pos, _ = self._mask[slit_id].get_pix_position(float(piece))

                delta_disp = self.ids.get_delta_dispersion(pos, line_pos[:, 1:])
                model = self.ids.refit_slit(line_pos[:, 0], delta_disp)
                model.log_coeffs()

                z.append(model.coeffs)
                zvar.append(model.variance)
                xy.append(pos)

        # Covariance is ignored
        xy = np.array(xy)
        self.ids.refit(xy[:, 0], xy[:, 1], z, var=zvar)

    def plot_slice(self, slit_id: str, t: float = 0.1):
        """Plots a slit slice along dispersion direction.
        This is just an auxiliary function. It can be usefull to match lines and references plots

        Parameters
        ----------
        slit_id: str
          The ID of slit to slice

        t: float, optional
          The t parameter used to select where perfome the slicing.
          This parameter goes from 0 up to 1. 0 is the left edge and 1 is the right edge.
          Every value between 0 and 1 is a point in the middle of the slit
        """
        # Get the slit position
        slit = self._mask[slit_id]
        slit_pixel, _ = slit.get_pix_position(t)

        # Get the Curvature Model to follow the slit
        crv_model = self.crv.get_local_model(slit_pixel)

        if self._ds9.has_image() is False:
            raise AttributeError("Image not loaded yet!")

        # Span the whole image along the dispersion direction
        if self.ids.dispersion_direction in ('LR', 'RL'):
            waves = np.arange(self._ds9.image_dims[0])
            delta_disp = waves - slit_pixel[0]
        else:
            waves = np.arange(self._ds9.image_dims[1])
            delta_disp = waves - slit_pixel[1]

        # Compute the spectrum slice
        delta_curve, _ = crv_model(delta_disp)
        fluxes, _ = self._ds9.image.interpolate(slit_pixel + delta_curve)

        # parse regions
        regions = self._parse_ids_regions()[slit_id]

        # find nearest region to the input t value
        dist = np.inf
        for key in regions:
            new_dist = abs(float(key) - t)
            if new_dist < dist:
                dist = new_dist
                treg = key

        # Plot region markers
        regions = regions[treg]
        for reg in regions:
            label = reg[0]
            if self.ids.dispersion_direction in ('LR', 'RL'):
                x = reg[1]
            else:
                x = reg[2]

            plt.annotate(label, xy=(x, 0), xytext=(x, -100), color='royalblue', size=12, rotation=90,
                         horizontalalignment='center', verticalalignment='top',
                         arrowprops=dict(color='sandybrown', alpha=0.8))  # , shrink=0.05))

        plt.tight_layout()
        plt.plot(waves, fluxes, color='firebrick')
        plt.show()

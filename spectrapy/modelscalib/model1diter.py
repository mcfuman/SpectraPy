#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#
#
# File model1diter.py
#
# Created on: Mar 19, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class to iterate IDS model calibration"""


import os
import pickle


class Model1dIterator(object):
    """Class used to iterate across the slits and store 1d models"""

    def __init__(self, slit_ids, catalog=None):
        """Sets slit order (and catalog if required)

        Parameters
        ----------
        slit_ids: array_like, shape(N,)
          List of slits ids

        catalog: astropy.table.Table
          List of lines
        """
        self._slit_ids = slit_ids
        self._catalog = catalog
        self._1d_models = {}
        self._k = 0

    def dump_models(self, filename: str, overwrite: bool = False):
        """Saves the models already stored

        Parameters
        ----------
        filename: str
          Output file name

        overwrite: bool, optional
          Overwrite flag in case file already exists
        """
        if overwrite is False and os.path.isfile(filename):
            raise IOError(f"File {filename} already exists!")

        with open(filename, 'wb') as outfile:
            pickle.dump(self._1d_models, outfile)

    def load_models(self, filename: str):
        """Loads already computed models

        Parameters
        ----------
        filename: str
          Input file name
        """
        if os.path.isfile(filename) is False:
            raise IOError(f"Invalid file {filename}!")

        with open(filename, 'rb') as infile:
            models = pickle.load(infile)

        for model_id in models:
            if model_id not in self._slit_ids:
                raise IndexError(f"Model ID {model_id} not in slit IDs!")

        self._1d_models = models

    @property
    def current_slit(self):
        """Gets the current slit ID
        """
        return self._slit_ids[self._k]

    def __len__(self):
        """Gets the number of slits
        """
        return len(self._1d_models)

    def __getitem__(self, obj_id):
        return self._1d_models[obj_id]

    def __iter__(self):
        return iter(self._1d_models)

    def has_model(self, slit_id: str = None):
        """Checks if model of a given slit ID already exists

        Parameters
        --------
        slit_id: str, optional
          The slit ID. In case is None, uses the current slit ID

        Returns
        -------
        bool
          True if the model exists
        """
        if slit_id is None:
            slit_id = self.current_slit

        if slit_id not in self._slit_ids:
            raise IndexError(f"Invalid slit ID {slit_id}!")

        return slit_id in self._1d_models

    def get_model(self, slit_id: str = None):
        """Gets the model of a given slit ID

        Parameters
        --------
        slit_id: str, optional
            The slit ID. In case is None, uses the current slit ID

        Returns
        -------
        1dModel
          The related 1D model
        """
        if slit_id is None:
            slit_id = self.current_slit

        return self._1d_models[slit_id]

    def set_model(self, model, slit_id: str = None):
        """Set the model for a given slit ID

        Parameters
        ----------
        model: Model1d
          The model 1d instance to set

        slit_id: str, optional
            The slit ID. In case is None, uses the current slit ID

        Returns
        -------
        1dModel
          The related 1D model
        """

        if slit_id is None:
            slit_id = self.current_slit

        if slit_id not in self._slit_ids:
            raise IndexError(f"Invalid slit ID {slit_id}!")

        self._1d_models[slit_id] = model

    @property
    def catalog(self):
        """Gets the catalog line list if available

        Returns
        -------
        array_like, shape(N, 3)
          List of list catalog
        """
        if self._catalog is None:
            raise AttributeError("No catalog!")

        return self._catalog

    @property
    def has_next(self):
        """Returns True if further slits are avaialble after the current one.
        Returns False if the end of the slit IDs list is reached
        """
        return self._k + 1 < len(self._slit_ids)

    def next(self):
        """Sets the cursor to the next slit.
        """
        if self.has_next:
            self._k += 1

    @property
    def has_prev(self):
        """Returns True if further slits are available before the current one.
        Returns False if the begin of the slit IDs list is reached
        """
        return self._k > 0

    def prev(self):
        """Sets the cursor to the previos slit.
        """
        if self.has_prev:
            self._k -= 1

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#
#
# File calib.py
#
# Created on: Sep 03, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the interface class to calibrate the models"""

from astropy import log

from .idscalib import IdsModelCalibration
from .model1diter import Model1dIterator


class ModelsCalibration(IdsModelCalibration):
    """Class to calibrate the IdsModel"""

    def __init__(self, instrument, mask=None, opt=None, crv=None, ids=None):
        """
        Calibrator initializer: it sets the models and open DS9

        Parameters
        ----------
        instrument: Instrument,str
           The instrument related to the calibration models

        mask: Mask, str, optional
          The instrument Mask in use


        opt: OptModel2d, str, optional
          The OptModel2d to calibrate

        crv: CrvModel2d, str, optional
          The CrvModel2d to calibrate

        ids: IdsModel2d, str, optional
          The IdsModel2d to calibrate
        """
        IdsModelCalibration.__init__(self, instrument, mask, opt, crv, ids)
        self._ids_iter = None

    def _calibrate_single_slit(self):
        """The method calibrates IDS model on a single slit.
        """
        slit_id = self._ids_iter.current_slit
        log.info(f"Calibrating slit {slit_id} "
                 f"[{self._ids_iter._k+1}/{len(self._ids_iter._slit_ids)}]...")

        if self._ids_iter.has_model(slit_id):
            # Reset global model
            self.ids.model *= 0.0
            model = self._ids_iter.get_model(slit_id)['0.00']
            for idx, coeff in enumerate(model.coeffs):
                self.ids.model[idx][0][0] = coeff

        IdsModelCalibration.display_single_slit(self, slit_id)
        self._plot_catalog_regions(self._ids_iter.catalog, slit_id)

    def ids_iter(self, catalog, wstart: float = None, wend: float = None, use_ref: bool = False):
        """Starts iterate the mask slit by slit, to achieve IDS calibration.

        Parameters
        ----------
        catalog: array_like or str
          The catalog used in IDS calibration

        wstart: float, optional
          The wavelength starting value for the catalog

        wend: float, optional
          The wavelength ending value for the catalog

        use_ref: bool
          If True, use also reference slits for IDS calibration
        """
        if self._ds9.has_image() is False:
            raise AttributeError("Image not loaded yet!")

        catalog = self._load_catalog(catalog, wstart=wstart, wend=wend)
        slit_ids = self._sort_slit_by_distances(use_ref)

        self._ids_iter = Model1dIterator(slit_ids, catalog)

        self._calibrate_single_slit()

    def _fit_slit(self):
        """Fits IDS model on the single slit
        """
        dispersions = self._parse_ids_regions()
        if len(dispersions) == 0:
            raise AttributeError("No regions!")

        slit_id = self._ids_iter.current_slit
        if slit_id not in dispersions:
            raise IndexError(f"No slit {slit_id} in regions!")

        regions = dispersions[slit_id]

        self._ids_iter.set_model(self._refit_single_slit(self._mask[slit_id], regions))

        if len(self._ids_iter) == 1:
            self._ds9.set('frame new')
            self._ds9.set('tile yes')
            if self.ids.dispersion_direction in ('LR', 'RL'):
                self._ds9.set('tile row')
            else:
                self._ds9.set('tile column')

    def next(self, fit=True):
        """Iterate to the next slit in IDS calibration.

        Parameters
        ----------
        fit: bool, optional
          If fit is False, move to the next slit without fitting the IDS model on the current slit.
        """
        if self._ids_iter is None:
            log.warning("IDS interactive calibation is OFF")
            return

        if fit is True:
            self._fit_slit()

        if self._ids_iter.has_next is False:
            log.warning("No more slits")
            return

        self._ids_iter.next()
        self._calibrate_single_slit()

    def prev(self, fit=True):
        """Iterate to the previous slit in IDS calibration.

        Parameters
        ----------
        fit: bool, optional
          If fit is False, move to the previous slit without fitting the IDS model on the current
          slit.
        """
        if self._ids_iter is None:
            log.warning("IDS interactive calibation is OFF")
            return

        if fit is True:
            self._fit_slit()

        if self._ids_iter.has_prev is False:
            log.warning("No more slits")
            return

        self._ids_iter.prev()
        self._calibrate_single_slit()

    def stop_iter(self, fit=True):
        """Stop IDS iteration and refit the global model if fit is True

        Parameters
        ----------
        fit: bool, optional
          If bool is False no IDS fit is performed
        """
        if self._ids_iter is None:
            log.warning("IDS interactive calibation is OFF")
            return

        if fit is True:
            x = []
            y = []
            z = []
            zvar = []

            for slit_id in self._ids_iter:
                model = self._ids_iter.get_model(slit_id)
                for t in model:
                    pos, _ = self._mask[slit_id].get_pix_position(float(t))

                    z.append(model[t].coeffs)
                    zvar.append(model[t].variance)
                    x.append(pos[0])
                    y.append(pos[1])

            #Covariance is ignored
            self.ids.refit(x, y, z, var=zvar)

        else:
            log.info(f"Fit is False. Ignored calibration adjustment")

        self._ds9.set('frame 2')
        self._ds9.set('frame delete')
        self.plot_ids_model(self._ids_iter.catalog)
        self._ids_iter = None

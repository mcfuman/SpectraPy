#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File extractiontable.py
#
# Created on: Sep 07, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains classes the handle the Extraction Table"""

import os
import warnings

import numpy as np
from astropy.io import fits
from astropy import log

from spectrapy.models.instrument import Instrument
from spectrapy.models.crvmodel import CrvModel1d
from spectrapy.models.idsmodel import IdsModel1d
from spectrapy.modelscalib.idscalib import IdsModelCalibration


class ExtTable:
    """Class to handle the data structure used to to store all model solutions.
    """
    HDU_NAME = "EXTTABLE"

    def __init__(self):
        """Initialize the table
        """
        self._slits = {}
        self._instrument = None

        self._opt = None
        self._crv = None
        self._ids = None

    @property
    def instrument(self):
        """The instrument configuration"""
        return self._instrument

    @instrument.setter
    def instrument(self, value):
        """Sets the instrument configuration

        Parameters
        ----------
        value: Instrument
          The new instrument configuration instance
        """
        if isinstance(value, Instrument) is False:
            raise TypeError("Invalid instrument instance")

        self._instrument = value

    # pylint: disable=too-many-arguments
    def add_row(self, slit_id: str, pos, var_pos, crv, ids):
        """Add row to the table.

        Parameters
        ----------
        slit_is: str
          The unique slit ID

        pos: array_like, shape(2,) [pixel]
          The detector (x,y) position (in pixels) for the models

        var_pos: array_like, shape(2,)
          The variance on `pos`

        crv: CrvModel1d
          The Curvature model for this position

        ids: IdsModel1d
          The Inverse Dispersion Solution model for this position
        """
        if slit_id not in self._slits:
            self._slits[slit_id] = ExtTableSlit(slit_id)

        self._slits[slit_id].add_row(pos, var_pos, crv, ids)

    def writeto(self, filename: str, overwrite=False):
        """Save the Extraction Table as FITS file.

        Parameters
        ----------
        filename: str
          The output filename

        overwrite: bool, optional
          The overwrite flag.

        Raises
        ------
        Exception
          Raise error in case file already exists and overwrite flag is False
        """
        # Check file
        if overwrite is False and os.path.isfile(filename):
            raise IOError(f"Extraction table {filename} already exists!")

        # Create empty data
        data = dict((key, []) for key in ExtTableRow.COLUMNS)

        # Fill data
        for slit_id in self._slits:
            for key in data:
                data[key].extend([row[key] for row in self._slits[slit_id]])

        # Set format dict
        formats = ExtTable._get_format(data)

        # Create table
        cols = [fits.Column(name=key, format=formats[key], array=data[key]) for key in data]
        hdu = fits.BinTableHDU.from_columns(fits.ColDefs(cols))
        hdu.header['EXTNAME'] = ExtTable.HDU_NAME

        self._add_instrument_info(hdu)

        # Save table
        hdu.writeto(filename, overwrite=overwrite)
        log.info(f"Saved extraction table: {filename}")

    def _add_instrument_info(self, hdu):
        """Add to the FITS header instrument informations

        Parameters
        ----------
        hdu: fits.TableHdu
          The Extraction Table HDU instance
        """
        if self._instrument is None:
            return

        hdu.header['PIXSCALE'] = self._instrument.pixel_scale
        hdu.header['XPIXELS'] = self._instrument.xpixels
        hdu.header['YPIXELS'] = self._instrument.ypixels

        hdu.header['DISPDIR'] = self._instrument.dispersion_direction
        hdu.header['LINDISP'] = self._instrument.linear_dispersion
        hdu.header['REFLAMB'] = self._instrument.reference_lambda

        if self._instrument.data_hdu:
            hdu.header['DATAHDU'] = self._instrument.data_hdu

        if self._instrument.var_hdu:
            hdu.header['VARHDU'] = self._instrument.data_hdu

        if self._instrument.err_hdu:
            hdu.header['ERRHDU'] = self._instrument.err_hdu

        if self._instrument.flag_hdu:
            hdu.header['FLAGHDU'] = self._instrument.flag_hdu

    @staticmethod
    def _get_format(data):
        """Returns dictionary containing Fits formats for the Extraction Table columns.

        Returns
        -------
        dict
          The format dictionary
        """
        # Set format dict
        formats = {}

        for key in data:
            if key == ExtTableRow.SLIT:
                id_len = max(map(len, set(data[key]))) + 1
                formats[key] = f"{id_len}A"
            elif key == ExtTableRow.ROTATION:
                formats[key] = "4I"
            elif key == ExtTableRow.REF_LAMBDA:
                formats[key] = "E"
            else:
                formats[key] = f"{len(data[key][0])}E"

        return formats

    @staticmethod
    def load(filename):
        """Loads the extraction table from file.

        Parameters
        ----------
        filename: str
          The input file name
        """

        hdul = fits.open(filename)
        table = hdul[ExtTable.HDU_NAME]

        exr = ExtTable()

        # pylint: disable=E1101
        for row in table.data:
            exr.add_row(row[ExtTableRow.SLIT], row[ExtTableRow.DET_POS], row[ExtTableRow.POS_VAR],
                        CrvModel1d(row[ExtTableRow.CRV_COEFFS],
                                   row[ExtTableRow.ROTATION].reshape(2, 2),
                                   row[ExtTableRow.CRV_VAR]),
                        IdsModel1d(row[ExtTableRow.IDS_COEFFS],
                                   row[ExtTableRow.REF_LAMBDA],
                                   row[ExtTableRow.IDS_VAR]))

        exr.instrument = ExtTable.load_instrument(table)
        log.info(f"Loaded extraction table from {filename}")

        return exr

    @staticmethod
    def load_instrument(hdu):
        """Loads instrument configuration from hdu

        Parameters
        ----------
        hdu: HDUImage
          The fits HDU containg the instrument information
        """
        instrument = Instrument()

        if 'PIXSCALE' in hdu.header:
            instrument.pixel_scale = hdu.header['PIXSCALE']

        if 'XPIXELS' in hdu.header:
            instrument.xpixels = hdu.header['XPIXELS']

        if 'YPIXELS' in hdu.header:
            instrument.ypixels = hdu.header['YPIXELS']

        if 'DISPDIR' in hdu.header:
            instrument.dispersion_direction = hdu.header['DISPDIR']

        if 'LINDISP' in hdu.header:
            instrument.linear_dispersion = hdu.header['LINDISP']

        if 'REFLAMB' in hdu.header:
            instrument.reference_lambda = hdu.header['REFLAMB']

        if 'DATAHDU' in hdu.header:
            instrument.data_hdu = hdu.header['DATAHDU']

        if 'VARHDU' in hdu.header:
            instrument.var_hdu = hdu.header['VARHDU']

        if 'ERRHDU' in hdu.header:
            instrument.err_hdu = hdu.header['ERRHDU']

        if 'FLAGHDU' in hdu.header:
            instrument.flag_hdu = hdu.header['FLAGHDU']

        return instrument

    def keys(self):
        """Gets slits IDs

        Returns
        -------
        set-like object
          A set-like providing a view on list IDs
        """
        return self._slits.keys()

    def __len__(self):
        """Returns number of slits contained into the Extraction Table.
        """
        return len(self._slits)

    def __getitem__(self, obj_id):
        return self._slits[obj_id]

    def __iter__(self):
        """Iterates over slits IDs
        """
        return iter(self._slits)

    def get_spectrum_area(self, slit_id, wstart, wend, px_margin=2):
        """Computes spectrum area on the detector.
        Area provided by this method can be out of image

        Parameters
        ----------
        slit_id: str
          The unique slit ID

        wstart: float
          The wavelength starting point for area computation

        wend: float
          The wavelength ending point for area computation

        px_margin: int, optional
          Extra pixel margin along each direction in spectrum area computation
        """
        box = np.zeros((4, 2))
        box[0][0], box[0][1] = self._slits[slit_id][0].compute(wstart)
        box[1][0], box[1][1] = self._slits[slit_id][0].compute(wend)
        box[2][0], box[2][1] = self._slits[slit_id][-1].compute(wstart)
        box[3][0], box[3][1] = self._slits[slit_id][-1].compute(wend)
        xmin = np.floor(box[:, 0].min()) - px_margin
        xmax = np.floor(box[:, 0].max()) + px_margin
        ymin = np.floor(box[:, 1].min()) - px_margin
        ymax = np.floor(box[:, 1].max()) + px_margin

        return np.array([[xmin, ymin], [xmax - xmin + 1, ymax - ymin + 1]], dtype=np.int32)

    def rectify(self, deg: int = 2, margin: int = 0):
        """Refit IDS along each slit of the ExtractionTable in order to find a smoother solution

        Parameters
        ----------
        deg: int, optional
          Degree of the fit

        margin: int, optional
          Number of pixel next to the edges (both) to exclude in refit

        Returns
        -------
        ExtTable
          New extraction table for the current slit
        """
        for slit_id in self._slits:
            log.info(f"Processing slit {slit_id}...")
            self._slits[slit_id].rectify(deg=deg, margin=margin)
        log.info(f"Done")

    def create_region_str(self, lines, slit_id: int, rows=None, wrange=None, color: str = 'green'):
        """Display entire slit solutions
        """
        _str = f""

        for num, row in enumerate(self._slits[slit_id]):
            if rows is not None and num not in rows:
                continue

            points = row.compute(lines)

            for point in points:
                point = IdsModelCalibration.data_to_region(point)
                _str += f"image; point {point[0]} {point[1]} #point=cross 5 color={color}\n"

            if wrange is not None:
                points = row.compute(np.array(wrange))
                for point in points:
                    point = IdsModelCalibration.data_to_region(point)
                    _str += f"image; point {point[0]} {point[1]} #point=diamond 5 color={color}\n"

        return _str


class ExtTableSlit(object):
    """Class to handle single slit of the Extraction Table"""

    def __init__(self, slit_id: str):
        self._slit_id = slit_id
        self._rows = []
        self._model = {}

    def __len__(self):
        """Returns number of slits contained into the Extraction Table.
        """
        return len(self._rows)

    def __getitem__(self, item: int):
        return self._rows[item]

    def __iter__(self):
        """Iterates over slits rows
        """
        return iter(self._rows)

    def rectify(self, deg: int = 2, margin: int = 0):
        """Refit IDS along a single slit in order to find a smoother solution

        Parameters
        ----------
        slit_id: str
          Slit ID of the current slit

        deg: int
          Degree of the fit

        margin:
          Number of pixel next to the edges (both) to exclude in refit

        """

        ids_deg = len(self._rows[0][ExtTableRow.IDS_COEFFS])

        x, y, yvar = self.get_ids_values()
        # move fit close to zero
        x -= x[len(x) // 2]

        if margin > 0:
            x.mask[:margin] = True
            x.mask[-margin:] = True

        new_coeffs = np.empty((ids_deg, len(x)))
        new_var = np.empty((ids_deg, len(x)))
        for k in range(ids_deg):
            val, cov = np.ma.polyfit(x, y[:, k], deg, w=1. / np.sqrt(yvar[:, k]), cov=True)
            # Uses x.data to compute rectified values also on masked values
            new_coeffs[k] = np.poly1d(val)(x.data)
            new_var[k] = np.vander(x.data, deg + 1).dot(cov.diagonal())

        for k in range(len(x)):
            self._rows[k].ids = IdsModel1d(new_coeffs[:, k], self._rows[k][ExtTableRow.REF_LAMBDA],
                                           cov=new_var[:, k])

    def get_ids_values(self):
        """Gets the IDS position and coefficients for the whole slit

        Parameters
        ----------
        slit_id: str
          The slit ID
        """
        slit_versor = self._rows[0][ExtTableRow.ORIENTATION]
        deg = len(self._rows[0][ExtTableRow.IDS_COEFFS])

        pos = np.ma.masked_all(len(self._rows))
        coeffs = np.ma.masked_all((len(self._rows), deg))
        coeffs_var = np.ma.masked_all((len(self._rows), deg))

        for i, row in enumerate(self._rows):
            pos[i] = abs(slit_versor.dot(row[ExtTableRow.DET_POS]))
            coeffs[i] = row[ExtTableRow.IDS_COEFFS]
            coeffs_var[i] = row[ExtTableRow.IDS_VAR]
            if not np.alltrue(np.isfinite(row[ExtTableRow.IDS_COEFFS])):
                pos.mask[i] = True
                coeffs.mask[i] = [True] * deg
                coeffs_var.mask[i] = [True] * deg

        return pos, coeffs, coeffs_var

    # pylint: disable=too-many-arguments
    def add_row(self, pos, var_pos, crv, ids):
        """Add row to the table.

        Parameters
        ----------
        pos: array_like, shape(2,) [pixel]
          The detector (x,y) position (in pixels) for the models

        var_pos: array_like, shape(2,)
          The variance on `pos`

        crv: CrvModel1d
          The Curvature model for this position

        ids: IdsModel1d
          The Inverse Dispersion Solution model for this position
        """

        self._rows.append(ExtTableRow(self._slit_id, pos, var_pos, crv, ids))

    def _get_positions(self):
        """Returns the list positions.

        Returns
        -------
        position: array_like, shape(N,2)
          The pairs of (x,y) slit positions
        """
        poss = np.zeros((len(self), 2))
        for k, row in enumerate(self):
            poss[k] = row[ExtTableRow.DET_POS]

        return poss

    def _compute_t(self, x, margin: int = 1):
        """Converts the x array into a normalized array which goes from 0 up to 1

        Parameters
        ----------
        x: array_like, shape(N,)
          The input sorted array to normalize in the interval [0, 1]

        margin: int, optional
          The number of pixels to mask into the normalized t array
        """
        t = (x - x[0]) / (x[-1] - x[0])
        self._model['a'] = x[0]
        self._model['b'] = x[-1]

        # mask edges
        if margin > 0:
            t.mask[:margin] = True
            t.mask[-margin:] = True
        return t

    def create_model(self, opt_deg: int = 2, crv_deg: int = 2, ids_deg: int = 2, margin: int = 1):  # pragma: no cover
        """Creates a 2D slit model used to compute every position of slit regardless of list rows

        Parameters
        ----------
        opt_deg: int, optional
          The degree of the polinomial use to fit the slit position

        crv_deg: int, optional
          The degree of the polinomial used to fit the Curvature model variantion inside the slit

        ids_deg: int, optional
          The degree of the polinomial used to fit the Inverse dispersion solution variantion inside
          the slit

        margin: int, optional
          The number of pixel to exclude at the edge of the slit for the IDS fitting
        """
        warnings.warn("This method is under development, use it at your own risk")

        x, y, _ = self.get_ids_values()

        # t parameter moves along the slit from 0 up to 1
        t = self._compute_t(x, margin)

        # Fits IDS
        deg = len(self._rows[0][ExtTableRow.IDS_COEFFS])
        self._model['IDS'] = []

        for k in range(deg):
            val = np.ma.polyfit(t, y[:, k], ids_deg)
            self._model['IDS'].append(np.poly1d(val))

        # Fits CRV
        deg = len(self._rows[0][ExtTableRow.CRV_COEFFS])
        coeffs = np.zeros((len(self._rows), deg))
        for k, row in enumerate(self._rows):
            coeffs[k] = row[ExtTableRow.CRV_COEFFS]

        self._model['CRV'] = []
        for k in range(deg):
            val = np.ma.polyfit(t, coeffs[:, k], crv_deg)
            self._model['CRV'].append(np.poly1d(val))

        # Fits slit position
        pos = self._get_positions().dot(self._rows[0].crv.dispersion_vector)
        self._model['OPT'] = np.poly1d(np.ma.polyfit(t, pos, opt_deg))

    def compute(self, t: float, wavelengths):  # pragma: no cover
        """Compute the pixel for each arbitrary row in the slit

        Parameter
        ---------
        t: float
          The position along the slit. t goes from 0 up to 1

        wavelengths: array_like, shape(N,)
          The wavelength values

        Returns
        -------
        position: array_like, shape(N,2)
          The position of the wavelenghts
        """
        warnings.warn("This method is under development, use it at your own risk")

        if len(self._model) == 0:
            raise AttributeError("Slit model not computed yet!")

        # Gets the slit position
        pos = self._rows[0].crv.dispersion_vector.dot(self._model['OPT'](t))
        if self._rows[0].crv.slit_versor[0] == 0:
            pos[1] = self._model['a'] + t * (self._model['b'] - self._model['a'])
        else:
            pos[0] = self._model['a'] + t * (self._model['b'] - self._model['a'])

        # Gets the CRV model
        coeffs = []
        for order in self._model['CRV']:
            coeffs.append(order(t))
        crv = CrvModel1d(coeffs, self._rows[0]['CRV']._rotation)

        # Gets the IDS model
        coeffs = []
        for order in self._model['IDS']:
            coeffs.append(order(t))
        ids = IdsModel1d(coeffs, self._rows[0]['IDS']._reference_lambda)

        # Computes the positions
        delta_disp = ids(wavelengths)[0]
        return pos + crv(delta_disp)[0]


class ExtTableRow(object):
    """Class to handle single row of the Extraction Slit"""
    SLIT = "SLIT"
    DET_POS = "DET_POS"
    POS_VAR = "POS_VAR"
    CRV_COEFFS = "CRV_COEFFS"
    ROTATION = "ROTATION"
    CRV_VAR = "CRV_VAR"
    IDS_COEFFS = "IDS_COEFFS"
    REF_LAMBDA = "REF_LAMBDA"
    IDS_VAR = "IDS_VAR"
    COLUMNS = (SLIT, DET_POS, POS_VAR, CRV_COEFFS, ROTATION, CRV_VAR, IDS_COEFFS, REF_LAMBDA,
               IDS_VAR)
    ORIENTATION = "ORIENTATION"

    def __init__(self, slit_id, pos, var_pos, crv, ids):
        """Initialize the row

        Parameters
        ----------
        slit_id: str
          The unique list ID

        pos: array_like, share(2,)
          The position of the local 1d models (CRV and IDS)

        crv: CrvModel1d
          The local Curvature Model

        ids: IdsModel1d
          The local Inverse Dispersion Solution model
        """
        self.items = {}
        self.items[ExtTableRow.SLIT] = slit_id

        self.items[ExtTableRow.DET_POS] = pos
        self.items[ExtTableRow.POS_VAR] = var_pos

        self.items["CRV"] = crv
        self.items["IDS"] = ids

    def compute(self, values):
        """Apply all models and get the position of lambda for the current row.

        Parameters
        ----------
        values: array_like, shape(N,)
          The list of wavelength values [A]

        Returns
        -------
        array_like, shape(N,2)
          The positions of the input wavelengths list
        """
        delta_disp = self.items["IDS"](values)[0]
        return self.items[ExtTableRow.DET_POS] + self.items["CRV"](delta_disp)[0]

    @property
    def crv(self):
        """Gets the CRV model of the current row"""
        return self.items["CRV"]

    @property
    def ids(self):
        """Gets the IDS model of the current row"""
        return self.items["IDS"]

    @ids.setter
    def ids(self, value):
        """Sets the IDS model for the current row"""
        self.items["IDS"] = value

    def __getitem__(self, key):
        # pylint: disable=R0911
        if key == ExtTableRow.CRV_COEFFS:
            return self.items["CRV"].coeffs

        elif key == ExtTableRow.IDS_COEFFS:
            return self.items["IDS"].coeffs

        elif key == ExtTableRow.CRV_VAR:
            return self.items["CRV"].variance

        elif key == ExtTableRow.IDS_VAR:
            return self.items["IDS"].variance

        elif key == ExtTableRow.ROTATION:
            return self.items["CRV"].rotation

        elif key == ExtTableRow.REF_LAMBDA:
            return self.items["IDS"].reference_lambda

        elif key == ExtTableRow.ORIENTATION:
            return self.items["CRV"].slit_versor

        return self.items[key]

    @property
    def center(self):
        """The center of the slit
        """
        return self.compute(self[ExtTableRow.REF_LAMBDA])

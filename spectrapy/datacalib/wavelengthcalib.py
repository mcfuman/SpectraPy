#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File wavelengthcalib.py
#
# Created on: Mar 28, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the class the calibrate the wavelenght solution on real data"""

import numpy as np
from astropy import log
from astropy.utils.console import ProgressBar

from spectrapy.math.peaks import find_peak_1d
from spectrapy.datacalib.extractiontable import ExtTable
from spectrapy.dataio.catalog import Catalog
from spectrapy.models.idsmodel import IdsModel1d
from spectrapy.modelscalib.idscalib import IdsModelCalibration


class WavelengthCalibration(IdsModelCalibration):
    """Class to calibrate the IdsModel"""
    LINE_MARKER_SIZE = 10

    def __init__(self, instrument, mask, opt, crv, ids):
        """
        Initialize the calibrator: set the model and open DS9
        @param model: the OptModel2d to calibrate
        @type model: OptModel2d instance
        """
        IdsModelCalibration.__init__(self, instrument, mask, opt, crv, ids)
        self._wstart = None
        self._wend = None

    def new_ids_model(self, deg, xdeg, ydeg):
        """Not implemented method"""
        raise NotImplementedError()

    def has_lambda_range(self):
        """Checks is the wavelength range is set
        """
        return self._wstart is not None and self._wend is not None

    def set_lambda_range(self, blue: float, red: float):
        """Sets the lambda range used for computing IDS model on data

        Parameters
        ----------
        blue: float [Angstrom]
          Blue limit for IDS computation

        red : float [Angstrom]
          Red limit for IDS computation
        """
        self._wstart = blue
        self._wend = red
        if red <= blue:
            raise ValueError(f"Invalid lambda range: ({blue}, {red})")

        if blue <= 0:
            raise ValueError(f"Invalid lambda range: blue and red must be positive")

        log.info(f"New lambda range: [{blue:.2f}, {red:.2f}]")

    def _get_waves_range(self):
        """Returns wavelenght range used to "cut" a slice of frame for IDS fitting

        Returns
        -------
        np.arange
          Waves range string from wstart until wend
        """
        return np.arange(self._wstart, self._wend, 1. / self._ids.inv_dispersion)

    def compute_spectra_wave(self, catalog, margin: int = 10):
        """Compute lines positions on frames and create the extraction table used for extraction.

        Parameters
        ----------
        catalog: array_like or str
          Line catalog to use. See _load_catalog method

        margin: int [pixels]
          The margin around the expected line position use to compute the real line position

        Returns
        -------
        ExtTable
          The extraction table containing the solutions used by extraction procedure
        """

        if self.has_lambda_range() is False:
            raise AttributeError("Lambda range not set yet!")

        exr = ExtTable()

        self._ds9.reset_background()

        lines = self._load_catalog(catalog, wstart=self._wstart, wend=self._wend)
        waves = self._get_waves_range()

        reliable_lines = lines[lines['flag'] == Catalog.STRONG]['pos'].data

        for slit_id in self._mask:
            log.info(f"Calibrating slit {slit_id}...")
            slit = self._mask[slit_id]

            # Skip reference slits
            if slit.reference is True:
                continue

            pixels, pixels_var = slit.get_slit_pix()

            slit_rows = self._fit_slit_ids(pixels, pixels_var, waves, lines, margin=margin)

            for row in slit_rows:
                exr.add_row(slit_id, *row)

            ds9_str = exr.create_region_str(reliable_lines, slit_id)
            self._ds9.set("regions", ds9_str)

        exr.instrument = self.instrument
        log.info("Extraction table created")
        return exr

    def _fit_slice_ids(self, slit_pixel, waves, lines, margin: int = 10):
        """Computes real line positions and fits a new IDS model on a "slice" of slit

        Parameters
        ----------
        waves: array_like, shape(N, )
          The wavelenghts values (in Angstrom)

        delta_disp: array_like, shape(N, )
          The displacement (in pixels) from the reference position of the waves

        fluxes: array_like, shape(N, )
          The rebinned image values alonge the slice positions

        lines: astropy.table.Table
          The catalog line positions

        margin: int, optional [pixels]
          The margin (in pixel) around the expected line position used to compute the real position

        Returns
        -------
        IdsModel1d
          The new IDS model according with real line position. Returns None is computations fails
        """

        ids_model = self._ids.get_local_model(slit_pixel)
        crv_model = self.crv.get_local_model(slit_pixel)

        # Select spectrum inside the range
        # pylint: disable=assignment-from-no-return

        #good_waves = waves

        loop_counter = 0
        old_good_lines = None
        while True:
            delta_disp, _ = ids_model(waves)
            delta_curve, _ = crv_model(delta_disp)

            fluxes, _ = self._ds9.image.interpolate(slit_pixel + delta_curve)
            good_fluxes = np.isfinite(fluxes)

            good_waves = waves[good_fluxes]
            delta_disp = delta_disp[good_fluxes]
            fluxes = fluxes[good_fluxes]

            new_positions = WavelengthCalibration.compute_line_positions(good_waves, delta_disp,
                                                                         fluxes, lines, margin)

            # pylint: disable=assignment-from-no-return
            new_good_lines = np.isfinite(new_positions[:, 1])
            try:
                ids_model = self.ids.refit_slit(new_positions[:, 0][new_good_lines],
                                                new_positions[:, 1][new_good_lines])
            except:
                return None

            if old_good_lines is not None and np.alltrue(new_good_lines == old_good_lines):
                # Nothing changed
                return ids_model

            loop_counter += 1
            if loop_counter >= len(new_positions) or \
                    len(new_positions[new_good_lines]) < self._ids.deg + 2:
                return ids_model

            old_good_lines = new_good_lines

    # pylint: disable=too-many-locals
    def _fit_slit_ids(self, pixels, pixels_var, waves, lines, margin: int = 10):
        """Computes new ids models for each slice in rows and returns list with models values
        of each slice.

        Parameters
        ----------
        pixels: array_like, shape(N, 2)
          Starting position (according with the Optical Model) of each slice

        pixels_var: array_like, shape(N, 2)
          Variances on pixels positions

        waves: array_like, shape(M, )
          Waves range used by IDS model for pixel dispalcement computation

        lines: astopy.table.Table
          Catalog lines

        margin: int, [pixels]
          The margin (in pixels) around the expected line position

        Returns
        -------
        list
          List of models and positions for each slice
        """

        slit_rows = []
        with ProgressBar(len(pixels)) as pbar:
            for slit_pixel, slit_var in zip(pixels, pixels_var):
                ids_model = self._fit_slice_ids(slit_pixel, waves, lines, margin)

                if ids_model is None:
                    log.warning(f"IDS model not computed at pixel {slit_pixel}")
                    ids_model = IdsModel1d(np.full(self.ids.deg + 1, np.nan), self.ids.reference_lambda)

                crv_model = self.crv.get_local_model(slit_pixel)
                slit_rows.append([slit_pixel, slit_var, crv_model, ids_model])
                pbar.update()

        return slit_rows

    def _display_slit(self, exttable, slit_id, lines, color='green'):
        """Display entire slit solutions
        """
        ds9_str = f""
        for row in exttable[slit_id]:
            points = row.compute(lines)

            for point in points:
                point = IdsModelCalibration.data_to_region(point)
                ds9_str += f"image; point {point[0]} {point[1]} #point=cross 5 color={color}\n"

        self._ds9.set("regions", ds9_str)

    @staticmethod
    def compute_line_positions(waves, delta_disp, fluxes, lines, margin: int):
        """Compute position of lines
        """
        lambda_vs_disp = []

        #delta_line = margin / self._ids.inv_dispersion

        # Uses only lines with flag == 1
        for line in lines[lines['flag'] == Catalog.STRONG]:
            idx = np.flatnonzero(waves >= line['pos'])
            if len(idx) == 0 or idx[0] == 0:
                continue
            idx = np.arange(max(0, idx[0] - margin), min(len(waves), idx[0] + margin))
            #idx = np.flatnonzero(np.abs(waves - line['pos']) <= delta_line)
            # if len(idx) <= margin:
                # line out of spetrum. Skip it
            #    continue

            # delta_disp are not uniformelly sampled
            # pos represents the float index
            pos, _ = find_peak_1d(fluxes[idx], 2)
            if pos is not None:
                frac = pos - int(pos)
                value = (1.0 - frac) * delta_disp[idx][int(pos)] + \
                    frac * delta_disp[idx][int(pos) + 1]
                lambda_vs_disp.append((line['pos'], value))
            else:
                lambda_vs_disp.append((line['pos'], np.nan))

        return np.array(lambda_vs_disp)

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File tracingcalib.py
#
# Created on: Mar 19, 2019
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the classes to compute the tracing on data"""

import numpy as np
from astropy import log

from spectrapy.modelscalib.crvcalib import CrvModelCalibration
from spectrapy.datacalib.edges import DoubleTanH, Sobel


class TraceCalibration(CrvModelCalibration):
    """Class to apply models on science data"""

    def __init__(self, instrument, mask, opt, crv, engine='Sobel'):
        """
        Initialize the calibrator: set the model and open DS9

        Parameters
        ----------
        instrument: str or Instrument
          The instrument file name or instance

        mask: str or Mask
          Tha mask file name or instance

        opt: str or OptModel2d
          The optical model file name or instance

        crv: str or CrvModel2d
          The cruvature model filename or instance

        engine: str, optional
          The engine module: DTanH or Sobel
        """
        CrvModelCalibration.__init__(self, instrument, mask, opt, crv)

        if engine == 'DTanH':
            self._engine = DoubleTanH(self.opt.dispersion_direction)
        elif engine == 'Sobel':
            self._engine = Sobel(self.opt.dispersion_direction)
        else:
            raise ValueError(f"Invalid method {engine}")

    def load_image(self, *filenames):
        """Loads images and set them in DS9

        Parameters
        ----------
        filenames: slit
          The list of images
        """
        CrvModelCalibration.load_image(self, *filenames)
        self._engine.image = self._ds9.image

    def new_crv_model(self, deg, xdeg, ydeg):
        """Not implemented method"""
        raise NotImplementedError()

    def reload_traces_from_regions(self):
        """Not implemented method"""
        raise NotImplementedError()

    def _get_spectrum_area(self, slit_id, left_margin=0, right_margin=0):
        """Not implemented method"""
        raise NotImplementedError()

    def _get_delta_dispersions(self, pix_bin: int):
        """Returns dispersion range from blue up to red edges.

        Parameters
        ----------
        pix_bin: int
          Step in dispersion range

        Returns
        -------
        array_like, shape(N,)
          The delta of dispersions
        """
        if self._red is None or self._blue is None:
            raise AttributeError("Red/blue pixels not set yet!")

        # return np.arange(self._blue, self._red, self.crv.red_direction*pix_bin)
        return np.arange(self._blue, self._red, pix_bin)

    def _get_subimage_shape(self, slit_win: int, pix_bin: int):
        """Returns the shape of the sub image according with dispersion direction

        Parameters
        ----------
        slit_win: int
          The window slit size along the cross dispersion direction

        pix_bin: int
          The window size along the dispersion direction

        Returns
        -------
        np.array, shape(2,)
          The sub image shape properly orientated
        """
        return np.abs(self._opt.rotation).dot([pix_bin, slit_win])

    def _get_subimage_center(self, slit_id: str, delta_pos: np.array, t: float = 0.0):
        """Compute the center of the sub image according with the Curvature model

        Parameters
        ----------
        slit_id: str
          The slit ID

        delta_pos: array_like, shape(N,)
          The displacements (in pixels) respect of the slit position

        Returns
        -------
        np.array, shape(N,2)
          The center (x,y) positions in pixels
        """

        pos, _ = self._mask[slit_id].get_pix_position(t)

        return pos + self.crv.compute(pos, delta_pos)[0]

    # pylint: disable=too-many-locals
    def compute_spectra_traces(self, slit_win=20, pix_bin=50, var=False, sub_pixels=5, plot=False,
                               right=False, logp=False):
        """Fit the edge of the spectrum trace for each slit.

        Parameters
        ----------
        slit_win: int, optional
          Size of the window around the expected position of the trace

        pix_bin: int, optional
          Size of the collapsing window to obtain the profile edge

        """
        self._ds9.reset_background()

        if right is False:
            edges = ('0.00', )
        else:
            edges = ('0.00', '1.00')

        self._traces = dict([(edge, {}) for edge in edges])
        self._variances = dict([(edge, {}) for edge in edges])
        failures = dict([(edge, {}) for edge in edges])

        if var is True:
            log.warning(f"Var paramter is experimental. In case of faluire try to turn if off")

        delta_disp = self._get_delta_dispersions(pix_bin)
        subimage_shape = self._get_subimage_shape(slit_win, pix_bin)

        for edge in edges:
            for slit_id in self._mask:
                self._traces[edge][slit_id] = []
                self._variances[edge][slit_id] = []
                failures[edge][slit_id] = []

                subimage_centers = self._get_subimage_center(slit_id, delta_disp, t=float(edge))

                for center in subimage_centers:
                    edge_val, edge_var = self._engine.compute_slice_edge(center, subimage_shape,
                                                                         var=var,
                                                                         sub_pixels=sub_pixels,
                                                                         plot=plot,
                                                                         right=(edge == '1.00'),
                                                                         logp=logp)
                    if edge_val is None:
                        log.debug(f"Error computing step in ({center[0]:.1f}, {center[1]:.1f})")
                        failures[edge][slit_id].append(center)
                        continue
                    self._traces[edge][slit_id].append(edge_val)
                    if edge_var is not None:
                        self._variances[edge][slit_id].append(edge_var)

                self._traces[edge][slit_id] = np.array(self._traces[edge][slit_id])
                if len(self._variances[edge][slit_id]) > 0:
                    self._variances[edge][slit_id] = np.array(self._variances[edge][slit_id])
                else:
                    del self._variances[edge][slit_id]
                failures[edge][slit_id] = np.array(failures[edge][slit_id])
            if len(self._variances[edge]) == 0:
                del self._variances[edge]
        self._reg_plot_crv_regions(self._traces)
        self._reg_plot_crv_regions(failures, color=TraceCalibration.FAILURE_COLOR)

    def load_flags(self, filename: str):  # pragma: no cover
        """Loads bad pixels from bad pixel image.

        Parameters
        ----------
        filename: str
          The file containg bad pixels
        """

        if self._ds9.image is not None:
            self._ds9.image.load_flags(filename, flag_hdu=self.instrument.flag_hdu)

    def writeto(self, filename: str, overwrite: bool = False):
        """Save the already computed CRVModel

        Parameters
        ----------
        filename: str
          The output filename

        overwrite: bool, optional
          The overwrite flag for output file

        Raises
        ------
        Exception
          If output file already exits an the overwrite is off.
        """
        return self.crv.writeto(filename, overwrite=overwrite)

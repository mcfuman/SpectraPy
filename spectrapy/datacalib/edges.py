#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File edges.py
#
# Created on: Sep 06, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains classes to compute slit edges"""

from itertools import groupby, count
import numpy as np

from scipy import ndimage
from scipy.optimize import curve_fit

from astropy import log
from astropy.stats import sigma_clip

from spectrapy.math.peaks import find_peak_1d
from spectrapy.dataio.image import Image

try:
    import pylab
except ImportError:  # pragma: no cover
    HAS_PYLAB = False
else:
    HAS_PYLAB = True


class EdgeEngine(object):
    """The Edge interface class"""

    def __init__(self, dispersion_direction='LR'):
        """Initialize the class setting the dispersion direction

        Parameters
        ----------
        dispersion_direction: str, optional
          The dispersion direction string
        """
        self.axis = int(dispersion_direction in ('LR', 'RL'))
        self.down_step = dispersion_direction in ('UB', 'LR')
        self._image = None

    @property
    def image(self):
        """The loaded image
        """
        return self._image

    @image.setter
    def image(self, value):
        """Sets the image

        Parameters
        ----------
        value: Image
          The image instance
        """
        self._image = value

    def _get_slice(self, center: np.array, shape: np.array):
        """Returns the sub image, used for compute spectra tracing according to center position
        and shape. None is returned in case image is too small for edge fitting.

        Parameters
        ----------
        center: np.array, shape(2,)
          X,Y position of the center in pixel of the slice

        shape: np.array, shape(2,)
          Shape of the slice, already properly oriented

        Returns
        -------
        Image
          The slice image
        """
        _half_shape = shape // 2
        start = center.astype(np.int32) - _half_shape
        sub_image = self._image.get_subimage(start[0], start[1],
                                             _half_shape[0] * 2 + 1, _half_shape[1] * 2 + 1)

        return sub_image

    # pylint: disable=too-many-locals
    def compute_slice_edge(self, center, edge, edge_var):
        """Computes the edge of the slice collapsion it along the dispersion direction

        Parameters
        ----------
        center: np.array, shape(2,)
          The center of the slice

        Returns
        -------
        edge, variance: np.array, float
          The edge position in pixels and the variance along the cross dispersion direction
          CHECK!!!
        """

        if edge is None:
            return None, None

        if self.axis == 1:  # ('LR', 'RL')
            return (center[0], edge), edge_var

        return (edge, center[1]), edge_var

    def xprofile(self, sub_image, yprofile):
        """Gets the x values for the yprofile, starting from the sub image offset starts

        Parameters
        ----------
        sub_image: Image
          The image of the yprofile

        yprofile: array_like, shape(N, )
          The profile of the sub image

        Returns
        -------
        np.arange
          The range along the profile
        """
        return np.arange(sub_image.start[self.axis], sub_image.start[self.axis] + len(yprofile))

    def _get_slice_profile(self, center: np.array, shape: np.array, logp: bool = False,
                           sigma: float = 2.0, maxiters: int = 3):
        """Get the X and Y profile according with the dispersion direction,
        of the slice with center and shape

        Parameters
        ----------
        center: np.array, shape(2,)
          Center of the slice

        shape: np.array, shape(2,)
          The shape of the slice

        Returns
        -------
        tuple of np.array, shape(2, N)
          The X and Y profile of the slice along the cross dispersion direction
        """
        # Compute profile
        sub_image = self._get_slice(center, shape)
        yprofile, vprofile = sub_image.compute_profile(sigma=sigma, maxiters=maxiters,
                                                       axis=self.axis)
        xprofile = self.xprofile(sub_image, yprofile)

        if logp is True:
            yprofile += (max(-yprofile.min(), 0) + 1.)
            # pylint: disable=E1111
            yprofile = np.log(yprofile)
        return xprofile, yprofile, vprofile


class DoubleTanH(EdgeEngine):
    """Class to evaluate tracing step position fitting pair of TanH"""
    EDGE_FLEX = 0.5 * np.log((1. - np.sqrt(2. / 3.)) / (1 + np.sqrt(2. / 3.)))
    MIN_PIX_TRACE = 10

    def __init__(self, dispersion_direction='LR'):
        """Initialize the class
        """
        # LR is the default configuration
        EdgeEngine.__init__(self, dispersion_direction)
        self._popt = None
        self._pcov = None

        self._f = {'left': DoubleTanH.left, 'right': DoubleTanH.right, 'double': DoubleTanH.double}
        self._df = {'left': DoubleTanH.dleft, 'right': DoubleTanH.dright,
                    'double': DoubleTanH.ddouble}
        self._p0 = {}

    def _has_fit(self):
        """Returns True is fit has been computed
        """
        return self._popt is not None

    @staticmethod
    def double(x, *p):
        """Defines a function which has one step on left, another step on right
        and a gap in the midle.
        """
        # A1 = p[0]
        # A2 = p[1]
        # center1 = p[2]
        # center2 = p[3]
        # steep1 = p[4]
        # steep2 = p[5]
        # level = p[6]

        return p[0] * (np.tanh(-p[4] * (x - p[2])) / 2. + .5) + \
            p[1] * (np.tanh(p[5] * (x - p[3])) / 2. + 0.5) + p[6]

    @staticmethod
    def ddouble(x, *p):
        """The Jacobian matrix of the `double` function.
        """
        # A1 = p[0]
        # A2 = p[1]
        # center1 = p[2]
        # center2 = p[3]
        # steep1 = p[4]
        # steep2 = p[5]
        # level = p[6]

        jac = np.zeros((len(x), 7))
        # pylint: disable=assignment-from-no-return
        # pylint: disable=invalid-name
        t1 = np.tanh(-p[4] * (x - p[2]))
        # pylint: disable=invalid-name
        t2 = np.tanh(p[5] * (x - p[3]))
        jac[:, 0] = t1 / 2. + 0.5
        jac[:, 1] = t2 / 2. + 0.5
        jac[:, 2] = p[0] / 2. * p[4] * (1 - t1**2)
        jac[:, 3] = -p[1] / 2. * p[5] * (1 - t2**2)
        jac[:, 4] = -p[0] / 2. * (x - p[2]) * (1 - t1**2)
        jac[:, 5] = p[1] / 2. * (x - p[3]) * (1 - t2**2)
        jac[:, 6] = 1.0

        return jac

    @staticmethod
    def left(x, *p):
        """Defines a function which has step on left.
        """
        # A1 = p[0]
        # center1 = p[1]
        # steep1 = p[2]
        # level = p[3]

        return p[0] * (np.tanh(-p[2] * (x - p[1])) / 2. + .5) + p[3]

    @staticmethod
    def dleft(x, *p):
        """The Jacobian matrix of the `left` function.
        """
        # A1 = p[0]
        # center1 = p[1]
        # steep1 = p[2]
        # level = p[3]

        jac = np.zeros((len(x), 4))
        # pylint: disable=assignment-from-no-return
        t1 = np.tanh(-p[2] * (x - p[1]))
        jac[:, 0] = t1 / 2. + 0.5
        jac[:, 1] = p[0] / 2. * p[2] * (1 - t1**2)
        jac[:, 2] = -p[0] / 2. * (x - p[1]) * (1 - t1**2)
        jac[:, 3] = 1.0

        return jac

    @staticmethod
    def right(x, *p):
        """Defines a function which has step on right.
        """
        # A1 = p[0]
        # center1 = p[1]
        # steep1 = p[2]
        # level = p[3]

        return p[0] * (np.tanh(p[2] * (x - p[1])) / 2. + .5) + p[3]

    @staticmethod
    def dright(x, *p):
        """The Jacobian matrix of the `right` function.
        """
        # A1 = p[0]
        # center1 = p[1]
        # steep1 = p[2]
        # level = p[3]

        jac = np.zeros((len(x), 4))
        # pylint: disable=assignment-from-no-return
        # pylint: disable=invalid-name
        t2 = np.tanh(p[2] * (x - p[1]))
        jac[:, 0] = t2 / 2. + 0.5
        jac[:, 1] = -p[0] / 2. * p[2] * (1 - t2**2)
        jac[:, 2] = p[0] / 2. * (x - p[1]) * (1 - t2**2)
        jac[:, 3] = 1.0

        return jac

    def _compute_p0(self, xdata, ydata, edges, sigma=1):
        """Compute first guess for fit in 3 available configurations: left step, right step and
        double steps.
        """
        if sigma <= 0 or sigma > 5:
            return

        # Added try to allow both astropy >=3.x and astropy <3.x
        try:  # pragma: no cover
            clip_edges = sigma_clip(edges, sigma=sigma, maxiters=1)
        except:  # pragma: no cover
            clip_edges = sigma_clip(edges, sigma=sigma, iters=1)
        # pylint: disable=C0121
        idx = np.flatnonzero(clip_edges.mask == False)

        # All data are below the sigma threshold. Decrease sigma limit
        if len(idx) == 0:
            return  # self.compute_p0(xdata, ydata, edges, sigma=sigma-0.2)

        # Compute data groups between edges
        groups = groupby(idx, lambda n, c=count(): n - next(c))

        # Get levels and position of steps
        levels = []
        positions = []
        for _, group in groups:
            idx = list(group)
            levels.append(np.mean(ydata[idx]))
            positions.append(xdata[idx][-1])

        # Ideal case. 3 steps: 1st slit, gap and 2nd slit
        # Nothing more to do
        if len(levels) == 3:
            self._p0['double'] = [levels[0], levels[2], positions[0], positions[-1],
                                  3, 3, levels[1]]
            return

        # Only 2 steps: slit and gap. Since it could be possible that there is a faint 2nd slit
        # Try to crease the threshold and iterate the process
        if len(levels) == 2:
            if levels[0] > levels[1]:
                # This is a left step
                if 'left' not in self._p0:
                    self._p0['left'] = [levels[0], positions[0], 3, levels[1]]
            else:
                # This is a right step
                if 'right' not in self._p0:
                    self._p0['right'] = [levels[1], positions[0], 3, levels[0]]
            # Iterate
            return self._compute_p0(xdata, ydata, edges, sigma=sigma - 0.2)

    def compute_fg(self, xdata, ydata, edges):
        """Computes the parameters first guesses

        Parameters
        ----------
        xdata: array_like, shape(N,)
          The x data of the edge

        ydata: array_like, shape(N,)
          The y data of the edge

        edges: array_like, shape(N,)
          The edges values
        """
        self._p0 = {}
        self._compute_p0(xdata, ydata, edges)

    @staticmethod
    def _interpolate_data(sub_pixels, xdata, ydata, edges, vdata=None):
        """This function increases the number of point before fit.
        Sharp transitions from one slit to the next, could prevent to identify region
        between slit and the other, because there are no edge data close to zero between slits.

        For this reason we interpolate data in order to find gaps between slits
        """
        xxdata = np.linspace(xdata[0], xdata[-1], sub_pixels * (len(xdata) - 1) + 1)
        eedges = np.interp(xxdata, xdata, edges)
        yydata = np.interp(xxdata, xdata, ydata)
        if vdata is not None:
            vvdata = np.interp(xxdata, xdata, vdata)
        else:
            vvdata = None

        return xxdata, yydata, eedges, vvdata

    def _compute(self, xdata, ydata, edges, vdata=None, sub_pixels=5):
        """Fits the slit edges.
        """

        # Reset the previous computation
        self._popt = None
        self._pcov = None

        xdata, ydata, edges, vdata = DoubleTanH._interpolate_data(sub_pixels, xdata,
                                                                  ydata, edges, vdata)

        # Reset the first guesses previously computed
        self.compute_fg(xdata, ydata, edges)

        if 'right' in self._p0:
            self._popt, self._pcov, chi2 = self._compute_fit(xdata, ydata, 'right', vdata)

        if 'left' in self._p0:
            popt, pcov, _chi2 = self._compute_fit(xdata, ydata, 'left', vdata)

            if self._popt is None:
                self._popt = popt
                self._pcov = pcov
                chi2 = _chi2
            else:
                if _chi2 < chi2:
                    self._popt = popt
                    self._pcov = pcov
                    chi2 = _chi2
                    del self._p0['right']
                else:
                    del self._p0['left']

        if 'double' in self._p0:
            popt, pcov, _chi2 = self._compute_fit(xdata, ydata, 'double', vdata)

            if self._popt is None:
                self._popt = popt
                self._pcov = pcov
            else:
                if _chi2 < chi2:
                    self._popt = popt
                    self._pcov = pcov
                    for side in ('left', 'right'):
                        if side in self._p0:
                            del self._p0[side]
                else:
                    del self._p0['double']

    def _compute_fit(self, xdata, ydata, step_type, vdata=None):
        """Compute curve fit using the appropriated function and estimate the chi2 value.
        """
        # Select functions
        # pylint: disable=C0103
        f = self._f[step_type]
        df = self._df[step_type]

        if vdata is None:
            sigma = None
        else:
            sigma = np.sqrt(vdata)

        try:
            popt, pcov = curve_fit(f, xdata, ydata, p0=self._p0[step_type], jac=df, sigma=sigma)
        except RuntimeError:
            log.debug("Failed lm fit. Try trf method")
            try:
                popt, pcov = curve_fit(f, xdata, ydata, p0=self._p0[step_type], jac=df, sigma=sigma,
                                       method='trf')
            except RuntimeError:
                return None, None, np.inf

        # Compute chi2
        if vdata is None:
            chi2 = np.sum((ydata - f(xdata, *popt))**2)
        else:
            chi2 = np.sum((ydata - f(xdata, *popt))**2 / vdata)

        return popt, pcov, chi2

    def get_bottom_step(self):
        """Computes edge position in case the step is going down
        """
        return self.get_step(bottom=True)

    def get_up_step(self):
        """Computes edge position in case the step is going up
        """
        return self.get_step(bottom=False)

    def get_step(self, bottom):
        """Computes the position of the edge
        """

        if bottom is True:
            value = DoubleTanH.EDGE_FLEX
        else:
            value = -DoubleTanH.EDGE_FLEX

        if 'double' in self._p0:
            if bottom is True:
                steep_idx = 4
                position_idx = 2
            else:
                steep_idx = 5
                position_idx = 3

        else:
            if 'right' in self._p0 and bottom is True:
                return None, None
            if 'left' in self._p0 and bottom is False:
                return None, None

            steep_idx = 2
            position_idx = 1

        steep = self._popt[steep_idx]
        position = self._popt[position_idx]

        steep_var = self._pcov[steep_idx][steep_idx]
        position_var = self._pcov[position_idx][position_idx]
        covariance = self._pcov[steep_idx][position_idx]

        dsteep = -1. * value / steep**2
        var = steep_var * dsteep**2 + position_var + 2 * dsteep * covariance
        edge = value / steep + position

        # pylint: disable=C0121
        if np.isfinite(var) == False or var < abs(edge) * 1E-12:
            return None, None

        return edge, var

    def plot(self, xdata, ydata, profile_edges, edge=None, vdata=None):  # pragma: no cover
        """Plots edge profile data, fitted profile, edge position and egde profile
        """
        if HAS_PYLAB is False:
            return

        pylab.subplot(121)
        pylab.plot(xdata, ydata)
        x = np.linspace(xdata[0], xdata[-1], 1000)

        step_type = list(self._p0)[0]
        pylab.plot(x, self._f[step_type](x, *self._popt), 'red')

        if edge is not None:
            pylab.plot([edge, edge], [min(ydata), max(ydata)], 'green')

        pylab.subplot(122)
        pylab.plot(xdata, profile_edges)
        pylab.show()

    # pylint: disable=too-many-locals
    def compute_slice_edge(self, center, slice_shape, var=False, sub_pixels=5, plot=False,
                           right=False, logp=False):
        """Computes the edge of the slice collapsion it along the dispersion direction

        Parameters
        ----------
        center: np.array, shape(2,)
          The center of the slice

        slice_shape: np.array, shape(2,)
          The shape of the slice

        plot: bool
          Debugging parameter. If True plot the profile, the fit and the edge

        Returns
        -------
        edge, variance: np.array, float
          The edge position in pixels and the variance along the cross dispersion direction
          CHECK!!!
        """

        xprofile, yprofile, vprofile = self._get_slice_profile(center, slice_shape, logp=logp)
        _, profile_edges = self._get_slice_edges(center, slice_shape, logp=logp)

        if var is False:
            # Do not use variance
            vprofile = None

        self._compute(xprofile, yprofile, profile_edges, vprofile, sub_pixels=sub_pixels)
        if self._has_fit() is False:
            return None, None

        edge, edge_var = self.get_step(bottom=(self.down_step ^ right))  # XOR

        if plot is True:
            self.plot(xprofile, yprofile, profile_edges, edge=edge, vdata=vprofile)

        return EdgeEngine.compute_slice_edge(self, center, edge, edge_var)

    def _get_slice_edges(self, center: np.array, shape: np.array, logp=False):
        """Gets the X and Y profile according with the dispersion direction,
        of the slice with center and shape

        Parameters
        ----------
        center: np.array, shape(2,)
          Center of the slice

        shape: np.array, shape(2,)
          The shape of the slice

        Returns
        -------
        tuple of np.array, shape(2, N)
          The X and Y profile of the slice along the cross dispersion direction
        """

        sub_image = self._get_slice(center, shape)
        if logp is True:
            data = sub_image.data + max(-sub_image.data.min(), 0.) + 1.
            # pylint: disable=E1111
            data = np.log(data)
        else:
            data = sub_image.data

        edges = ndimage.sobel(data, axis=(self.axis + 1) % 2)

        # Edges is no more masked. Try to use the previous mask to avoid bad and cosmic
        masked_edges = np.ma.array(edges, mask=sub_image.data.mask)
        edges_profile = np.ma.sum(masked_edges, axis=self.axis)
        xprofile = self.xprofile(sub_image, edges_profile)

        return xprofile, edges_profile

    def _get_slice(self, center: np.array, shape: np.array):
        """Returns the sub image, used for compute spectra tracing according with center position
        and shape. None is returned in case image is too small for edge fitting.

        Parameters
        ----------
        center: np.array, shape(2,)
          X,Y position of the center in pixel of the slice

        shape: np.array, shape(2,)
          Shape of the slice, already properly oriented

        Returns
        -------
        Image
          The slice image
        """

        sub_image = EdgeEngine._get_slice(self, center, shape)

        # Not enough points. Skip slice
        if min(sub_image.shape) < DoubleTanH.MIN_PIX_TRACE:
            log.debug(f"Slice in ({center[0]:.1f}, {center[1]:.1f}) too small")
            return None

        return sub_image


class Sobel(EdgeEngine):
    """This class uses sobel filter to compute spectra edges"""

    def __init__(self, dispersion_direction='LR'):
        # LR is the default configuration
        EdgeEngine.__init__(self, dispersion_direction)
        self._down_step = None
        self._up_step = None

    @property
    def image(self):
        return self._image

    @image.setter
    def image(self, value):
        self._orig = value
        self._image = Image(data=ndimage.sobel(value.data.data, axis=(self.axis + 1) % 2))

    # pylint: disable=too-many-locals
    # pylint: disable=unused-argument
    def compute_slice_edge(self, center, slice_shape, var=False, sub_pixels=5, plot=False,
                           right=False, logp=False, sigma=2.0, maxiters=3):
        """Computes the edge of the slice collapsion it along the dispersion direction

        Parameters
        ----------
        center: np.array, shape(2,)
          The center of the slice

        slice_shape: np.array, shape(2,)
          The shape of the slice

        plot: bool
          Debugging parameter. If True plot the profile, the fit and the edge

        Returns
        -------
        edge, variance: np.array, float
          The edge position in pixels and the variance along the cross dispersion direction
          CHECK!!!
        """

        xprofile, yprofile, _ = self._get_slice_profile(center, slice_shape, logp=logp, sigma=sigma,
                                                        maxiters=maxiters)

        self._compute(xprofile, yprofile)
        if self._has_fit() is False:
            return None, None

        edge, _ = self.get_step(bottom=(self.down_step ^ right))  # XOR

        if plot is True:  # pragma: no cover
            log.info("No plot facility")

        return EdgeEngine.compute_slice_edge(self, center, edge, None)

    def _compute(self, xprofile, yprofile):
        self._up_step = None
        self._down_step = None

        if len(yprofile) > 3:
            pos, _ = find_peak_1d(np.where(yprofile > 0, yprofile, 0), 2)

        if len(yprofile) <= 3 or pos is None:
            pos = np.argmax(yprofile)

        self._up_step = xprofile[0] + pos

        if len(yprofile) > 3:
            pos, _ = find_peak_1d(np.where(yprofile > 0, 0, -yprofile), 2)

        if len(yprofile) <= 3 or pos is None:
            pos = np.argmin(yprofile)

        self._down_step = xprofile[0] + pos

    def _has_fit(self):
        return self._up_step is not None and self._down_step is not None

    def get_step(self, bottom):
        """Gets the trace step

        Parameters
        ----------
        bottom: bool
          If True returns the down step
        """
        if bottom is True:
            return self._down_step, None
        return self._up_step, None

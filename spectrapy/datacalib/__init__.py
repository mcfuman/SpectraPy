"""
This module contains classes used to apply and adjust calibration from model
on the current data in use.
"""

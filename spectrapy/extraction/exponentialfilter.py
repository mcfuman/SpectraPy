#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File exponentialfilter.py
#
# Created on: Oct 01, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the class to achieve the spectra resampling"""

import numpy as np

from astropy.io import fits
from astropy.table import Table
from astropy import log
from astropy.stats import sigma_clipped_stats
from astropy.utils.console import ProgressBar

from spectrapy.dataio.image import Image
from spectrapy.dataio.spectra import Spectra
from spectrapy.dataio.catalog import Catalog
from spectrapy.datacalib.edges import Sobel
from spectrapy.datacalib.extractiontable import ExtTable
from spectrapy.datacalib.wavelengthcalib import WavelengthCalibration


from .resamplingkernel import ResamplingKernel
from .fastresamplingkernel import FastResamplingKernel


class ExponentialFilter(object):
    """This class implements extraction using one exponential filter"""

    def __init__(self, radius: float = 2, pix_sample: int = 1000, cython=True):
        """Initialize the class

        Parameters
        ----------
        radius:
        """
        if cython is True:
            log.warning("Cython version used! Ignored radius and pix_sample parameters.")
            self._kernel = FastResamplingKernel()
        else:
            self._kernel = ResamplingKernel(radius, pix_sample)

        self._wstart = None
        self._wend = None
        self._wstride = None

        self._image = None

    def set_extraction_range(self, wstart, wend, wstride):
        """Set the extraction range

        Parameters
        ----------
        wstart: float
          The wavelength start

        wend: float
          The wavelength end

        wstride: float
          The wavelength step in spectra extraction
        """
        if wend < wstart:
            raise ValueError('Invalid extracion range')

        if wstride <= 0.0:
            raise ValueError('Invalid extracion range')

        self._wstart = wstart
        self._wend = wend
        self._wstride = wstride

    def has_extraction_range(self):
        """Checks it extraction range is set

        Returns
        -------
        Returns True if extraction range has been set
        """
        return None not in [self._wstart, self._wend, self._wstride]

    def load_image(self, ext_table, datafile):
        """Loads image data

        Parameters
        ----------
        ext_table: ExtTable
          The extraction table containg HDU info

        datafile: str
          The input image
        """
        if ext_table.instrument.data_hdu is not None:
            self._image = Image.load(datafile, data_hdu=ext_table.instrument.data_hdu)
        else:
            self._image = Image.load(datafile)

        if ext_table.instrument.var_hdu is not None:
            self._image.load_variances(datafile, var_hdu=ext_table.instrument.var_hdu)

        if ext_table.instrument.err_hdu is not None:
            self._image.load_variances(datafile, err_hdu=ext_table.instrument.err_hdu)

        if ext_table.instrument.flag_hdu is not None:
            self._image.load_flags(datafile, flag_hdu=ext_table.instrument.flag_hdu)

    def extract(self, ext_table, datafile=None, objlist=None, row_start=None, row_end=None,
                xadjust=False, lines=None):
        """Extracts 2D spectra

        Parameters
        ----------
        ext_table: ExtTable or str
          The extraction table

        datafile: str
          The image data file

        objlist: list, optional
          The list of objects to extract

        row_start: int, optional
          The starting row number to extract a sub portion of slit

        row_end: int, optional
          The ending row number to extract a sub portion of slit

        xadjust: bool, optional
          If True compute ajustment on current data along the cross dispersion direction

        lines: str or array_like, optional
          If different from None, a catalog of bright lines must be provided.
          This catalog is used to compute the adjustment along the dispersion direction
        """

        if self.has_extraction_range() is False:
            raise AttributeError("No extraction range")

        if isinstance(ext_table, ExtTable) is False:
            ext_table = ExtTable.load(ext_table)

        if datafile is not None:
            self.load_image(ext_table, datafile)

        if self._image is None:
            raise AttributeError("Image not loaded yet!")

        shift = np.zeros((2,))
        if xadjust is True:
            shift += self.compute_trace_adjustment(ext_table, objlist=objlist)
        if lines is not None:
            shift += self.compute_lambda_adjustment(ext_table, lines, objlist=objlist)
        if np.allclose(shift, [0., 0.]) is True:
            shift = None

        hdulist = Spectra([fits.PrimaryHDU()])

        waves = np.arange(self._wstart, self._wend, self._wstride)

        if objlist is None:
            objlist = ext_table.keys()

        for slit_id in objlist:
            if slit_id not in ext_table:
                log.warning(f'No object {slit_id} in extraction table')
                continue

            log.info(f'Extracting object {slit_id}...')
            data = self._slit_extraction(ext_table[slit_id], waves, row_start, row_end, shift=shift)

            hdulist.add_dataset(data, self._wstart, self._wstride, slit_id, '2DCOUNTS')

        return hdulist

    def _slit_extraction(self, slit, waves, row_start, row_end, shift=None):
        rows = len(slit[row_start:row_end])
        cols = len(waves)

        # 3 layers: data, variance and quality
        data = np.zeros((3, rows, cols))

        # Define variances
        if self._image.has_variances():
            sigma = np.sqrt(self._image.variances)
        else:
            sigma = None

        with ProgressBar(rows) as pbar:
            for row, exr_row in enumerate(slit[row_start:row_end]):
                points = exr_row.compute(waves)
                if shift is not None:
                    points += shift

                for col, point in enumerate(points):
                    data[:, row, col] = self._kernel.interpolate(point, self._image.data,
                                                                 sigma=sigma)
                pbar.update()
        return data

    def compute_trace_adjustment(self, ext_table, datafile=None, objlist=None):
        """Computes shift along cross dispersion direction compared to the reference slit position

        Parameters
        ----------
        ext_table: ExtTable or str
          The extraction table instance or file

        datafile: str
          The image file

        objlist: array_like, optional
          A subsample of slit on which performe the computation

        Returns
        -------
        shift: np.array, shape_like (2,)
          The pixel shift along the cross dispersion
        """
        if datafile is not None:
            self.load_image(ext_table, datafile)

        if self._image is None:
            raise AttributeError("Image not loaded yet!")

        if objlist is None:
            objlist = ext_table.keys()

        engine = Sobel(ext_table.instrument.dispersion_direction)
        engine.image = self._image

        shifts = []

        for slit_id in objlist:
            slit = ext_table[slit_id]
            orientation = slit[0][slit[0].ORIENTATION]

            # Left shift
            center = slit[0].center
            edge, _ = engine.compute_slice_edge(center, np.array([20, 20]))
            shifts.append(orientation.dot(edge) - orientation.dot(center))

            # Right shift
            center = slit[-1].center
            edge, _ = engine.compute_slice_edge(center, np.array([20, 20]), right=True)
            shifts.append(orientation.dot(edge) - orientation.dot(center))

        shift, _, scatter = sigma_clipped_stats(shifts, cenfunc=np.mean)
        log.info(f"Shift along the cross dispersion direction {shift:.2f}+/-{scatter:.2f}")
        return orientation * shift

    def compute_lambda_adjustment(self, ext_table, lines, datafile=None, objlist=None):
        """Computes shift along dispersion direction compared to the reference solution

        Parameters
        ----------
        ext_table: ExtTable or str
          The extraction table instance or file

        datafile: str
          The image file

        lines: Catalog or str
          The catalog file or instance

        objlist: array_like, optional
          A subsample of slit on which performe the computation

        Returns
        -------
        r: float
          The shift along the dispersion direction
        """
        if datafile is not None:
            self.load_image(ext_table, datafile)

        if self._image is None:
            raise AttributeError("Image not loaded yet!")

        if isinstance(lines, Table) is False:
            lines = Catalog.load(lines, wstart=self._wstart, wend=self._wend)

        if len(lines) == 0:
            log.info(f"Empty catalog. No shift computed")
            return np.zeros((2, ))

        if objlist is None:
            objlist = ext_table.keys()

        # engine = Sobel(ext_table.instrument.dispersion_direction)
        # engine.image = self._image

        waves = np.arange(self._wstart, self._wend, self._wstride)

        slit_shifts = []
        stds = []
        for slit_id in objlist:
            shifts = np.zeros((len(ext_table[slit_id]), len(lines)))
            for k, row in enumerate(ext_table[slit_id]):

                ids_model = row.ids
                delta_disp, _ = ids_model.compute(waves)

                crv_model = row.crv
                delta_curve, _ = crv_model.compute(delta_disp)

                fluxes, _ = self._image.interpolate(row.center + delta_curve)

                real = WavelengthCalibration.compute_line_positions(waves, delta_disp, fluxes,
                                                                    lines, 10)
                delta = row.compute(real[:, 0])[:, 0]
                delta -= (row.items[row.DET_POS] + crv_model.compute(real[:, 1])[0])[:, 0]
                shifts[k] = delta

            shifts = shifts[~np.isnan(shifts)]
            if len(shifts) == 0:
                log.info(f"No shifts for slit {slit_id}")
                continue

            shift, _, scatter = sigma_clipped_stats(shifts[~np.isnan(shifts)], cenfunc=np.mean)
            log.info(f"Shift on slit {slit_id}: {shift:.3f}+/-{scatter:.3f}")

            slit_shifts.append(shift)
            stds.append(scatter)

        if len(slit_shifts) == 0:
            log.info(f"No shift computed")
            return np.zeros((2,))

        shift = np.average(slit_shifts, weights=1. / np.array(stds))
        return crv_model.dispersion_vector * shift

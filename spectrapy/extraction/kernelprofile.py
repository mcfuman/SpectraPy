#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File kernelprofile.py
#
# Created on: Sep 12, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains class the compute the resampling kernel profile 1D"""

import numpy as np


class KernelProfile(object):
    """This class implements the resampling kernel profile using a cutoff function
    in Fourier space"""
    DEFAULT_PROFILE_STEEP = 5

    def __init__(self, radius: float, pxsample: int):
        self._steep = KernelProfile.DEFAULT_PROFILE_STEEP
        self.pxsample = 0
        self._data = np.zeros(0)
        self.__generate_profile(radius, pxsample)

    @staticmethod
    def _cutoff(x, steep):
        """Evaluates the cut off function

        Parameters
        ----------
        x: np.array, shape(N,)
          The x values of the function

        steep: float
          The steepness of the cutoff function
        """
        return ((np.tanh(steep * (x + 0.5)) + 1.0) / 2.0) * \
               ((np.tanh(steep * (-x + 0.5)) + 1.0) / 2.0)

    def has_profile(self) -> bool:
        """Returns True is the kernel profile has been computed"""
        return bool(len(self._data))

    @property
    def profile(self):
        """Gets the profile values

        Returns
        -------
        np.array, shape(N,)
          The profile values
        """
        if self.has_profile() is False:
            raise AttributeError("Profile not yet generated")

        return self._data

    @profile.setter
    def profile(self, data):
        """Sets the profile values

        Parameters
        ----------
        data: array_like, shape(N,)
          The profile data values
        """
        self._data = np.array(data)

    def __generate_profile(self, radius: float, pxsample: int):
        """Computes the profile and sets pixel sample of the profile

        Parameters
        ----------
        radius: float [pixels]
          The resampling kernel radius

        pxsample: int
          The number of points inside each pixel in kernel computation
        """

        profile_sample = int(radius * pxsample)
        if profile_sample == 0:
            return

        _len = 32768  # 2**15
        freq_step = pxsample / _len

        z = np.zeros(_len, np.complex64)

        x = np.arange(_len // 2) * freq_step
        z.real[0:_len // 2] = KernelProfile._cutoff(x, self._steep)

        x = (np.arange(_len // 2, _len) - _len) * freq_step
        z.real[_len // 2:] = KernelProfile._cutoff(x, self._steep)

        out = _len * np.fft.ifft(z)

        self._data = out.real[0:profile_sample + 1] * freq_step

        self.pxsample = pxsample

    def get_profile_value(self, subpixel):
        """Get the profile value

        Parameters
        ----------
        subpixel: float
          The pixel value (float) where evaluate the profile

        Returns
        -------
        float
          The profile value
        """
        # Find the nearest index
        index = (subpixel * self.pxsample + 0.5).astype(np.int32)

        return self._data[index]

    @staticmethod
    def load(filename):  # pragma: no cover
        """Temporary method
        """
        import struct
        import array
        kernel = KernelProfile(0, 0)
        #k._radius = 2.
        kernel.pxsample = 1000.

        size = struct.calcsize('d')

        # Read len
        with open(filename, 'rb') as inputf:
            length = 0
            while inputf.read(size):
                length += 1

        # Create array
        with open(filename, 'rb') as inputf:
            y_data = array.array('d')
            y_data.fromfile(inputf, length)
            kernel.profile = y_data

        return kernel

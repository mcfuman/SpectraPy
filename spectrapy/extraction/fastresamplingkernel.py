#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File fastresamplingkernel.py
#
# Created on: Mar 15, 2020
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains handle resampling faster the Python version"""

from .kernelprofile import KernelProfile
from .cresamplingkernel import CResamplingKernel


class FastResamplingKernel(KernelProfile, CResamplingKernel):
    """The resampling kernel class"""

    def __init__(self):
        CResamplingKernel.__init__(self)
        KernelProfile.__init__(self, self.radius, self.pix_sample)

        self._weights = None

    def compute_weights(self, pos):
        """Computes kernel weights

        Parameters
        ----------
        pos: array_like, shape(2,)
          The (x,y) center position of the kernel
        """
        return self._compute_weights(pos[0], pos[1], self.profile)

    def out_of_circle(self):
        """Sets to 0 every weights out the circle centered in the kernel center position
        """
        return self._out_of_circle(self._weights)

    def out_of_image(self, data):
        """Sets to 0 every weights out the image to rebin

        Parameters
        ----------
        data: np.array, shape(N, M)
          Image data
        """
        return self._out_of_image(self._weights, *data.shape)

    def invalid_mask(self, data):
        """Sets to 0 every weights where data mask is >0

        Parameters
        ----------
        data: np.ma.array, shape(N, M)
          Image masked data
        """
        return self._invalid_mask(data.mask, self._weights)

    def compute_value(self, all_weight, data, sigma=None, cov=False):
        """Computes the resampling kernel value.

        Parameters
        ----------
        all_weight: float
          The sum of all avaible weights exclude out_of_circle weights

        data: np.array, shape(N, M)
          Image data

        sigma:np.array, optional
          The square root of the variances on Image if available

        Returns
        -------
        value: array_like, shape(3,)
          The value, variance and quality of the resampling
        """
        return self._compute_value(all_weight, data, sigma, cov, self._weights)

    def interpolate(self, pos, data, sigma=None, cov=False):
        """Interpolates the image in one point

        Parameters
        ----------
        pos: array_like, shape(2,)
          The (x,y) position to interpolate

        data: np.ma.array
          The Image to interpolate

        sigma:np.array, optional
          The square root of the variances on Image if available
        """
        self.compute_weights(pos)

        all_weight = self.out_of_circle()
        self.out_of_image(data)
        self.invalid_mask(data)

        return self.compute_value(all_weight, data, sigma=sigma, cov=cov)

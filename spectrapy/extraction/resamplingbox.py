#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File resamplingbox.py
#
# Created on: Oct 01, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the ResamplingBox class"""

import numpy as np


class ResamplingBox(object):
    """This is the class to handle the box area around a resampling point"""

    def __init__(self, radius: float = 0.0):
        self._radius = radius
        self._radius2 = radius * radius
        self._pos = None
        self._xrange = None
        self._yrange = None

    def _set_ranges(self):
        _start = np.ceil(self._pos - self._radius).astype(np.int32)
        _end = np.floor(self._pos + self._radius).astype(np.int32) + 1

        self._xrange = np.arange(_start[0], _end[0])
        self._yrange = np.arange(_start[1], _end[1])

    @property
    def xstart(self) -> int:
        """The X starting point of the box"""
        return self._xrange[0]

    @property
    def ystart(self) -> int:
        """The Y starting point of the box"""
        return self._yrange[0]

    @property
    def xend(self) -> int:
        """The X ending point of the box"""
        return self._xrange[-1]

    @property
    def yend(self) -> int:
        """The Y ending point of the box"""
        return self._yrange[-1]

    @property
    def xsize(self) -> int:
        """The X size of the box"""
        return len(self._xrange)

    @property
    def ysize(self) -> int:
        """The Y size of the box"""
        return len(self._yrange)

    @property
    def xrange(self) -> np.arange:
        """Gets a range along the X axis of the box"""
        return self._xrange

    @property
    def yrange(self) -> np.arange:
        """Gets a range along the Y axis of the box"""
        return self._yrange

    def set_position(self, pos):
        """Sets the position of the resampling box and computes the starting end the ending points
        along X and Y axes

        Parameters
        ----------
        pos: array_like, shape(2,)
          The center of the box: the point to resample
        """
        self._pos = pos

        self._set_ranges()

    def has_position(self) -> bool:
        """Returns True if the position has been set
        """
        return not self._pos is None

    def is_out_of_circle(self, pos) -> bool:
        """Checks if the pixel position is out of the resampling kernel circle

        Parameters
        ----------
        pos: array_like, shape(2,)
          The pixel position

        Returns
        -------
        bool
          True in case the pixel is out of the resampling kernel circle
        """
        if self.has_position() is False:
            raise AttributeError("Position not set yet!")

        delta = pos - self._pos
        return delta.dot(delta) > self._radius * self._radius

    def is_out_of_box(self, pos) -> bool:
        """Checks if the pixel position is out of the box

        Parameters
        ----------
        pos: array_like, shape(2,)
          The pixel position

        Returns
        -------
        bool
          True in case the pixel is out of the resampling box
        """
        if self.has_position() is False:
            raise AttributeError("Position not set yet!")

        return not (self.xstart <= pos[0] <= self.xend and self.ystart <= pos[1] <= self.yend)

    @property
    def radius(self):
        """Gets the radius of the resampling kernel"""
        return self._radius

    @radius.setter
    def radius(self, value: float):
        """Sets the radius of the rasampling kernel and reset the box position

        Parameters
        ----------
        value: float
          The radius of the resampling kernel
        """
        self._radius = value
        self._radius2 = value * value
        self._pos = None
        self._xrange = None
        self._yrange = None

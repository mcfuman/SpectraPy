from libc.stdlib cimport malloc, free
import numpy as np
DTYPE = np.float64
cimport cython
cimport numpy as np

cdef extern from "math.h":
    int ceil(double)
    int floor(double)
    double fabs(double)

from numpy.math cimport INFINITY, NAN

cdef class CResamplingKernel:
    cdef int X[5]
    cdef int Y[5]
    cdef int xwlen
    cdef int ywlen
    cdef double _radius
    cdef double _radius2
    cdef int _pix_sample
    cdef double xpos
    cdef double ypos

    def __cinit__(self):
        self._radius = 2.0
        self._radius2= 4.0
        self._pix_sample = 1000

    @property
    def radius(self):
        return self._radius

    @property
    def pix_sample(self):
        return self._pix_sample


    @cython.boundscheck(False)
    cpdef _compute_weights(self, double xpos, double ypos, double [:] profile):
        cdef int idx
        cdef double xw[5]
        cdef double yw[5]

        cdef int xstart = ceil(xpos-self._radius)
        cdef int ystart = ceil(ypos-self._radius)

        cdef int xend= floor(xpos+self._radius)
        cdef int yend= floor(ypos+self._radius)

        cdef int i=xstart
        while i<=xend:
            idx = int(fabs(i-xpos)*self._pix_sample+0.5)
            xw[i-xstart]=profile[idx]
            i+=1
        self.xwlen=i-xstart

        i=ystart
        while i<=yend:
            idx = int(fabs(i-ypos)*self._pix_sample+0.5)
            yw[i-ystart]=profile[idx]
            i+=1
        self.ywlen=i-ystart

        self._weights = np.empty([self.ywlen, self.xwlen], dtype=DTYPE)

        cdef double[:, :] w = self._weights

        cdef int x, y
        for y in range(self.ywlen):
            self.Y[y]=ystart+y
            for x in range(self.xwlen):
                w[y, x]=xw[x]*yw[y]

        for x in range(self.xwlen):
            self.X[x]=xstart+x

        self.xpos = xpos
        self.ypos = ypos

    @cython.boundscheck(False)
    cpdef double _out_of_circle(self, double [:,:] weights):
        cdef int x, y
        cdef double all_weight = 0.0

        for y in range(self.ywlen):
            for x in range(self.xwlen):
                if (self.X[x]-self.xpos)*(self.X[x]-self.xpos)+\
                (self.Y[y]-self.ypos)*(self.Y[y]-self.ypos)> self._radius2:
                    weights[y, x]=0.0
                else:
                    all_weight+=fabs(weights[y, x])

        return all_weight

    @cython.boundscheck(False)
    @cython.cdivision(True)
    cpdef _out_of_image(self, double [:,:] weights, Py_ssize_t ylim, Py_ssize_t xlim):
        cdef int i

        for i in range(self.ywlen):
            if self.Y[i] < 0 or self.Y[i]>= ylim:
                weights[i]=0.0
                #Wrap around to prevent out of data error
                self.Y[i]=self.Y[i]%ylim

        for i in range(self.xwlen):
            if self.X[i] < 0 or self.X[i]>= xlim:
                weights[:,i]=0.0
                #Wrap around to prevent out of data error
                self.X[i]=self.X[i]%xlim

    @cython.boundscheck(False)
    cpdef _invalid_mask(self, np.ndarray mask, double [:,:] weights):
        cdef int i,j

        for j in range(self.ywlen):
            for i in range(self.xwlen):
                if mask[self.Y[j], self.X[i]] == True:
                    weights[j, i]=0.0

    @cython.boundscheck(False)
    @cython.cdivision(True)
    cpdef _compute_value(self, double all_weight, double[:,:] data, double [:,:] sigma, int cov,
                         double [:,:] weights):
        cdef int i,j

        cdef double rate = 0.0
        cdef double value = 0.0
        cdef double wsum = 0.0
        cdef double wsum2 = 0.0
        cdef double wsum_abs = 0.0
        cdef double var = 0.0
        cdef double w = 0.0

        for j in range(self.ywlen):
            for i in range(self.xwlen):
                value+=weights[j,i]*data[self.Y[j],self.X[i]]
                wsum += weights[j,i]
                wsum2 += (weights[j,i]*weights[j,i])
                wsum_abs+=fabs(weights[j,i])

        if all_weight > 0:
            rate = wsum_abs / all_weight

        if wsum != 0.:
            value /= wsum

        if sigma is None:
            var = NAN
        else:
            if wsum == 0.0:
                var = INFINITY
            else:
                if cov == 0:
                    for j in range(self.ywlen):
                        for i in range(self.xwlen):
                            w = weights[j,i]*sigma[self.Y[j],self.X[i]]
                            var += (w*w)
                else:
                    #First sum
                    for j in range(self.ywlen):
                        for i in range(self.xwlen):
                            var+=(weights[j,i]*sigma[self.Y[j],self.X[i]])
                    var*=var
                var/=wsum2

        return value, var, rate


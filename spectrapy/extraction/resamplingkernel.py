#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File resamplingkernel.py
#
# Created on: Oct 01, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains core class the handle resampling"""

import numpy as np
from .kernelprofile import KernelProfile
from .resamplingbox import ResamplingBox


class ResamplingKernel(ResamplingBox, KernelProfile):
    """The resampling kernel class"""

    def __init__(self, radius: float = 2., pix_sample: int = 1000):
        ResamplingBox.__init__(self, radius)
        KernelProfile.__init__(self, radius, pix_sample)
        self._weights = None
        # pylint: disable=C0103
        self._X = None
        self._Y = None

    @property
    def position(self):
        """Gets the kernel position"""
        return self._pos

    @position.setter
    def position(self, pos):
        """Sets the kernel position

        Parameters
        ----------
        pos: array_like, shape(2,)
          The new kernel (x,y) position
        """
        self.set_position(pos)
        self._compute_weights(pos)
        self._X, self._Y = np.meshgrid(self._xrange, self._yrange)

    def _compute_weights(self, pos):

        _xweights = self.get_profile_value(np.abs(self._xrange - pos[0]))
        _yweights = self.get_profile_value(np.abs(self._yrange - pos[1]))

        self._weights = np.tensordot(_yweights, _xweights, axes=0)

    def _out_of_circle(self):
        return (self._X - self._pos[0])**2 + (self._Y - self._pos[1])**2 > self._radius2

    def _out_of_image(self, data):
        return np.any((self._X < 0, self._X >= data.shape[1],
                       self._Y < 0, self._Y >= data.shape[0]), axis=0)

        # return np.logical_or(np.logical_or(self._X < 0, self._X >= data.shape[1]),
        #                     np.logical_or(self._Y < 0, self._Y >= data.shape[0]))

    def _invalid_mask(self, data):
        # pylint: disable=C0121
        return data.mask[self._Y, self._X] == True

    def interpolate(self, pos, data, sigma=None, cov=False):
        """Interpolates the image in one point

        Parameters
        ----------
        pos: array_like, shape(2,)
          The (x,y) position to interpolate

        data: np.ma.array
          The Image to interpolate

        sigma:np.array, optional
          The square root of the variances on Image if available
        """
        self.position = pos

        self._weights[self._out_of_circle()] = 0.0
        all_weight = np.abs(self._weights).sum()

        self._weights[self._out_of_image(data)] = 0.0
        # Prevent errors in resampling out of image
        self._Y %= data.shape[0]
        self._X %= data.shape[1]

        self._weights[self._invalid_mask(data)] = 0.0

        value = (data[self._Y, self._X] * self._weights).sum()

        wsum = self._weights.sum()
        wsum_abs = np.abs(self._weights).sum()

        if all_weight > 0:
            rate = wsum_abs / all_weight
        else:
            rate = 0.0

        if wsum != 0.:
            value /= wsum
        else:
            value = 0.0

        if sigma is None:
            var = None
        else:
            wsum2 = (self._weights**2).sum()
            if wsum2 != 0:
                if cov is False:
                    var = ((sigma[self._Y, self._X] * self._weights)**2).sum()
                else:
                    var = (sigma[self._Y, self._X] * self._weights).sum()
                    var *= var
                var /= wsum2
            else:
                var = np.inf

        return value, var, rate

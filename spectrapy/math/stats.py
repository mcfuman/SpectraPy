#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File stats.py
#
# Created on: Sep 06, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module collects statistical functions for SpectraPy"""

import numpy as np
from astropy.stats import sigma_clip


def clipped_average(data, var=None, sigma=3.0, maxiters=1, axis=0):
    """Perform sigma-clipping on the provided data and return average and variance on average.

    See https://en.wikipedia.org/wiki/Weighted_arithmetic_mean

    Parameters
    ---------
    data: numpy.array
      The data use to compute average
    var: numpy.array, optional
      The variance on data
    sigma: float, optional
      The number of standard deviations to use for both the lower and upper clipping limit.
    maxiters: int, optional
      The number of iterations to perform sigma clipping
    axis: int, optional
      Clip along the given axis

    Returns
    -------
    float, float
      The clippend average and the variance on the clipped average
    """
    try:  # pragma: no cover
        data_clipped = sigma_clip(data, sigma=sigma, maxiters=maxiters, axis=axis)
    except:  # pragma: no cover
        data_clipped = sigma_clip(data, sigma=sigma, iters=maxiters, axis=axis)

    if var is not None:
        average, wsum = np.ma.average(data_clipped, weights=1. / var, axis=axis, returned=True)
        variance = 1. / wsum
    else:
        average, wsum = np.ma.average(data_clipped, axis=axis, returned=True)
        variance = np.ma.var(data_clipped, axis=axis) / wsum

    return average, variance

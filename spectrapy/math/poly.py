#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File poly.py
#
# Created on: Sep 06, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains functions for polynomial fitting and evaluation"""

import numpy as np
from numpy.polynomial import polynomial

# pylint: disable=C0103


def polyfit2d(x, y, z, degs, var=None, cov=False):
    """Fits polynomial 2D

    Parameters
    ----------
    x: array_like, shape(N,)
      X values

    y: array_like, shape(N,)
      Y values

    z: array_like, shape(N,)
      Z values

    degs: array_like, shape(2,)
      The X and Y degrees of the polynomial

    var: array_like, shape(N,), optional
      The Z variances

    cov: bool
      If True computes covariances on fitted parameters
    """
    if len(degs) != 2:
        raise TypeError("Invalid degs")

    X = polynomial.polyvander2d(x, y, degs)
    return _polyfit_core(X, z, degs, var, cov)


def polyfit1d(x, y, deg, var=None, cov=False):
    """Fits polynomial 1D

    Parameters
    ----------
    x: array_like, shape(N,)
      X values

    y: array_like, shape(N,)
      Y values

    degs: int
      The degree of the polynomial

    var: array_like, shape(N,), optional
      The Y variances

    cov: bool
      If True computes covariances on fitted parameters
    """

    X = polynomial.polyvander(x, deg)
    return _polyfit_core(X, y, (deg,), var, cov)


def _polyfit_core(X, z, degs, var=None, cov=False):
    """The core fitting function

    Parameters
    ----------
    X: array_like
      The Vandermonde matrix

    z: array_like, shape(N,)
      The dependent variable value

    var: array_like, shape(N,), optional
      The variance on the dependent variable

    cov: bool
      If True computes covariances on fitted parameters
    """

    if var is None:
        Vy_inv = np.diag(np.ones(len(z)))
    else:
        # Compute inverse of diagonal of variance
        Vy_inv = np.diag(1. / var)

    Va = np.linalg.inv(X.T.dot(Vy_inv.dot(X)))

    H = Va.dot(X.T.dot(Vy_inv))

    coeffs = H.dot(z)

    delta = X.dot(coeffs) - z

    resids = delta.dot(delta)

    tmp = z - z.mean()
    r2 = 1.0 - resids / tmp.dot(tmp)

    nparam = 0
    for deg in degs:
        nparam += (deg + 1)

    resids = delta.dot(delta * Vy_inv.diagonal())
    chi2 = resids / (len(X) - nparam)

    covariance = None
    if cov is True:
        covariance = Va * chi2

    shape = np.array(degs) + 1
    coeffs.shape = shape

    return coeffs, chi2, r2, covariance


def polyval2d(pos, matrix, cov=None):
    """Evaluates 2d polynomial and variance on it value in case covariance matrix is provided.

    Parameters
    ----------
    pos: array_like, shape(2,)
      (x,y) polynoial coordinates

    matrix: array_like, shape(N, M)
      Matrix describing polynomial

    cov: array_like, shape(N*M,N*M), optional
      Covaiance matrix on coeffiecients `matrix``

    Returns
    -------
    float, float or None
      The value the polynomial and the variance on this value.
      Variance is None is case covariance matrix is not provided
    """

    val = polynomial.polyval2d(pos[0], pos[1], matrix)

    if cov is None:
        return val, 0.0

    elif np.allclose(cov, 0.0):
        return val, 0.0

    else:
        degs = np.array(matrix.shape) - 1
        J = polynomial.polyvander2d(pos[0], pos[1], degs)[0]
        var = J.dot(cov.dot(J.T))

    return val, var

#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File peaks.py
#
# Created on: Sep 06, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains function to compute line positions"""

import numpy as np

from astropy import log


def old_find_peak_1d(data, min_points):  # pragma: no cover
    """Old find peak 1D"""
    size = len(data)

    if size < 5:
        return None

    median = np.median(data)

    _max = max(data)

    if _max - median < 1e-25:
        return None

    level = (_max + median) / 2

    idx = np.nonzero(data > level)

    if len(idx[0]) < min_points:
        return None

    weights = (data[idx] - median).sum()
    _sum = (data[idx] - median).dot(idx[0])
    pos = _sum / weights

    weights = len(idx)
    _sum = (idx[0] - pos).dot(idx[0] - pos)
    variance = np.sqrt(_sum / weights)

    uniform_variance = np.sqrt(size * size / 3 - pos * size + pos * pos)

    if variance > 0.8 * uniform_variance:
        return None

    if pos <= 1.0 or pos >= (size - 2):
        return None

    return pos


def find_peak_1d(data: np.ma.array, min_points: int, variance: np.array = None):
    """Computes the line peak position

    Parameters
    ----------
    data: np.ma.array, shape(N,)
      The masked data values

    min_points: int
      The min number of points required to compute the peak position

    variance: np.array, shape(N,)
      The data variance values
    """
    # Check if is masked!!!
    try:
        size = data.count()
    except AttributeError:
        size = len(data)

    if size < min_points:
        log.debug(f"Input array too short ({size} valid items)")
        return None, None

    _median = np.ma.median(data)
    _max = np.ma.max(data)

    if np.allclose(_max, _median) is True:
        log.debug("Flat region")
        return None, None

    level = (_max + _median) / 2
    idx = np.ma.where(data > level)[0]

    if len(idx) < min_points:
        log.debug(f"Not enought points out of median level ({len(idx)} items)")
        return None, None

    # Assume no error no median
    data_no_level = data[idx] - _median

    if variance is None:
        den = np.ma.sum(data_no_level)
        num = float(np.ma.dot(data_no_level, idx))
    else:
        weights = 1. / variance[idx]
        den = float(np.ma.dot(data_no_level, weights))
        num = np.ma.sum(weights * idx * data_no_level)

    pos = num / den
    idx_pos = idx.astype(np.float64) - pos

    if variance is not None:
        pos_var = np.ma.sum((weights * (idx_pos * variance[idx]))**2) / den
    else:
        pos_var = None

    if _check_variance(pos, size, idx_pos) is False:
        return None, None

    return pos, pos_var


def _check_variance(pos, size, indexes):
    den = len(indexes)
    num = indexes.dot(indexes)
    _var = np.sqrt(num / den)

    uniform_variance = np.sqrt(size * size / 3 - pos * size + pos * pos)

    if _var > 0.8 * uniform_variance:
        log.debug("Variance too uniform")
        return False

    if (1.0 <= pos <= size - 2.0) is False:
        log.debug("Peak position too close to the edge")
        return False

    return True

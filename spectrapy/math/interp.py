#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File interp.py
#
# Created on: Sep 06, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains the class to compute a bilinear interpolation on image"""

import numpy as np


class BiLinearInterpolator(object):  # pylint: disable=too-few-public-methods
    """Class to compute bi-linear interpolation"""

    def __init__(self, data, variance=None):
        """see https://en.wikipedia.org/wiki/Bilinear_interplation
        """
        self._data = data
        self._variance = variance
        self._xsize = data.shape[1]
        self._ysize = data.shape[0]

    def __call__(self, x, y):
        return self.compute(x, y)

    @staticmethod
    def _get_int(values):
        if np.isscalar(values):
            return np.array([values], dtype=int)

        return values.astype(int)

    def compute(self, x: np.array, y: np.array):  # pylint: disable=too-many-locals
        """Interpolate the image along x and y values

        Parameters
        ----------
        x: array_like, shape(N,)
          The x positions on the image

        y: array_like, shape(N,)
          The y positions on the image
        """
        # pylint: disable=C0103

        # x,y interger points
        xint = BiLinearInterpolator._get_int(x)
        yint = BiLinearInterpolator._get_int(y)

        # Select points inside image
        idx = np.nonzero((x >= 0) & (x < self._xsize - 1) & (y >= 0) & (y < self._ysize - 1))

        x1 = xint[idx]
        x2 = x1 + 1
        y1 = yint[idx]
        y2 = y1 + 1

        # Evaluate data around expected x,y points (x1<x<x2, y1<y<y2)
        Q11 = self._data[y1, x1]
        Q12 = self._data[y2, x1]
        Q21 = self._data[y1, x2]
        Q22 = self._data[y2, x2]

        if self._variance is not None:
            W11 = 1. / self._variance[y1, x1]
            W12 = 1. / self._variance[y2, x1]
            W21 = 1. / self._variance[y1, x2]
            W22 = 1. / self._variance[y2, x2]

        _dx1 = x[idx] - x1
        _dx2 = -_dx1 + 1

        _dy1 = y[idx] - y1
        _dy2 = -_dy1 + 1

        # Compute delta x and delta y to improve performances
        d11 = _dx1 * _dy1
        d12 = _dx1 * _dy2
        d21 = _dx2 * _dy1
        d22 = _dx2 * _dy2

        # Create results array
        # pylint: disable=unsupported-assignment-operation
        resampled = np.full_like(x, np.nan, dtype=np.float64)

        if self._variance is None:
            resampled[idx] = Q11 * d22 + Q21 * d12 + Q12 * d21 + Q22 * d11
            variances = None
        else:
            wsum = np.full_like(x, np.nan, dtype=np.float64)
            # pylint: disable=unsupported-assignment-operation
            wsum[idx] = 4. / (W11 + W12 + W21 + W22)

            # pylint: disable=unsupported-assignment-operation
            resampled[idx] = W11 * Q11 * d22 + W21 * Q21 * d12 + W12 * Q12 * d21 + W22 * Q22 * d11
            resampled[idx] *= wsum[idx]

            # pylint: disable=unsupported-assignment-operation
            variances = np.full_like(x, np.nan, dtype=np.float64)
            variances[idx] = ((W11 * d22)**2 + (W21 * d12)**2 + (W12 * d21)**2 +
                              (W22 * d11)**2) * wsum[idx]**2

        return resampled, variances

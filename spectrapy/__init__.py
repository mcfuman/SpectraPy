"""The SpectraPy library. The library to extract astronomical spectra from raw data"""

# Set log level
from astropy import log
log.setLevel('INFO')
log.enable_color()
# Problems with Astropy 4.x
# log.disable_warnings_logging()

__version__ = '1.0.1'

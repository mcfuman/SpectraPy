#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File instrument.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""Module containg class to handle instrument and files properties"""


import os
import configparser

from astropy import log


class Instrument(object):
    """Class which collects instrument parameters"""

    def __init__(self):
        """Initialize the instrument class
        """
        self._pixel_scale = 0.0
        self._pixel_size = 0.0
        self._xpixels = 0
        self._ypixels = 0
        self._dispersion_direction = 'LR'
        self._linear_dispersion = 0.0
        self._reference_lambda = 0.0
        self._data_hdu = 0
        self._var_hdu = None
        self._err_hdu = None
        self._flag_hdu = None
        self.description = {}

    @staticmethod
    def load(filename):
        """Load instrument configuration file

        Parameters
        ----------
        filename: str
          Input file name
        """
        if os.path.isfile(filename) is False:
            raise IOError("Invalid instrument file")

        config = configparser.ConfigParser()
        config.read(filename)

        instrument = Instrument()
        instrument.pixel_scale = float(config['Detector']['pixel_scale'])
        instrument.xpixels = int(config['Detector']['xpixels'])
        instrument.ypixels = int(config['Detector']['ypixels'])

        instrument.dispersion_direction = str(config['Grism']['dispersion_direction'])
        instrument.linear_dispersion = float(config['Grism']['linear_dispersion'])
        instrument.reference_lambda = float(config['Grism']['reference_lambda'])

        if 'data_hdu' in config['Files']:
            instrument.data_hdu = config['Files']['data_hdu']
        else:
            instrument.data_hdu = 'Primary'

        if 'var_hdu' in config['Files']:
            instrument.var_hdu = config['Files']['var_hdu']

        if 'err_hdu' in config['Files']:
            instrument.err_hdu = config['Files']['err_hdu']

        if (instrument.var_hdu is not None) and (instrument.err_hdu is not None):
            raise ValueError("Invalid instrument file: use or var_hdu either err_hdu, not both!")

        if 'flag_hdu' in config['Files']:
            instrument.flag_hdu = config['Files']['flag_hdu']

        if 'Description' in config:
            for key in config['Description']:
                instrument.description[key] = config['Description'][key]

        log.info(f"Loaded instrument configuration from {filename}")

        return instrument

    @property
    def pixel_scale(self):
        """Get pixel scale [arcsec/pix]

        Returns
        -------
        float
          The detector pixel scale
        """
        return self._pixel_scale

    @pixel_scale.setter
    def pixel_scale(self, value):
        """Set the detector pixel scale

        Parameters
        ----------
        value: float
          The detector pixel scale
        """
        self._pixel_scale = float(value)

    @property
    def xpixels(self):
        """Get the number of pixels along X axis

        Returns
        -------
        int
          The number of pixels along X axis
        """
        return self._xpixels

    @xpixels.setter
    def xpixels(self, value):
        """Set the number of pixels along X axis

        Parameters
        ----------
        xpixels: int
          The number of pixels along X axis
        """
        if abs(float(value) - int(value)) > 0:
            raise ValueError(f"Invalid number of x pixels {value}")
        self._xpixels = int(value)

    @property
    def ypixels(self):
        """Get the number of pixels along Y axis

        Returns
        -------
        int
          The number of pixels along Y axis
        """
        return self._ypixels

    @ypixels.setter
    def ypixels(self, value):
        """Set the number of pixels along Y axis

        Parameters
        ----------
        ypixels: int
          The number of pixels along X axis
        """
        if abs(float(value) - int(value)) > 0:
            raise ValueError(f"Invalid number of y pixels {value}")
        self._ypixels = int(value)

    @property
    def dispersion_direction(self):
        """Get the instrument dispersion direction

        Returns
        -------
        str
          The valid string: LR, RL, DU or UD
        """
        return self._dispersion_direction

    @dispersion_direction.setter
    def dispersion_direction(self, value):
        """Set instrument dispertion direction.

        Parameters
        ----------
        value: str
          The instrument dispersion direction

        Raises
        ------
        Exception
          In case string is not in: LR, RL, DU or UD
        """
        value = value.upper().strip()
        if value not in ('LR', 'RL', 'BU', 'UB'):
            raise ValueError("Invalid grism dispersion direction")

        self._dispersion_direction = value

    @property
    def linear_dispersion(self):
        """Get the nominal linear dispersion value

        Returns
        -------
        float
          The nominal lineal dispersion value
        """
        return self._linear_dispersion

    @linear_dispersion.setter
    def linear_dispersion(self, value):
        """Set the nominal dispersione value

        Parameters
        ----------
        value: float
          The nominal dispersion value
        """
        self._linear_dispersion = float(value)

    @property
    def reference_lambda(self):
        """Get the arbitrary reference lambda value
        """
        # E' corretto stia qui?!?!?!?
        return self._reference_lambda

    @reference_lambda.setter
    def reference_lambda(self, value):
        """Set the arbitrary reference lambda value

        Parameters
        ----------
        value: float
          The arbitrary reference lambda value
        """
        self._reference_lambda = value

    @property
    def data_hdu(self):
        """Get the extention name or number (Primart is 0), where scientific data are stored.

        Returns
        ---------
        int or str
          Extenton name or number
        """
        return self._data_hdu

    @staticmethod
    def _parse_hdu(value):
        if isinstance(value, int):
            return value

        elif isinstance(value, str):
            if value.lower().strip() == 'primary':
                return 0
            return value

        else:
            raise TypeError(f"Invalid hdu {value}")

    @data_hdu.setter
    def data_hdu(self, value):
        """Set extention name or number (Primary is 0), where retieve scientific data.

        Parameters
        ----------
        value: int or str
          Extention name or number
        """
        self._data_hdu = Instrument._parse_hdu(value)

    @property
    def var_hdu(self):
        """Get the extention name or number (Primary is 0),
        where variances on scientific data are stored.

        Returns
        ---------
        int or str
          Extenton name or number
        """
        return self._var_hdu

    @var_hdu.setter
    def var_hdu(self, value):
        """Set extention name or number (Primary is 0), where retieve variances on scientific data.

        Parameters
        ----------
        value: int or str
          Extention name or number
        """
        self._var_hdu = Instrument._parse_hdu(value)

    @property
    def err_hdu(self):
        """Get the extention name or number (Primary is 0),
        where variances on scientific data are stored.

        Returns
        ---------
        int or str
          Extenton name or number
        """
        return self._err_hdu

    @err_hdu.setter
    def err_hdu(self, value):
        """Set extention name or number (Primary is 0), where retieve variances on scientific data.

        Parameters
        ----------
        value: int or str
          Extention name or number
        """
        self._err_hdu = Instrument._parse_hdu(value)

    @property
    def flag_hdu(self):
        """Get the extention name or number (Primart is 0),
        where quality flags on scientific data are stored.
        This extentions contains interger numbers, where 0 means good pixels.

        Returns
        ---------
        int or str
          Extenton name or number
        """
        return self._flag_hdu

    @flag_hdu.setter
    def flag_hdu(self, value):
        """Set extention name or number (Primary is 0),
        where retieve quality flags on scientific data.

        Parameters
        ----------
        value: int or str
          Extention name or number
        """
        self._flag_hdu = Instrument._parse_hdu(value)

    def __str__(self) -> str:
        """Returns the representation string
        """
        _str = ""
        for key in self.description:
            _str += f"{key.replace('_', ' ')}: {self.description[key]}\n"
        return _str

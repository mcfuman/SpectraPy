#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File crvmodel.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains classes of CRV models 2D and 1D"""


import numpy as np
from astropy import log

from spectrapy.math.poly import polyfit2d, polyval2d

from .model import Model2d, Model1d

from .instrument import Instrument


class CrvModel2d(Model2d):
    """Class used to model 2D spectra curvatures"""

    def __init__(self, deg, xdeg, ydeg, instrument=None):
        """Initialize the Curvature Model 2d

        Parameters
        ----------
        deg: int
          The order of the local 1d polynomial

        xdeg: int
          The order along X axis of the global 2d polynomial

        ydeg: int
          The order along Y axis of the global 2d polynomial

        instrument: str or Instrument instance, optional
          The instrument description file or class

        Raises
        ------
        Exception
          Raise error in case degrees of both local or global polinomials are nagative.
        """
        Model2d.__init__(self)

        if deg < 0:
            raise ValueError(f"Invalid deg {deg} value")
        if xdeg < 0:
            raise ValueError(f"Invalid xdeg {xdeg} value")
        if ydeg < 0:
            raise ValueError(f"Invalid ydeg {ydeg} value")

        self._model = np.zeros((deg + 1, xdeg + 1, ydeg + 1))

        size = self._model[0].size
        self._cov = np.zeros((deg + 1, size, size))

        if instrument is not None:
            if isinstance(instrument, str):
                instrument = Instrument.load(instrument)

            self.dispersion_direction = instrument.dispersion_direction

    def _get_local_coeffs(self, pos):
        """Evaluate each layer of the 2d global polynomials

        Parameters
        ----------
        pos: array_like, shape(2,)
          The (x,y) point where polynomial is evaluated.

        Returs
        ------
        coeffs: array_like, shape(N,)
          The evaluations of the 2d polynomials
        """
        iterable = (polyval2d(pos, self._model[i], self._cov[i]) for i in range(self.deg + 1))
        return np.fromiter(iterable, dtype=Model1d.dtype)

    def get_local_model(self, pos):
        """Get the local 1d model

        Parameters
        ----------
        pos: array_like, shape(2,)
          The center of the local model

        Returns
        -------
        CrvModel1d
          The CRV model
        """

        coeffs = self._get_local_coeffs(pos)
        return CrvModel1d(coeffs['values'], self._rotation, cov=np.diag(coeffs['variances']))

    # pylint: disable=arguments-differ
    def compute(self, pos, delta_disp):
        """Compute the local curvature model and returns the list of (x,y) positions along the slit
        trace.

        Parameters
        ----------
        pos: array_like, shape(2,)
          The reference position in pixels where the curvature model is computed


        delta_disp: array_like, shape(N,)
          The list of distances from the `pos` reference position (in pixels),
          computed along dispersion direction

        Returns
        -------
        array_like, shape(N, 2)
          The pixels trace positions
        """

        model = self.get_local_model(pos)
        return model(delta_disp)

    # def __call__(self, pos, delta_disp):
    #    """Call the compute method

    #    See Also
    #    --------
    #    compute: compute position in pixels
    #    """
    #    return self.compute(pos, delta_disp)

    def refit_trace(self, pos, xdata, ydata, wdata=None):
        """Fit a curve of trace position give (x,y) point positions along the trace.

        Parameters
        ----------
        pos: array_like, shape(2,)
          The center of the local model 1d

        xdata: array_like, shape(N,)
          The absoulte X positions of regions (in pixels) on the FOV

        ydata: array_like, shape(N,)
          The absoulte X positions of regions (in pixels) on the FOV

        Returns
        -------
        CrvModel1d instance
          The local curvature model which maps the trace
        """
        # Inverse rotation == transpose
        delta_disp, delta_cross = np.tensordot(self._rotation.T, [xdata - pos[0], ydata - pos[1]],
                                               axes=1)
        results = np.polyfit(delta_disp, delta_cross, self.deg, w=wdata, cov=True)
        return CrvModel1d(results[0], self._rotation, cov=results[1])

    def refit(self, xdata, ydata, zdata, var=None):
        """Recomputes the global model fitting variation of the coeffs model in the FOV

        Parameters
        ----------
        x: array_like, shape(N,)
          The X positions in the FOV [pixels]

        y: array_like, shape(N,)
          The Y positions in the FOV [pixels]

        z: array_like, shape(N,L)
          The coefficients values in the FOV. One layer for each polynomial's power

        var: array_like, shape(N,L), optional
          The variance on coefficients in the FOV. One layer for each polynomial's power
        """

        degs = (self.xdeg, self.ydeg)
        zdata = np.array(zdata)
        if var is not None:
            var = np.array(var)

        for k in range(len(self.model)):
            if var is None:
                _coeffs, chi2, r_2, _cov = polyfit2d(xdata, ydata, zdata[:, k], degs,
                                                     var=None, cov=True)
            else:
                _coeffs, chi2, r_2, _cov = polyfit2d(xdata, ydata, zdata[:, k], degs,
                                                     var=var[:, k], cov=True)

            self.model[k] = _coeffs
            log.info(f"Global curvature model coeffs {k}: chi2={chi2:.4g} R2={r_2:.4g}")

            self.covariance[k] = _cov

    @staticmethod
    def load(filename):
        """Load file containg CrvModel parameters and returns a brand new model

        Parameters
        ----------
        filename: str
          The input filename
        """
        model = Model2d.load(filename)

        crv = CrvModel2d(1, 1, 1)
        crv.dispersion_direction = model[0]
        crv.model = np.array(model[1])
        if len(model) == 3:
            crv.covariance = np.array(model[2])
        else:
            crv.covariance = []

        log.info(f"Loaded CRV model from {filename}")
        return crv

    def _get_dispersion_component(self, pos):
        """Return the vector component along dispersion direction
        """
        return np.abs(self._rotation[0]).dot(pos)

    @property
    def dispersion_vector(self):
        """Returns the direction versor oriented according with model rotation"""
        return self._rotation.dot([1, 0])

    def get_trace_edges(self, pos, image_dims):
        """Get the trace limits on image
        """
        deltas = np.zeros((2, 2))
        deltas[:, 1] = image_dims
        deltas[:, 1] -= (pos + 1)
        deltas[:, 0] -= pos

        if self.red_direction > 0:
            blue, red = self._get_dispersion_component(deltas)
        else:
            red, blue = -1.0 * self._get_dispersion_component(deltas)

        model = self.get_local_model(pos)
        return pos + model(blue)[0], pos + model(red)[0]


class CrvModel1d(Model1d):
    """Class to handle local Curvature 1d model"""

    def __init__(self, coeffs, rotation, cov=None):
        """Create a Local Curvature Model

        Parameters
        ----------
        coeffs: np.array 1d
           The coefficients of the local polinomial
        rotation:
           The orientation of the frame
        cov: np.array 2d, optional
           The covariance matrix associated to the polynomial coefficients
        """
        Model1d.__init__(self, coeffs, cov)
        self._rotation = rotation

    def compute(self, delta_disp):
        """Compute variation respect to spectra trace perfectly aligned to the pixel grid.

        Parameters
        ----------
        delta_disp: array_like, shape(N,)
          Distances (in pixels) respect to reference position

        Returns
        -------
        array_linke, shape(N,2)
          Displacements respect to pixels grid

        array_linke, shape(N,2)
          Variances on displacements
        """
        values = self.poly(delta_disp)
        variances = self.vpoly(delta_disp**2)
        return np.tensordot(self._rotation, [delta_disp, values], axes=1).T,\
            np.tensordot(self._rotation, [variances * 0, variances], axes=1).T

    def __call__(self, delta_disp):
        return self.compute(delta_disp)

    @property
    def rotation(self):
        """Get the rotation matrix from this model
        """
        return self._rotation

    @property
    def slit_versor(self):
        """Get the orientation of the slit.

        Returns
        -------
        array_like, shape(1,2)
          The versor containg the slit orientation
        """
        return -1 * self._rotation[:, 1]

    @property
    def dispersion_vector(self):
        """Returns the direction versor oriented according with model rotation"""
        return self._rotation.dot([1, 0])

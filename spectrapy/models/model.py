#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File model.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module is the base class for all instrument geometrical models"""

import os
import pickle
import numpy as np
from astropy import log


class Model2d(object):
    """Generic class for 2D calibration models"""

    def __init__(self):
        """Initialize a generic model
        """
        # Model coeffs
        self._model = None

        # Covariance on coeffs
        self._cov = None

        # Rotation matrix
        self._rotation = np.zeros((2, 2), dtype=np.int8)

    def writeto(self, filename, overwrite=False):
        """Save the OptModel parameters values

        Parameters
        ----------
        filename: str
          The output filename

        overwrite: bool, optional
          The overwrite flag for output file

        Raises
        ------
        Exception
          If output file already exits an the overwrite is off.
        """

        if overwrite is False and os.path.isfile(filename):
            raise IOError(f"Ouput file {filename} already exists!")

        with open(filename, 'wb') as _output:
            pickle.dump([self.dispersion_direction, self.model, self.covariance],
                        _output, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def load(filename):
        """Load file containg OptModel parameters and returns a new OptModel

        Parameters
        ----------
        filename: str
          The input filename to load

        Returns
        -------
        pickled object
          The new loaded model
        """

        with open(filename, 'rb') as _input:
            model = pickle.load(_input)
        return model

    @property
    def dispersion_direction(self):
        """Converts rotation matrix to dispersion direction string

        Returns
        -------
        str
          The dispersion direction string: LR, RL, UD or DU.

        Raises
        ------
        Exception
          Raise exception in case the rotation matrix is not valid
        """
        if np.abs(self._rotation).sum() == 0:
            raise ArithmeticError("Invalid dispersion direction")

        if self._rotation[0][0] != 0:
            if self._rotation[0][0] > 0:
                return 'LR'
            return 'RL'

        if self._rotation[1][0] > 0:
            return 'BU'
        return 'UB'

    @dispersion_direction.setter
    def dispersion_direction(self, value):
        """Converts dispersion direction str into rotation matrix

        Parameters
        ----------
        value: str
          The dispersion direction string: LR, RL, UD, DU

        Raises
        ------
        Exception
          In case the the input string is not a valid direction
        """

        if value.upper() == 'LR':
            self._rotation = np.array([[1, 0], [0, 1]])
        elif value.upper() == 'RL':
            self._rotation = np.array([[-1, 0], [0, -1]])
        elif value.upper() == 'BU':
            self._rotation = np.array([[0, -1], [1, 0]])
        elif value.upper() == 'UB':
            self._rotation = np.array([[0, 1], [-1, 0]])
        else:
            raise ValueError("Invalid dispersion direction")

    @property
    def rotation(self):
        """Get the rotation matrix related to the dispersion direction"""
        if np.all(self._rotation == np.zeros((2, 2), dtype=np.int8)):
            raise AttributeError("Dispersion direction not set yet!")
        return self._rotation

    @property
    def model(self):
        """The coefficents model

        Returns
        -------
        array_like, shape(L,M,N)
          The model coefficents
        """
        return self._model

    @model.setter
    def model(self, value):
        """Set model matrixes

        Parameters
        ---------
        value: array_like, shape(L,M,N)
          The list of model matrixes
        """
        if len(value) == 0:
            raise TypeError("Empty model")

        model = np.array(value, dtype=np.number)

        if len(model.shape) != 3:
            raise TypeError("Invalid model shape")

        self._model = model

    @property
    def red_direction(self):
        """Get the position of the red region of the spectrum respect of the center of it.
        Red position is 1 if dispertion is going from botton to up or from left to right.
        It is -1 in the other cases.

        Returns
        -------
        int
          The position of spectrum red region respect to the "center" of the spectrum
        """
        return self._rotation[1].sum()

    @property
    def blue_direction(self):
        """Get the position of the blue region of the spectrum respect of the center of it.
        Blue position is 1 if dispertion is going from up to bottom or from right to left.
        It is -1 in the other cases.

        Returns
        -------
        int
          The position of spectrum blue region respect to the "center" of the spectrum
        """
        return -1 * self.red_direction

    @property
    def covariance(self):
        """Get the covariance matrixes associated to model coefficient matrixes

        Returns
        -------
        array_like, shape(L,M,N)
        """
        return self._cov

    @covariance.setter
    def covariance(self, value):
        """Set the covariance matrixes list.

        Parameters
        ----------
        value: array_like, shape(L,M,N) or shape(0,)
        """

        if self._model is None:
            raise AttributeError("Model not yet set")

        size = self._model[0].size

        if len(value) == 0:
            self._cov = np.zeros((len(self._model), size, size))
            return

        cov = np.array(value, dtype=np.number)

        if len(cov) != len(self._model):
            raise TypeError("Invalid covariance matrixes number")

        if cov[0].shape != (size, size):
            raise TypeError("Invalid covariance matrix shape")

        self._cov = cov

    @property
    def deg(self):
        """Get order of local 1d polynomial.
        """
        if self._model is None:
            raise AttributeError("Model missing")
        return self._model.shape[0] - 1

    @property
    def xdeg(self):
        """The order of the global model along X axis

        Returns
        -------
        int
        """
        if self._model is None:
            raise AttributeError("Model missing")

        return self._model.shape[1] - 1

    @property
    def ydeg(self):
        """The order of the global model along Y axis

        Returns
        -------
        int
        """
        if self._model is None:
            raise AttributeError("Model missing")

        return self._model.shape[2] - 1

    @deg.setter
    def deg(self, value: int):

        if value < 0:
            raise ValueError("Invalid negative deg")

        delta_deg = value - self.deg
        if delta_deg == 0:
            # Nothing to change
            return

        elif delta_deg > 0:
            new_model = np.zeros((delta_deg, *self._model.shape[1:]))
            self._model = np.concatenate((new_model, self._model))
            if self._cov is not None:
                new_cov = np.zeros((delta_deg, *self._cov.shape[1:]))
                self._cov = np.concatenate((new_cov, self._cov))
        else:
            self._model = self._model[-delta_deg:]
            if self._cov is not None:
                self._cov = self._cov[-delta_deg:]

    @xdeg.setter
    def xdeg(self, value: int):
        if value < 0:
            raise ValueError("Invalid negative deg")

        if value == self.xdeg:
            # Nothing changed
            return

        if value < self.xdeg:
            new_model = self._model[:, 0:value + 1, :].copy()
            if self._cov is not None:
                size = (value + 1) * (self.ydeg + 1)
                new_cov = self._cov[:, :size, :size].copy()

        else:
            new_model = np.zeros((self._model.shape[0], value + 1, self._model.shape[2]))
            new_model[:, 0:self.xdeg + 1, :] = self._model.copy()
            if self._cov is not None:
                new_cov = np.zeros((self._model.shape[0], (value + 1) * self._model.shape[2],
                                    (value + 1) * self._model.shape[2]))
                size = self._cov[0].shape[0]
                new_cov[:, :size, :size] = self._cov.copy()

        self._model = new_model
        if self._cov is not None:
            self._cov = new_cov

    @ydeg.setter
    def ydeg(self, value: int):
        if value < 0:
            raise ValueError("Invalid negative deg")

        if value == self.ydeg:
            # Nothing to change
            return

        if value < self.ydeg:
            new_model = self._model[:, :, 0:value + 1].copy()
            if self._cov is not None:
                log.warning("Covariance reshape not implemented yet!")
                self._cov = np.zeros((self._model.shape[0], self._model.shape[1] * (value + 1),
                                      self._model.shape[1] * (value + 1)))

        else:
            new_model = np.zeros((self._model.shape[0], self._model.shape[1], value + 1))
            new_model[:, :, 0:self.ydeg + 1] = self._model.copy()
            if self._cov is not None:
                log.warning("Covariance reshape not implemented yet!")
                self._cov = np.zeros((self._model.shape[0], self._model.shape[1] * (value + 1),
                                      self._model.shape[1] * (value + 1)))

        self._model = new_model


class Model1d(object):
    """Class to handle 1d local model"""
    dtype = [('values', np.float64), ('variances', np.float64)]

    def __init__(self, coeffs, cov=None):
        """Initialize the model

        Parameters
        ----------
        coeffs: array_like, shape(N,)
          The polynomial’s coefficients, in decreasing powers

        cov: array_like, shape(N,N), optional
          The covariance on coefficients
        """
        self._coeffs = np.array(coeffs)
        if cov is not None:
            cov = np.array(cov)
            if self._coeffs.shape == cov.shape:
                self._cov = np.diag(cov)
                self._vars = cov

            elif (len(coeffs), len(coeffs)) == cov.shape:
                if np.allclose(cov, cov.T) is False:
                    raise TypeError("Covariance is not simmetric")
                self._cov = cov
                self._vars = cov.diagonal()

            else:
                raise TypeError("Coefficients length and covariance sizes didn't match")
        else:
            self._cov = None
            self._vars = np.zeros(len(coeffs))

        self._poly = np.poly1d(self._coeffs)
        self._vpoly = np.poly1d(self._vars)

    @property
    def coeffs(self):
        """The polynomial’s coefficients

        Returns
        -------
        array_like, shape(N,)
          The polynomial’s coefficient values
        """
        return self._coeffs

    @property
    def covariance(self):
        """Get the covariance matrix if exists

        Returns
        -------
        None or array_like, shape(N,N)
          The covariance matrix if exists
        """
        return self._cov

    @property
    def variance(self):
        """Get the variance on polynomial’s coefficients (if covariance exists)

        Returns
        -------
        None or array_like, shape(N,)
          The variance on polynomial’s coefficients or None if covariance is missing
        """
        return self._vars

    def log_coeffs(self):  # pragma: no cover
        """Displays the polynomial’s coefficients and the error on them.
        """
        for idx in range(len(self._coeffs))[::-1]:
            log.debug(f"{len(self._coeffs)-idx-1} coeff: {self._coeffs[idx]:2.4e} "
                      f"+/-{np.sqrt(self._vars[idx]):.4e}")

    @property
    def poly(self) -> np.poly1d:
        """Gets the poly1d associated to the model coefficents
        """
        return self._poly

    @property
    def vpoly(self) -> np.poly1d:
        """Gets the poly1d associated to the model variances
        """
        return self._vpoly

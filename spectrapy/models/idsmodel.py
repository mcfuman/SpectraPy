#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File idsmodel.py
#
# Created on: Aug 28, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains classes of IDS models 2D and 1D"""

import os
import pickle
import numpy as np
from astropy import log

from .instrument import Instrument
from .crvmodel import CrvModel2d
from .model import Model2d, Model1d


class IdsModel2d(CrvModel2d):
    """Class used to describe 2D wavelenght solution in the FOV"""

    def __init__(self, deg, xdeg, ydeg, instrument=None):
        """Initialize the Inverse Dispersion Solution model

        Parameters
        ---------
        deg: int
          The order of the local 1d polynomial

        xdeg: int
          The order along X axis of the global 2d polynomial

        ydeg: int
          The order along Y axis of the global 2d polynomial

        instrument: str or Instrument instance, optional
          The instrument description file or class
        """
        CrvModel2d.__init__(self, deg, xdeg, ydeg, instrument)
        self._reference_lambda = None

        if instrument is not None:
            if isinstance(instrument, str):
                instrument = Instrument.load(instrument)

            if instrument.linear_dispersion <= 0:
                raise ValueError(f"Invalid linear dispersion value {instrument.linear_dispersion}")

            self.inv_dispersion = 1.0 / instrument.linear_dispersion
            self.reference_lambda = instrument.reference_lambda

    @property
    def inv_dispersion(self):
        """Get the first approximation of the inverse of linear dispersion

        Returns
        -------
        float
          The first order approximation of the linear dispersion
        """
        return self.model[-2][0][0]

    @inv_dispersion.setter
    def inv_dispersion(self, value):
        """Set the 1st order approximation of the linear dispersion

        Parameters
        ----------
        value: float
          The 1st order approximation of the inverse linear dispersion
        """
        self.model[-2][0][0] = value

    @property
    def reference_lambda(self):
        """The reference lambda value where the 1d local polynomial is centered.

        Returns
        -------
        float
          The reference lambda value
        """
        return self._reference_lambda

    @reference_lambda.setter
    def reference_lambda(self, value):
        """Set the lambda reference value for the 1d local model.

        Parameters
        ----------
        value: float
          The reference lambda value
        """
        self._reference_lambda = value

    def get_delta_dispersion(self, pos, xy):
        """Gets the delta values between a reference position (pos) and lines positions,
        along the dispersion direction according to grism orientation

        Parameters
        ----------
        pos: array_like, shape(2,)
          The reference position

        xy: array_like, shape(2, N)
          The line positions

        Returns
        -------
        np.array, shape(N,)
          The delta values
        """
        # Inverse transformation
        return (xy - pos).dot(self._rotation.T[0])

    # pylint: disable=arguments-differ
    def compute(self, pos, lambdas):
        """Computes line positions `lambdas` respect to the reference position

        Parameters
        ----------
        pos: array_like, shape (2,)
          The (x,y) reference position

        lambdas: array_like, shape (N,)
          The list of line position [A]

        Returns
        -------
        array_like, shape(N,2)
          The expected position of the lines [pixels]
        """
        model = self.get_local_model(pos)
        return model(lambdas)

    def get_local_model(self, pos):
        """Get the local 1d model

        Parameters
        ----------
        pos: array_like, shape(2,)
          The center of the local model

        Returns
        -------
        IdsModel1d
          The IDS model
        """
        coeffs = self._get_local_coeffs(pos)
        return IdsModel1d(coeffs['values'], self._reference_lambda, cov=coeffs['variances'])

    def refit_slit(self, lambdas, delta_disp):
        """Recompute the model for a single slit.

        @param lambdas: line positions [A]
        @type lambdas: np.array 1 by n

        """

        results = np.polyfit(lambdas - self.reference_lambda, delta_disp, self.deg, cov=True)
        return IdsModel1d(results[0], self.reference_lambda, cov=results[1])

    @staticmethod
    def load(filename):
        """Load file containg IdsModel parameters and returns a brand new model.

        Parameters
        ----------
        filename: str
          The file name containg the model
        """
        model = Model2d.load(filename)

        ids = IdsModel2d(1, 1, 1)
        ids.dispersion_direction = model[0]
        ids.model = np.array(model[1])
        ids.reference_lambda = model[2]
        if len(model) == 4:
            ids.covariance = np.array(model[3])
        else:
            ids.covariance = []

        log.info(f"Loaded IDS model from {filename}")
        return ids

    def writeto(self, filename, overwrite=False):
        """Save the IdsModel parameters values

        Parameters
        ----------
        filename: str
          The ouput filename

        overwrite: bool, optional
          The overwrite flag in case output file already exists

        Raises
        ------
        Exception
          If file exists and overwrite flag is False
        """

        if overwrite is False and os.path.isfile(filename):
            raise IOError(f"Ouput file {filename} already exists!")

        with open(filename, 'wb') as _output:
            pickle.dump([self.dispersion_direction, self.model, self.reference_lambda,
                         self.covariance], _output, protocol=pickle.HIGHEST_PROTOCOL)

        log.info(f"Saved IDS model in {filename}")  # pragma: no cover


class IdsModel1d(Model1d):
    """Class to handle 1d ids local model"""

    def __init__(self, coeffs, reference_lambda, cov=None):
        """Initialize the model

        Parameters
        ----------
        coeffs: array_like, shape(N,)
          The polynomial’s coefficients, in decreasing powers

        reference_lambda: float
          The value of the reference lambda

        cov: array_like, shape(N,N), optional
          The covariance on coefficients
        """
        Model1d.__init__(self, coeffs, cov)
        self._reference_lambda = reference_lambda

    def compute(self, lambdas):
        """Evaluate the model in a give lambda positions

        Parameters
        ----------
        lambdas: array_like, shape(N,)
          The list of lambda values [A]

        Returns
        -------
        array_like, shape(N,)
          The of delta values respect of reference lambda in pixels
        """
        delta_lambdas = lambdas - self._reference_lambda
        return self.poly(delta_lambdas), self.vpoly(delta_lambdas**2)

    def __call__(self, lambdas):
        """Call the compute method

        See Also
        --------
        compute: compute position in pixels
        """
        return self.compute(lambdas)

    @property
    def reference_lambda(self):
        """The lambda reference value

        Returns
        -------
        float
          The reference lambda value
        """
        return self._reference_lambda

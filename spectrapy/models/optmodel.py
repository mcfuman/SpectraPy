#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#

#
# File optmodel.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""The module contains classes to handle the Optical Model"""

import numpy as np
from astropy import log

from spectrapy.math.poly import polyfit2d, polyval2d

from .model import Model2d
from .instrument import Instrument


class OptModel2d(Model2d):
    """Class used to describe optical distortion in the FOV. It converts mm into pixels"""

    def __init__(self, xdeg, ydeg, instrument=None, fov_scales=None):
        """Initialize the Optical Model 2d

        Parameters
        ----------
        xdeg: int
          Order along the X axis of the model

        ydeg: int
          Order along the Y axis of the model

        instrument: str, Instrument instance
          Instrument configuration file or class

        mask: str, Mask
          Mask configuration file or class

        Raises
        ------
        Exception
          Error if X or Y polynomial degrees are < 1

        """
        Model2d.__init__(self)

        if xdeg < 1 or ydeg < 1:
            raise ValueError("Invalid Optical Model x/y degs")

        self._model = np.zeros((2, xdeg + 1, ydeg + 1))
        self._cov = np.zeros((2, self._model[0].size, self._model[0].size))

        if instrument is not None:
            if isinstance(instrument, str):
                instrument = Instrument.load(instrument)

            self.dispersion_direction = instrument.dispersion_direction

            self.xshift = instrument.xpixels / 2
            self.yshift = instrument.ypixels / 2
            log.info(f"Optical model center: {self.xshift:.2f}, {self.yshift:.2f}")
            if fov_scales is not None:

                self.xscale = fov_scales[0] / instrument.pixel_scale
                self.yscale = fov_scales[1] / instrument.pixel_scale
                log.info(f"Optical model scales: x {self.xscale:.2f}, y {self.yscale:.2f}")
        else:
            self.dispersion_direction = 'LR'

    @property
    def xmodel(self):
        """The OptModel for X: it converts from x,y [mm] in x [pixels]

        Returns
        -------
        array_like, shape(N,M)
          The Optical Model for X positions.
        """
        return self._model[0]

    @property
    def ymodel(self):
        """The OptModel for X: it converts from x,y [mm] in y [pixels]

        Returns
        -------
        array_like, shape(N,M)
          The Optical Model for Y positions.
        """
        return self._model[1]

    @property
    def xshift(self):
        """The center of OptModel for X [pixels]

        Returns
        -------
        float
          The value of the shift along X axis
        """
        return self.xmodel[0][0]

    @xshift.setter
    def xshift(self, value):
        """The center of OptModel for X [pixels]

        Parameters
        ----------
        value: float
          The value of the shift along X axis
        """
        self.xmodel[0][0] = value

    @property
    def yshift(self):
        """The center of OptModel for Y [pixels]

        Parameters
        ----------
        value: float
          The value of the shift along Y axis
        """
        return self.ymodel[0][0]

    @yshift.setter
    def yshift(self, value):
        """The center of OptModel for Y [pixels]

        Parameters
        ----------
        value: float
          The value of the shift along Y axis
        """
        self.ymodel[0][0] = value

    @property
    def shift(self):
        """Get both X and Y shifts

        Returns
        -------
        array_like, shape(2,)
          The shifts along X and Y axes
        """
        return np.array((self.xshift, self.yshift))

    @shift.setter
    def shift(self, value):
        """Set both X and Y shift values.

        Parameters
        ----------
        value: array_like, shape(2,)
          The values of the shifts
        """
        value = np.array(value)
        if len(value) != 2 or len(value.shape) != 1:
            raise TypeError("Invalid shift type")

        self.xshift = value[0]
        self.yshift = value[1]

    @property
    def xscale(self):
        """The main scale of OptModel for X [pixels/mm]

        Returns
        -------
        float
          The main scale factor long X axis
        """
        return self.xmodel[1][0]

    @xscale.setter
    def xscale(self, value):
        """The main scale of OptModel for X [pixels/mm]

        Parameters
        ----------
        value: float
          The main scale factor long X axis
        """
        self.xmodel[1][0] = value

    @property
    def yscale(self):
        """The main scale of OptModel for Y [pixels/mm]

        Returns
        -------
        float
          The main scale factor long Y axis
        """
        return self.ymodel[0][1]

    @yscale.setter
    def yscale(self, value):
        """The main scale of OptModel for Y [pixels/mm]

        Parameters
        ----------
        value: float
          The main scale factor long Y axis
        """
        self.ymodel[0][1] = value

    def flipy(self):
        """Flip optical model along Y axis
        """
        self.yscale *= -1

    def flipx(self):
        """Flip optical model along X axis
        """
        self.xscale *= -1

    @property
    def sign(self):
        """Get signs of the scales along X and Y directions.

        Returns
        -------
        np.array, shape(2,)
          X and Y sign the Optical model scales
        """
        return np.array([np.sign(self.xscale), np.sign(self.yscale)])

    # pylint: disable=arguments-differ
    def compute(self, pos):
        """Apply the OptModel and convert position from mm into pixels

        Parameters
        ----------
        pos: array_like, shape(2,)
          The (x,y) position in mm

        Returns
        -------
        np.array, shape(2,), np.array or None
          The (x,y) position in pixels and the variances on this position.
          In case model 2d covariance matrices are None, the variance is None.
        """
        xval, xvar = polyval2d(pos, self._model[0], self._cov[0])
        yval, yvar = polyval2d(pos, self._model[1], self._cov[1])

        return np.array((xval, yval)), np.array((xvar, yvar))

    def __call__(self, pos):
        """Call the compute method

        See Also
        --------
        compute: compute position in pixels
        """
        return self.compute(pos)

    @staticmethod
    def load(filename):
        """Load file containg Optical Model parameters and return a new OptModel

        Parameters
        ----------
        filename: str
          The input filename containig OptModel2d coefficients

        Returns
        -------
        OptModel2d instance
          The Optical Model contained in the input file.
        """

        model = Model2d.load(filename)

        opt = OptModel2d(1, 1)
        opt.dispersion_direction = model[0]
        opt.model = np.array(model[1])
        log.info(f"Loaded Optical Model from {filename}")
        if len(model) == 3:
            opt.covariance = np.array(model[2])
        else:
            opt.covariance = []
        return opt

    def refit(self, mm_pos, pix_pos):
        """Computes new OptModel coefficients, fitting positions in mm versus positions in pixels

        Parameters
        ----------
        mm_pos: array_like, shape(N,2)
          The mask mm positions

        pix_pos: array_like, shape(N,2)
          The new pixels positions (retrieved from DS9 regions)
        """

        degs = np.array(self.model[0].shape) - 1

        for k in range(len(self.model)):
            _coeffs, chi2, r_2, _cov = polyfit2d(mm_pos[:, 0], mm_pos[:, 1], pix_pos[:, k], degs,
                                                 var=None, cov=True)

            self.model[k] = _coeffs
            self.covariance[k] = _cov
            if k == 0:
                _label = 'X'
            else:
                _label = 'Y'
            log.info(f"{_label} model coeffs: chi2={chi2:.4g} R2={r_2:.4g}")

        log.info("Optical Model updated")

    @property
    def slit_versor(self):
        """Get the orientation of the slit.

        Returns
        -------
        array_like, shape(1,2)
          The versor containg the slit orientation
        """
        return -1 * self._rotation[:, 1]

    @property
    def dispersion_versor(self):
        """Get the orientation of dispersion direction.

        Returns
        -------
        array_like, shape(1,2)
          The versor containg the dispersion orientation
        """
        return self._rotation[:, 0]

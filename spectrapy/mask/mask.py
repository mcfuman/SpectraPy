#
# SpectraPy is a library to extract astronomical spectra from raw data
#
# Copyright (C) 2018-2021 Marco Fumana - INAF-IASF Milano
#
# This library is free software: you can redistribute it and/or modify it under the terms of the GNU
# General Public License as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
# even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.
#


#
# File mask.py
#
# Created on: Aug 22, 2018
#
# Author: Marco Fumana <marco.fumana@inaf.it>
#

"""This module contains classes to handle MOS/LS mask slits"""


import numpy as np
from scipy.stats import mode

from astropy import log


class Mask(object):
    """This class contains the geometrical description of the mask"""

    def __init__(self):
        """Initialize the mask
        """
        self._slits = {}
        self._opt = None

    def add_slit(self, slit):
        """Adds slit description to the mask
        """
        if isinstance(slit['id'], (bytes, np.bytes_)):
            slit_id = slit['id'].decode('UTF-8')
        else:
            slit_id = str(slit['id'])

        if slit_id in self._slits:
            raise IndexError(f"Slit {slit_id} already exists! Please edit mask description file")

        self._slits[slit_id] = MaskSlit(slit_id, slit)

    def __getitem__(self, _id):
        return self._slits[_id]

    def __iter__(self):
        return iter(self._slits)

    def keys(self):
        """Returns the slits IDs"""
        return self._slits.keys()

    @staticmethod
    def load(filename):
        """Loads mask from a mask description file and return a new Mask instance

        Parameters
        ----------
        filename: str
          File name containg the mask description
        """

        data = np.loadtxt(filename, dtype=MaskSlit.dtype, ndmin=1)
        mask = Mask()

        if data.size == 0:
            raise TypeError(f"No slits in mask file {filename}")

        for slit in data:
            mask.add_slit(slit)

        log.info(f"Loaded mask from {filename}")  # pragma: no cover

        return mask

    def compute_scales(self, slit_width=1.0):
        r"""Searches for mask orientation: slit lenght is alolng X or Y axis
        Defines a rough conversion from mm to pixels

        Parameters
        ----------
        slit_width: float, optional
          The typical slit with in arcsec

        Returns
        -------
        array_like (2,)
          The x and y scales in arcsec/mm
        """

        dims = np.array([self._slits[k].semi_axis for k in self._slits])
        width = np.array([self._slits[k].width for k in self._slits])
        length = np.array([self._slits[k].length for k in self._slits])

        if np.allclose(width, 0.0) or np.allclose(length, 0.0):
            # Width and length in arcsec are not provided by the mask decriptor file
            # Uses the input slit_width
            xmode = mode(dims[:, 0])
            ymode = mode(dims[:, 1])

            if xmode.count[0] > ymode.count[0]:
                scale = slit_width / (2 * xmode.mode[0])
            else:
                scale = slit_width / (2 * ymode.mode[0])

            return np.array([scale, scale])

        # mdf contains width and length in arcsec
        if dims[:, 0].max() > dims[:, 1].max():
            xscale = length[0] / (2 * dims[0][0])
            yscale = width[0] / (2 * dims[0][1])
        else:
            xscale = width[0] / (2 * dims[0][0])
            yscale = length[0] / (2 * dims[0][1])

        return np.array([xscale, yscale])

    @property
    def opt(self):
        """Returns the Optical Model instance"""
        return self._opt

    @opt.setter
    def opt(self, value):
        """Sets the optical model instance
        """
        self._opt = value
        for key in self._slits:
            self._slits[key].opt = value


class MaskSlit(object):
    """This class contains the geometrical description of the slit"""
    dtype = np.dtype([('id', '|S100'), ('dimx', 'f8'), ('dimy', 'f8'), ('x', 'f8'), ('y', 'f8'),
                      ('rot', 'f8'), ('wid', 'f8'), ('len', 'f8'), ('ref', bool)])

    def __init__(self, slit_id, geometry, opt=None):
        """Creates a new slit instance

        @param slit_id: unique slit ID
        @param slit_id: str

        """
        self._slit_id = slit_id

        self._center = np.array([geometry['x'], geometry['y']])
        self._semi_axis = np.array([geometry['dimx'], geometry['dimy']]) / 2.

        self._len = geometry['len']
        self._wid = geometry['wid']
        self._rot = geometry['rot']
        self._ref = bool(geometry['ref'])

        self._opt = opt

    @property
    def slit_id(self):
        """The unique slit ID"""
        return self._slit_id

    @property
    def reference(self):
        """True is this slit is a reference slit"""
        return self._ref

    @property
    def length(self):
        """The length of the slit along the cross dispersion direction"""
        return self._len

    @property
    def width(self):
        """The width of the slit along the dispersion direction"""
        return self._wid

    @property
    def semi_axis(self):
        """The slit semi axis"""
        return self._semi_axis

    @property
    def center(self):
        """The slit center"""
        return self._center

    @property
    def rotation(self):
        """The slit rotation angle"""
        return self._rot

    @property
    def vertices(self):
        """Vertices of the slit in mm anticlockwise direction starting from lower left corner.

        Returns
        -------
        array_like, shape(4, 2)
          The (x,y) vertices of the slit [mm]
        """
        vertices = np.zeros((4, 2))

        # Set corners (anti clock wise)
        vertices[0] = self._center + self.xdispersion_edge + self.dispersion_edge
        vertices[1] = self._center + self.xdispersion_edge - self.dispersion_edge
        vertices[2] = self._center - self.xdispersion_edge - self.dispersion_edge
        vertices[3] = self._center - self.xdispersion_edge + self.dispersion_edge

        return vertices

    @property
    def opt(self):
        """The Optical Model instance"""
        return self._opt

    @opt.setter
    def opt(self, value):
        """Sets the Optical Model"""
        self._opt = value

    @property
    def xdispersion_edge(self):
        """Gets the edge along cross dispersion direction
        """
        if self._opt is None:
            raise AttributeError("Optical Model not set yet!")
        # Minus because I choose the left edge as stating point
        xdisp_edge = -1. * self._opt.slit_versor * self._opt.sign * self.semi_axis
        if self._rot == 0:
            return xdisp_edge

        # Compute the shift ammount due to rotation
        shift_rot = xdisp_edge.dot(self._opt.slit_versor) * np.tan(np.radians(self._rot)) * \
            self._opt.dispersion_versor

        return xdisp_edge - shift_rot

    @property
    def dispersion_edge(self):
        """Gets the edge along the dispersion direction
        """
        if self._opt is None:
            raise AttributeError("Optical Model not set yet!")
        # Minus because I choose the blue edge as stating point

        return -1. * self._opt.dispersion_versor * self._opt.sign * self.semi_axis

    def get_pix_left_edge(self):
        """Get the left margin of the slit according with dispersion direction
        """
        x, y = self.center + self.xdispersion_edge
        return self._opt((x, y))

    def get_pix_right_edge(self):
        """Get the right margin of the slit according with dispersion direction
        """
        x, y = self.center - self.xdispersion_edge
        return self._opt((x, y))

    def get_pix_position(self, t=0):
        """Handles slit as a linear bezier curve. t=0 is the left margin and t=1 is the rigth margin
        """
        left = self.get_pix_left_edge()
        right = self.get_pix_right_edge()
        return (np.tensordot(left[0], 1 - t, axes=0) + np.tensordot(right[0], t, axes=0)).T,\
               (np.tensordot(left[1], (1 - t)**2, axes=0) + np.tensordot(right[1], t**2, axes=0)).T

    def get_pix_center(self):
        """Gets the pixel of slit center
        """
        x, y = self.center
        return self._opt((x, y))

    def get_num_rows(self):
        """Gets the number of pixels of the slit
        """
        if self._opt is None:
            raise AttributeError("Optical Model not set yet!")

        start = self._opt.slit_versor.dot(self.get_pix_left_edge()[0])
        end = self._opt.slit_versor.dot(self.get_pix_right_edge()[0])
        return int(round(abs(end - start))) + 1

    def get_pix_vertices(self):
        """Returns vertices of slit in pixels
        """
        if self._opt is None:
            raise AttributeError("Optical Model not set yet!")

        mm_vertices = self.vertices
        pix_vertices = np.zeros(mm_vertices.shape)

        for k, mm_vertice in enumerate(mm_vertices):
            pix_vertices[k], _ = self._opt(mm_vertice)

        return pix_vertices

    def _get_parameter_value(self, pos):
        start, _ = self.get_pix_left_edge()
        end, _ = self.get_pix_right_edge()
        return self._opt.slit_versor.dot((pos - start)) / self._opt.slit_versor.dot((end - start))

    @property
    def deltat(self) -> float:
        """Delta t inside along the slit"""
        return 1.0 / (self.get_num_rows() - 1)

    def get_slit_pix(self, left_margin=0, right_margin=0):
        """Get the slit discretized in pixels
        If left_margin > 0: starts inside the slit of left_margin pixels
        If left_margin < 0: virtually extends the slit on the left side
        If right_margin > 0: ends befor the natural slit end of right_margin pixels
        If right_margin < 0: virtually extends the slit on the right side
        """

        t0 = 0.0 + self.deltat * left_margin
        t1 = 1.0 - self.deltat * right_margin
        n = self.get_num_rows() - (left_margin + right_margin)
        t = np.linspace(t0, t1, n)

        return self.get_pix_position(t)

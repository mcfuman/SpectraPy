# SpectraPy

[![pipeline](https://gitlab.com/mcfuman/SpectraPy/badges/main/pipeline.svg)](https://gitlab.com/mcfuman/SpectraPy/pipelines)
[![coverage](https://gitlab.com/mcfuman/SpectraPy/badges/main/coverage.svg)](https://mcfuman.gitlab.io/SpectraPy/coverage/)
[![Python version](https://img.shields.io/badge/Python-3.7-green.svg?style=flat)](https://www.python.org/)
[![astropy](http://img.shields.io/badge/powered%20by-AstroPy-orange.svg?style=flat)](http://www.astropy.org/)


SpectraPy is an [Astropy affiliated package](https://www.astropy.org/affiliated/), which collects algorithms and methods for data reduction of astronomical spectra obtained by a through slits spectrograph.

The library is designed to be spectrograph independent. It comes with a set of already configured spectrographs, but it can be easily configured to reduce data of other instruments.

Current implementation of SpectraPy is focused on the extraction of 2D spectra: it produces wavelength calibrated spectra, rectified for instrument distortion. The library can be used on both longslit (LS) and multi object spectrograph (MOS) data.


## Dependencies

SpectraPy dependency:

  * [Python 3.7](https://www.python.org/) (or greater)
  * [Astropy](https://www.astropy.org/)
  * [matplotlib](https://matplotlib.org/)
  * [PyDS9](http://hea-www.harvard.edu/RD/pyds9/)
  * [pyregion](https://github.com/astropy/pyregion)
  * [Cython](https://cython.org/)
  * [DS9](http://ds9.si.edu/site/Home.html)


## Install

To install the development version from gitlab

    > pip install git+https://gitlab.com/mcfuman/SpectraPy.git


You can clone the [git repository](https://gitlab.com/mcfuman/SpectraPy.git) or download and unpack the zip/tar file. Then ``cd`` into
the spectrapy directory and issue:

    > python setup.py [--user] install

Or clone the required tag from [spectra tags](https://gitlab.com/mcfuman/SpectraPy/-/tags/)


## Documentation

SpectraPy user guide is available [here](https://mcfuman.gitlab.io/SpectraPy/)


## Test coverage

Here the detailed [test coverage](https://mcfuman.gitlab.io/SpectraPy/coverage/)


## Issues

You can open issues [here](https://gitlab.com/mcfuman/SpectraPy/-/issues)


## Acknowledging or citing SpectraPy

If you use SpectraPy in your work, we would be grateful if you could include an acknowledgment in papers and/or presentations

### In Publications

If you use SpectraPy for research presented in a publication, we ask that you please cite the SpectraPy DOI [10.20371/inaf/sw/2021_00001](https://www.ict.inaf.it/index.php/31-doi/133-sw-2021-01)


### In Projects or Presentations

If you are using SpectraPy as part of a code project, or if you are giving a presentation/talk featuring work/research that makes use of SpectraPy and would like to acknowledge SpectraPy, we suggest using this [badge](https://mcfuman.gitlab.io/SpectraPy/_images/powered_by-SpectraPy-B18904.svg)

![](https://mcfuman.gitlab.io/SpectraPy/_images/powered_by-SpectraPy-B18904.svg)


## Acknowledgements

We  would  like  to  thank  the  communities  of  Astropy, NumPy, SciPy, Matplolib, IPython and Astopy affiliated packages for providing their packages. These people allow us to create SpectraPy
